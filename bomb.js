function loadBombsettings(s) {   
    if (typeof s.startGame == 'function') {
        currentMapSettings = s; 
        s.startGame()
    }
}

class BombSettings {
    #grid = []
    #computedGrid = [];
    #spawns = []
    #spIdx = -1;
    #zone = []
    #settings = {}
    #brickNames = []
    #spawnMapping = new Map();

    toJSON = () => ({     
        grid : this.#grid,
        spawns : this.#spawns,                   
        settings: this.#settings,    
        brickNames: this.#brickNames    
    })

    addBrickWObjectName = (woname) => {this.#brickNames.push(woname); return this;}
    setGrid=(x, y, w, h)=> {this.#grid = [x,y, w, h]; return this;}
    addSpawn=(x,y)=> {this.#spawns.push([x,y]); return this;}
    setSettings = (settings) => {this.#settings=settings??{}; return this;}
    hasSettings = () => this.#settings && Object.entries(this.#settings).length>0

    startGame = () => {             
        // needs to spawn in corners but they can't spawn in the same corner     
        this.#computeGrid();    
        this.#defaultSpawn();
        this.#initSpawnMappings()
        console.log("settings", JSON.stringify(this.#settings))
        loadGameSettings("bomb", this.#settings)        
    }

    #defaultSpawn = () => {
        console.log("set default spawn");
        const c = this.#getCell(Math.floor(this.#grid[2]/2), Math.floor(this.#grid[3]/2))
        window.WLROOM.setSpawn(1, c.x, c.y)  
    }
    #computeGrid = () => {
        if (this.#grid.length!==4) return console.log("couldn't compute grid");
       // let act =  getActivePlayers().map((p) => p.id);
       console.log("compute grid");
        for (let x = 0; x<this.#grid[2]; x++ ) {
            for (let y = 0; y<this.#grid[3]; y++) {
                (() => {
                    if (typeof this.#computedGrid[x] == "undefined") this.#computedGrid[x] = [];
                    let xg = this.#grid[0]+(x*32)
                    let yg = this.#grid[1]+(y*32)
                    let o = { obj: null, x:xg, y: yg}
                    if ((x+1)%2===0 && (y+1)%2===0) { // pillars
                        this.#computedGrid[x][y] = o; 
                        return;
                    }
            
                    for (let s of this.#spawns) {
                        if (x<=s[0]+1 && x>=s[0]-1 && y<=s[1]+1 && y>=s[1]-1) {
                            this.#computedGrid[x][y] = o;     
                            return;                    
                        }
                    }
                    if (Math.random()< 0.85) {
                        o.obj = this.#brickNames[0];
                        
                    }
                    this.#computedGrid[x][y] = o;  
                   
                })();
               
               
            }
        }
        console.log(JSON.stringify(this.#computedGrid))
    }
    #playerHasSpawn = (pid) => this.#spawnMapping.has(pid)
    #initSpawnMappings = () => {
        console.log("init spawn mapping");
        this.#spawnMapping = new Map()
        let players = getActivePlayers();
        for (let p of players) {
            this.#getSpawnMapping(p);
        }
    }
    #getSpawnMapping = (pid) => {
        if (!this.#playerHasSpawn(pid)) {
            this.#spawnMapping.set(pid, this.#getNextSpawnMapping())
        }
        return this.#spawnMapping.get(pid)
    }
    #getNextSpawnMapping = () => {
        this.#spIdx++
        if (this.#spIdx>=this.#spawns.length) this.#spIdx = 0;
        try {
            const s = this.#spawns[this.#spIdx];
            const c = this.#getCell(s[0], s[1])
            console.log(s[0], s[1],JSON.stringify(c))
            return {x: c.x, y: c.y} 
        } catch(error){ console.log('failed getting next spawn');}
        return {x:0, y:0}
    }
    #getCell = (x,y) => {
        return this.#computedGrid[x][y]
    }
    onGameStart() {
        //draw bricks  
        for (let x = 0; x<this.#computedGrid.length; x++ ) {
            for (let y = 0; y<this.#computedGrid[x].length; y++) {
                let c = this.#getCell(x, y);
                if (c.obj) {
                    createObject({type:c.obj, x: c.x, y: c.y, onExplode:this.#handleBrick})
                }
            }
        }
    
    }
    #handleBrick = (args) => {  
                     
        try {
            if (Math.random()<0.50) {
                if (Math.random()<0.50) {
                      createObject({type:'bonus_flame', x: args.x, y: args.y, onExplode:this.#handleFlameBonus})
                } else {
                    createObject({type:'bonus_bomb', x: args.x, y: args.y, onExplode:this.#handleBombBonus})
                }
            }
        } catch (e) {console.log(e)}
    }
    
    #handleFlameBonus = (args) => {  
        if (typeof args.player == 'undefined') { return;}                
        try {
            
            let weap = window.WLROOM.getPlayerWeapons(args.player)
            let ret = [weap[0].id??0,weap[1].id??3,weap[2].id??3,weap[3].id??3,weap[4].id??3, 666,666,666,666,666]
            switch(ret[0]) {
                case 0:
                    ret[0]= 4;
                break;
                case 4:
                case 5:
                case 6:
                    ret[0]= ret[0]+1;          
                break;
                // already BB
                case 8:
                case 9:
                case 10:
                case 11:
                    ret[0]= ret[0]+1;
                break;            
            }
            
            window.WLROOM.setPlayerWeapons(args.player, ret);
        } catch (e) {console.log("eeee", e)}
    }
    
       
    #handleBombBonus = (args) => {  
        if (typeof args.player == 'undefined') { return;}                
        try {
            
            let weap = window.WLROOM.getPlayerWeapons(args.player)
            let ret = [weap[0].id??0,weap[1].id??3,weap[2].id??3,weap[3].id??3,weap[4].id??3, 666,666,666,666,666]
            switch(ret[0]) {
                case 0:
                    ret[0]= 8;
                break;
                case 4:
                case 5:
                case 6:
                case 7:
                    ret[0]= ret[0]+5;
                break;
                
            }
            
            window.WLROOM.setPlayerWeapons(args.player, ret);
        } catch (e) {console.log("eeee", e)}
    }
    
    onPlayerSpawn = (player) => {
        window.WLROOM.setPlayerWeapons(player, [0,3,3,3,3,1,1,1,1,1]);
        let pos = this.#getSpawnMapping(player.id)
        window.WLROOM.setPlayerPosition(player, pos.x, pos.y)
        //
    }   
}
  

if (typeof window === 'undefined') { // node js context
    exports.BombSettings = BombSettings
}