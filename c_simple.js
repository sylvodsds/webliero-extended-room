class SimpleSettings {
    #spawns = []
    #settings = {}
    #options = false

    toJSON = () => ({
        spawns : this.#spawns,                
        settings: this.#settings,        
    })

    addSpawn=(x,y)=> {this.#spawns.push([x,y]); return this;}
    setSettings = (settings) => {this.#settings=settings??{}; return this;}  
    hasSettings = () => this.#settings && Object.entries(this.#settings).length>0

    startGame = (options) => {  
        this.#options = (options) ?options:false;
        if (options) console.log("simple options", JSON.stringify(Object.keys(options)))
        this.#initSpawnFunc()       
        this.#nextSpawns()
        let sett = this.#settings
        if (this.#options && typeof this.#options.settingsModifier == "function") sett = this.#options.settingsModifier(sett)
        let gm = sett.gameMode??"dm"
        loadGameSettings(gm, sett)        
    }
    #initSpawnFunc = () => {
        if (this.#spawns.length>0) this.#nextSpawns = () => { window.WLROOM.setSpawn(1, ...randomItem(this.#spawns)) }
        if (this.#spawns.length>1) this.onPlayerSpawn = this.#nextSpawns
    } 
    #nextSpawns = () => {}

    onGameStart = () => {
        console.log("simple on game start", this.#options)
        if (this.#options && typeof this.#options.onGameStart == "function") this.#options.onGameStart()
    }  
    onPlayerSpawn = (p) => {
        console.log("simple on player spawn")
        if (this.#options && typeof this.#options.onPlayerSpawn == "function") this.#options.onPlayerSpawn(p)
    }  
}
  

if (typeof window === 'undefined') { // node js context
    exports.SimpleSettings = SimpleSettings
}