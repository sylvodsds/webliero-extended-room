var modCache = new Map()

var defaultMod = "https://webliero.gitlab.io/webliero-mods/Jerac/ReRevisited.zip";
var currentMod = null;
var extendedModsBaseUrl = "https://sylvodsds.gitlab.io/webliero-extended-mods/" 

WLK.syncHash = true;

class ModCache_Entry {
    constructor(zip, mod) {
        this.zip=zip
        this.wlk=mod
    }
    static fromZip(zip, soundpack) {
        let mod = new WLK_Mod(zip) 
        console.log("fromZip", typeof mod)        
        return this.fromJsonAndSprites(mod.write_json(), mod.write_wlsprt().buffer, soundpack)
    }
    static fromJsonAndSprites(json, sprites, soundpack) {
        let mod = new WLK_Mod()
        mod.import_json5(json)
        mod.import_wlsprt(sprites)        
        if (typeof mod.config =='undefined') {
            mod.config = {name:"", version:"", author:""}
        }
        if(soundpack) {
             mod.config.soundpack =soundpack
        }        
        console.log("eh",typeof mod)
        return ModCache_Entry.fromWlk(mod)
    }
    static fromWlk(mod) {
        console.log("fromwlk",typeof mod)
        if (typeof mod.config =='undefined') {
            mod.config = {name:"", version:"", author:""}
        }
        let zip = mod.write_zip({type:"arraybuffer",files:{json:1,wlsprt:1}})
        return new ModCache_Entry(zip, mod)
    }
}


function preloadMods(search = false) {
    (async () => {
        if (!search || "Jerac/ReRevisited.zip".includes(search.toLowerCase())) {
            await preloadMod("https://webliero.gitlab.io/webliero-mods/Jerac/ReRevisited.zip")
        }
        for (const [map, sett] of mapSettings) { 
            if (sett.mods) {
                await preloadAndMergeMods(sett.mods, false, false, sett.colorAnim, search)
            }     
        }
        console.log(`rebuilt mod cache`)
        if (typeof announce == "function") announce(`rebuilt mod cache`)    
        console.log(window.WLLink, window.EXTENDED_VERSION)
	})();    
}

function objToString (obj) {
    let str = '';
    for (const [p, val] of Object.entries(obj)) {
     		if (str) str+='|';
        str += `${p}::${val}`;
    }
    return str;
}
function optionsToString(o) {
    return o.reduce((a,b)=>{
        console.log(typeof b)
         let n = b
         if (typeof b == 'object') { 
                if (typeof b.constants != 'undefined') {
                    n = JSON.stringify(b)
                } else {
                    n = objToString(b)
                }
            }
         if (!a) return n
         return `${a}|${n}`
         },"")
}
/* also returns the complete cache key */
async function preloadMod(mod, force = false) {    
    if (typeof mod == 'string') {
        if (!modCache.has(mod) || force) {
            console.log(`preloading zipped ${mod}`)  
            let mz = await fetchZip(mod);
            modCache.set(mod, ModCache_Entry.fromZip(mz))
        }
        return mod
    }
    if (mod.name) {
        if (!modCache.has(mod.name) || force) {
            let entry = null                  
            if (mod.zip) {
                let mz = await fetchZip(mod.zip)
                entry = ModCache_Entry.fromZip(mz,mod.soundpack??null)         
                modCache.set(mod.name, entry)      
            } else if(mod.json && mod.sprites) {                
                let mj = await (await fetch(mod.json)).text();
                let ms = await (await fetch(mod.sprites)).arrayBuffer();
                entry = ModCache_Entry.fromJsonAndSprites(mj,ms,mod.soundpack??null)     
            }
            if (entry) {
                console.log(`preloading ${mod.name}`)  
                modCache.set(mod.name, entry)            
            }
        }
        return mod.name    
    }
    if (mod.from) {
        let from = await preloadMod(mod.from, force)
        let obj = ''
        if (mod.options) {
            obj +=  '#options('+optionsToString(mod.options)+')'
        }
        if (mod.wObjects) {
            obj += '#wobj('+Object.keys(mod.wObjects).join('|')+')'
        }
        if (mod.weapons) {
            obj += '#weap('+mod.weapons.join('|')+')'
        }
        if (mod.behaviors) {
            obj += '#beh('+mod.behaviors.join('|')+')'
        }
        return `${from}${obj}`
    }
    if (typeof mod.loadData == 'function') {
        await mod.loadData()
    }
    if (typeof mod.toWLK_Mod == 'function') {
        if (!modCache.has(mod.nameString()) || force) {
            let entry =  ModCache_Entry.fromWlk(mod.toWLK_Mod())
            modCache.set(mod.nameString(), entry)   
        }
          
        return mod.nameString()
    }
    return ''
}

hashUintArray = (ui) => {    
    var hash = 0, i, chr;
    if (ui.length === 0) return hash;
    for (i = 0; i < ui.length; i++) {
      chr   = ui[i];
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  };

async function preloadAndMergeMods(mods, palette, materials, colorAnim, search = false) {  
    mods = [...mods]  //local copy  
    let sm = false;
    if (search!=false) {
        for (const mod of mods) {
            let mname = mod; // duplicate name retrieval 
            if (mod.name) {
                mname= mod.name;
            } else if (mod.from) {
                mname=mod.from;
            } else if (mod.nameString) {
                mname = mod.nameString()
            }
            if (mname.toLowerCase().includes(search.toLowerCase())) {
                sm = true
            }
        }
    }
    let names = []    
    for (const mod of mods) {        
        let name = await preloadMod(mod, sm)
        if (name!="") {
            names.push(name)            
        }        
    }
    /* today merge palette & materials?
    if (palette) {
        names.push('pal#'+hashUintArray(palette))
    }
    */
    if (typeof colorAnim !== 'undefined' || colorAnim !==null) {
        let name='cA#'+(colorAnim?colorAnim.join('_'):'false')
        names.push(name)
        mods.push({colorAnim:colorAnim?colorAnim:[]})
    }
    const mergedName = names.join('||')
    if (names.length>1) {
        let merged = new WLK_Mod(getModFromSettings(mods.shift()).zip)
        console.log("merged",typeof merged)
        for (const mod of mods) {
            merged = mergeMods(merged, mod)    
        }
        modCache.set(mergedName, ModCache_Entry.fromWlk(merged))
    }    
    console.log(`preloaded ${mergedName}`)
    return mergedName
}

function mergeMods(main, changes) {
    let c = getModFromSettings(changes)
    console.log('mergeMods', JSON.stringify(changes), typeof c)

    if (!c && typeof changes.colorAnim !== 'undefined') {
        main.config.colorAnim = changes.colorAnim
        return main
    }

    let nmod = new WLK_Mod(c.zip)
  
    if (typeof changes == 'string' || changes.name || changes.nameString) { // full merge
        let opt = {}
        opt.weapons=nmod.weapons.list.map(w=>w.name)
        opt.wObjects=nmod.wObjects.list.map(w=>w.id)
        opt.behaviors=nmod.behaviors.list.map(b=>b.name)
        console.log("full merge", JSON.stringify(changes), changes, JSON.stringify(opt))
        main.merge(nmod, opt)
      /*  nmod.wObjects.list.forEach(w=>{    
            try {             
                main.mergeObject(nmod,'w',w);
            } catch (e) {
                console.log("merge failed"+w.id, JSON.stringify(e))
            }
           
        });
        console.log('yop') */
        return main
    } else if (changes.from) { // partial merge
        if (changes.options && changes.options.includes("merge_missing")) { 
            let mnames = main.weapons.list.map((w) => w.name)      
            let opt = {}
            opt.weapons = nmod.weapons.list.map((w) => w.name).filter(wpn => !mnames.includes(wpn))
            console.log("merge_missing", JSON.stringify(changes), JSON.stringify(changes), JSON.stringify(opt))
            main.merge(nmod, opt)
        }
        if (changes.options && changes.options.includes("replace_existing_weapons")) {
            for (const w of nmod.weapons.list) {                
                while (main.weapons.getBy('name', w.name)) {
                    console.log("remove weapon"+w.name)
                    main.weapons.remove(w.name) // does not handle full removal if duplicates because today i'm lazy                              
                }
            }
        }
        if (changes.options && changes.options.includes("merge_all")) {
            let opt = {}
            opt.weapons=nmod.weapons.list.map(w=>w.name)
            opt.wObjects=nmod.wObjects.list.map(w=>w.id)
            console.log("full merge", JSON.stringify(changes), changes, JSON.stringify(opt))
            main.merge(nmod, opt)
        }
        if (changes.weapons) {
          //  opt.weapons =[]
            for (const idOrName of changes.weapons) {
                if (isNaN(idOrName)) {
                    let w = nmod.weapons.getBy('name', idOrName)
                    if (w && w.id) {
                        main.mergeObject(nmod,'wp',w);
                      ///  opt.weapons.push(w.id)
                    }
                } else {
                   main.mergeObject(nmod,'wp',nmod.weapons.list[parseInt(idOrName)]);
                    
                }
            }       
        }   
        if (changes.options) {
            for (let opt of changes.options) {
                if (typeof opt.remove_weapon != 'undefined') {
                    while (main.weapons.getBy('name', opt.remove_weapon)) {
                        console.log("remove weapon"+opt.remove_weapon)
                        main.weapons.remove(opt.remove_weapon) // does not handle full removal if duplicates because today i'm lazy                              
                    }
                }
                if (typeof opt.constants != 'undefined') {
                    for (let constk of Object.keys(opt.constants)) {
                        main.constants.obj[constk] = opt.constants[constk]
                    }
                }
            }
        }
        if (changes.wObjects) {            
            for (const [idOrName, newname] of Object.entries(changes.wObjects)) {
                if (isNaN(idOrName)) {
                    let w = nmod.wObjects.getByName(idOrName)
                    if (w && w.id) {
                        main.mergeObject(nmod,'w',w);
                        
                    }
                } else {
                    if (newname)  {     
                       // WLK.log =  {log: (...msg)=>console.log(JSON.stringify(msg)) ,warn: (...msg)=>console.log(JSON.stringify(msg)),info: (...msg)=>console.log(JSON.stringify(msg)) } 
                        let id = parseInt(idOrName);
                        console.log("old id", id) 
                        //let newId = main.mergeObject(nmod,'w',nmod.wObjects.list[id],{force:1});   
                        nmod.wObjects.list[id].name= newname
                        main.merge(nmod, {wObjects: [id], weapons: []});
                        let newId = main.wObjects.getByName(newname).id
                        console.log("new id", newId)                    
                       // main.wObjects.list[newId].name= newname
                        console.log(JSON.stringify(main.wObjects.list[newId]))
                    }              
                }
            }
        }
        if (changes.behaviors) {
            //  opt.behaviors =[]
            //   for (const name of changes.behaviors) {
            //         let b = nmod.behaviors.getByName(name)
            //         console.log(JSON.stringify(b))
            //         console.log("merging behavior", name)
            //         if (b) {
            //            main.mergeObject(nmod,'b', b);
            //         }
            //   }    
              main.merge(nmod, {behaviors: changes.behaviors});   
          } 
    }
    return main
}

function pushToIfNone (a, s)  {
    if (typeof s =="string")  {
        s = s.replace(/\D+/,'')
        s = s*1
    }
    if (!a.includes(s)) {
       a.push(s)
    }
}

function extractAllFromBehavior(beh) {
    let sobj = []
    let nobj = []
    let wobj = []
   let bclean = beh.replace(/\/\*[\s\S]*?\*\/|(?<=[^:])\/\/.*|^\/\/.*/g,'')
   for (const m of  bclean.match( /NID_\d+|WID_\d+|SID_\d+/gm)) {
   		switch (true) {
         case m.startsWith("NID_"):
            pushToIfNone(nobj, m)
            break;
          case m.startsWith("WID_"):
            pushToIfNone(wobj, m)
            break;
           case m.startsWith("SID_"):
            pushToIfNone(sobj, m)
            break;
      }
   }
   return {nobj:nobj, sobj: sobj, wobj: wobj}
}

function getModFromSettings(settings) {
    console.log('getModFromSettings', JSON.stringify(settings))
    if (typeof settings == 'string') {
        return modCache.get(settings)
    } else if (settings.name) {
        return modCache.get(settings.name)
    } else if (settings.from) {
        return modCache.get(settings.from)
    } else if (settings.nameString) {
        return modCache.get(settings.nameString())
    }    
    return null
}

async function fetchZip(url) {
    if (!(url.substring(0,8)=='https://'||url.substring(0,7)=='http://')) {
        url=extendedModsBaseUrl+url+'.zip'
    }        
    console.log("fetchzip "+url)
    try {
        return (await fetch(url)).arrayBuffer();
    } catch (e) {
        console.log('error fetchzip',e)
    }
    
}

/*
function extractWobjects(modname, map) {
    let mz = modCache.get(modname)
    let nmod = new WLK_Mod()
    let mm = new WLK_Mod(mz);
    wobjects.forEach((k,n) => {        
        nmod.mergeObject(mm,'w',mm.wObjects.list[k]);
    })
    return mm
}

*/

// function extractLasers() {
//     let wobjects = new Map()    
//     wobjects.set(62,"laser_green_up")
//     wobjects.set(63,"laser_blue_up")
//     wobjects.set(64,"laser_green_down")
//     wobjects.set(65,"laser_blue_down")
//     wobjects.set(66,"laser_green")
//     wobjects.set(67,"laser_blue")
//     wobjects.set(75,"laser_orange")

//     modCache.set("lasers", extractWobjects("dtf", wobjects).asZip())
    

// }
// function extractPlatforms() {
//     let wobjects = new Map()    
//     wobjects.set(57,"platform_square")
//     wobjects.set(59,"platform_classic")
//     wobjects.set(60,"platform_wobbly")
//     wobjects.set(61,"platform_small_wobbly")  
//     modCache.set("platforms", extractWobjects("dtf", wobjects).asZip())    
// }
// function extractLava() {
//     let wobjects = new Map()    
//     wobjects.set(56,"lava")
//     modCache.set("lava", extractWobjects("dtf", wobjects).asZip()) 
// }
// function extractWater() {
//     let wobjects = new Map()    
//     wobjects.set(8,"water")
//     modCache.set("water", extractWobjects("dtf", wobjects).asZip()) 
// }
// function extractOthers() {
//     let wobjects = new Map()    
//     wobjects.set(71,"fan")
//     wobjects.set(72,"trampoline")
//     modCache.set("others", extractWobjects("dtf", wobjects).asZip())
// }
// function extractTeleport() {
//     let wobjects = new Map()    
//     wobjects.set(68,"vortex_in")
//     wobjects.set(69,"vortex_out")
//     modCache.set("teleport", extractWobjects("dtf", wobjects).asZip())
// }


// function changeModTo(zipped_mod){
// 	let m = new WLK_Mod(zipped_mod);
// 	for(let i=0;i<EXT_OBJ.length;i++) mergeIt(m,i);	 		
// 	room.loadMod(m.asZip());
// }

/* */
async function loadMod(asked, config, palette, materials, colorAnim) {  
    //
    if (!config && !asked && (!palette || palette.length==0) && (!materials || materials.length==0) && (typeof colorAnim == 'undefined' || colorAnim==null) && currentMod!==defaultMod) {
        changeToMod(defaultMod)
        return
    }
    if (!config) {
        config = [defaultMod]
    }
    if (asked) {
        config = [...config]
        config[0] = asked
    }
    const mergedName = await preloadAndMergeMods(config, palette, materials, colorAnim)
    changeToMod(mergedName, palette, materials)
    
}

function changeToMod(cacheKey, palette, materials) {
    try {
        let ck = cacheKey
        if (palette && palette.length>0) {
          //  console.log("palette", JSON.stringify(palette), hashUintArray(palette))
            ck +="||pal#"+hashUintArray(palette)
        }
        if (materials && materials.length>0) {
            ck +="||mat#"+hashUintArray(materials)
        }
        if (ck!==currentMod) {
         //   console.log(modCache.get(cacheKey).wlk.write_json())
            window.WLROOM.loadMod(modCache.get(cacheKey).zip, palette, materials)
            currentMod = ck
            currentModTK =  cacheKey
            console.log(`loaded mod ${cacheKey}`, palette && palette.length?' with custom palette':'', materials && materials.length?' with custom materials':'')
            announce(`loaded mod ${cacheKey}`+(palette  && palette.length?' with custom palette':'')+(materials && materials.length?' with custom materials':''))
        }
    } catch(e) {
        console.log(`failed loading mod ${cacheKey}`, e, JSON.stringify(e))
        announce(`failed loading mod ${cacheKey}`)
    }
  
}

function getCurrentWobjectIdByName(name) {
    try {
    //    console.log(JSON.stringify(modCache.get(currentModTK).wlk.wObjects.list))
    //    console.log(currentModTK)
        return modCache.get(currentModTK).wlk.wObjects.getByName(name).id
    } catch(e) {
        console.log(`failed loading wobject ${name} for mod ${currentModTK}`,e)        
    }
    return 0
    
}

preloadMods();
