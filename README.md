## webliero extended room

this room aims to be a simple working room demonstrating hacks on webliero

### REQUIRED!

you need to use the extended client javascript to connect to rooms running these modifications
follow the steps here for how to:
[https://bit.ly/lieroX](https://bit.ly/lieroX)

page at:
[https://sylvodsds.gitlab.io/webliero-extended-room](https://sylvodsds.gitlab.io/webliero-extended-room)

room script by dsds most hacking by Ophi