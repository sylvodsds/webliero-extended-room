class DefuseSettings {
    #bomb = []
    #plants = []
    #spawns = [[],[]]
    #settings = {}
    #options = false
    #currentSpawns = []
    #started = false;

    toJSON = () => ({
        spawns : this.#spawns,                
        settings: this.#settings,        
    })

    setBombSpawn=(x,y)=> {this.#bomb = [x,y]; return this;}
    addPlantingZone=(x,y)=> {this.#plants.push([x,y]); return this;}
    #addSpawn=(team,x,y)=> {this.#spawns[team-1].push([x,y]); return this;}
    addGreenSpawn = (x,y) =>this.#addSpawn(1, x, y)
    addBlueSpawn = (x,y) =>this.#addSpawn(2, x, y)
    setSettings = (settings) => {this.#settings=settings??{}; return this;}  
    hasSettings = () => this.#settings && Object.entries(this.#settings).length>0

    startGame = (options) => {  
        this.#options = (options) ?options:false;
        if (options) console.log("defuse options", JSON.stringify(Object.keys(options)))
        this.#currentSpawns = []    
        this.#randomizeAllSpawns()     
        let sett = this.#settings
        loadGameSettings("defuse", sett)        
        this.#loadObjects()
    }
    onGameStart = () => {        
    }
    #loadObjects = () => {
        let sett =  mapSettings.get(currentMapName);
        if (sett.objects) { // initial objects
            if (typeof sett.objects=="function") {
                console.log("loads objects as function")
                loadMe = [...sett.objects()]
            } else {
                console.log("loads objects")
                loadMe = [...sett.objects]
            }

        } else {
            loadMe = []
        }
        console.log("bomb load objects")
        loadMe.push({type: 'bomb', x: this.#bomb[0], y: this.#bomb[1]})
        for(let plant of this.#plants) {
            loadMe.push({type: 'planting_zone', x:  plant[0], y:  plant[1]})
        }
        loadMe.push({type: 'relocation_zone', x:  this.#bomb[0], y:  this.#bomb[1], onExplode: () => {window.WLROOM.endGame(); console.log("relocation_zone exploded");}  })
        // loadMe.push({type: "announcer", x: this.#bomb[0], y: this.#bomb[1]})
    }

    #setSpawn = (k, x, y) => {
        window.WLROOM.setSpawn(k, x, y)
        this.#currentSpawns[k] = [x,y]
    }

    #randomizeAllSpawns= () => {
        for (let k in this.#spawns) {
            this.#setSpawn(+k+1, ...randomItem(this.#spawns[k]))
        }
    }
 
    onPlayerSpawn = (p) => {
        const k = p.team-1;
        this.#setSpawn(p.team, ...randomItem(this.#spawns[k]))
    }  
}
  

if (typeof window === 'undefined') { // node js context
    exports.DefuseSettings = DefuseSettings
}
