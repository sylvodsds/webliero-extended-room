var gameSettings = new Map();

gameSettings.set("dtf",
{
    scoreLimit: 15,
    timeLimit: 10,
    loadingTimes: 0.4,
    gameMode: "dtf",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "health",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 10,
    teamsLocked: false,
    weaponChangeDelay: 0,
    forceRandomizeWeapons: true,
    damageMultiplier: 1,
    lalaMode: true,
}
);

gameSettings.set("haz",
{
    scoreLimit: 15,
    timeLimit: 10,
    loadingTimes: 0.4,
    gameMode: "haz",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "health",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 10,
    teamsLocked: false,
    weaponChangeDelay: 0,
    forceRandomizeWeapons: true,
    damageMultiplier: 1,
    lalaMode: true,
}
);

gameSettings.set("dm",
{
    scoreLimit: 15,
    timeLimit: 10,
    loadingTimes: 0.4,
    gameMode: "dm",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "health",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 10,
    teamsLocked: false,
    weaponChangeDelay: 20,
    forceRandomizeWeapons: true,
    damageMultiplier: 1,
    lalaMode: true,
}
);

gameSettings.set("pred",
{
    scoreLimit: 15,
    timeLimit: 10,
    loadingTimes: 0.4,
    gameMode: "pred",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "none",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 10,
    teamsLocked: false,
    weaponChangeDelay: 20,
    forceRandomizeWeapons: true,
    damageMultiplier: 1,
    lalaMode: true,
}
);

gameSettings.set("ctf",
{
    scoreLimit: 15,
    timeLimit: 10,
    loadingTimes: 0.4,
    gameMode: "ctf",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "health",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 10,
    teamsLocked: false,
    weaponChangeDelay: 0,
    forceRandomizeWeapons: true,
    damageMultiplier: 1,
    lalaMode: true,
}
);

gameSettings.set("ctf2",
{
    scoreLimit: 15,
    timeLimit: 10,
    loadingTimes: 0.4,
    gameMode: "ctf2",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "health",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 10,
    teamsLocked: false,
    weaponChangeDelay: 0,
    forceRandomizeWeapons: true,
    damageMultiplier: 1,
    lalaMode: true,
}
);


gameSettings.set("duel",
{
    scoreLimit: 5,
    timeLimit: 2,
    loadingTimes: 0.4,
    gameMode: "lms",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "none",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 0,
    teamsLocked: false,
    weaponChangeDelay: 0,
    forceRandomizeWeapons: true,
    damageMultiplier: 1,
    lalaMode: true,
}
);

gameSettings.set("htf",
{
    scoreLimit: 5,
    timeLimit: 2,
    loadingTimes: 0.4,
    gameMode: "htf",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "none",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 0,
    teamsLocked: false,
    weaponChangeDelay: 0,
    forceRandomizeWeapons: true,
    damageMultiplier: 1,
    lalaMode: true,
}
);


gameSettings.set("racing",
{
    scoreLimit: 999,
    timeLimit: 20,
    loadingTimes: 0.4,
    gameMode: "lms",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "none",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 0,
    teamsLocked: false,
    weaponChangeDelay: 0,
    forceRandomizeWeapons: true,
    damageMultiplier: 1,
}
);

gameSettings.set("skiijumping",
{
    scoreLimit: 999,
    timeLimit: 20,
    loadingTimes: 0.4,
    gameMode: "lms",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "none",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 0,
    teamsLocked: false,
    weaponChangeDelay: 0,
    forceRandomizeWeapons: true,
    damageMultiplier: 1,
}
);

gameSettings.set("bomb",
{
    scoreLimit: 15,
    timeLimit: 10,
    loadingTimes: 0.4,
    gameMode: "dm",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "health",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 10,
    teamsLocked: false,
    weaponChangeDelay: 0,
    forceRandomizeWeapons: true,
    damageMultiplier: 1,
    weapons: {
        only: ["BOMB", "NO WEAPON"]
    }
}
);

gameSettings.set("defuse",
{
    scoreLimit: 999,
    timeLimit: 8,
    loadingTimes: 0.4,
    gameMode: "tdm",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "health",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 10,
    teamsLocked: false,
    weaponChangeDelay: 0,
    forceRandomizeWeapons: true,
    damageMultiplier: 1
}
);
