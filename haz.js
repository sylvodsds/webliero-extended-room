function loadHAZsettings(s) {   
    if (typeof s.startGame == 'function') {
        currentMapSettings = s; 
        s.startGame()
    } else {
        window.WLROOM.setZone(...s)
    } 
}

class HazSettings {
    #spawns = []
    //#order = false
    #zone = []
    #settings = {}

    toJSON = () => ({
        zone : this.#zone,
        spawns : this.#spawns,                
        settings: this.#settings,        
    })

    addSpawn=(x,y)=> {this.#spawns.push([x,y]); return this;}
    //setOrder = (order) => {this.#order=order; return this;}
    setSettings = (settings) => {this.#settings=settings??{}; return this;}
    setZone = (x,y,zx,zy) => { this.#zone = [x,y,zx,zy];return this; }    
    hasSettings = () => this.#settings && Object.entries(this.#settings).length>0

    startGame = () => {      
        this.#initSpawnFunc()       
        this.#nextSpawns()
        window.WLROOM.setZone(...this.#zone)
        console.log("settings", JSON.stringify(this.#settings))
        loadGameSettings("haz", this.#settings)        
    }
    #initSpawnFunc = () => {
        if (this.#spawns.length>0) this.#nextSpawns = () => { window.WLROOM.setSpawn(1, ...randomItem(this.#spawns)) }
        if (this.#spawns.length>1) this.onPlayerSpawn = this.#nextSpawns
    } 
    #nextSpawns = () => {}
    
    onPlayerSpawn = (player) => {}   
}
  

if (typeof window === 'undefined') { // node js context
    exports.HazSettings = HazSettings
}