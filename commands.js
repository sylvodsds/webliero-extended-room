var autoExpand = -1;

COMMAND_REGISTRY.init(window.WLROOM, {});

COMMAND_REGISTRY.add("admin", ["!admin: gives you admin if you're entitled to it"], (player) => {
    let a = auth.get(player.id);
    if (!(admins.has(a) || hidden.has(a))) {
        announce("sorry you can't do that 😅😅", player)
        return true;
    } 
    window.WLROOM.setPlayerAdmin(player.id, true);
    return false;
}, false);

COMMAND_REGISTRY.add("autoexp", ["!autoexp #number#: set auto expand with #number# as threshold"], (player, threshold) => {
    if (typeof threshold=="undefined" || threshold=="" || isNaN(threshold)) {
        autoExpand = -1;
        notifyAdmins("cleared autoexpand threshold");
        return false;
    }
    autoExpand = parseInt(threshold);
    notifyAdmins("autoexpand threshold set to `"+threshold+"`");
    return false;
}, true);

COMMAND_REGISTRY.add("seen", ["!seen #id/@name: actually just prints auth atm"], (player, nameOrId) => {
    var list = window.WLROOM.getPlayerList()
    if (nameOrId.charAt(0)=="#") {
        nameOrId = 0+nameOrId.substring(1)        
    } else if (nameOrId.charAt(0)=="@") {
        nameOrId.substring(1)     
    }
   
    
    list = list.filter((p) => p.name== nameOrId|| p.id == nameOrId)

    for (let k in list) {
        announce(`${list[k].id} - ${list[k].name} - ${auth.get(list[k].id)}`, player)
    }
    
    return false;
}, true);

COMMAND_REGISTRY.add(["next","n"], ["!next: skips to next map"], () => {
    next()    
    return false;
}, true);

COMMAND_REGISTRY.add(["choose","c"], ["!choose: opens choice menu for next map"], (player, ...params) => {
    if (!player.admin && getAdmins().filter(p => p.team !=0).length>0) {
        announce("sorry, due to abuse, !c is disabled when there's an admin playing",player, 0xFFF0000);
        return false;
    }
    let tagFilter = ["main"];
    if (params.length>0) {
        tagFilter = params[0].split('+');
    }
    let skipFilter = tagFilter.length==1 && tagFilter.includes("all");
    let isMain = tagFilter.includes("main");
    let l = []
    for (const idx in mypoolIdx) {
        let n = mypoolIdx[idx];
        let sn = n.split('#').shift().split('/').pop().split('.').shift() // <- don't do that kids
        let s = mapSettings.get(n);
        if (s && s.subMenu) {
            continue;
        }
        if(!skipFilter) {
            let found = 0;
            for(const t of tagFilter) {
                if(s.tags.includes(t) || t=="all") {
                    found++;
                }
            }
            if(found!=tagFilter.length) {
                continue;
            }
        }
        l.push({
            img: thumbs[n]??getMapUrl(n),
            title: sn,
            cmd: `!mapi ${idx}`
        })
    }
    if(isMain||skipFilter) {
        l.push({
            img:`${assetsUrl}/menu/trainer/aim_train.webp`,
            title: "train your aim!",
            cmd: `!sm trainer`
        })
        if(isMain) {
            let textSetting = {bg:"#023"};
            l.push({
                img:thumbFromString("Capture\nThe Flag",textSetting),
                title: "choose from maps tagged ctf",
                cmd: `!c ctf`
            })
            l.push({
                img:thumbFromString("Defend\nThe Flag",textSetting),
                title: "choose from maps tagged dtf",
                cmd: `!c dtf`
            })
            l.push({
                img:thumbFromString("Hold\nThe Flag",textSetting),
                title: "choose from maps tagged htf",
                cmd: `!c htf`
            })
            l.push({
                img:thumbFromString("Hold\nA Zone",textSetting),
                title: "choose from maps tagged haz",
                cmd: `!c haz`
            })
            l.push({
                img:thumbFromString("Defuse\nThe Bomb",textSetting),
                title: "choose from maps tagged defuse",
                cmd: `!c defuse`
            })
            l.push({
                img:thumbFromString("Deathmatch",textSetting),
                title: "choose from maps tagged dm",
                cmd: `!c dm`
            })
            l.push({
                img:thumbFromString("Last Man\nStanding",textSetting),
                title: "choose from maps tagged lms",
                cmd: `!c lms`
            })
            l.push({
                img:thumbFromString("Predator",textSetting),
                title: "choose from maps tagged pred",
                cmd: `!c pred`
            })
            l.push({
                img:thumbFromString("Duel",textSetting),
                title: "choose from maps tagged duel",
                cmd: `!c duel`
            })
            l.push({
                img:thumbFromString("Race",textSetting),
                title: "choose from maps tagged racing",
                cmd: `!c racing`
            })
            l.push({
                img:thumbFromString("All Maps",textSetting),
                title: "choose from all maps",
                cmd: `!c all`
            })
        }
    }
    window.WLROOM.sendExtra({type:"select", list:l}, player.id)
    return false;
}, false);

COMMAND_REGISTRY.add(["submenu","sm"], ["!sm: opens submenu, you don't have to know"], (player, ...params) => {
    if (!player.admin && getAdmins().filter(p => p.team !=0).length>0) {
        announce("sorry, due to abuse, !mapi is disabled when there's an admin playing",player, 0xFFF0000);
        return false;
    } 
    let l = []
    if (params.length>0 && params[0]== "trainer") {
        if (params.length==1) {
            l = [
                {
                    img:`${assetsUrl}/menu/trainer/static.webp`,
                    title: "train on static targets",
                    cmd: `!sm trainer static`
                },
                {
                    img:`${assetsUrl}/menu/trainer/dyn.webp`,
                    title: "train on dynamic targets",
                    cmd: `!sm trainer dyn`
                },
                {
                    img:`${assetsUrl}/menu/trainer/both.webp`,
                    title: "train on static & dynamic targets",
                    cmd: `!sm trainer both`
                },
                {
                    img:`${assetsUrl}/menu/trainer/adventure.webp`,
                    title: "enjoy adventure mode!",
                    cmd: `!sm trainer adventure`
                }
            ]       
        } else if (params.length==2 && 'adventure'==params[1]) {
            l = [
                {
                    img:`${assetsUrl}/menu/trainer/easy.webp`,
                    title: "I'm Too Young to Die",
                    cmd: `!sm trainer adventure easy`
                },
                {
                    img:`${assetsUrl}/menu/trainer/normal.webp`,
                    title: "Hurt Me Plenty",
                    cmd: `!sm trainer adventure normal`
                },
                {
                    img:`${assetsUrl}/menu/trainer/hard.webp`,
                    title: "Ultra Violence",
                    cmd: `!sm trainer adventure hard`
                },
                {
                    img:`${assetsUrl}/menu/trainer/impossible.webp`,
                    title: "Nightmare",
                    cmd: `!sm trainer adventure impossible`
                }
            ]
        } else if (
            (params.length==3 && 'adventure'==params[1] && ['easy','normal','hard', 'impossible'].includes(params[2]))
             || (params.length==2 && ['static','dyn','both'].includes(params[1]))
             ) {
            for (const idx in mypoolIdx) {
                let n = mypoolIdx[idx];
                let sn = n.split('#').shift().split('/').pop().split('.').shift() // <- don't do that kids
                let s = mapSettings.get(n);
                if (!s || !s.subMenu) {
                    continue;
                }
                if (s.subMenu=="aim_trainer") {
                            l.push({
                    img: thumbs[n]??getMapUrl(n),
                    title: sn,
                    cmd: `!sm ${params.join(' ')} ${idx}`
                }) 
                }
       
            }
        } else if  (
            (params.length==4 && 'adventure'==params[1] && ['easy','normal','hard', 'impossible'].includes(params[2]))
        || (params.length==3 && ['static','dyn','both'].includes(params[1]))
        ) {
            let mname = mypoolIdx[params[params.length -1]]
            if (!hasGameMode(mname, "lms")) {
                announce("this game mode isn't set for this map yet sorry",player, 0xFFF0000);
                return false;
            }
            // gotta move this somewhere else, wtf bro
            let md =  modCache.get("Larcelo/aim_trainer").wlk             
            let twobj = {
                static: md.weapons.getBy('name', "STATIC AIM TRAINER").bulletType,
                dyn: md.weapons.getBy('name', "DYNAMIC AIM TRAINER").bulletType,
                both: md.weapons.getBy('name', "STATIC AND DYNAMIC AIM TRAINER").bulletType,
                easy: md.weapons.getBy('name', "ADVENTURE AIM TRAINER EASY").bulletType,
                normal: md.weapons.getBy('name', "ADVENTURE AIM TRAINER NORMAL").bulletType,
                hard: md.weapons.getBy('name', "ADVENTURE AIM TRAINER HARD").bulletType,
                impossible: md.weapons.getBy('name', "ADVENTURE AIM TRAINER IMPOSSIBLE").bulletType,

            }
           /// window.WLROOM.setPlayerWeapons(player, [,nw,nw,nw,nw,1,1,1,1,1]);
            let options = (() => {
                const opt = {
                    p: 0,
                    reset: function () { this.p=0},
                    onGameStart: function () { this.reset()},                 
                    onPlayerSpawn: function (player) {
                        if (this.p) return
                        this.p++                      
                        const paramidx = (params.length==3)?1:2
                        window.WLROOM.createObject({wobject: twobj[params[paramidx]], x: player.x, y: player.y, onKillObject: () => { setTimeout(window.WLROOM.endGame, 2000) }})   
                    }
            }
            return opt
        })()
            
            loadMapByName(mname, "lms", null, options);
        } else {
            return false;
        }
        window.WLROOM.sendExtra({type:"select", list:l}, player.id)
    } 
    
    return false;
}, false);
