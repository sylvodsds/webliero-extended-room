function loadGameSettings(def, sett) {
    console.log(`loadGameSettings ${def} ${JSON.stringify(sett)}`)
    let base = gameSettings.get(def)
    let d = {}
    for (const k in base) {
        d[k] = base[k]
    }

    if (sett && Object.entries(sett).length>0) {
        for (const k in sett) {
          //  if (k=="weapons") { continue; }
            console.log("k"+k)
            d[k] = sett[k]
        }
    }
    if (typeof d.weapons != 'undefined') {            
        if (typeof d.weapons.only != 'undefined') {
            window.WLROOM.unbanAllWeapons()
            for (const b of window.WLROOM.getWeapons()) {                    
                window.WLROOM.banWeapon(b.name, !(d.weapons.only.includes(b.name)))
            }
            // to implement    
        } else if (typeof d.weapons.ban != 'undefined') {                
            window.WLROOM.unbanAllWeapons()
            for (const b of  d.weapons.ban) {
                window.WLROOM.banWeapon(b, true)
            }
            
        } else if (typeof d.weapons.banById != 'undefined') {                
            window.WLROOM.unbanAllWeapons()
            for (const id of  d.weapons.banById) {
                window.WLROOM.banWeaponById(id, true)
            }           
        }
    } else {
        window.WLROOM.unbanAllWeapons()
    }    
    window.WLROOM.setSettings(d)
}