var currentPal = "default"

function loadPalette(name, data) {
    if (!data) {
        if (!palettes.has(name)) {
            loadPalette("default")
            return
        }
        data = palettes.get(name)
    }
    if (name!=currentPal) {        
        window.WLROOM.setPalette(data)
        currentPal = name        
    }    
}

async function getPaletteDataFromImageData(name, data) {
    if (palettes.has(name)) {
        return palettes.get(name)
    }
    
    let ret = extractPalette(data)
    //console.log("palette loaded", name, JSON.stringify(ret))
    const arr = new Uint8Array(ret)
    palettes.set(name, arr)
    return arr
}

function extractPalette(arrayBuffer) {
    const dataView = new DataView(arrayBuffer);
  
    // Verify PNG signature
    const signature = [0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A];
    for (let i = 0; i < 8; i++) {
      if (dataView.getUint8(i) !== signature[i]) {
        throw new Error('Not a valid PNG file.');
      }
    }
  
    let offset = 8; // Start after the signature
    let palette = null;
  
    while (!palette) {
      const chunkLength = dataView.getUint32(offset);
      offset += 4;
      const chunkType = String.fromCharCode(
        dataView.getUint8(offset),
        dataView.getUint8(offset + 1),
        dataView.getUint8(offset + 2),
        dataView.getUint8(offset + 3)
      );
      offset += 4;
  
      const chunkDataStart = offset;
      const chunkDataEnd = offset + chunkLength;
      offset = chunkDataEnd;
  
      // Read CRC (not strictly necessary unless verifying integrity)
      const crc = dataView.getUint32(offset);
      offset += 4;
  
      if (chunkType === 'PLTE') {
        const chunkData = new Uint8Array(arrayBuffer, chunkDataStart, chunkLength);
        palette = [];
        for (let i = 0; i < chunkLength; i++) {
          palette.push(chunkData[i]);
        }
        break; // Found the palette
      }
  
      if (chunkType === 'IEND') {
        // Reached the end of the file without finding a PLTE chunk
        break;
      }
    }
  
    return palette;
  }
  

// function initDefaultPalette() {
//     if (!palettes.has("default")) {

//     }
// }
// "test":  RangeError: Offset is outside the bounds of the DataView
//     at DataView.getUint8 (<anonymous>)
//     at D.o (https://www.webliero.com/1XxDKEyx/__cache_static__/g/headless-min.js:751:27)
//     at D.Mh (https://www.webliero.com/1XxDKEyx/__cache_static__/g/headless-min.js:808:33)
//     at Z.B (https://www.webliero.com/1XxDKEyx/__cache_static__/g/headless-min.js:7233:27)
//     at Z.normalize (https://www.webliero.com/1XxDKEyx/__cache_static__/g/headless-min.js:7276:15)
//     at Function.ba (https://www.webliero.com/1XxDKEyx/__cache_static__/g/headless-min.js:5168:21)
//     at Object.setPalette (https://www.webliero.com/1XxDKEyx/__cache_static__/g/headless-min.js:2231:11)
//     at loadPalette (__puppeteer_evaluation_script__:10:23)
//     at __puppeteer_evaluation_script__:61:13
