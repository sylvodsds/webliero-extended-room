
async function fetchZipData(url) {   
    console.log("fetch zip "+url)
    try {
        let buf = await (await fetch(url)).arrayBuffer();
        return new JSZip(buf);
    } catch (e) {
        console.log('error fetchZip',e)
    }    
}

class AnimatedSpritesWobject {
    static OVERLAY = 1;
    static UNDERLAY = 0;
    #name = 'ANIM'
    #uri = ''
    #data = []
    #omod = null
    #tickskip= '1'
    #baseUrl = 'https://sylvodsds.gitlab.io/webliero-extended-mods/'
    #layer = -1;
    behavior = "[frame, idle] = api.unpack(this,'ii',[0, 0]);((o) => { if (tick%%%tickskip%%===0) { frame=(frame==%%numframes%%)?0:frame+1; }   api.setObjectFrame(o, frame);})(this); api.pack(this,'ii',[frame, idle]); "
    #json = `{
        "weapons": [],
        "wObjects":
        [
          {
            // wid0
            "detectDistance": 0,
            "blowAway": 0,
            "gravity": 0,
            "exploSound": -1,
            "addSpeed": 0,
            "distribution": 0,
            "multSpeed": 1,
            "createOnExp": -1,
            "dirtEffect": -1,
            "wormExplode": false,
            "explGround": false,
            "wormCollide": false,
            "collideWithObjects": false,
            "affectByExplosions": 0,
            "bounce": 0,
            "bounceFriction": 0,
            "timeToExplo": 0,
            "timeToExploV": 0,
            "hitDamage": 0,
            "bloodOnHit": 0,
            "startFrame": 2,
            "numFrames": %%numframes%%,
            "loopAnim": false,
            "shotType": 0,
            "repeat": 1,
            "colorBullets": 0,
            "splinterAmount": 1,
            "splinterColour": 50,
            "splinterType": -1,
            "splinterScatter": 0,
            "objTrailType": -1,
            "objTrailDelay": 0,
            "partTrailType": 0,
            "partTrailObj": -1,
            "partTrailDelay": 0,
            "speed": 0,
            "immutable": true,
            "fixed": true,
            "platform": false,
            "removeonsobject": 0,
            "teamImmunity": 0,
            "behavior": 0,
            "detonable": 0,
            "id": 0,
            %%layer%%
            "name": "%%name%%"
          }
        ],
        "nObjects": [],
        "sObjects": [],
        "constants": {},
        "behaviors": [
          {
            "id": 0,
            "name": "%%name%%_anim_behavior",
            "update": "%%behavior%%"
          }
        ],
        "textures": [],
        "colorAnim": [],
        "textSpritesStartIdx": "",
        "crossHairSprite": "",
        "name": "SPRITE_MOD",
        "author": "",
        "version": ""
      }`
    
    #baseSprite = `V0xTUFJUAAABAAAAbDgAbFAApJSAAJAAPKw8/FRUqKioVFRUVFT8VNhUVPz8eEAIgEQIiEgMkFAQmFQUoFgYrGAcTExMVFRUXFxcZGRkbGxsdHR0fHx8hISEjIyMlJSUnJycODiIUFDAaGj4kJD0uLj0bGxskJCQtLS02NjYIGAgLIQsPKw8cLxwpNSkbGxskJCQtLS02NjYqKj40ND0/Pz0PFAAWHAAdJAAlLAAeEg0nHhYxKh87NignHhYxKh87NigyGQAoFAASEhIbGxskJCQtLS02NjY/Pz8xMTEkJCQmDwAtGQA0IwA7LQAqFQA2AAAvAAApAAAyAAArAAA2AAAvAAApAAA2AAAvAAApAAAUFDAaGj4kJD0UFDAaGj4kJD0lIgAiHwAfHAAdGQAhFwooIRIvLBo2NyI+Pi89PT8/AAA+BgE+DQI+FAQ+GwU+IgY+KQg+MAk+Nwo9Og89PRQ9PRw9PSU8PC08PDU8PD4LIQsPKw8cLxwLIQsPKw8cLxw+Dw89Hx89Ly8aGj4kJD0uLj0kJD0PKw8cLxwpNSkcLxwlIgAiHQAfGAAcEwAZDgAWCgAaGiIkJDAvLz4yMj03Nz0KHAoLIQsNJg0PKw8/MjI9KSk+Fxc9ExM9Dw89ExM9Fxc9KSkVCgAWCgAXCwAYDAAPBwAQBwARCAASCQA/Pz83NzcvLy8nJycfHx8nJycvLy83NzcbEwsfFQwjGA4nGxArHhIAAAAKCQIUEwUeHQcoJgoyMAw9Og8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/AAA/CQA/EgA/GwA/JAA/LQA/NgA/PwAqPAAVOgAAOAA/AAA6AQU2AwsxBREtBhYoCBwkCiIfCycbDS0WDzMSETkAgAJAAkA/P/5/wAAAAA6AAAAAAAAADo6OgAAAAAAACA6ISAAAAAAACEgIB8AAAAAACEmJiZDQgAAACAhISAfAAAfIB8hIAAAAB4gISEgHwAAAB8gACAfAAAAAAgAYwD9/57/RUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVF`

    constructor(uri, name='', layer = -1, tickskip = '1') {  
        if (uri.substring(0,8)!='https://') {
            this.#uri=this.#baseUrl+uri+'.zip'
        } else {
            this.#uri = uri
        }               
        this.#name = name!=""?name:this.#uri.split('/').pop().toLowerCase().replace('.zip','')
        if (layer>=0) {
            this.#layer = layer
        }
        this.#tickskip=tickskip
       
    }

    async fetchZip(name, force = false) {
        if (!spriteCache.has(this.#uri) || force) {        
            spriteCache.set(this.#uri, {name: name, zip: await fetchZipData(this.#uri)})
            console.log('----zip data loaded for ', this.#uri)
        }
        return spriteCache.get(this.#uri).zip
    }

    async loadData() {
      //  let tpl = await preloadMod('dsds/_sprite', false)
      //  this.#omod = new WLK_Mod(tpl.zip)
        let zip = await this.fetchZip(this.#uri)  
        let tmpFrames= new Map()
        for(let [filename, file] of Object.entries(zip.files)) {
            // TODO Your code goes here
            console.log("adding frame ", filename);

            tmpFrames.set(filename, window.__ReadPNG(file.asArrayBuffer()))
        }
        var framesAsc = new Map([...tmpFrames.entries()].sort());
        this.#data = [...framesAsc.values()]
        // zip.forEach(function (relativePath, zipEntry) {  // 2) print entries
        //     console.log("found frame", zipEntry.name)
        //     let content = zipEntry.async("arraybuffer").then(window.__ReadPNG)
        //     this.#data.push(content)
                
        // });
        // this.#data = await this.fetchZip(this.#uri)        
    }
    
    getLayerProp = () => {
        if (this.#layer===-1) return ''
        if (this.#layer===0) return '"underlay": true,'
        if (this.#layer===1) return '"overlay": true,'
    }

    toWLK_Mod = () => {   
        this.prepare()  
        console.log(this.#name, "to wlk mod")
        let mod = new WLK_Mod()
        let j = this.#json.replace("%%behavior%%", this.behavior).replaceAll("%%name%%", this.#name).replace("%%layer%%", this.getLayerProp()).replaceAll("%%numframes%%", this.#data.length-1).replace("%%tickskip%%", this.#tickskip);
        console.log(j)
        mod.import_json5(j)
        mod.import_wlsprt(_base64ToArrayBuffer(this.#baseSprite))  
        for (const idx in this.#data) mod.add('spr', this.getSprite(idx))
        mod.config = {name:"", version:"", author:""}

        return mod
    }

    prepare = () => {}

    nameString = () =>  `anim_${this.#name}`   
    getName = () => this.#name
    getSprite = (idx) => {
        let cleaned = this.#data[idx]
        console.log("image size",cleaned.width,cleaned.height, cleaned.image.size)
    //    const anchor = this.#computeAnchor(cleaned)

       /* let sp = new WLK_Sprite(cleaned.width, cleaned.height, anchor.x, anchor.y)
        sp.data = cleaned.data
        sp.palette = palette
        sp.updateHash()*/
       
        return {
            id: idx+1,
            width: cleaned.width,
            height: cleaned.height,
            data: cleaned.image,
            x: 0,
            y: 0
        }
    }
    

    // #computeAnchor = (mdata) => {
    //     const w = mdata.width
    //     const h = mdata.height
    //     const anchorable = [MATERIAL.ROCK, MATERIAL.UNDEF, MATERIAL.WORM, MATERIAL.DIRT, MATERIAL.DIRT_2]
    //     for (let j = h-1; j > 0; j--) {
    //         const startx = Math.round(w/2)-(w%2)            
    //         let ii = startx-1
    //         let iii = startx
    //         while (iii < w || ii >=0) {
    //             let p = defaultMaterials[mdata.image[(j * w) + ii]]
    //             if (typeof p != 'undefined') {
    //                 if (p && anchorable.includes(p)) {
    //                     return {x: -ii, y: -j}
    //                 }
    //             }
    //             ii--
    //             let pp = defaultMaterials[mdata.image[(j * w) + iii]]
    //             if (typeof pp != 'undefined') {
    //                 if (pp && anchorable.includes(pp)) {
    //                     return {x: -iii, y: -j}
    //                 }
    //             }
    //             iii++
    //         }
    //     }
    //     return {x: 0, y: 0}
    // }
}


// complex behavior
/*
[r, frame, lastAnim] = api.unpack(this,'iii',[0, 0, 0]);

let mulberry32= (a) => {
      var t = a += 0x6D2B79F5;
      t = Math.imul(t ^ t >>> 15, t | 1);
      t ^= t + Math.imul(t ^ t >>> 7, t | 61);
      return Math.round(((t ^ t >>> 14) >>> 0) / 4294967296 * 100);
};


let anims = %%anims%%;

((o) => {
    if (tick%%%tickskip%%===0) { 
        frame=(() => {
           if (!lastAnim && r<%%repeat%%) {         
              if (frame==anims[lastAnim].length-1) {
                  ++r;
                  if (r==%%repeat%%) {
                      r = 0;
                      lastAnim = (mulberry32(tick)%(anims.length-1))+1;
                  }
                  return 0;
              } 
              return frame+1;
           }
        let isEnd=(frame==anims[lastAnim].length-1);
        if (isEnd) {
            lastAnim = 0;
            return 0;
        }
            return frame+1;
        })();
    }
    console.log(frame, lastAnim, r);
    api.setObjectFrame(o, anims[lastAnim][frame]);
    
})(this);

api.pack(this,'iii',[r, frame, lastAnim]); 

*/



class ComplexAnimatedSpritesWobject extends AnimatedSpritesWobject {
    #anims = [[{repeat:1}]]
    #bbeh = "[r, frame, lastAnim] = api.unpack(this,'iii',[0, 0, 0]);  let mulberry32= (a) => {       var t = a += 0x6D2B79F5;       t = Math.imul(t ^ t >>> 15, t | 1);       t ^= t + Math.imul(t ^ t >>> 7, t | 61);       return Math.round(((t ^ t >>> 14) >>> 0) / 4294967296 * 100); };   let anims = %%anims%%;  ((o) => {     if (tick%%%tickskip%%===0) {          frame=(() => {            if (!lastAnim && r<%%repeat%%) {       if (frame==anims[lastAnim].length-1) {                   ++r;                   if (r==%%repeat%%) {                       r = 0;                       lastAnim = (mulberry32(tick)%(anims.length-1))+1;                   }                   return 0;               }                return frame+1;            }         let isEnd=(frame==anims[lastAnim].length-1);        if (isEnd) {             lastAnim = 0;             return 0;         }             return frame+1;         })();     }   api.setObjectFrame(o, anims[lastAnim][frame]);      })(this);   api.pack(this,'iii',[r, frame, lastAnim]);  "
    behavior = ""
    constructor(uri, name='', layer = -1, tickskip = '1') {
        super(uri, name, layer, tickskip)
    }
    setBaseAnim = (frames, repeat = 3) => { 
        this.#anims[0] = {
            frames: frames,
            repeat: repeat
        }
        return this;
    }
    addAnim = (frames) => {
        this.#anims.push({
            frames: frames
        })
        return this;
    }
    prepare = () => {
        this.behavior = this.#bbeh.replaceAll("%%repeat%%", this.#anims[0].repeat).replace("%%anims%%", JSON.stringify(this.#anims.map((v)=>v.frames)))
    }
}


if (typeof window === 'undefined') { // node js context    
    exports.AnimatedSpritesWobject = AnimatedSpritesWobject
    exports.ComplexAnimatedSpritesWobject = ComplexAnimatedSpritesWobject
}