// window.WLROOM.onFlagPickup = function(worm){
//     const name = worm.name.trim()==""?"noname":worm.name;
//     announce(`${name} caught the flag!`,null, 0xFDCC59, "small");
//     if (currentMapSettings && typeof currentMapSettings.onFlagPickup == "function") {
//         currentMapSettings.onFlagPickup(worm)
//     }
// }

// window.WLROOM.onFlagScore = function(worm){
//     if (currentMapSettings && typeof currentMapSettings.onFlagScore == "function") {
//         currentMapSettings.onFlagScore(worm)
//     }
// }

// window.WLROOM.onPlayerKilled = function(killed, killer){
//     if (currentMapSettings && typeof currentMapSettings.onPlayerKilled == "function") {
//         currentMapSettings.onPlayerKilled(killed, killer)
//     }
// }

function loadDuelSettings(s) {   
    currentMapSettings = s; 
    s.startGame()  
}

function setLock(b = true) {
	let sett = window.WLROOM.getSettings();
	sett.teamsLocked = b;
	window.WLROOM.setSettings(sett);
}
function isLocked() {
    return window.WLROOM.getSettings().teamsLocked;
}
function resolvePlayerInfo(player) {
    return {name:player.name,auth:auth.get(player.id), id: player.id};
}

function moveToSpec(player) {
    window.WLROOM.setPlayerTeam(player.id, 0);
}

function moveToGame(player, team=1) {
    window.WLROOM.setPlayerTeam(player.id, team);
}


class DuelSettings {
    MIN_PLAYERS = 3;
        

    NOT_STARTED = 0;
    FIRST_SPLASH = 1;
    PLAYING = 2;
    END_GAME = 3;
    END_TOURNAMENT = 4;
    constructor() {}
    #state = this.NOT_STARTED
    #spawns = []
    //#order = false
    #queue = new Map()
    #currentSelection = []
    #round = -1
    #settings = {}
    #trainingsettings = {}
    #nextMatch = false
    
    #games = []

    toJSON = () => ({
        spawns : this.#spawns,                
        settings: this.#settings,
        trainingSettings : this.#trainingsettings
    })

    #addSpawn = (x, y) => {
        this.#spawns.push([x,y])
    }

    addSpawn=(x,y)=> {this.#addSpawn(x,y); return this;}
    //setOrder = (order) => {this.#order=order; return this;}
    setSettings = (settings) => {this.#settings=settings??{}; return this;}
    setTrainingSettings = (settings) => {this.#trainingsettings=settings??{}; return this;}
    hasSettings = () => this.#settings && Object.entries(this.#settings).length>0
    addToQueue = (player) => { this.#queue.set(player.id, {id:player.id,name: player.name}); }
    removeFromQueue = (player) => { this.#queue.delete(player.id); }
    #removeFromCurrentSelection = (player) => {
        for (let idx in this.#currentSelection) {
            if (this.#currentSelection[idx]==player.id) {
                this.#currentSelection.splice(idx, 1)
                console.log('removed from selection'+player.id, JSON.stringify(this.#currentSelection))
                return
            }
        }
    }
    #isInNextMatch = (player) => {
        for (let idx in this.#nextMatch) {
            if (this.#nextMatch[idx].id==player.id) {                
                return idx
            }
        }
        return -1
    }

    startGame = () => {
        startingSound = "";
        console.log("starting training round")       
        this.#round = -1
        this.#queue= new Map();
        this.#state = this.NOT_STARTED;    
        getActivePlayers().forEach(this.addToQueue)        
        this.#randomizeAllSpawns()
        console.log("settings", JSON.stringify(this.#settings), JSON.stringify(this.#trainingsettings))
        loadGameSettings("duel", this.#trainingsettings)
        announce(`join now if you want to participate in a small tournament, at least ${this.MIN_PLAYERS} players are required`)        
        window.WLROOM.playSound(`https://sylvodsds.gitlab.io/liero-soundpacks/samples/tournament.mp3`)
        window.WLROOM.sendExtra({type:"overlay",text:"Tournament - training round", color: '#FFB200'})
    }

    join = (player) => {        
        this.addToQueue({id:player.id, name:player.name})
    }
    
    #tournamentEnded = () => this.#state==this.END_TOURNAMENT||(this.#queue.size<this.MIN_PLAYERS&&this.#currentSelection.length<2);
    #canBegin = () => this.#state==this.NOT_STARTED && this.#queue.size>=this.MIN_PLAYERS;
    #nextSpawns = () => {
        window.WLROOM.setSpawn(1, ...randomItem(this.#spawns))  
    }
    endGame = () => {
        console.log("tournament stop")
    }
   
    onPlayerSpawn = (player) => {
        this.#nextSpawns()        
    }
    onGameEnd = () => {
        if (!this.#tournamentEnded()) {
            console.log("tourney ok", this.#state==this.END_TOURNAMENT, this.#state, this.#queue.size, this.MIN_PLAYERS)       
            if (this.#canBegin()) {
                console.log("starting tournament")
                setLock(true);
                this.#state = this.END_GAME;
                this.#settings.teamsLocked = true;
                loadGameSettings("duel", this.#settings);
            } else {
                this.#state = this.END_GAME;
                this.#computeScores()
            }                              
            this.#resolveNewMatch()
            return
        } 
        setLock(false);
    }
    #resolveNewMatch = () => {
        this.#nextMatch = this.#computeNextMatch()
        if (this.#nextMatch && this.#nextMatch.length==2) {      
            console.log("ng---", JSON.stringify(this.#nextMatch))                          
            announce(`next game will begin: ${this.#nextMatch[0].name} VS ${this.#nextMatch[1].name}`)
           // window.WLROOM.sendExtra({type:"overlay",text:`Next Game:  ${this.#nextMatch[0].name} VS ${this.#nextMatch[1].name}`, color: '#FF6B00'})
            // get ready sound to both players
            for (let idx in this.#nextMatch) {
                let p = this.#nextMatch[idx]
                let op = this.#nextMatch[idx==1?0:1]

                setTimeout(() => {
                    window.WLROOM.sendExtra({type:"overlay",text:`Get Ready, you're playing next against ${op.name}!!`, color: '#FFF900'}, p.id)
                    window.WLROOM.playSound(`https://sylvodsds.gitlab.io/liero-soundpacks/samples/get_ready.mp3`, p.id)
                }, 3000)  
                
            }
        } else if(this.#nextMatch.length==1) {
            this.#state=this.END_TOURNAMENT
            announce(` ${this.#nextMatch[0].name} won the tournament`)
        }
    }
    #computeScores = () => {        
        if (this.#round<0) {
            return;
        }
        console.log("#computeScores")
        let scores = getActivePlayers().map(p => {return {player:resolvePlayerInfo(p),score: window.WLROOM.getPlayerScore(p.id)};});
        if (scores.length!=2) {
            console.log("incorrect number of players, scores dropped");         
            return;
        }
        console.log(JSON.stringify(scores))
        if (scores[0].score.score>scores[1].score.score) {
            announce(`player \`${scores[0].player.name}\` won`)
            window.WLROOM.sendExtra({type:"overlay",text:`${scores[0].player.name} wins`, color: '#00FFED'})
            this.#currentSelection.push(scores[0].player.id)
        } else if(scores[0].score.score<scores[1].score.score){
            announce(`player \`${scores[1].player.name}\` won`)
            window.WLROOM.sendExtra({type:"overlay",text:`${scores[1].player.name} wins`, color: '#00FFED'})
            this.#currentSelection.push(scores[1].player.id)
        } else {
            announce(`>> TIE << both players move to the next round`)
            window.WLROOM.sendExtra({type:"overlay",text:`>> TIE ${scores[0].player.name} VS ${scores[1].player.name}`, color: '#00FFED'})
            this.#currentSelection.push(scores[0].player.id)
            this.#currentSelection.push(scores[1].player.id)
        }
    }
    #computeNextMatch = () => {    
        if (this.#round<0) {
            this.#currentSelection = Array.from(this.#queue.keys());
            shuffleArray(this.#currentSelection);           
            this.#round = 0;
            console.log("starting first round", JSON.stringify(this.#currentSelection));
        } else {
            console.log("#computeNextMatch ---------- new round")
        }
        let ret = []
        while (ret.length<2 && (this.#currentSelection.length>0)) {    
            let pid = this.#currentSelection.shift();
            if (pid!==null) {
                let pq = window.WLROOM.getPlayer(pid);
                if (pq) {                 
                    ret.push({id:pq.id, name: pq.name})
                }
            }
        }
        return ret
    }
    onPlayerTeamChange = (p, bp) => {
        if (this.#state == this.NOT_STARTED) {
            if (p.team==0) {
                console.log(`${p.name} moved to spec`);
                this.removeFromQueue(p)
            } else {
                console.log(`${p.name} moved to game`);
                this.addToQueue(p)
            }
        } else if (bp!=null) { // we authorize automatic moves, but not admin force joins/quits
            if (p.team==0) {
                console.log(`${p.name} moved to spec during game? by ${bp.name}`);
                moveToGame(p);
            } else {
                console.log(`${p.name} moved to game during game? by ${bp.name}`);
                moveToSpec(p)
            }
            announce(`⚠️violation of the tournament process detected⚠️`)
            window.WLROOM.setPlayerAdmin(bp.id, false);
            window.WLROOM.restartGame();
            this.#loadObjects();
        }
    }
    onPlayerLeave = (player) => {    
        if (this.#state==this.NOT_STARTED) {
            this.removeFromQueue(player); 
        } else {        
            console.log('mf left')    
            this.removeFromQueue(player); 
            this.#removeFromCurrentSelection(player)
            const nid = this.#isInNextMatch(player)
            if (nid>-1) {
                this.#nextMatch.splice(nid, 1)
                let leftp = this.#nextMatch.pop()
                if (typeof leftp != 'undefined' && leftp) {
                    this.#currentSelection.push(leftp.id)
                    announce(`${player.name} forfeited, ${leftp.name} wins`)
                    window.WLROOM.sendExtra({type:"overlay",text:`${player.name} forfeit ${leftp.name} wins`, color: '#00FFED'})
                    moveToSpec(leftp)
                }         
                if (this.#state!=this.END_GAME) {   
                    console.log('force end game because player left')                                             
                    window.WLROOM.endGame()
                } else {
                    this.#nextMatch = this. #computeNextMatch()
                }
            }
        }

    }
    onGameEnd2 = () => {
        if (!this.#tournamentEnded()) {
            if (this.#nextMatch && this.#nextMatch.filter((x)=>x!=null).length==2) {
                moveAllPlayersToSpectator();
                moveToGame(this.#nextMatch[0]);
                moveToGame(this.#nextMatch[1]);          
                window.WLROOM.sendExtra({type:"overlay",text:`${this.#nextMatch[0].name} VS ${this.#nextMatch[1].name}`, color: '#FF6B00'});
                
                (async () => {     
                    try {   
                        let data = await getMapData(getMapUrl(currentMapName));
                        if (data == null) {
                            notifyAdmins(`map ${currentMapName} could not be loaded`)
                            window.WLROOM.restartGame();
                            return;
                        }                        
                        window.WLROOM.loadPNGLevel(currentMapName, data)

                        this.#loadObjects();

                    } catch (e) {
                        console.log("duel.onGameEnd2 map could not be changed", JSON.stringify(e))
                        notifyAdmins(`error loading map ${currentMapName} ${JSON.stringify(e)}`)
                    }
                })();
            }
        } else {
            next(); // goes back to normal flow & loads new map
        }
    }
    #loadObjects = () => {
        let sett =  mapSettings.get(currentMapName);
        if (sett.objects) { // initial objects
            if (typeof sett.objects=="function") {
                console.log("loads objects as function")
                loadMe = sett.objects()
            } else {
                console.log("loads objects")
                loadMe = sett.objects
            }

        }
    }
    onGameStart = () => {
        //does it need fix when players left before the start?
        if (this.#state != this.NOT_STARTED) {
            this.#state = this.PLAYING;
            if (getActivePlayers().length!=2) {
                console.log("game was started without 2 players?!")
                this.#resolveNewMatch()
                window.WLROOM.restartGame();
                this.#loadObjects();               
            }
        }
    }
    #randomizeAllSpawns= () => {        
        window.WLROOM.setSpawn(1, ...randomItem(this.#spawns))        
    }

}



if (typeof window === 'undefined') { // node js context
    exports.DuelSettings = DuelSettings
}