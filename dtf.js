if (typeof window!='undefined') {
    window.WLROOM.onFlagPickup = function(worm){
        const name = worm.name.trim()==""?"noname":worm.name;
        announce(`${name} caught the flag!`,null, 0xFDCC59, "small");
        if (currentMapSettings && typeof currentMapSettings.onFlagPickup == "function") {
            currentMapSettings.onFlagPickup(worm)
        }
    }
    
    window.WLROOM.onFlagScore = function(worm, score){
        if (currentMapSettings && typeof currentMapSettings.onFlagScore == "function") {
            currentMapSettings.onFlagScore(worm, score)
        }
        if (mapFlagScoreHandler && typeof mapFlagScoreHandler == "function") {
            mapFlagScoreHandler(worm, score)
        }
    }
    
    window.WLROOM.onPlayerKilled = function(killed, killer){
        if (currentMapSettings && typeof currentMapSettings.onPlayerKilled == "function") {
            currentMapSettings.onPlayerKilled(killed, killer)
        }
        if (mapKillHandler && typeof mapKillHandler == "function") {
            mapKillHandler(killed, killer)
        }
    }
    
    window.WLROOM.onPlayerSpawn = function(p){
        if (currentMapSettings && typeof currentMapSettings.onPlayerSpawn == "function") {
            currentMapSettings.onPlayerSpawn(p)
        }
        if (mapSpawnHandler) {
            mapSpawnHandler(p)
        }
        if (lalaMode) {
            lalaMode(p);
        }
    }    
}

function loadDTFsettings(s) {
    if (typeof s.startGame == "undefined") { // case with only 1 spawn point per "type" defined
        for (let k in s) {
            window.WLROOM.setSpawn(...s[k])
        }
    } else {
        currentMapSettings = s; 
        s.startGame()
    }    
}

function randomizeAllSpawns() {
    for (let k in s) {
        window.WLROOM.setSpawn(...randomItem(s[k]))
    }
}

class DTFSettings {
    constructor() {}
    
    #types = ["flag", "defense","attack"]
    #spawns = [[],[],[]]
    #order = false
    #orders = [-1,-1,-1]
    #settings = []

    toJSON = () => ({
        spawns : this.#spawns,
        order: this.#order,
        orders: this.#order?this.#orders:null,
        settings: this.#settings,
    })

    #addSpawn = (type, x, y) => {
        this.#spawns[type].push([x,y])
    }
    addSpawns = (spawns) => { this.#spawns=spawns; return this;}

    addFlagSpawn=(x,y)=> {this.#addSpawn(0,x,y); return this;}
    addDefenseSpawn=(x,y)=> {this.#addSpawn(1,x,y); return this;}
    addAttackSpawn=(x,y)=> {this.#addSpawn(2,x,y); return this;}
    setOrder = (order) => {this.#order=order; return this;}
    setSettings = (settings) => {this.#settings=settings; return this;}
    hasSettings = () => this.#settings && Object.entries(this.#settings).length>0

    startGame = () => {
        console.log("starting dtf")
        this.#orders = [-1,-1,-1]
        this.onFlagPickup()
        loadGameSettings("dtf", this.#settings)
    }
    
    #nextSpawns = () => {
        for (let k in this.#orders) {
            this.#orders[k] = (this.#orders[k]>=this.#spawns[k].length-1)? 0 : this.#orders[k] + 1
            window.WLROOM.setSpawn(k, ...this.#spawns[k][this.#orders[k]])
        } 
    }
    endGame = () => {
        console.log("dtf clear")
    }
    onFlagPickup = () => {
        if (!this.#order) {
            this.#randomizeAllSpawns()
        } else {
            this.#nextSpawns()
        }
    }
    onPlayerKilled = (killed, killer) => {
        if (!this.#order) {            
            if ([1,2].includes(killed.team)) {
                window.WLROOM.setSpawn(killed.team, ...randomItem(this.#spawns[killed.team]))
            }            
        }
    } 
    #randomizeAllSpawns= () => {
        for (let k in this.#spawns) {
            window.WLROOM.setSpawn(k, ...randomItem(this.#spawns[k]))
        }
    }
    
    static fromJson = (json) => {
        let s = new DTFSettings()
        let obj = typeof json == 'string' ? JSON.parse(json): json

        if (obj.settings) {
            s.setSettings(obj.settings)
        }
        if (obj.order) {    
            s.setOrder(obj.order)
        }
        if (obj.spawns) {
            s.addSpawns(obj.spawns)
        }
        return s
    }
}

if (typeof window === 'undefined') { // node js contexts    
    exports.DTFSettings = DTFSettings
}