
function loadRacingSettings(s) {
        currentMapSettings = s; 
        console.log(typeof s)
        s.startGame()   
}

class RacingSettings {
    NOT_STARTED = 0
    RUNNING = 1
    INTERSECT_RANGE = 4 // pixels before/after the line we match as well
    #countDownStartTime = null
    #startTime = null
    #state = this.NOT_STARTED
    #spawns = []
    #settings = []
    #goal = []    
    #gl = [] // every points of the goalline
    #start = []
    #sl = [] // every points of the startline
    #szone = [] // in case we're usine a zone instead of a line ~ for the start it's treated as a no exit zone
    #fzone = [] // in case we're usine a zone instead of a line
    #null = null
    #arrivals = {}
    #laps = {}
    #lastStep = {}
    #currentLapProg = {}
    #steps = []
    #stepCenters = []
    #lapsNum = 1
    #secondsToStart = 10    
    #countDownID = undefined
    #checkfuncs = []
    #maxStep = -1
    #onStartRace = null
    addSpawn = (x, y) => { this.#spawns.push([x,y]); return this;}
    
    setSettings = (settings) => {this.#settings=settings; return this;}
    hasSettings = () => this.#settings && Object.entries(this.#settings).length>0
    addFinalLine = (x,y,zx, zy) => {this.#goal =[x,y,zx,zy]; this.#gl = this.#getlinecoordinates(x,y,zx,zy);return this;}
    addStartLine = (x,y,zx, zy) => {this.#start =[x,y,zx,zy]; this.#sl = this.#getlinecoordinates(x,y,zx,zy);return this;}
    addFinalZone = (x,y,zx, zy) => {this.#fzone =[x,y,zx,zy]; return this;}
    addStartZone = (x,y,zx, zy) => {this.#szone =[x,y,zx,zy]; return this;} // this is treated as no exit zone
    // (x,y,zx, zy) define a rectangle to validate collision (cx, cy) is the optionnal "center" of that rectangle (used to spawn the worm back into the zone after dying)
    addStep = (x,y,zx, zy, cx=-1, cy=-1) => {
        this.#steps.push([x,y,zx,zy]);        
        this.#maxStep = this.#steps.length-1;
        if (cx<0 && cy<0) {
            cx = x+Math.round((zx-x)/2)
            cy = y+Math.round((zy-y)/2)
        }
        this.#stepCenters.push([cx, cy]);      
        return this;
    }
    addNumberLaps = (num) => {this.#lapsNum = num; return this;}
    addOnStartRace = (callback) => {this.#onStartRace=callback; return this;}
    toJSON() { return {        
        spawns : this.#spawns,
        settings: this.#settings,
        finalLine: this.#goal,
        finalZone: this.#fzone,
        startZone: this.#szone,
        startLine: this.#start,
        steps: this.#steps,
        lapsNumber: this.#lapsNum,
        }
    }

    startGame() { 
        this.#arrivals = {}
        this.#lastStep = {}
        this.#currentLapProg = {}
        this.#laps = {}
        this.#state = this.NOT_STARTED
        setLock(false)
        this.#checkfuncs=[this.#sl.length>0?this.#checkBadStart:this.#checkBadStartZone]                
        console.log("starting racing")      
        this.#arrivals = {}
        this.#startTime = null
        this.#nextSpawnsRandom()          
        loadGameSettings('racing', this.#settings)                
    }
    #cancelCountdown = () => {
        if (typeof this.#countDownID === 'number') {
            this.#state = this.NOT_STARTED
            clearInterval(this.#countDownID)
            this.#countDownStartTime = null
        }
    }
    #newCountdown = () => { 
        let now = performance.now()
        if ((now - this.#countDownStartTime)<(18*1000) || this.#state == this.RUNNING) {
            return;
        }
        this.#cancelCountdown();
        this.#countDownStartTime = performance.now();
        announce('race will begin in 20 seconds'+(this.#lapsNum<=1?"":` ${this.#lapsNum} laps to win`))

        this.#countDownID = setInterval(() => {
            now = performance.now()
            const currDur =now - this.#countDownStartTime
            if (currDur>20*1000) {
                this.#endCountDown()
                announce('!!GO')
                window.WLROOM.sendExtra({type:"overlay",text:`🟢🟢🟢🟢`, color: '#00FF00'})
                window.WLROOM.playSound(`https://sylvodsds.gitlab.io/liero-soundpacks/samples/piou.mp3`)
            } else if (currDur>19*1000) {
                window.WLROOM.sendExtra({type:"overlay",text:`🟠🟠🟠`, color: '#FF8E00'})
                window.WLROOM.playSound(`https://sylvodsds.gitlab.io/liero-soundpacks/samples/pi.mp3`)
            } else if (currDur>18*1000) {
                window.WLROOM.sendExtra({type:"overlay",text:`🔴🔴`, color: '#FF0000'})
                window.WLROOM.playSound(`https://sylvodsds.gitlab.io/liero-soundpacks/samples/pi.mp3`)
            } else if (currDur>17*1000) {
                window.WLROOM.sendExtra({type:"overlay",text:`🔴`, color: '#FF0000'})
                window.WLROOM.playSound(`https://sylvodsds.gitlab.io/liero-soundpacks/samples/pi.mp3`)
            } 
        }, 1*1000);
    }
    #endCountDown = () => {
        this.#cancelCountdown()
        this.#state = this.RUNNING
        if (typeof this.#onStartRace == 'function') this.#onStartRace()
        this.#startTime = performance.now()
        this.#checkfuncs = []
        if (this.#steps.length>0) {
            this.#checkfuncs.push(this.#checkSteps)
        } else {
            this.#checkfuncs.push(this.#checkArrival)
        }        
        console.log("starting race")
        setLock(true)
    }

    endGame = () => {
        this.#cancelCountdown();
        setLock(false)                
        console.log("racing clear")
    }
    
    onGameTick = (g) => {      
        let players = getActivePlayers();        
        let elapsed = performance.now() - this.#startTime;
        let endgame = false
        for (let p of players) {
           for (let cf of this.#checkfuncs) {
               endgame = cf(p, elapsed, players) || endgame
           }
        }     
        if (endgame) {
            window.WLROOM.endGame()
        }
    }
    #handleBadStart = (p, msg) => {
       // announce(msg)       
       // moveToSpec(p)
      //  moveToGame(p)
       window.WLROOM.setPlayerPosition(p,...this.#spawns[0])
       window.WLROOM.setPlayerHealth(p,0)
    }
    #checkBadStart = (p, elapsed, players) => {
        if(p.team==0 || p.x==null || p.y==null) return;       
        if (this.#lineintersect(p.x, p.y, this.#sl)) {                                      
            this.#handleBadStart(p,`${p.name} crossed startline before race started`)
        }
    }
    #checkBadStartZone = (p, elapsed, players) => {
        if(p.team==0 || p.x==null || p.y==null) return;       
        if (!this.#intersect(p.x, p.y, ...this.#szone)) {                                      
            this.#handleBadStart(p,`${p.name} did not stay in the zone before race started`)
        }
    }   
    #checkSteps = (p, elapsed, players) => {
        if(p.team==0 || p.x==null || p.y==null) return;
        for (let idx in this.#steps) {
            let ls = this.#lastStep[p.id] 
            let noLs = typeof ls == 'undefined'                       
            if (this.#intersect(p.x, p.y, ...this.#steps[idx]) && (noLs || ls !=idx)) {                          
                let isGood = ((noLs? this.#maxStep-1 : ls==this.#maxStep?-1:ls)==idx-1) // direction just for the overlay
                if (!isGood && !noLs) {
                    window.WLROOM.sendExtra({type:"overlay",text:`⚠️ going the wrong way! ⚠️`, color: '#FF0000'}, p.id)
                }

                this.#lastStep[p.id] = idx
                
                let isLap = false
                if (idx==0) {
                    this.#currentLapProg[p.id] = [idx]
                } else if (this.#currentLapProg[p.id] && this.#currentLapProg[p.id][this.#currentLapProg[p.id].length-1]==idx-1) {
                    this.#currentLapProg[p.id].push(idx)
                    if (idx==this.#maxStep) {
                        isLap = isGood
                    }
                }
                

                //let isLap = isGood && idx==this.#maxStep
                
                
                const tdur = (new Date(elapsed)).toISOString().substr(14,9).replace(/^00:/,'')
                if (isLap) {   
                    if (typeof this.#laps[p.id] == 'undefined') {
                        this.#laps[p.id] = []                        
                    }    
                    if (!this.#arrivals[p.id]) {
                        this.#laps[p.id].push(elapsed)
                        let l = this.#laps[p.id].length
                        if (l==this.#lapsNum) {
                            this.#arrivals[p.id] = elapsed
                            /*------------------*/
                            const al = Object.keys(this.#arrivals).length
                            const pl = players.length
                            const won = al===1;
                            const last = al==pl;
                            const st = won?'won! time:': (last?'arrived last in:':'arrived in :');
                            const txt = `${p.name} ${st} ${tdur}`
                            announce(txt)
                            window.WLROOM.sendExtra({type:"overlay",text:txt, color: '#FFB200'})
                            if (last) {
                                return true;
                            }
                             /*------------------*/
                        } else {
                            announce(`${p.name} lap ${l} time ${tdur}`)
                        }                    
                    }                           
                    return                    
                }
                console.log(`${p.name} going thru ${idx} -- ${tdur} -- ${isGood}`, p.id, JSON.stringify(this.#currentLapProg[p.id]))
                return
            }
        }
    }
    onPlayerSpawn = (player) => {       
        this.respawnPlayerOnPevPosition(player);
    }
    respawnPlayerOnPevPosition = (player) => {
        if (this.RUNNING== this.#state && typeof this.#lastStep[player.id] !='undefined') {
            let lastStepCenter = this.#stepCenters[this.#lastStep[player.id]];
            window.WLROOM.setPlayerPosition(player,lastStepCenter[0],lastStepCenter[1])
        }
    }
    #checkArrival = (p, elapsed, players) => {
        if(p.team==0 || p.x==null || p.y==null) return;
        //if (this.#intersect(p.x,p.y, ...this.#goal)) {
        if (this.#arrivals[p.id] == null
            && ((this.#gl.length && this.#lineintersect(p.x, p.y, this.#gl))
                || (this.#fzone.length && this.#intersect(p.x, p.y, ...this.#fzone))
            )) {                
            this.#arrivals[p.id] =  (new Date(elapsed)).toISOString().substr(14,9).replace(/^00:/,'');
            const al = Object.keys(this.#arrivals).length
            const pl = players.length
            const won = al===1;
            const last = al==pl;
            const st = won?'won! time:': (last?'arrived last in:':'arrived in :');
            const txt = `${p.name} ${st} ${this.#arrivals[p.id]}`
            announce(txt)
            window.WLROOM.sendExtra({type:"overlay",text:txt, color: '#FFB200'})
            if (last) {
                return true;
            }
            console.log(p.x, p.y)

        }
    }
    onGameStart() {
        if (getActivePlayers().length>0) {
            this.#newCountdown()
        }        
    }

    onPlayerTeamChange = (p, bp) => {
        if (this.#state == this.NOT_STARTED) {
            if (p.team==0) {
                console.log(`${p.name} moved to spec`);                
            } else {
                console.log(`${p.name} moved to game`);
                this.#newCountdown()
            }
        }
    }

    #nextSpawnsRandom = () => {
        let  idx = randomIdx(this.#spawns);              
        window.WLROOM.setSpawn(1, ...this.#spawns[idx])            
    }

    #getlinecoordinates = (x0, y0, x1, y1) => {
        let ret = []
        var dx = Math.abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
        var dy = Math.abs(y1 - y0), sy = y0 < y1 ? 1 : -1; 
        var err = (dx>dy ? dx : -dy)/2;        
        while (true) {
          ret.push([x0, y0])
          if (x0 === x1 && y0 === y1) break;
          var e2 = err;
          if (e2 > -dx) { err -= dy; x0 += sx; }
          if (e2 < dy) { err += dx; y0 += sy; }
        }
        return ret;
    }
    #intersect = (px, py, zx, zy, zxx, zyy) => {
        // px = Math.round(px)
        // py = Math.round(py)
      return  px>=zx && py>=zy &&px<=zxx && py<=zyy
    }
    #lineintersect = (px, py, coords) => {                
        px = Math.floor(px)
        py = Math.floor(py)
        for (let c of coords) {
            if (c.length!=2) continue;
            if ((c[0] >= px-this.INTERSECT_RANGE && c[0] <= px+this.INTERSECT_RANGE)
                 && (c[1] <= py+this.INTERSECT_RANGE && c[1] >= py-this.INTERSECT_RANGE )) {
                    return true
                 } 
        }
        return false
    }

}

class LieroKartSettings extends RacingSettings {
    constructor() {
        super()
    }
    #wBonuses = []
    #noWeaponId = 0
    #forceWeapons = []
    #weaponCount = 79
    #weaponsAmmo = []
    #wBonusTimeout = 4000

    toJSON = () => {
        let p = super.toJSON()
        return Object.assign({}, p, { 
            weaponBonuses:this.#wBonuses,           
         });
    }
    setForceWeapons = (arr) => {this.#forceWeapons = arr; return this;}
    addWeapBonus = (x, y) => { this.#wBonuses.push([x,y]); return this;}
    setWeapBonusTimeout = (i) => { this.#wBonusTimeout=i; return this;}


    startGame = () => { 
        super.startGame()
        let weap = window.WLROOM.getWeapons()
        for (let w of weap) {
            if (w.name=="NO WEAPON") {
                this.#noWeaponId = w.id
            }
            //X == ammo
            this.#weaponsAmmo.push(w.X)
        }
        this.#weaponCount = weap.length-1
    }


    onPlayerSpawn = (player) => {      
        this.respawnPlayerOnPevPosition(player);
        window.WLROOM.setPlayerWeapons(player, [
            this.#noWeaponId,
            this.#noWeaponId,
            this.#noWeaponId,
            this.#noWeaponId,
            this.#noWeaponId,
            1,1,1,1,1]);
    }
    #getBonusDef = (x,y) => { return {type:"bonus", x:x, y:y, onExplode: this.#handleBonus} }
    #popbonus   =  (x, y) => createObject(this.#getBonusDef(x, y))
    #getNewWeap = () =>  {
        let ret = -1
        while (ret==-1 || ret == this.#noWeaponId || this.#forceWeapons.includes(ret)) {
          ret =  Math.round(Math.random()*this.#weaponCount)  
        }
        return ret
    }

    #handleBonus = (args) => {  
        if (typeof args.player == 'undefined') { return;}                
        try {
            
            let weap = window.WLROOM.getPlayerWeapons(args.player)
            //console.log(JSON.stringify(weap))
            let reload = [-1,-1,-1,-1,-1]
            //console.log(weap)
            let ok = false;
            for (let x = 0; x<5; x++) {
                let w = weap[x]
                if (w && w.id==this.#noWeaponId && !ok) {
                   weap[x]=    this.#getNewWeap()                 
                   ok = true
                   reload[x] = this.#weaponsAmmo[weap[x]]       
                } else {
                    weap[x] = w.id
                    reload[x] = w.ammo                  
                }
            }
            if (!ok) {
                weap.shift()
                weap.push(this.#getNewWeap())
                reload.shift()
                reload.push(this.#weaponsAmmo[weap[4]])
            }
            window.WLROOM.setPlayerWeapons(args.player, weap.concat(reload));
        } catch (e) {console.log(e)}
        garbageTimeouts.push(setTimeout(() => this.#popbonus(args.x, args.y), this.#wBonusTimeout));
    }
    
    onGameStart = () => {
        super.onGameStart()
        for (let b of this.#wBonuses) this.#popbonus(...b)
    }
}


if (typeof window === 'undefined') { // node js context
    exports.RacingSettings = RacingSettings
    exports.LieroKartSettings = LieroKartSettings
}