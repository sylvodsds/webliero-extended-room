if (typeof window === 'undefined') { // node js context
    console.log('hello')
    LineWobject = require('./l_mod_linewobject').LineWobject
    DestroyableSolidSpriteWobject = require('./l_mod_sprite').DestroyableSolidSpriteWobject
    SolidSpriteWobject = require('./l_mod_sprite').SolidSpriteWobject
    SpriteWobject = require('./l_mod_sprite').SpriteWobject
    RacingSettings = require('./c_race').RacingSettings
    LieroKartSettings = require('./c_race').LieroKartSettings
    CTFSettings = require('./ctf').CTFSettings
    CTF2Settings = require('./ctf').CTF2Settings
    PredSettings = require('./c_pred').PredSettings
    DTFSettings = require('./dtf').DTFSettings
    HazSettings = require('./haz').HazSettings
    SkiiJumpSettings = require('./c_skijump').SkiiJumpSettings
    DuelSettings = require('./duel').DuelSettings
    BombSettings = require('./bomb').BombSettings
    SimpleSettings = require('./c_simple').SimpleSettings
    DefuseSettings = require('./c_defuse').DefuseSettings
    AnimatedSpritesWobject = require('./l_mod_sprites_anim').AnimatedSpritesWobject
    ComplexAnimatedSpritesWobject = require('./l_mod_sprites_anim').ComplexAnimatedSpritesWobject
    ParallaxSpriteWobject = require('./l_mod_sprites_parallax').ParallaxSpriteWobject
    require('./_material_init')
    var palettes = new Map()
    palettes.set("default",[])
    WLPI = 3.141592653589793;
}

var ngrokUrl = "https://a4e8-88-121-198-99.ngrok-free.app"
var ngrokMUrl= `${ngrokUrl}/mods/`;
var assetsUrl= `https://sylvodsds.gitlab.io/webliero-extended-room/assets/`;
//var assetsUrl= `${ngrokUrl}/room/assets/`;

var mapSettings = new Map();
var tags = [];
var warmodurl = "https://www.vgm-quiz.com/dev/webliero/wledit/mods/latest/war";
var warmodExturl ="https://www.vgm-quiz.com/dev/webliero/wledit/mods/latest/warext";
//var warmodurl = "Larcelo/war_mod";
//var warmodExturl = "Larcelo/war_mod_ext";

var olx_base = "https://webliero.gitlab.io/webliero-maps/olx/ext/";

// API .setZone(x,y,x2,y2) (et .setZone(-1) pour clear) :
// micro-arena.png: 411,331,488,363
// eye-arena.png: 395,206,505,316
// chipper-arena.png: 392,233,484,345

// mapSettings.set("https://127.0.0.1/maps/test_dirt.png", {
//     pred: [[504,109]],
//     layers: {
//         clean_background: "https://127.0.0.1/maps/test_sub.png"
//     }
// })

// mapSettings.set("https://127.0.0.1/maps/test_dirt.png#t2", {
//     pred: [[504,109]],
//     layers: {
//         clean_background: true
//     }
// })

var defWarExSettings = {scoreLimit: 15,weaponChangeDelay: 20,damageMultiplier: 1.0,loadingTimes: 0.33,bonusDrops:"healthAndWeapons",bonusSpawnFrequency: 9,weapons:{ban:["CONCRETE", "LIVENS LARGE GALLERY FLAME PROJECTOR", "NON AGRESSION PACT", "ATOM BOMB", "RAMBO BOW", "MALARIA INFECTED MOSQUITOES", "AIR COMBAT GROUP", "AIR BOMBERS GROUP", "GAUSS RIFLE", "WHITE PHOSPHORUS", "EXOSKELETON", "TEAM RADIO", "HOMING PIGEON", "VZ ONE PAWNEE", "RADAR", "EMU SOLDIER", "WOJTEK BEAR", "BOOMERANG", "CARABINE SOLDIER", "SNIPER SOLDIER", "ROCKET SOLDIER", "FRIENDLY SOLDIER", "FRONTLINE SOLDIER", "WATCH TOWER", "KE GO", "DRIP POP RIFLE", "BERLIN WALL", "MAGINOT LINE", "KARL GERAT MORTAR", "MORALE DRUGS", "RADIOPLANE QQ TWO"]}}

var defWarHazSettings = {weaponChangeDelay: 20,damageMultiplier: 0.7,loadingTimes: 0.33,bonusDrops:"healthAndWeapons",bonusSpawnFrequency: 9,weapons:{ban:["LIVENS LARGE GALLERY FLAME PROJECTOR", "NON AGRESSION PACT", "ATOM BOMB", "RAMBO BOW", "MALARIA INFECTED MOSQUITOES", "AIR COMBAT GROUP", "AIR BOMBERS GROUP", "GAUSS RIFLE", "CONCRETE", "BARRAGE BALLOON", "MAGINOT LINE", "BERLIN WALL", "VZ ONE PAWNEE", "WHITE FLAG", "KARL GERAT MORTAR", "WATCH TOWER", "BACHEM BA 349 NATTER", "COMBAT UAV", "BOMBER UAV", "RADIOPLANE OQ TWO", "ZEPPELIN", "TELETANK", "BARREL BOMBS HELICOPTER", "DIE GLOCKE", "WHITE PHOSPHORUS", "FAT MAN BOMB", "EXOSKELETON", "FIRING POSITION", "TEAM RADIO", "HOMING PIGEON", "NATO AREA", "RADAR", "EMU SOLDIER", "WOJTEK BEAR", "BOOMERANG", "CARABINE SOLDIER", "SNIPER SOLDIER", "ROCKET SOLDIER", "FRIENDLY SOLDIER", "FRONTLINE SOLDIER", "WATCH TOWER", "KE GO", "DRIP POP RIFLE", "MORALE DRUGS"]}}

var defWarFlagsSettings = {scoreLimit: 15,weaponChangeDelay: 20,damageMultiplier: 1.0,loadingTimes: 0.4,bonusDrops:"healthAndWeapons",bonusSpawnFrequency: 5,weapons:{ban:["CONCRETE", "LIVENS LARGE GALLERY FLAME PROJECTOR", "NON AGRESSION PACT", "ATOM BOMB", "RAMBO BOW", "MALARIA INFECTED MOSQUITOES", "AIR COMBAT GROUP", "AIR BOMBERS GROUP", "GAUSS RIFLE", "WHITE PHOSPHORUS", "EXOSKELETON", "EMU SOLDIER", "WOJTEK BEAR", "BOOMERANG", "MORALE DRUGS"]}}

var defDTFHazSettings = {weaponChangeDelay: 20,damageMultiplier: 0.75, weapons:{ban:["SMOKE GRENADE", "BUBBLE TRAPS", "CRACKFIELD", "ORB LAUNCHER", "FLOAT MINE", "SACRIFICE", "ELECTRIC FENCE", "FORCE FIELD", "GRAVITY MINE", "JETPACK", "KEVLAR MISSILE", "PHASER RIFLE", "FIRECRACKER", "DRAGON FLIES", "AIR STRIKE", "APOCALYPSE RUNE", "SPIRIT BALL", "UZUMAKI", "DESOLATOR", "PIXEL BOMB"]}}

var defDTFPredSettings = {weaponChangeDelay: 20,bonusSpawnFrequency: 10,bonusDrops:"weapons",weapons:{ban:["VAMPIRE BITE", "REGENERATION", "GRAVITY MINE", "BUBBLE TRAPS", "CRACKFIELD", "FORTIFY", "PROTECTION", "JETPACK", "OBSTACLE", "HELICOPTER", "ELECTRIC FENCE", "FORCE FIELD", "PHASER RIFLE", "DEATH STAR", "BARRIER", "REMOTE BOMB", "DIRT TRAP", "SAFETY ZONE", "DESOLATOR", "PIXEL BOMB", "SACRIFICE", "APOCALYPSE RUNE", "DISARMABLE BOMB", "DIGGER"]}}

var crampedDTFHazSettings = {weaponChangeDelay: 20,weapons:{ban:["BARRIER", "PROTECTION", "OBSTACLE", "AIR STRIKE", "HELICOPTER", "SMOKE GRENADE", "BUBBLE TRAPS", "CRACKFIELD", "ORB LAUNCHER", "FLOAT MINE", "SACRIFICE", "ELECTRIC FENCE", "FORCE FIELD", "GRAVITY MINE", "JETPACK", "FIRECRACKER", "DRAGON FLIES", "APOCALYPSE RUNE", "DESOLATOR", "FORTIFY", "SPIRIT BALL", "FIRE BALL", "UZUMAKI", "X RAY", "REMOTE BOMB", "PIXEL BOMB"]}}

var newKhanSettings = {weaponChangeDelay: 20,weapons:{ban:["BASILISK💀", "BUDDHA💀", "CALTRAP💀", "CHAIN SICKLE💀", "CRASH BOMB💀", "CROSSBOW💀", "CRYSTAL💀", "DEATH MACE💀", "DRAGON💀", "EYE OF GENGIS💀", "FIRECRACKERS💀", "FROST SHARDS💀", "FUNGI💀", "GAUNTLET💀", "HELL STAFF💀", "KATANA💀", "KHORKHOI💀", "KI FORCE💀", "KUNAI💀", "LANTERNS💀", "PHOENIX ROD💀", "PODS💀", "ROCK BOTTOM💀", "ROOF CHARGE💀", "SHAMAN💀", "SHINGAMI💀", "SHURIKEN💀", "SMOKE BOMB💀", "SPEAR💀", "SPIRIT💀", "THUNDER BOW💀", "TIGER CLAW💀", "TRANCE💀", "WEED💀", "XANADU💀", "ZUB PAO💀", "💀TOME OF POWER💀"]}}

var defDTFDeathmatchSettings = {
    timeLimit: 15,
    scoreLimit: 15,
    respawnDelay: 3,
    loadingTimes: 0.4,
    damageMultiplier: 1.0,
    weaponChangeDelay: 20,
    maxDuplicateWeapons: 0,
    forceRandomizeWeapons: true,
    bonusDrops:"health",
    bonusSpawnFrequency: 10,
    gameMode: "dm",
    weapons:{ban:["VAMPIRE BITE", "DESOLATOR", "GRAVITY MINE", "BUBBLE TRAPS", "CRACKFIELD", "FORTIFY", "PROTECTION", "JETPACK", "OBSTACLE", "HELICOPTER", "ELECTRIC FENCE", "FORCE FIELD", "BARRIER", "KEVLAR MISSILE", "AIR STRIKE", "DIGGER", "SPIRIT BALL", "UZUMAKI", "FIRE BALL", "APOCALYPSE RUNE", "SACRIFICE"]}
};

var defWarDeathmatchSettings = {
    timeLimit: 15,
    scoreLimit: 15,
    respawnDelay: 3,
    loadingTimes: 0.33,
    damageMultiplier: 1.0,
    weaponChangeDelay: 20,
    maxDuplicateWeapons: 0,
    forceRandomizeWeapons: true,
    bonusDrops:"healthAndWeapons",
    bonusSpawnFrequency: 7,
    gameMode: "dm",
    weapons:{ban:["CONCRETE", "LIVENS LARGE GALLERY FLAME PROJECTOR", "NON AGRESSION PACT", "ATOM BOMB", "RAMBO BOW", "MALARIA INFECTED MOSQUITOES", "AIR COMBAT GROUP", "AIR BOMBERS GROUP", "GAUSS RIFLE", "WHITE PHOSPHORUS", "EXOSKELETON", "TEAM RADIO", "HOMING PIGEON", "VZ ONE PAWNEE", "RADAR", "EMU SOLDIER", "WOJTEK BEAR", "BOOMERANG", "CARABINE SOLDIER", "SNIPER SOLDIER", "ROCKET SOLDIER", "FRIENDLY SOLDIER", "FRONTLINE SOLDIER", "WATCH TOWER", "KE GO", "DRIP POP RIFLE", "BERLIN WALL", "MAGINOT LINE", "KARL GERAT MORTAR", "MORALE DRUGS", "RADIOPLANE QQ TWO"]}
};

var defWarLMSSettings = {
    timeLimit: 15,
    scoreLimit: 10,
    respawnDelay: 3,
    loadingTimes: 0.4,
    damageMultiplier: 0.9,
    weaponChangeDelay: 20,
    maxDuplicateWeapons: 0,
    forceRandomizeWeapons: true,
    bonusDrops:"healthAndWeapons",
    bonusSpawnFrequency: 7,
    gameMode: "lms",
    weapons:{ban:["LIVENS LARGE GALLERY FLAME PROJECTOR", "NON AGRESSION PACT", "ATOM BOMB", "RAMBO BOW", "MALARIA INFECTED MOSQUITOES", "AIR COMBAT GROUP", "AIR BOMBERS GROUP", "GAUSS RIFLE", "WHITE PHOSPHORUS", "EXOSKELETON", "TEAM RADIO", "HOMING PIGEON", "VZ ONE PAWNEE", "RADAR", "EMU SOLDIER", "WOJTEK BEAR", "BOOMERANG", "MORALE DRUGS", "RADIOPLANE QQ TWO"]}
};

var defDefaultDeathmatchSettings = {
    timeLimit: 15,
    scoreLimit: 15,
    respawnDelay: 3,
    loadingTimes: 0.4,
    damageMultiplier: 1.0,
    weaponChangeDelay: 20,
    maxDuplicateWeapons: 0,
    forceRandomizeWeapons: true,
    bonusDrops:"health",
    bonusSpawnFrequency: 10,
    gameMode: "dm"
};

var defDTKDeathmatchSettings = {
    timeLimit: 15,
    scoreLimit: 15,
    respawnDelay: 3,
    loadingTimes: 0.4,
    damageMultiplier: 1.0,
    weaponChangeDelay: 0,
    maxDuplicateWeapons: 0,
    forceRandomizeWeapons: true,
    bonusDrops:"health",
    bonusSpawnFrequency: 10,
    gameMode: "dm",
    weapons:{ban:["ACCELERATE"]}
};

var defSuperSnowmodeSettings = {
    timeLimit: 15,
    scoreLimit: 20,
    respawnDelay: 3,
    loadingTimes: 0.4,
    damageMultiplier: 1.25,
    weaponChangeDelay: 20,
    maxDuplicateWeapons: 0,
    forceRandomizeWeapons: true,
    bonusDrops:"health",
    bonusSpawnFrequency: 10,
    gameMode: "lms"
};

var defSuperRFMSettings = {
    timeLimit: 15,
    scoreLimit: 20,
    respawnDelay: 3,
    loadingTimes: 0.4,
    damageMultiplier: 2.0,
    weaponChangeDelay: 20,
    maxDuplicateWeapons: 0,
    forceRandomizeWeapons: true,
    bonusDrops:"health",
    bonusSpawnFrequency: 10,
    gameMode: "lms",
    weapons:{ban:["PULSAR", "REGENERATION"]}
};

var defRifleDeathmatchSettings = {
	timeLimit: 10,
	scoreLimit: 15,
	respawnDelay: 1,
	maxDuplicateWeapons: 0,
	bonusDrops:"none",
	bonusSpawnFrequency: 0,
	loadingTimes: 0.4,
	damageMultiplier: 4.0,
	weaponChangeDelay: 20,
	forceRandomizeWeapons: true,
	gameMode: "dm",
	weapons:{only: ["RIFLE"]}
};

var subDTFDeathmatchSettings = {
    timeLimit: 15,
    scoreLimit: 15,
    respawnDelay: 3,
    loadingTimes: 0.4,
    damageMultiplier: 0.9,
    weaponChangeDelay: 15,
    maxDuplicateWeapons: 0,
    forceRandomizeWeapons: true,
    bonusDrops:"none",
    bonusSpawnFrequency: 10,
    gameMode: "dm",
    weapons:{ban:["PHASER RIFLE","DEATH STAR","FIRECRACKER","FAKE FLAG","SACRIFICE","REMOTE BOMB","APOCALYPSE RUNE","DRAGON FLIES","AIR STRIKE","DIRT TRAP","BARRIER","VAMPIRE BITE","REGENERATION","GRAVITY MINE","BUBBLE TRAPS","CRACKFIELD","FORTIFY","PROTECTION","JETPACK","OBSTACLE","HELICOPTER","ELECTRIC FENCE","FORCE FIELD","KEVLAR MISSILE","AIR STRIKE","SAFETY ZONE","UZUMAKI","PIXEL BOMB","SPIRIT BALL","FIRE BALL","DESOLATOR"]}
};

var subDTFHoldTheFlagSettings = {
    timeLimit: 15,
    scoreLimit: 15,
    respawnDelay: 3,
    loadingTimes: 0.4,
    damageMultiplier: 0.9,
    weaponChangeDelay: 15,
    maxDuplicateWeapons: 0,
    forceRandomizeWeapons: true,
    bonusDrops:"none",
    bonusSpawnFrequency: 10,
    gameMode: "htf",
    weapons:{ban:["PHASER RIFLE","DEATH STAR","FIRECRACKER","FAKE FLAG","SACRIFICE","REMOTE BOMB","APOCALYPSE RUNE","DRAGON FLIES","AIR STRIKE","DIRT TRAP","BARRIER","VAMPIRE BITE","REGENERATION","GRAVITY MINE","BUBBLE TRAPS","CRACKFIELD","FORTIFY","PROTECTION","JETPACK","OBSTACLE","HELICOPTER","ELECTRIC FENCE","FORCE FIELD","KEVLAR MISSILE","AIR STRIKE","SAFETY ZONE"]}
};

var napalmDTFDeathmatchSettings = {
    timeLimit: 15,
    scoreLimit: 15,
    respawnDelay: 3,
    loadingTimes: 0.3,
    damageMultiplier: 0.9,
    weaponChangeDelay: 15,
    maxDuplicateWeapons: 0,
    forceRandomizeWeapons: true,
    bonusDrops:"none",
    bonusSpawnFrequency: 10,
    gameMode: "dm",
    weapons:{only: ["NAPALM"]}
};

var boomerangWarDeathmatchSettings = {
    timeLimit: 15,
    scoreLimit: 15,
    respawnDelay: 3,
    loadingTimes: 0.3,
    damageMultiplier: 2.0,
    weaponChangeDelay: 15,
    maxDuplicateWeapons: 0,
    forceRandomizeWeapons: true,
    bonusDrops:"none",
    bonusSpawnFrequency: 10,
    gameMode: "dm",
    weapons:{only: ["BOOMERANG"]}
};


/* --------------------------- */

mapSettings.set("dsds/xmas2020/izbla-xmas2020.png", {
    modes: { ctf: [
        // spawn flag left

        (new CTFSettings())
            .addFlagGreenSpawn(244, 334)
            .addGreenSpawn(402, 112)
            .addGreenSpawn(110, 224)
            .addGreenSpawn(382, 224)
            .addGreenSpawn(382, 224)

            .addFlagBlueSpawn(1008-244, 334)
            .addBlueSpawn(1008-402, 112)
            .addBlueSpawn(1008-110, 224)
            .addBlueSpawn(1008-382, 224)
            .addBlueSpawn(1008-382, 224)       
    
            
        ],},
    expand:true,
    palette:true,     
    objects: (() => {
        const up = WLPI/2*3
        const down = WLPI/2
        const right = 0
        const left = WLPI
        
        let obj = []
         
        obj.push({ type:'laser_green',x:388,y:254,speed:6, angle: WLPI*0.3}) // green left,        
        obj.push({ type:'laser_green',x:99,y:254,speed:6, angle: WLPI*0.7}) // green left,        
        obj.push({ type:'laser_green',x:367,y:244,speed:6, angle: left}) // green right,
        obj.push({ type:'laser_orange',x:504,y:116,speed:6, angle: up}) // orange up,       
        obj.push({ type:'laser_blue',x:1008-388,y:254,speed:6, angle: WLPI*0.7}) // blue left,        
        obj.push({ type:'laser_blue',x:1008-99,y:254,speed:6, angle: WLPI*0.3}) // blue left,        
        obj.push({ type:'laser_blue',x:1008-367,y:244,speed:6, angle: right}) // blue right,      
        return obj
     })(),
     mods: [
        {
            name: "xmas2020",
            json:"https://webliero.gitlab.io/webliero-mods/kangaroo+dsds/xmas2020/mod.json5",
            sprites:"https://webliero.gitlab.io/webliero-mods/kangaroo+dsds/xmas2020/sprites.wlsprt",
            soundpack:"https://sylvodsds.gitlab.io/liero-soundpacks/xmas.zip"
        },
            "kangur/_lasers"
        ],

    tags: ['xmas']
})

mapSettings.set("dsds/xmas2020/cathode-xmas2020.png", {
    modes: { ctf: [
        // spawn flag left

        (new CTFSettings())
            .addFlagGreenSpawn(124, 106)
            .addGreenSpawn(283, 27)
            .addGreenSpawn(286, 148)
            .addGreenSpawn(124, 13)

            .addFlagBlueSpawn(1004-124, 106)
            .addBlueSpawn(1004-283, 27)
            .addBlueSpawn(1004-286, 148)
            .addBlueSpawn(1004-124, 13)       
    
            
        ],},
    expand:true,
    palette:true,
    //materials: defaultMaterials.map(noUndef),    
    objects: (() => {
        const up = WLPI/2*3
        const down = WLPI/2
        const right = 0
        const left = WLPI
        
        let obj = []

       // obj.push({ type:64,x:2,y:2,speed:0}) // green down,       
        obj.push({ type:'laser_green',x:104,y:122,speed:6, angle: left}) // green left,        
        obj.push({ type:'laser_green',x:72,y:30,speed:6, angle: right}) // green right,
        obj.push({ type:'laser_orange',x:147,y:88,speed:6, angle: up}) // orange up,       
        obj.push({ type:'laser_blue',x:1004-100,y:122,speed:6, angle: right}) // blue left,        
        obj.push({ type:'laser_blue', x:1004-70,y:30,speed:6, angle: left}) // blue right,
        obj.push({ type:'laser_orange',x:1004-147,y:88,speed:6, angle: up}) // orange up,       
        return obj
     })(),
     mods: [ 
           "xmas2020",
           //"kangur/dtf",
            "kangur/_lasers"
        ],
    // layers: {
    //     "front":"same"
    // }
    tags: ['xmas']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/dsds/evil_flag_map.png", {
    modes: { ctf: [
        // spawn flag left

        (new CTFSettings())
            .addFlagGreenSpawn(98, 30)
            .addGreenSpawn(60, 15)
           
            .addFlagBlueSpawn(408, 30) 
            .addBlueSpawn(436, 10)
            
        ],},
    materials: defaultMaterials.map(noUndef),    
    objects: (() => {
        const up = WLPI/2*3
        const down = WLPI/2
        const right = 0
        const left = WLPI
        
        let obj = []

        obj.push({wlx:"platform", form:"platform_classic", id: 80, x:44, y:160, angle: up, speed:0.2, steps:
        [
            ["y","lower_than",60,down],     
            ["y","more_than",280,up],               
        ]
        })
        obj.push({wlx:"platform", form:"platform_classic", id: 81, x:462, y:160, angle: down, speed:0.2, steps:
        [
            ["y","more_than",280,up],  
            ["y","lower_than",60,down],                              
        ]
        })
        obj.push({wlx:"platform", form:"platform_square", id: 82, x:158, y:158, angle: right, speed:0.2, steps:
            [
                ["x","more_than",345,down],     
                ["y","more_than",242,left], 
                ["x","lower_than",158,up],      
                ["y","lower_than",160,right],      
            ]
        })
        obj.push({wlx:"platform", form:"platform_square", id: 83, x:331, y:172, angle: left, speed:0.2, steps:
            [
                ["x","lower_than",172,down],     
                ["y","more_than",227,right], 
                ["x","more_than",331,up],      
                ["y","lower_than",172,left],      
            ]
        })
        obj.push({wlx:"platform", form:"platform_square", id: 84, x:190, y:212, angle: right, speed:0.2, steps:
            [
                ["x","more_than",312,up, 0.5],     
                ["y","lower_than",190,left, 0.1], 
                ["x","lower_than",190,down, 0.4],      
                ["y","more_than",212,right, 0.3],      
            ]
        })
        
        for (let x = 10; x<=500;x+=35) {
            obj.push({type:"lava", x:x, y:330})
        }

        obj.push({ type:'laser_green_down',x:2,y:2,speed:0}) // green down,
        obj.push({ type:'laser_green',x:248,y:2,speed:6, angle: left}) // green left,
        obj.push({ type:'laser_blue_down',x:501,y:2,speed:0}) // blue down,
        obj.push({ type:'laser_blue',x:256,y:2,speed:6, angle: right}) // blue right,
        obj.push({ type:'laser_green_up',x:93,y:123,speed:0}) // green up,
        obj.push({ type:'laser_green_up',x:115,y:41,speed:0}) // green up,
        obj.push({ type:'laser_blue_up',x:409,y:123,speed:0}) // blue up,
        obj.push({ type:'laser_blue_up',x:387,y:41,speed:0}) // blue up,
       
        return obj
     })(),
     mods: [ 
            "kangur/dtf",
            "kangur/_lasers",
            "kangur/_platforms",
            "kangur/_lava",            
        ],
    tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set("dsds/space_one.png", {
    modes: {pred: [(new PredSettings).addLemonSpawn(706,56).setSettings(defDTFPredSettings)],},
    objects: [
        { wlx:"turret", wobject:'dtf_laser_pistol',x:706,y:56,freq:2,dist:1000,speed:1}, // laser pistol,
        { wlx:"turret", wobject:'dtf_solar_flare',x:628,y:202,freq:8,dist:1000,speed:4}, // solar flare,
        { wlx:"turret", wobject:'dtf_solar_flare',x:786,y:202,freq:8,dist:1000,speed:4}, // solar flare,
        { wlx:"turret", wobject:'dtf_railgun',x:562,y:500,freq:3,dist:1000,speed:3}, // RAILGUN,
        { wlx:"turret", wobject:'dtf_railgun',x:850,y:500,freq:3,dist:1000,speed:3}, // RAILGUN,
        { wlx:"directionnal", wobject:'dtf_railgun',x:364,y:600,freq:4,angle: WLPI*1.74,dist:1000,speed:2}, // RAILGUN,
        { wlx:"directionnal", wobject:'dtf_railgun',x:385,y:602,freq:6,angle: WLPI*1.8,dist:1000,speed:3}, // RAILGUN,
        { wlx:"directionnal", wobject:'dtf_panzer',x:1169,y:589,freq:8,angle: WLPI*1.25,dist:1000,speed:1}, // Panzer,
        { wlx:"directionnal", wobject:'dtf_laser_pistol',x:1155,y:762,freq:2,angle: WLPI*1.33,dist:1000,speed:1}, // laser pistol,
        { wlx:"directionnal", wobject:'dtf_laser_pistol',x:1334,y:662,freq:3,angle: WLPI*1.22,dist:1000,speed:1.2}, // laser pistol,
    ],
    materials: defaultMaterials.map(noUndef),
    palette: [21,19,19,108,56,0,108,80,0,164,148,128,0,144,0,61,173,61,252,84,84,168,168,168,85,85,85,84,84,252,84,216,84,84,252,252,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,35,30,45,84,84,84,92,92,92,100,100,100,109,109,109,116,116,116,125,125,125,132,132,132,140,140,140,148,148,148,157,157,157,56,56,136,81,81,193,105,105,249,145,145,245,185,185,245,110,110,110,145,145,145,181,181,181,217,217,217,32,96,32,45,133,45,62,174,62,113,189,113,165,213,165,111,111,111,146,146,146,182,182,182,218,218,218,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,157,121,89,197,169,125,237,217,161,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,147,147,147,180,180,180,216,216,216,253,253,253,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,217,1,1,189,1,1,165,1,1,200,0,0,172,0,0,218,2,2,190,2,2,166,2,2,216,0,0,188,0,0,164,0,0,82,82,194,106,106,250,146,146,246,80,80,192,107,107,251,147,147,247,149,137,1,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,253,1,1,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,245,233,61,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,46,134,46,63,175,63,114,190,114,47,135,47,64,176,64,115,191,115,248,60,60,244,124,124,244,188,188,104,104,248,148,148,248,184,184,244,144,144,244,65,177,65,116,192,116,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,6,2,18,15,2,23,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,245,165,165,248,92,92,245,77,77,244,60,60,244,76,76,244,92,92,244,164,164,84,40,0,19,34,96,18,27,85,28,40,88,7,0,13,39,6,80,18,6,32,0,0,0,252,252,252,221,221,221,189,189,189,158,158,158,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,254,2,2,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
    mods: [
            "kangur/dtf",
            {
                from: "kangur/dtf",
                wObjects:{3:'dtf_solar_flare',8:'dtf_panzer',9:'dtf_railgun',38:'dtf_laser_pistol'}
            }

    ],
    tags: ['classic', 'big', 'dtfmod']
})

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/arena/micro-arena.png", {
    modes: { haz: [
        (new HazSettings())
            .setZone(450,375,49*49,0)
            .addSpawn(450,134)
            .addSpawn(290,253)
            .addSpawn(610,253)
            .setSettings(defDTFHazSettings),
    ]},
    mods: [ 
        "kangur/dtf",
    ],
    tags: ['classic', 'dtfmod']
})

mapSettings.set("wgetch/arena/eye-arena.png", {
    modes: { haz: [
    (new HazSettings())
    .setZone(395,206,505,316)
    .setSettings(defDTFHazSettings),
    ],},
    objects: [
        {type:"trampoline", x:450, y:472},
    ],
    mods: [
           "kangur/dtf",
           "kangur/_fans",
    ],
    tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set("wgetch/arena/chipper-arena.png", {
    modes: {  haz: [
    (new HazSettings())
    .setZone(392,233,485,285)
    .setSettings(defDTFHazSettings),
    ],},
    mods: [
        "kangur/dtf",
    ],
    tags: ['classic', 'dtfmod']
})

mapSettings.set("wgetch/zone/igloo-zone.png", {
    modes: { haz: [
    (new HazSettings())
    .setZone(512,293,598,329)
    .setSettings(crampedDTFHazSettings)
    ],},
    mods: [
        "kangur/dtf_base_icefloor",
        "kangur/dtf",
    ],
    tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/zone/inter-zone.png", {
    modes: { haz: [
    (new HazSettings())
    .setZone(497,255,553,288)
    .setSettings(defDTFHazSettings)
    ],},
    mods: [
        "kangur/dtf",
    ],
    tags: ['classic', 'dtfmod']
})

mapSettings.set("wgetch/zone/light-zone.png", {
    modes: {  haz: [
        (new HazSettings())
        .setZone(265,207,348,255)
        .addSpawn(240,136)
        .addSpawn(284,141)
        .addSpawn(340,145)
        .addSpawn(379,139)
        .addSpawn(288,187)
        .addSpawn(332,192)
        .addSpawn(234,198)
        .addSpawn(381,209)
        .addSpawn(243,241)        
        .addSpawn(375,249)
        .addSpawn(240,286)
        .addSpawn(380,284)
        .addSpawn(233,164)
        .addSpawn(385,165)
        .addSpawn(307,230)
        .setSettings(defDTFHazSettings)
    ], },
    objects: [
        {type:"lava", x:229, y:334},
        {type:"lava", x:242, y:334},
        {type:"lava", x:257, y:334},
        {type:"lava", x:271, y:334},
        {type:"lava", x:287, y:334},
        {type:"lava", x:291, y:334},
        {type:"lava", x:307, y:334},
        {type:"lava", x:320, y:334},
        {type:"lava", x:334, y:334},
        {type:"lava", x:350, y:334},
        {type:"lava", x:361, y:334},
        {type:"lava", x:373, y:334},
        {type:"lava", x:388, y:334},
       // {type:"platform", x:245, y:491},

    ],
    materials: defaultMaterials.map(noUndef),
    mods: [
           "kangur/dtf",
           "kangur/_lava",
    ],
    tags: ['classic', 'dtfmod']
})

mapSettings.set("pilaf/hole.png", {
    modes: {pred: [(new PredSettings).addLemonSpawn(341,256).setSettings(defDTFPredSettings)],},
    objects: [
        {type:"lava", x:174, y:616},
        {type:"lava", x:186, y:622},
        {type:"lava", x:195, y:628},
        {type:"lava", x:208, y:635},
        {type:"lava", x:222, y:645},
        {type:"lava", x:237, y:652},
        {type:"lava", x:252, y:658},
        {type:"lava", x:270, y:665},
        {type:"lava", x:287, y:668},
        {type:"lava", x:299, y:672},
        {type:"lava", x:316, y:670},
        {type:"lava", x:330, y:672},
        {type:"lava", x:345, y:668},
        {type:"lava", x:360, y:670},
        {type:"lava", x:380, y:666},
        {type:"lava", x:395, y:670},
        {type:"lava", x:411, y:666},
        {type:"lava", x:425, y:663},
        {type:"lava", x:435, y:659},
        {type:"lava", x:450, y:655},
        {type:"lava", x:467, y:650},
        {type:"lava", x:480, y:640},
        {type:"lava", x:495, y:639},
        {type:"lava", x:515, y:629},               
        {type:"lava", x:536, y:621},
        {type:"lava", x:542, y:616},
    ],
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.ROCK,0, 155, 141,142,182,183)),
    mods: [ 
           "kangur/dtf",
           "kangur/_lava",
    ],
    tags: ['classic', 'dtfmod']
})

mapSettings.set("piebaron/PieBaron_BR_pokolTribut_v0.lev", {
    modes: {pred: [(new PredSettings).addLemonSpawn(504,109).setSettings(defDTFPredSettings)],},
    expand:true,
    mods: [
        "kangur/dtf",
    ],
    tags: ['classic', 'dtfmod']
})

mapSettings.set("dsds/squigglyswiggly/squig_fourth.png", {
    modes: {pred: [(new PredSettings).addLemonSpawn(238,58).setSettings(defDTFPredSettings)],},
    materials: defaultMaterials.map(noUndef),
    mods: [
        "kangur/dtf",
    ],
    tags: ['classic', 'dtfmod']
})

mapSettings.set("pilaf/axiom.lev", {
    modes: {pred: [(new PredSettings).addLemonSpawn(504,119).setSettings(defDTFPredSettings)],},
    expand:true,
    mods: [
        "kangur/dtf",
    ],
    tags: ['classic', 'dtfmod']
})

mapSettings.set("piebaron/fArtefact_00.png", 
    {
        modes: { pred:[
            (new PredSettings())
            .addLemonSpawn(256,187)
            .addWormSpawn(318, 1870)
            .addWormSpawn(117, 1756)
            .addWormSpawn(340, 1667)
            .addWormSpawn(174, 1676)
            .addWormSpawn(222, 1656)
            .addWormSpawn(178, 1554)
            .addWormSpawn(225, 1554)
            .addWormSpawn(143, 1489)
            .addWormSpawn(327, 1489)
            .addWormSpawn(186, 1422)
            .addWormSpawn(340, 1420)
            .addWormSpawn(108, 1351)
            .addWormSpawn(181, 1319)
            .addWormSpawn(366, 1319)
            .addWormSpawn(395, 1270)
            .addWormSpawn(347, 1218)
            .addWormSpawn(122, 1115)
            .addWormSpawn(379, 1073)
            .addWormSpawn(134, 1028)
            .addWormSpawn(372, 969)
            .addWormSpawn(372, 969)
            .addWormSpawn(110, 892)
            .addWormSpawn(400, 800)
            .addWormSpawn(80, 760)
            .addWormSpawn(425, 690)
            .addWormSpawn(60, 600)
            .addWormSpawn(450, 530)
            .addWormSpawn(111, 403)
            .addWormSpawn(410, 265)
            .addWormSpawn(256, 189)
            .addWormSpawn(458, 73)
            .addWormSpawn(313, 10)
            .setOrder(PredSettings.ORDER.LOOP_LAST_FOUR)
        ],},
        objects: (() => {
            let obj = []
            
            for (let x = 10; x<=500;x+=35) {
                obj.push({type:"lava", x:x, y:1910})
            }
            return obj
         })(),
         objectsSteps: (() => {        
            let steps = [10]
    
            for (let y = 1878; y>=1020;y-=32) {
                let step = []
                for (let x = 10; x<=500;x+=35) {
                    step.push({type:"lava", x:x, y:y})
                }
                steps.push(step)
            }
    
            return steps
         })(),
         mods: [ 
                "kangur/dtf",
                "kangur/_lava",            
            ],
        tags: ['crazy', 'dtfmod']
    }    
)

mapSettings.set("wgetch/arena/snek-arena.png", 
    {
        modes: { dtf: [
            [
                [0,677,309],
                [1,659,238],
                [2,214, 157],
            ],
            [
                [0,197,164],
                [1,242,92],
                [2,645, 317],
              ]
        ]    },
        mods: [
            "kangur/dtf",
        ],
        tags: ['crazy']
    }
)

mapSettings.set("wgetch/flag/bunker-flag.png", 
    {
        modes: { dtf: [
            // spawn flag left
            (new DTFSettings())
            .addFlagSpawn(784, 236)
            .addFlagSpawn(609, 340)
            .addFlagSpawn(711, 350)
            .addFlagSpawn(711, 315)
            .addFlagSpawn(259, 168)   
            .addFlagSpawn(359, 168)
            .addFlagSpawn(359, 231)
            .addFlagSpawn(259, 231)
            .addDefenseSpawn(784, 240)        
            .addAttackSpawn(1219, 294)
            .addAttackSpawn(1219, 376)
            .addAttackSpawn(1080, 256)
            .setSettings(crampedDTFHazSettings)
        ],
        ctf: [
            // spawn flag left
            (new CTFSettings())
                .addFlagGreenSpawn(258, 169)
                .addGreenSpawn(711, 315)
                .addBlueSpawn(915, 360)
                .addFlagBlueSpawn(1213, 297)
                .setSettings(crampedDTFHazSettings)
            ]
        },
        mods: [
            "kangur/dtf",
        ],
        tags: ['classic', 'big', 'cramped', 'dtfmod']
})

mapSettings.set("wgetch/arena/gallery-arena.png", 
    {
        modes: { ctf: [
            // spawn flag left
            (new CTFSettings())
                .addFlagGreenSpawn(228, 49)
                .addGreenSpawn(106, 133)
                .addFlagGreenSpawn(226, 309)                  
                .addGreenSpawn(114, 374)
                .addFlagGreenSpawn(672, 309) 
                .addGreenSpawn(794, 378)
                .addFlagGreenSpawn(668, 49)  
                .addGreenSpawn(794, 133)

                .addFlagBlueSpawn(672, 309) 
                .addBlueSpawn(794, 378)
                .addFlagBlueSpawn(668, 49)  
                .addBlueSpawn(794, 133)                
                .addFlagBlueSpawn(228, 49)
                .addBlueSpawn(106, 133)
                .addFlagBlueSpawn(226, 309)                  
                .addBlueSpawn(114, 374)
 


                .setOrder(true)
            ],},
        materials: defaultMaterials.map(noUndef),
        mods: [
            "kangur/dtf",
        ],
        tags: ['classic', 'big', 'dtfmod']
})

mapSettings.set("wgetch/flag/passage-flag.png", 
    {
        modes: { dtf: [
            // spawn flag left
            (new DTFSettings())
            .addFlagSpawn(236, 144)
            .addFlagSpawn(418, 193)  
            .addFlagSpawn(330, 194)  
            .addDefenseSpawn(418, 193)        
            .addDefenseSpawn(569, 176)        

            .addAttackSpawn(812, 152)
            .addAttackSpawn(812, 236)
            .addAttackSpawn(713, 179)
            .addAttackSpawn(713, 200)
            .addAttackSpawn(608, 175)
            .setSettings(crampedDTFHazSettings)

        ] },
        mods: [
            "kangur/dtf",
        ],
        tags: ['classic', 'dtfmod']
})

mapSettings.set("wgetch/flag/ambi-flag.png", 
    {
        modes: {  ctf: [            
            (new CTFSettings())
            .addFlagGreenSpawn(267, 155) 
            .addGreenSpawn(232, 175)                          

            .addFlagBlueSpawn(481, 297)
            .addBlueSpawn(515, 281)
            .setSettings(crampedDTFHazSettings)
        ]},
        mods: [
            "kangur/dtf",
        ],
        tags: ['classic', 'cramped', 'dtfmod']
})

mapSettings.set("wgetch/flag/ambi150-flag.png", 
    {
        modes: { ctf: [            
            (new CTFSettings())
            .addFlagGreenSpawn(293, 170) 
            .addGreenSpawn(257, 190)
            .addFlagGreenSpawn(645, 189) 
            .addGreenSpawn(626, 171)                          

            .addFlagBlueSpawn(616, 387)
            .addBlueSpawn(540, 421)
            .addFlagBlueSpawn(261, 421)
            .addBlueSpawn(279, 396)
            .setSettings(crampedDTFHazSettings)
            .setOrder(true)
        ]},
        mods: [
            "kangur/dtf",
        ],
        tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set("wgetch/flag/proton-flag.png", 
    {
        modes: { dtf: [
            // spawn flag left
            (new DTFSettings())
                .addFlagSpawn(269, 312)   
                .addFlagSpawn(273, 500)

                .addDefenseSpawn(336, 155)
                .addDefenseSpawn(302, 422)
                .addDefenseSpawn(336, 666)
                .addDefenseSpawn(269, 312)
                .addDefenseSpawn(273, 500)

                .addAttackSpawn(1206, 159)
                .addAttackSpawn(1206, 672)
                .addAttackSpawn(1120, 410),
            // spawn flag right
            (new DTFSettings())
                .addFlagSpawn(985,258)   
                .addFlagSpawn(1004,324)
                .addFlagSpawn(1077,312)
                .addFlagSpawn(1018,519)
                .addFlagSpawn(1074,535)
                .addFlagSpawn(1006,581)

                .addDefenseSpawn(917,616)
                .addDefenseSpawn(928,234)
                .addDefenseSpawn(991,421)
                .addDefenseSpawn(1072,421)

                .addAttackSpawn(244, 228)
                .addAttackSpawn(244, 672)
                .addAttackSpawn(265, 325) 
                .addAttackSpawn(265, 511) 
                .addAttackSpawn(265, 420) 
                .addAttackSpawn(240, 666) 
        ],
        ctf: [
            (new CTFSettings())
                .addFlagGreenSpawn(269, 312)   
                .addFlagGreenSpawn(273, 500)

                .addGreenSpawn(336, 155)
                .addGreenSpawn(302, 422)                
                .addGreenSpawn(269, 312)
                .addGreenSpawn(273, 500)
           
                .addFlagBlueSpawn(985,258)   
                .addFlagBlueSpawn(1074,535)

                .addBlueSpawn(917,616)
                .addBlueSpawn(928,234)
                .addBlueSpawn(991,421)
                .addBlueSpawn(1072,421)
        ],},
        mods: [
            "kangur/dtf",
        ],
        tags: ['classic', 'big', 'dtfmod'] 
    }
)

mapSettings.set("dsds/fortified_castles.png",{
    modes: {ctf: [
        // spawn flag left
        (new CTFSettings())
            .addFlagGreenSpawn(121, 194)   
            //.addFlagSpawn(273, 500)

            .addGreenSpawn(370, 189)
            // .addDefenseSpawn(302, 422)
            // .addDefenseSpawn(336, 666)
            // .addDefenseSpawn(269, 312)
            // .addDefenseSpawn(273, 500)

            .addBlueSpawn(800, 222)
            .addFlagBlueSpawn(1146, 202)
            //.addAttackSpawn(1120, 410),
        ]},

objects: [
           {type:"fog_of_war",x:175,y:165},
           {type:"fog_of_war",x:1150,y:170},
        ],
        mods: [ 
               "kangur/dtf",
               "kangur/_others",                      
           ],

        tags: ['classic', 'big', 'dtfmod']
})

mapSettings.set("dsds/space_station.png",{
    modes: { ctf: [
        // spawn flag left
        (new CTFSettings())
            .addFlagGreenSpawn(337, 224)   
            //.addFlagSpawn(273, 500)

            .addGreenSpawn(546, 221)
            // .addDefenseSpawn(302, 422)
            // .addDefenseSpawn(336, 666)
            // .addDefenseSpawn(269, 312)
            // .addDefenseSpawn(273, 500)

            .addBlueSpawn(897, 924)
            .addFlagBlueSpawn(1183, 981)
            //.addAttackSpawn(1120, 410),
        ],},
    palette: true,
    materials: defaultMaterials.map(noUndef),
    mods: [
        "kangur/dtf",
    ],
    tags: ['classic', 'big', 'dtfmod']
})

mapSettings.set("dsds/weblieroZ/kameshouseint.png",{
    modes: { ctf: [
        // spawn flag left
        (new CTFSettings())
            .addFlagGreenSpawn(684, 262)   
            .addFlagGreenSpawn(606, 234)
            .addFlagGreenSpawn(377, 303)

            .addGreenSpawn(318, 230)
            .addGreenSpawn(398, 294)
            .addGreenSpawn(606, 234)

            .addBlueSpawn(723, 704)
            .addBlueSpawn(660, 640)
            .addBlueSpawn(634, 686)

            .addFlagBlueSpawn(723,704)            
            .addFlagBlueSpawn(660, 640)     
            .addFlagBlueSpawn(692, 695)  
        ],},
    palette: true,
    materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,1,2)),
    colorAnim: false,
    backgroundLayer: "true",
    mods: [ 
        "kangur/dtf",
    ],
    tags: ['dbz', 'dtfmod']
})

mapSettings.set("wgetch/labs/jambon.png",{
    modes: {ctf: [
        // spawn flag left
        (new CTFSettings())
            .addFlagGreenSpawn(90, 500)   

            .addGreenSpawn(42, 388)

            .addBlueSpawn(858, 388)
            .addFlagBlueSpawn(810, 500)
        ],},
    materials: defaultMaterials.map(noUndef),
    palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,247,128,0,252,84,84,168,168,168,85,85,85,84,84,252,255,161,4,255,235,88,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,76,76,76,84,84,84,92,92,92,100,100,100,109,109,109,116,116,116,125,125,125,132,132,132,220,125,153,148,148,148,157,157,157,56,56,136,81,81,193,105,105,249,145,145,245,185,185,245,110,110,110,145,145,145,181,181,181,217,217,217,32,96,32,45,133,45,62,174,62,255,150,76,255,178,142,111,111,111,146,146,146,182,182,182,218,218,218,168,168,248,208,208,244,255,226,219,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,157,121,89,197,169,125,237,217,161,235,92,167,255,132,199,255,174,212,200,100,0,160,80,0,72,72,72,108,108,108,147,147,147,180,180,180,216,216,216,253,253,253,255,177,210,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,217,1,1,189,1,1,165,1,1,200,0,0,172,0,0,218,2,2,190,2,2,166,2,2,255,0,251,234,0,222,207,0,196,82,82,194,106,106,250,146,146,246,80,80,192,107,107,251,147,147,247,149,137,1,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,253,1,1,248,24,4,255,0,251,255,0,244,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,245,233,61,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,255,226,219,46,134,46,63,175,63,114,190,114,47,135,47,64,176,64,115,191,115,255,35,255,244,124,124,255,172,246,104,104,248,148,148,248,184,184,244,144,144,244,65,177,65,116,192,116,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,89,41,1,137,108,151,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,245,165,165,248,92,92,245,77,77,244,60,60,244,76,76,244,92,92,244,164,164,92,119,102,89,128,103,80,136,100,72,150,100,11,21,5,28,64,42,29,59,40,22,37,13,252,252,252,221,221,221,189,189,189,158,158,158,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,254,2,2,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,249,0,255,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
    mods: [
        "kangur/dtf",
    ],
    tags: ['crazy', 'dtfmod']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/dsds/frog_material_edit.png",{
    modes: { ctf: [
        // spawn flag left
        (new CTFSettings())
            .addFlagGreenSpawn(544, 83)   

            .addGreenSpawn(487, 51)

            .addBlueSpawn(553, 972)
            .addFlagBlueSpawn(510, 1016)
        ],},
    palette:true,
    materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(189,208))),
    backgroundLayer: "https://sylvodsds.gitlab.io/webliero-maps/dsds/frog_material_edit_bg.png",
    mods: [
        "kangur/dtf",
    ],
    tags: ['classic', 'big', 'dtfmod']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/kangaroo/gusanos/JDM_remix.png",{
    modes: {ctf: [
        // spawn flag left
        (new CTFSettings())
            .addFlagGreenSpawn(113, 118)   

            .addGreenSpawn(159, 72)

            .addBlueSpawn(829, 487)
            .addFlagBlueSpawn(770, 493)
        ]},
        palette:true,
        materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.BG_DIRT,..._range(82,84))).map(replaceMatIndexBy(MATERIAL.BG_DIRT,..._range(230,232))).map(replaceMatIndexBy(MATERIAL.BG,..._range(188,209))).map(replaceMatIndexBy(MATERIAL.ROCK,..._range(210,229))),
        backgroundLayer: "true",
        mods: [
            "kangur/dtf",
        ],
        tags: ['classic', 'big', 'dtfmod']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/wgetch/remix/spaceshipint-remix-with-bg.png", 
    {
        modes: {  dtf:[
        [
            [0,416,447],
            [1,590,450],
            [2,1345, 350],
        ],
        [
            [0,1051,163],
            [1,1106,318],
            [2,447, 444],
        ],
     ] },
        palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,61,173,61,252,84,84,168,168,168,70,70,70,84,84,252,84,216,84,84,252,252,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,63,63,63,76,76,76,92,92,92,100,100,100,109,109,109,121,121,121,130,130,130,135,135,135,143,143,143,150,150,150,162,162,162,56,56,136,81,81,193,105,105,249,145,145,245,185,185,245,110,110,110,145,145,145,181,181,181,217,217,217,32,96,32,45,133,45,62,174,62,113,189,113,165,213,165,116,116,116,151,151,151,182,182,182,218,218,218,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,157,121,89,197,169,125,237,217,161,156,120,88,196,168,124,223,208,167,200,100,0,160,80,0,57,57,57,100,100,100,147,147,147,180,180,180,216,216,216,253,253,253,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,217,1,1,189,1,1,165,1,1,200,0,0,172,0,0,218,2,2,190,2,2,166,2,2,216,0,0,188,0,0,164,0,0,82,82,194,106,106,250,146,146,246,80,80,192,107,107,251,147,147,247,149,137,1,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,253,1,1,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,245,233,61,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,46,134,46,63,175,63,114,190,114,47,135,47,64,176,64,115,191,115,248,60,60,244,124,124,244,188,188,104,104,248,148,148,248,184,184,244,144,144,244,65,177,65,116,192,116,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,89,41,1,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,245,165,165,248,92,92,245,77,77,244,60,60,244,76,76,244,92,92,244,164,164,84,40,0,88,40,0,92,44,0,96,48,0,60,28,0,64,28,0,68,32,0,72,36,0,252,252,252,221,221,221,189,189,189,158,158,158,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,1,1,1,21,14,3,15,21,43,38,9,12,13,15,15,41,26,41,53,30,29,15,30,55,27,40,50,31,34,30,51,38,12,89,62,33,46,52,46,46,64,74,67,41,35,31,71,124,54,73,81,79,88,80,125,71,43,81,92,84,86,108,113,64,109,137,124,106,70,102,121,120,93,106,105,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,254,2,2,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
        materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.BG,..._range(189,213))),
        backgroundLayer: "https://sylvodsds.gitlab.io/webliero-maps/wgetch/remix/spaceshipint-remix-bg-layer.png",
        mods: [
            "kangur/dtf",
        ],
        tags: ['main', 'classic', 'dtfmod']
    }
)

mapSettings.set("dsds/frog_cave_cave_only.png", 
    {
        modes: {dtf:[
        [
            [0,113,29],
            [1,109,176],
            [2,1157, 200],
        ],
        [
            [0,1172,221],
            [1,1022,353],
            [2,113,29],
        ],
     ] },
     colorAnim: [129,131,133,136,152,159,168,171],
     objects: [
           {type:"minigun",x:1100,y:275},
        ],
      mods: [ 
            "kangur/dtf",
            "kangur/_special",
        ],  
     tags: ['main', 'classic', 'dtfmod']
    }
)

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/kangaroo/JetmenRevival/GardenSmall_bgr.png", 
    {      
        modes: {ctf: [
            (new CTFSettings())
                .addFlagBlueSpawn(219, 648)   

                .addBlueSpawn(148, 482)
                .addBlueSpawn(290, 485)
                .addBlueSpawn(219, 600)
                .addBlueSpawn(129, 646)

                .addFlagGreenSpawn(585, 648)
                .addGreenSpawn(477, 477)
                .addGreenSpawn(638, 490)
                .addGreenSpawn(679, 646)
                .addGreenSpawn(585, 600)
                
            ],},

        objects: [
           {type:"khorkhoi",x:420,y:95,angle:WLPI,speed:1.5},
        ],
        mods: [ 
               "kangur/dtf",
               "kangur/_others",                      
           ],

        palette: [0,0,0,80,80,80,60,60,60,164,148,128,0,144,0,61,173,61,252,84,84,168,168,168,85,85,85,84,84,252,84,216,84,84,252,252,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,76,76,76,84,84,84,92,92,92,100,100,100,109,109,109,116,116,116,125,125,125,132,132,132,140,140,140,148,148,148,157,157,157,56,56,136,81,81,193,105,105,249,145,145,245,185,185,245,110,110,110,145,145,145,181,181,181,217,217,217,32,96,32,45,133,45,62,174,62,113,189,113,165,213,165,111,111,111,146,146,146,182,182,182,218,218,218,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,157,121,89,197,169,125,237,217,161,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,147,147,147,180,180,180,216,216,216,253,253,253,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,217,1,1,189,1,1,165,1,1,200,0,0,172,0,0,218,2,2,190,2,2,166,2,2,216,0,0,188,0,0,164,0,0,82,82,194,106,106,250,146,146,246,80,80,192,107,107,251,147,147,247,149,137,1,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,253,1,1,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,245,233,61,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,46,134,46,63,175,63,114,190,114,47,135,47,64,176,64,115,191,115,248,60,60,244,124,124,244,188,188,104,104,248,148,148,248,184,184,244,144,144,244,65,177,65,116,192,116,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,89,41,1,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,245,165,165,248,92,92,245,77,77,244,60,60,244,76,76,244,92,92,244,164,164,84,40,0,88,40,0,92,44,0,96,48,0,60,28,0,64,28,0,68,32,0,72,36,0,252,252,252,221,221,221,189,189,189,158,158,158,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,1,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,1,1,1,1,1,1,0,0,1,1,0,0,1,0,1,0,1,1,1,2,1,2,2,2,2,2,2,1,1,2,2,1,1,2,1,2,1,2,2,2,3,2,3,3,3,3,3,3,2,2,3,3,2,2,3,2,3,2,3,2,2,0,2,0,0,0,2,2,0,0,2,2,0,2,0,2,0,0,0,3,0,3,3,3,0,0,3,3,0,0,3,0,3,0,3,1,2,3,3,2,1,1,3,1,3,1,1,1,1,3,2,3,1,1,3,2,4,0,0,0,4,0,0,0,4,4,1,0,4,1,1,1,4,1,1,1,4,254,2,2,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
        materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG_DIRT,129,133)),
        backgroundLayer: "true",
        tags: ['classic', 'big', 'dtfmod']
    }
)

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/kangaroo/JetmenRevival/LunarDigSmall_bgr.png", 
    {      
        modes: {ctf: [
            (new CTFSettings())
                .addFlagBlueSpawn(723, 733)   
                .addBlueSpawn(723, 733)   
                .addFlagBlueSpawn(277, 122) 
                .addBlueSpawn(277, 122)        

                .addFlagGreenSpawn(277, 122)
                .addGreenSpawn(277, 122)
                .addFlagGreenSpawn(723, 733)  
                .addGreenSpawn(723, 733)  
                
                .setOrder(true)
            ],},
        palette: [0,0,0,80,80,80,60,60,60,164,148,128,0,144,0,61,173,61,252,84,84,168,168,168,85,85,85,84,84,252,84,216,84,84,252,252,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,76,76,76,84,84,84,92,92,92,100,100,100,109,109,109,116,116,116,125,125,125,132,132,132,140,140,140,148,148,148,157,157,157,56,56,136,81,81,193,105,105,249,145,145,245,185,185,245,110,110,110,145,145,145,181,181,181,217,217,217,32,96,32,45,133,45,62,174,62,113,189,113,165,213,165,111,111,111,146,146,146,182,182,182,218,218,218,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,157,121,89,197,169,125,237,217,161,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,147,147,147,180,180,180,216,216,216,253,253,253,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,217,1,1,189,1,1,165,1,1,200,0,0,172,0,0,218,2,2,190,2,2,166,2,2,216,0,0,188,0,0,164,0,0,82,82,194,106,106,250,146,146,246,80,80,192,107,107,251,147,147,247,149,137,1,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,253,1,1,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,245,233,61,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,46,134,46,63,175,63,114,190,114,47,135,47,64,176,64,115,191,115,248,60,60,244,124,124,244,188,188,104,104,248,148,148,248,184,184,244,144,144,244,65,177,65,116,192,116,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,89,41,1,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,245,165,165,248,92,92,245,77,77,244,60,60,244,76,76,244,92,92,244,164,164,84,40,0,88,40,0,92,44,0,96,48,0,60,28,0,64,28,0,68,32,0,72,36,0,252,252,252,221,221,221,189,189,189,158,158,158,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,1,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,1,1,1,1,1,1,0,0,1,1,0,0,1,0,1,0,1,1,1,2,1,2,2,2,2,2,2,1,1,2,2,1,1,2,1,2,1,2,2,2,3,2,3,3,3,3,3,3,2,2,3,3,2,2,3,2,3,2,3,2,2,0,2,0,0,0,2,2,0,0,2,2,0,2,0,2,0,0,0,3,0,3,3,3,0,0,3,3,0,0,3,0,3,0,3,1,2,3,3,2,1,1,3,1,3,1,1,1,1,3,2,3,1,1,3,2,4,0,0,0,4,0,0,0,4,4,1,0,4,1,1,1,4,1,1,1,4,254,2,2,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
        materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG_DIRT,129,133)),
        backgroundLayer: "true",
        mods: [
            "kangur/dtf",
        ],
        tags: ['classic', 'big', 'dtfmod']
    }
)

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/kangaroo/JetmenRevival/MountDoomSmall_bgr.png", 
    {      
        modes: {ctf: [
            (new CTFSettings())
                .addFlagBlueSpawn(73, 114)   
                .addBlueSpawn(73, 114)        
                .addFlagBlueSpawn(26, 264)   
                .addBlueSpawn(26, 264)
                .addFlagBlueSpawn(167, 562)   
                .addBlueSpawn(167, 562)

                .addFlagGreenSpawn(699, 514)
                .addGreenSpawn(699, 514)
                .addFlagGreenSpawn(735, 95)
                .addGreenSpawn(735, 95)
                .addFlagGreenSpawn(769, 258)
                .addGreenSpawn(769, 258)
                .setOrder(CTFSettings.ORDER.RANDOM_SAME_INDEX)
            ],},
        palette: [0,0,0,80,80,80,60,60,60,164,148,128,0,144,0,61,173,61,252,84,84,168,168,168,85,85,85,84,84,252,84,216,84,84,252,252,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,76,76,76,84,84,84,92,92,92,100,100,100,109,109,109,116,116,116,125,125,125,132,132,132,140,140,140,148,148,148,157,157,157,56,56,136,81,81,193,105,105,249,145,145,245,185,185,245,110,110,110,145,145,145,181,181,181,217,217,217,32,96,32,45,133,45,62,174,62,113,189,113,165,213,165,111,111,111,146,146,146,182,182,182,218,218,218,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,157,121,89,197,169,125,237,217,161,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,147,147,147,180,180,180,216,216,216,253,253,253,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,217,1,1,189,1,1,165,1,1,200,0,0,172,0,0,218,2,2,190,2,2,166,2,2,216,0,0,188,0,0,164,0,0,82,82,194,106,106,250,146,146,246,80,80,192,107,107,251,147,147,247,149,137,1,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,253,1,1,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,245,233,61,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,46,134,46,63,175,63,114,190,114,47,135,47,64,176,64,115,191,115,248,60,60,244,124,124,244,188,188,104,104,248,148,148,248,184,184,244,144,144,244,65,177,65,116,192,116,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,89,41,1,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,245,165,165,248,92,92,245,77,77,244,60,60,244,76,76,244,92,92,244,164,164,84,40,0,88,40,0,92,44,0,96,48,0,60,28,0,64,28,0,68,32,0,72,36,0,252,252,252,221,221,221,189,189,189,158,158,158,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,1,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,1,1,1,1,1,1,0,0,1,1,0,0,1,0,1,0,1,1,1,2,1,2,2,2,2,2,2,1,1,2,2,1,1,2,1,2,1,2,2,2,3,2,3,3,3,3,3,3,2,2,3,3,2,2,3,2,3,2,3,2,2,0,2,0,0,0,2,2,0,0,2,2,0,2,0,2,0,0,0,3,0,3,3,3,0,0,3,3,0,0,3,0,3,0,3,1,2,3,3,2,1,1,3,1,3,1,1,1,1,3,2,3,1,1,3,2,4,0,0,0,4,0,0,0,4,4,1,0,4,1,1,1,4,1,1,1,4,254,2,2,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
        materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG_DIRT,129,133)),
        backgroundLayer: "true",
        mods: [
            "kangur/dtf",
        ],
        tags: ['main', 'classic', 'dtfmod']
    }
)

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/kangaroo/JetmenRevival/PostNukeSmall_remix.png", 
    {      
        modes: {ctf: [
            (new CTFSettings())
                .addFlagBlueSpawn(178, 157)   
                .addBlueSpawn(178, 157)    
                .addFlagBlueSpawn(79, 523)   
                .addBlueSpawn(79, 523)

                .addFlagGreenSpawn(549, 337)
                .addGreenSpawn(549, 337)
                .addFlagGreenSpawn(713, 525)
                .addGreenSpawn(713, 525)
        
                .setOrder(CTFSettings.ORDER.RANDOM_SAME_INDEX)

                .setOnFlagSpawnChange((color,x,y) => {
                    console.log('creating object on flag position change')
                    if (color=='green') {
                        window.WLROOM.removeObject(69)
                        let xx = 562
                        let yy = 314
                        if (x==713) {
                            xx = 726
                            yy = 501
                        }
                        createObject({id:69, type: 'post_nuke_green_light', x: xx, y: yy})
                    } else if (color=='blue') {
                        window.WLROOM.removeObject(70)
                        let xx = 163
                        let yy = 129
                        if (x==79) {
                            xx = 64
                            yy = 500
                        }
                        createObject({id:70, type: 'post_nuke_blue_light', x: xx, y: yy})
                    }
                   
                })
                .setOnGameStart((cgame) => {
                    window.WLROOM.removeObject(69)
                    
                    let gs = cgame.getCurrentFlagSpawn('green')
                    let xx = 562
                    let yy = 314
                    if (gs.x==713) {
                        xx = 726
                        yy = 501
                    }
                    createObject({id:69, type: 'post_nuke_green_light', x: xx, y: yy})

                    window.WLROOM.removeObject(70)
                    
                    let bs = cgame.getCurrentFlagSpawn('blue')
                    xx = 163
                    yy = 129
                    if (bs.x==79) {
                        xx = 64
                        yy = 500
                    }
                    createObject({id:70, type: 'post_nuke_blue_light', x: xx, y: yy})
                    
                })
            ],},
        objects: [
           {wlx:"teleport",x:4,y:210,dist:2,toX:750,toY:210},
           {wlx:"teleport",x:794,y:210,dist:2,toX:40,toY:210},
           {type:"contamination", x:380, y:330},
           {type:"hospital_blue", x:214, y:470},
           {type:"hospital_blue", x:448, y:262},
           {type:"hospital_green", x:306, y:356},
           {type:"hospital_green", x:594, y:181},
        ],
        mods: [ 
               "kangur/dtf",
               "kangur/_teleport",
               "kangur/_others",
        //              {
        //     name: "kangur/_others",
        //     json:`${ngrokMUrl}kangur/_others/mod.json5`,
        //     sprites:`${ngrokMUrl}kangur/_others/sprites.wlsprt`
        // },
               "dsds/_postnukelights",                        
           ],
        palette: true,
        colorAnim: [129,132,133,136,152,159,168,171],
        materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(190,215))).map(replaceMatIndexBy(MATERIAL.BG_DIRT,219,220,221)).map(replaceMatIndexBy(MATERIAL.BG,188,189)).map(replaceMatIndexBy(MATERIAL.DIRT,..._range(216,218))),
        backgroundLayer: "https://sylvodsds.gitlab.io/webliero-maps/kangaroo/JetmenRevival/PostNukeSmall_remix_bglayer.png",
        tags: ['main', 'classic', 'dtfmod']
    }
)

mapSettings.set("kangaroo/JetmenRevival/CaveDot.png", 
    {
        modes: {dtf:[
        [
            [0,651,383],
            [1,550,479],
            [2,124,102],
        ],
        [
            [0,140,106],
            [1,305,168],
            [2,668,382],
        ],
     ],
     ctf: [
            (new CTFSettings())
                .addFlagBlueSpawn(651, 383)   

                .addBlueSpawn(550, 479)

                .addGreenSpawn(305, 168)
                .addFlagGreenSpawn(140, 106)
            ],},
        materials: defaultMaterials.map(noUndef),
        colorAnim: false,
        mods: [
            "kangur/dtf",
        ],
        tags: ['main', 'classic', 'dtfmod']
    }
)

mapSettings.set("PiPitek/SHAARK4.png", 
    {
        modes: { dtf:[
        (new DTFSettings())
            .addFlagSpawn(36,173)
            .addDefenseSpawn(39,144)
            .addAttackSpawn(486,150),
        (new DTFSettings())
            .addFlagSpawn(486,150)
            .addDefenseSpawn(447,114)
            .addAttackSpawn(100,56),
     ],},
     objects: (() => {
        let obj = [{type:"water", x:60, y:340}]
        
        for (let x = 70; x<469;x+=35) {
            obj.push({type:"water", x:x, y:340})
        }
        return obj
     })(),
     objectsSteps: (() => {        
        let steps = [30]

        for (let y = 340; y>=200;y-=20) {
            let step = []
            for (let x = 0; x<504;x+=35) {
                step.push({type:"water", x:x, y:y})
            }
            steps.push(step)
        }

        return steps
     })(),
     mods: [ 
            "kangur/dtf",
            "kangur/_water",           
        ],
    tags: ['main', 'classic', 'dtfmod']
    }
)

mapSettings.set("dsds/weblieroZ/pilaf.png", 
    {
        modes: {dtf:[
        [
            [0,920,175],
            [1,748,145],
            [2,62,302],
        ],
        [
            [0,288,80],
            [1,198,140],
            [2,894,456],
        ],
     ],},
     materials: defaultMaterials.map(noUndef),
     mods: [
         "kangur/dtf",
     ],
     tags: ['dbz', 'dtfmod']
    }
)

mapSettings.set("qmaps/qmap20.png", 
    {
        modes: { dtf:[
        [
            [0,94,373],
            [1,204,343],
            [2,613,41],
        ]
     ] },
     mods: [
         "kangur/dtf",
     ],
     tags: ['main', 'classic', 'dtfmod']
    }
)

mapSettings.set("wgetch/flag/electron-flag.png", 
    {
        modes: { dtf:[
        [
            [0,830,345],
            [1,870,345],
            [2,340,345],
        ],
        [
            [0,340,345],
            [1,255,284],           
            [2,830,345],
        ]
     ],
     ctf: [
         
         (new CTFSettings())
             .addFlagGreenSpawn(346, 339)   
 
             .addGreenSpawn(290, 232)
             .addGreenSpawn(320, 343)
             .addGreenSpawn(333, 502)

             .addFlagBlueSpawn(842, 339)
             .addBlueSpawn(877, 349)             
             .addBlueSpawn(904, 238)
             .addBlueSpawn(861, 503)


         ],},
    materials: defaultMaterials.map(noUndef),
    mods: [
        "kangur/dtf",
    ],
    tags: ['classic', 'big', 'dtfmod']
    }
)

mapSettings.set("wgetch/flag/dens-flag.png", 
    {
        modes: {  ctf: [
         
         (new CTFSettings())
             .addFlagGreenSpawn(274, 217)
             .addGreenSpawn(309, 210)

             .addBlueSpawn(380, 418)
             .addFlagBlueSpawn(429, 429)
             .setSettings(crampedDTFHazSettings)

         ],},
    materials: defaultMaterials.map(noUndef),
    mods: [ 
        "kangur/dtf",
    ],
    tags: ['classic', 'cramped', 'dtfmod']
    }
)
mapSettings.set("wgetch/flag/forts-flag.png", 
    {
        modes: {ctf: [
         
         (new CTFSettings())
             .addBlueSpawn(269, 208)
             .addBlueSpawn(354, 255)
             .addBlueSpawn(380, 207)
             .addBlueSpawn(362, 296)
             .addBlueSpawn(429, 336)
             .addBlueSpawn(332, 357)
             .addBlueSpawn(364, 337)
             .addBlueSpawn(322, 414)
             .addBlueSpawn(426, 415)
             .addBlueSpawn(475, 166)
             .addBlueSpawn(462, 293)
             .addBlueSpawn(395, 376)
             .addBlueSpawn(252, 396)
             .addFlagBlueSpawn(246, 252)             
             .addFlagBlueSpawn(252, 295)             
             .addFlagBlueSpawn(252, 334)             
             .addFlagBlueSpawn(228, 407)             
             .addFlagBlueSpawn(492, 218)             
             .addFlagBlueSpawn(492, 255)             
             .addFlagBlueSpawn(429, 330)    
             .addGreenSpawn(925, 208)
             .addGreenSpawn(840, 255)
             .addGreenSpawn(814, 207)
             .addGreenSpawn(832, 296)
             .addGreenSpawn(765, 336)
             .addGreenSpawn(861, 357)
             .addGreenSpawn(829, 337)
             .addGreenSpawn(872, 414)
             .addGreenSpawn(768, 415)
             .addGreenSpawn(718, 166)
             .addGreenSpawn(732, 293)
             .addGreenSpawn(798, 376)
             .addGreenSpawn(941, 396)
             .addFlagGreenSpawn(948, 252)             
             .addFlagGreenSpawn(941, 295)             
             .addFlagGreenSpawn(941, 334)             
             .addFlagGreenSpawn(963, 407)             
             .addFlagGreenSpawn(701, 218)             
             .addFlagGreenSpawn(701, 255)             
             .addFlagGreenSpawn(765, 330)           

         ],},
        objects: [
           {type:"minigun",x:965,y:260},
           {type:"minigun",x:235,y:260},
           {type:"minigun",x:370,y:420},
           {type:"minigun",x:825,y:420},
        ],
        mods: [ 
            "kangur/dtf",
            "kangur/_special",
        ],  
        tags: ['classic', 'big', 'dtfmod']
    }
)

mapSettings.set("wgetch/flag/tripwire-flag.png", {
    modes: { ctf: [    
        (new CTFSettings())
            .addGreenSpawn(490, 192)
            .addFlagGreenSpawn(254, 134)
           
            .addBlueSpawn(490, 385) 
            .addFlagBlueSpawn(254, 423)
            
        ],},     
    objects: (() => {
        const up = WLPI/2*3
        const down = WLPI/2
        const right = 0
        const left = WLPI
        
        let obj = []
        
        obj.push({ type:'laser_green',x:276,y:192,speed:6, angle: left}) // green right,
        obj.push({ type:'laser_blue',x:229,y:383,speed:6, angle: right}) // blue right,        
        obj.push({ type:'laser_orange',x:533,y:288,speed:6.0001, angle: right}) // orange right,
        obj.push({type:'minigun',x:558,y:148})
        obj.push({type:'minigun',x:250,y:244})
        obj.push({type:'minigun',x:560,y:341})

        return obj
     })(),
     mods: [ 
            "kangur/dtf",
            "kangur/_lasers",
            "kangur/_special",
        ],
    tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set("wgetch/flag/supersonic-flag.png", {
    modes: { ctf: [    
        (new CTFSettings())
            .addGreenSpawn(1133, 169)
            .addGreenSpawn(1214, 169)
            .addGreenSpawn(1133, 296)
            .addGreenSpawn(1214, 296)
            .addGreenSpawn(1121, 234)
            .addGreenSpawn(1226, 234)
            .addFlagGreenSpawn(1172, 234)
           
            .addBlueSpawn(235, 169) 
            .addBlueSpawn(316, 169) 
            .addBlueSpawn(235, 296) 
            .addBlueSpawn(316, 296) 
            .addBlueSpawn(223, 234) 
            .addBlueSpawn(328, 234) 
            .addFlagBlueSpawn(277, 234)
            
        ],},     
    objects: (() => {
        const up = WLPI/2*3
        const down = WLPI/2
        const right = 0
        const left = WLPI
        
        let obj = []
        
        obj.push({ type:'laser_green',x:1076,y:376,speed:6, angle: down}) // green right,
        obj.push({ type:'laser_blue',x:373,y:376,speed:6, angle: down}) // blue right,        

        return obj
     })(),
     mods: [ 
            "kangur/dtf",
            "kangur/_lasers",           
        ],
    tags: ['main', 'classic', 'big', 'dtfmod']
})

mapSettings.set("wgetch/flag/stereosonic-flag.png", {
    modes: { ctf: [    
        (new CTFSettings())
            .addGreenSpawn(1149, 592)
            .addGreenSpawn(1149, 623)
            .addGreenSpawn(1180, 304)
            .addGreenSpawn(1134, 240)
            .addGreenSpawn(843, 191)
            .addGreenSpawn(794, 240)
            .addFlagGreenSpawn(907, 352)
            .addFlagGreenSpawn(981, 395)
            .addFlagGreenSpawn(1054, 367)
           
            .addBlueSpawn(300, 592) 
            .addBlueSpawn(300, 623) 
            .addBlueSpawn(269, 304) 
            .addBlueSpawn(315, 240) 
            .addBlueSpawn(606, 191) 
            .addBlueSpawn(655, 240) 
            .addFlagBlueSpawn(542, 352)
            .addFlagBlueSpawn(468, 395)
            .addFlagBlueSpawn(395, 367)
            
        ],},     
    mods: [ 
        "kangur/dtf",
    ],
    tags: ['classic', 'dtfmod']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-builder-maps/maps/Eva_s_eastern_enquiry.png", {
    modes: { ctf: [    
        (new CTFSettings())
            .addGreenSpawn(64, 82)
            .addFlagGreenSpawn(64, 82)
            .addGreenSpawn(17, 405)
            .addFlagGreenSpawn(17, 405)

            .addBlueSpawn(1009, 405)           
            .addFlagBlueSpawn(1009, 405)           
            .addBlueSpawn(957, 82) 
            .addFlagBlueSpawn(957, 82)
            .setOrder(CTFSettings.ORDER.RANDOM_SAME_INDEX)
        ],},       
    objects: (() => {
        const up = WLPI/2*3
        const down = WLPI/2
        const right = 0
        const left = WLPI
        
        let obj = []
        
        obj.push({ type:'laser_orange',x:513,y:340,speed:6, angle: up}) // orange right
        obj.push({type:'minigun',x:125,y:430})
        obj.push({type:'minigun',x:895,y:430})
        obj.push({type:'minigun',x:135,y:285})
        obj.push({type:'minigun',x:885,y:285})
        obj.push({type:'minigun',x:210,y:160})
        obj.push({type:'minigun',x:820,y:160})

        return obj
     })(),
     mods: [ 
            "kangur/dtf",
            "kangur/_lasers",
            "kangur/_special",
        ],
    tags: ['classic', 'builder', 'big', 'dtfmod']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-builder-maps/maps/Rafael_s_reduced_regret_(bg_hack).png", {
    modes: { ctf: [    
        (new CTFSettings())
            .addGreenSpawn(1490, 202)
            .addFlagGreenSpawn(1610, 180)

            .addBlueSpawn(209, 202)           
            .addFlagBlueSpawn(91, 180)           
        ],},
    materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(188,195))),
    palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,61,173,61,252,84,84,168,168,168,85,85,85,84,84,252,84,216,84,84,252,252,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,76,76,76,84,84,84,92,92,92,100,100,100,109,109,109,116,116,116,125,125,125,132,132,132,140,140,140,148,148,148,157,157,157,56,56,136,81,81,193,105,105,249,145,145,245,185,185,245,110,110,110,145,145,145,181,181,181,217,217,217,32,96,32,45,133,45,62,174,62,113,189,113,165,213,165,111,111,111,146,146,146,182,182,182,218,218,218,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,157,121,89,197,169,125,237,217,161,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,147,147,147,180,180,180,216,216,216,253,253,253,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,217,1,1,189,1,1,165,1,1,200,0,0,172,0,0,218,2,2,190,2,2,166,2,2,216,0,0,188,0,0,164,0,0,82,82,194,106,106,250,146,146,246,80,80,192,107,107,251,147,147,247,149,137,1,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,253,1,1,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,245,233,61,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,46,134,46,63,175,63,114,190,114,47,135,47,64,176,64,115,191,115,248,60,60,244,124,124,244,188,188,104,104,248,148,148,248,184,184,244,144,144,244,65,177,65,116,192,116,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,89,41,1,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,245,165,165,248,92,92,245,77,77,244,60,60,244,76,76,244,92,92,244,164,164,84,40,0,88,40,0,92,44,0,96,48,0,60,28,0,64,28,0,68,32,0,72,36,0,252,252,252,221,221,221,189,189,189,158,158,158,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,29,30,54,14,11,19,38,25,56,54,27,87,31,34,24,16,15,9,18,35,18,29,42,23,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,254,2,2,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
    objects: (() => {
        // const up = WLPI/2*3
        // const down = WLPI/2
        // const right = 0
        // const left = WLPI
        
         let obj = []
        
        // obj.push({ type:'laser_orange',x:513,y:340,speed:6, angle: up}) // orange right, change the ID after xmas
        obj.push({ type:'laser_green_down',x:938,y:124,speed:0}) 
        obj.push({ type:'laser_blue_down',x:761,y:124,speed:0}) 
        return obj
     })(),
     backgroundLayer: "true",
     mods: [ 
            "kangur/dtf",
            "kangur/_lasers",           
        ], 
    tags: ['classic', 'builder', 'big', 'dtfmod']  
})

mapSettings.set("https://webliero.gitlab.io/webliero-maps/神風/cs/cs_ash.png", {
    modes: { ctf2: [    
        (new CTF2Settings())
            .addGreenSpawn(183, 390)
            .addGreenSpawn(80, 258)
            .addGreenSpawn(127, 380)
            .addGreenSpawn(100, 500)
            .addGreenSpawn(40, 440)
            .addFlagGreenSpawn(215, 408)

            .addBlueSpawn(897, 390)           
            .addBlueSpawn(1017, 258)           
            .addBlueSpawn(982, 380)           
            .addBlueSpawn(1000, 500)           
            .addBlueSpawn(1060, 440)           
            .addFlagBlueSpawn(884, 408)           
        ],},
    mods: [ 
            "神風/counter_strike_rewormed",            
        ], 
    palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,136,212,252,152,220,252,164,232,252,176,236,252,188,252,252,212,252,252,224,252,252,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,136,212,252,136,212,252,152,220,252,164,232,252,176,236,252,188,252,252,212,252,252,224,252,252,28,28,28,32,32,32,32,32,32,36,36,36,0,0,0,0,0,0,0,0,0,0,0,0,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,252,252,252,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,252,252,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],  
    tags: ['main', 'classic', 'cs']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/kangaroo/gusanos/dn_core_alt_remix.png", 
    {
        modes: { dtf: [
            (new DTFSettings())
            .addFlagSpawn(760, 61)
            .addDefenseSpawn(780, 175)        
            .addAttackSpawn(314, 408)
         ],
        },
        palette: true,
        materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.BG,..._range(188,201))).map(replaceMatIndexBy(MATERIAL.BG_DIRT,..._range(82,84))).map(replaceMatIndexBy(MATERIAL.BG_DIRT,..._range(202,216))).map(replaceMatIndexBy(MATERIAL.ROCK,..._range(217,227))),
        backgroundLayer: "true",
  
    objects: (() => {
        const up = WLPI/2*3
        const down = WLPI/2
        const right = 0
        const left = WLPI
        
        let obj = []

        obj.push({wlx:"platform", form:"platform_classic", id: 81, x:700, y:26, angle: left, speed:0.2, steps:
            [    
               ["x","lower_than",685,right], 
               ["x","more_than",835,left],           
            ]
        })

        obj.push({ type:'laser_green',x:647,y:69,speed:6, angle: right}) // green right 1,
        obj.push({ type:'laser_blue',x:877,y:69,speed:6, angle: left}) // blue left,
        obj.push({ type:'laser_green',x:641,y:9,speed:6, angle: right})  // green right 2,
        obj.push({ type:'trampoline',x:80,y:447}) // trampoline,
        obj.push({ type:'fan',x:81,y:97,speed:6, angle: right}) // fan,
        obj.push({ type:'door_right',x:415,y:409}) // door right 1,
        obj.push({ type:'door_left',x:158,y:444}) // door left,
        obj.push({ type:'disappearing_square',x:839,y:316}) // disappearing square,
        obj.push({ type:'door_right',x:616,y:26}) // door right 2,
        obj.push({ type:'disappearing_platform',x:320,y:315}) // disappearing platform,
        obj.push({wlx:"teleport",x:840,y:409,dist:2,toX:580,toY:27})  // teleport,
        obj.push({type:'thunder',x:760,y:2})  // thunder
        obj.push({type:'minigun',x:195,y:305})  // minigun
       
        return obj
     })(),

 mods: [ 
            "kangur/dtf",
            "kangur/_lasers",
            "kangur/_platforms",
            "kangur/_fans", 
            "kangur/_teleport",
            "kangur/_doors",
            "kangur/_others",
            "kangur/_special",
        ],    
    tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set("kangaroo/gusanos/wtf2_extended_alt2.png", 
    {
        modes: { dtf: [
            (new DTFSettings())
            .addFlagSpawn(466, 63)
            .addDefenseSpawn(425, 205)        
            .addAttackSpawn(832, 454)
            .addAttackSpawn(104, 475)
        ],

       ctf: [
            (new CTFSettings())
            .addFlagGreenSpawn(886, 157)
            .addGreenSpawn(878, 232)
            .addBlueSpawn(163, 128)
            .addFlagBlueSpawn(39, 71)
        ],

         pred: [(new PredSettings).addLemonSpawn(391,184).setSettings(defDTFPredSettings)],

         haz: [(new HazSettings).setZone(598,135,698,190).setSettings(defDTFHazSettings)],
  
},

objects: (() => {
        const up = WLPI/2*3
        const down = WLPI/2
        const right = 0
        const left = WLPI
        
        let obj = []

        obj.push({wlx:"platform", form:"platform_small", id: 80, x:370, y:92, angle: right, speed:0.2, steps:
            [ 
                ["x","more_than",432,left],     
                ["x","lower_than",322,right],      
            ]
        })
        obj.push({wlx:"platform", form:"platform_classic", id: 81, x:400, y:140, angle: left, speed:0.2, steps:
            [    
               ["x","lower_than",237,right], 
               ["x","more_than",511,down],
               ["y","more_than",450,up], 
               ["y","lower_than",141,left],            
            ]
        })

        obj.push({ type:'laser_green',x:387,y:17,speed:6, angle: right}) // green right,
        obj.push({ type:'laser_green',x:560,y:77,speed:6, angle: left}) // green left,
        obj.push({ type:'trampoline',x:831,y:455}) // trampoline,
        obj.push({ type:'fan',x:70,y:484,speed:6, angle: up}) // fan,
        obj.push({ type:'death_vortex',x:426,y:44}) // death vortex,
        obj.push({wlx:"teleport",x:646,y:162,dist:2,toX:102,toY:474})  // teleport 1,
        obj.push({wlx:"teleport",x:48,y:116,dist:2,toX:771,toY:455})  // teleport 2,
  
        return obj
     })(),

 mods: [ 
            "kangur/dtf",
            "kangur/_lasers",
            "kangur/_platforms",
            "kangur/_fans",
            "kangur/_teleport",
        ],    
    tags: ['classic', 'big', 'dtfmod'] 
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/神風/dirts.png", 
    {
        modes: { duel: [
            (new DuelSettings())                       
            .addSpawn(640, 799)            
            .addSpawn(727, 870)            
            .addSpawn(798, 877)            
            .addSpawn(850, 848)
            .setSettings({                              
                scoreLimit: 3,
                weaponChangeDelay: 90,            
            })
            .setTrainingSettings({   
                scoreLimit: 8,
                timeLimit: 5,            
                gameMode: "lms",
                weaponChangeDelay: 90,
            })       
        ],     
    
},
// onGameTick: (g) => {      
//    let players = getActivePlayers();        
//    for (let p of players) {
//        if (p.x==null || p.y==null) continue
//        if (p.x<5 || p.x>1490 || p.y<80 || p.y>1250) {
//            console.log(p.x, p.y)
//            window.WLROOM.setPlayerHealth(p.id, 0)
//        }
//    }
// },
objects: (() => {      
    let obj = []
    for (let x = 0; x<=1500; x+=50)  { 
        obj.push({type:"blacklineofdeath", x:x, y:1300})
        obj.push({type:"blacklineofdeath", x:x, y:80})
        obj.push({type:"vertical_line", x:1, y:x})
        obj.push({type:"vertical_line", x:1499, y:x})
     }
        return obj
     })(),
 palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,136,212,252,152,220,252,164,232,252,176,236,252,188,252,252,212,252,252,224,252,252,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,136,212,252,136,212,252,152,220,252,164,232,252,176,236,252,188,252,252,212,252,252,224,252,252,28,28,28,32,32,32,32,32,32,36,36,36,0,0,0,0,0,0,0,0,0,0,0,0,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,252,252,252,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,252,252,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
 mods: [             
            "https://sylvodsds.gitlab.io/webliero-extended-mods/%E7%A5%9E%E9%A2%A8/dirts.zip",           
           "https://sylvodsds.gitlab.io/webliero-extended-mods/dsds/_blacklineofdeath.zip",
           new LineWobject('vertical_line', 0, 0, 50, 1, 50, 666, 10)                  
        ],    
    tags: ['main', 'classic', 'dirts']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/dsds/dirts_2.png", 
    {
        modes: { duel: [
            (new DuelSettings())                       
            .addSpawn(631, 829)            
            .addSpawn(757, 837)            
            .addSpawn(939, 819)            
            .addSpawn(609, 930)
            .addSpawn(760, 922)
            .addSpawn(936, 928)
            .setSettings({                              
                scoreLimit: 3,
                weaponChangeDelay: 90,            
            })
            .setTrainingSettings({  
                scoreLimit: 8,
                timeLimit: 5,            
                gameMode: "lms",
                weaponChangeDelay: 90,
            })       
        ],     
  
    },
palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,136,212,252,152,220,252,164,232,252,176,236,252,188,252,252,212,252,252,224,252,252,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,136,212,252,136,212,252,152,220,252,164,232,252,176,236,252,188,252,252,212,252,252,224,252,252,28,28,28,32,32,32,32,32,32,36,36,36,0,0,0,0,0,0,0,0,0,0,0,0,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,252,252,252,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,252,252,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
// onGameTick: (g) => {      
//     let players = getActivePlayers();        
//     for (let p of players) {
//         if (p.x==null || p.y==null) continue
//         if (p.x<5 || p.x>1490 || p.y<80 || p.y>1380) {
//             console.log(p.x, p.y)
//             window.WLROOM.setPlayerHealth(p.id, 0)
//         }
//     }
//  },
objects: (() => {      
    let obj = []
    for (let x = 0; x<=1700; x+=50)  { 
        if (x<=1500) {
            obj.push({type:"blacklineofdeath", x:x, y:1429})
            obj.push({type:"blacklineofdeath", x:x, y:80})
        }
        obj.push({type:"vertical_line", x:1, y:x})
        obj.push({type:"vertical_line", x:1499, y:x})
     }
        return obj
     })(),

 mods: [             
            "https://sylvodsds.gitlab.io/webliero-extended-mods/%E7%A5%9E%E9%A2%A8/dirts.zip",           
            "https://sylvodsds.gitlab.io/webliero-extended-mods/dsds/_blacklineofdeath.zip", 
            new LineWobject('vertical_line', 0, 0, 50, 1, 50, 666, 10)           
        ],    
    tags: ['main', 'classic', 'dirts']
})

mapSettings.set("https://webliero.gitlab.io/webliero-maps/kangaroo/Remixes/dirt_platforms_alt.png", 
    {
        modes: { duel: [
            (new DuelSettings())                       
            .addSpawn(680, 885)            
            .addSpawn(873, 888)            
            .addSpawn(865, 1020)            
            .addSpawn(678, 1018)
            .addSpawn(767, 955)
            .setSettings({                              
                scoreLimit: 3,
                weaponChangeDelay: 90,            
            })
            .setTrainingSettings({  
                scoreLimit: 8,
                timeLimit: 5,            
                gameMode: "lms",
                weaponChangeDelay: 90,
            })       
        ],     
  
    },
palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,136,212,252,152,220,252,164,232,252,176,236,252,188,252,252,212,252,252,224,252,252,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,136,212,252,136,212,252,152,220,252,164,232,252,176,236,252,188,252,252,212,252,252,224,252,252,28,28,28,32,32,32,32,32,32,36,36,36,0,0,0,0,0,0,0,0,0,0,0,0,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,252,252,252,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,252,252,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],

objects: (() => {      
    let obj = []
    for (let x = 0; x<=1700; x+=50)  { 
        if (x<=1500) {
            obj.push({type:"blacklineofdeath", x:x, y:1600})
            obj.push({type:"blacklineofdeath", x:x, y:80})
        }
        obj.push({type:"vertical_line", x:1, y:x})
        obj.push({type:"vertical_line", x:1499, y:x})
     }
        // obj.push({ type:'disappearing_dirt_rectangle',x:770,y:1127}) // disappearing dirt rectangle,
        // obj.push({ type:'renewable_block_rails',x:745,y:960}) // renewable dirt,
        // obj.push({ type:'renewable_block_rails',x:745,y:975}) // renewable dirt,
        obj.push({ type:'renewable_block_rails',x:760,y:960}) // renewable dirt,
        obj.push({ type:'renewable_block_rails',x:760,y:975}) // renewable dirt,
        obj.push({ type:'renewable_block_rails',x:775,y:960}) // renewable dirt,
        obj.push({ type:'renewable_block_rails',x:775,y:975}) // renewable dirt,
        return obj
     })(),

 mods: [             
            "https://sylvodsds.gitlab.io/webliero-extended-mods/%E7%A5%9E%E9%A2%A8/dirts.zip",           
            "https://sylvodsds.gitlab.io/webliero-extended-mods/dsds/_blacklineofdeath.zip",
            "kangur/_dirt_blocks",
            new LineWobject('vertical_line', 0, 0, 50, 1, 50, 666, 10)           
        ],    
    tags: ['main', 'classic', 'dirts']
})

// // predator expand
// mapSettings.set("hellhole/Arena 2.lev", 
// {
//     modes: { ctf2: [(new CTF2Settings())
//                 .addFlagGreenSpawn(97,78)
//                 .addGreenSpawn(57,173)
//                 .addGreenSpawn(251,141)
//                 .addGreenSpawn(151,292)
//                 .addFlagBlueSpawn(911,78)
//                 .addBlueSpawn(950,173)
//                 .addBlueSpawn(756,141)
//                 .addBlueSpawn(856,292)
//             ] },
//     materials: [0,9,10,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,4,4,4,4,4,4,4,4,4,4,4,32,32,32,32,32,32,32,32,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,9,9,0,0,1,1,1,4,4,4,1,1,1,4,4,4,2,2,2,2,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,4,4,4,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,24,24,24,8,8,8,8,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,8,8,8,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
//     objects: (() => {
//         const up = WLPI/2*3
//         const down = WLPI/2
//         const right = 0
//         const left = WLPI
        
//         let obj = []

//        // obj.push({ type:64,x:2,y:2,speed:0}) // green down,       
//         obj.push({ type:'laser_green',x:175,y:303,speed:6, angle: WLPI*1.73}) // green left,          
//         obj.push({ type:'laser_green',x:260,y:46,speed:6, angle: WLPI*0.77}) // green left,          
//         obj.push({ type:'laser_green',x:421,y:303,speed:6, angle: WLPI*1.22}) // green left,          
      
//         obj.push({ type:'laser_blue',x:832,y:303,speed:6, angle: WLPI*1.27}) // green left,          
//         obj.push({ type:'laser_blue',x:747,y:46,speed:6, angle: WLPI*0.23}) // green left,          
//         obj.push({ type:'laser_blue',x:586,y:303,speed:6, angle: WLPI*1.78}) // green left,              
//         return obj
//      })(),
//     mods: [             
//             "https://www.vgm-quiz.com/dev/webliero/wledit/mods/latest/khan",
//             "kangur/_lasers"                     
//         ], 
//     palette: [72,44,36,84,52,44,112,104,52,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,84,56,24,96,64,28,108,68,32,120,76,32,128,84,36,140,88,40,148,96,44,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,244,164,164,248,92,92,244,76,76,244,60,60,244,76,76,244,92,92,244,164,164,32,32,0,32,32,16,0,36,16,0,28,0,0,16,0,12,12,0,0,12,4,0,12,4,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,188,84,12,216,96,12,236,104,16,240,120,40,240,132,56,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,32,16,44,60,32,60,80,96,112,76,128,188,76,168,188,156,192,192,252,200,108,252,236,8,144,232,252,188,244,0,84,180,0,40,124,0,0,68,0,140,144,128,180,180,180,244,240,244,232,140,116,4,4,4,4,4,4,44,28,16,76,24,12,108,56,0,140,88,28,192,136,56,220,164,84,252,212,132,252,232,124,252,248,156,252,244,252,8,36,68,32,64,80,64,68,128,92,120,156,164,232,252,72,160,224,32,88,176,0,248,0,0,188,0,96,136,64,40,100,76,188,208,64,252,104,108,252,172,176,224,196,204,204,152,160,252,0,0,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],   
//     fxs: ["mirror", "expand"],
//     tags: ['meh']
// })

// ctf no reset expand add lasers ?
mapSettings.set("hellhole/Arena 4.lev", 
{
    modes: { ctf2: [(new CTF2Settings())
        .addFlagGreenSpawn(134,520)
        .addGreenSpawn(134,520)
        .addGreenSpawn(381,429)
        .addGreenSpawn(273,612)
        .addGreenSpawn(151,229)
        .addFlagBlueSpawn(873,520)
        .addBlueSpawn(873,520)
        .addBlueSpawn(626,429)
        .addBlueSpawn(734,612)
        .addBlueSpawn(70,229)
    ]},
    materials: [0,9,10,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,4,4,4,4,4,4,4,4,4,4,4,32,32,32,32,32,32,32,32,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,9,9,0,0,1,1,1,4,4,4,1,1,1,4,4,4,2,2,2,2,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,4,4,4,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,24,24,24,8,8,8,8,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,8,8,8,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
     objects: (() => {
        const up = WLPI/2*3
        const down = WLPI/2
        const right = 0
        const left = WLPI
        
        let obj = []

        obj.push({ type:'laser_orange',x:413,y:298,speed:6, angle: down}) // orange down,       
        obj.push({ type:'laser_orange',x:594,y:401,speed:6, angle: up}) // orange up,       
        obj.push({ type:'laser_orange',x:462,y:436,speed:6, angle: right}) // orange right,       
        obj.push({ type:'laser_orange',x:545,y:263,speed:6, angle: left}) // orange left,    
        obj.push({ type:'laser_green',x:265,y:444,speed:6, angle: up}) // green up,
        obj.push({ type:'laser_green',x:265,y:556,speed:6, angle: down}) // green down,
        obj.push({ type:'laser_blue',x:742,y:444,speed:6, angle: up}) // up,                   
        obj.push({ type:'laser_blue',x:742,y:556,speed:6, angle: down}) // down,                   
        return obj
     })(),
    mods: [             
            "kangur/dtf",
            "kangur/_lasers"                     
        ],
    palette: [72,44,36,84,52,44,112,104,52,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,84,56,24,96,64,28,108,68,32,120,76,32,128,84,36,140,88,40,148,96,44,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,244,164,164,248,92,92,244,76,76,244,60,60,244,76,76,244,92,92,244,164,164,32,32,0,32,32,16,0,36,16,0,28,0,0,16,0,12,12,0,0,12,4,0,12,4,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,188,84,12,216,96,12,236,104,16,240,120,40,240,132,56,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,32,16,44,60,32,60,80,96,112,76,128,188,76,168,188,156,192,192,252,200,108,252,236,8,144,232,252,188,244,0,84,180,0,40,124,0,0,68,0,140,144,128,180,180,180,244,240,244,232,140,116,4,4,4,4,4,4,44,28,16,76,24,12,108,56,0,140,88,28,192,136,56,220,164,84,252,212,132,252,232,124,252,248,156,252,244,252,8,36,68,32,64,80,64,68,128,92,120,156,164,232,252,72,160,224,32,88,176,0,248,0,0,188,0,96,136,64,40,100,76,188,208,64,252,104,108,252,172,176,224,196,204,204,152,160,252,0,0,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
    fxs: ["expand","rotate", "expand", "rotate", "rotate", "rotate"],
    tags: ['crazy', 'big', 'dtfmod']
})

// predator expand
mapSettings.set("hellhole/Chip N Dale 1.lev", 
{
    modes:  { 
        pred: [
            (new PredSettings).addLemonSpawn(504,148)
                .setSettings(defWarExSettings)
        ],
        dm: [
            (new SimpleSettings())
                .setSettings(defWarDeathmatchSettings)
        ],
        lms: [
            (new SimpleSettings())
                .setSettings(defWarLMSSettings)
        ],
    },
    materials: [0,9,10,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,4,4,4,4,4,4,4,4,4,4,4,32,32,32,32,32,32,32,32,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,9,9,0,0,1,1,1,4,4,4,1,1,1,4,4,4,2,2,2,2,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,4,4,4,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,24,24,24,8,8,8,8,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,8,8,8,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    objects: [
        {type:"tank_spawner", x:0, y:0}, 
    ],
    mods: [             
        warmodExturl,    
        {from: warmodurl, options: ["merge_missing"] },                              
        ],   
    palette: [72,44,36,84,52,44,112,104,52,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,84,56,24,96,64,28,108,68,32,120,76,32,128,84,36,140,88,40,148,96,44,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,244,164,164,248,92,92,244,76,76,244,60,60,244,76,76,244,92,92,244,164,164,32,32,0,32,32,16,0,36,16,0,28,0,0,16,0,12,12,0,0,12,4,0,12,4,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,188,84,12,216,96,12,236,104,16,240,120,40,240,132,56,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,32,16,44,60,32,60,80,96,112,76,128,188,76,168,188,156,192,192,252,200,108,252,236,8,144,232,252,188,244,0,84,180,0,40,124,0,0,68,0,140,144,128,180,180,180,244,240,244,232,140,116,4,4,4,4,4,4,44,28,16,76,24,12,108,56,0,140,88,28,192,136,56,220,164,84,252,212,132,252,232,124,252,248,156,252,244,252,8,36,68,32,64,80,64,68,128,92,120,156,164,232,252,72,160,224,32,88,176,0,248,0,0,188,0,96,136,64,40,100,76,188,208,64,252,104,108,252,172,176,224,196,204,204,152,160,252,0,0,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228], 
    fxs: ["mirror", "expand"],
    tags: ['main', 'classic', 'war']
})

mapSettings.set("viero/Hills.lev", 
{
    modes: { ctf: [(new CTFSettings())
        .addFlagGreenSpawn(101,252)
        .addGreenSpawn(90,159)
        .addGreenSpawn(63,27)        
        .addGreenSpawn(205,137)        
        .addGreenSpawn(151,341)        
        .addGreenSpawn(127,217)        
        .addFlagBlueSpawn(906,252)
        .addBlueSpawn(883,159)
        .addBlueSpawn(806,124)
        .addBlueSpawn(924,155)
        .addBlueSpawn(952,24)
        .addBlueSpawn(857,342)
        .setSettings(defWarFlagsSettings)
    ]}, 
    objects: [
        {type:"tank_spawner", x:0, y:0}, 
    ],
    mods: [             
        warmodExturl,    
        {from: warmodurl, options: ["merge_missing"] },                                    
        ],
    palette: [72,44,36,84,52,44,112,104,52,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,84,56,24,96,64,28,108,68,32,120,76,32,128,84,36,140,88,40,148,96,44,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,244,164,164,248,92,92,244,76,76,244,60,60,244,76,76,244,92,92,244,164,164,32,32,0,32,32,16,0,36,16,0,28,0,0,16,0,12,12,0,0,12,4,0,12,4,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,188,84,12,216,96,12,236,104,16,240,120,40,240,132,56,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,32,16,44,60,32,60,80,96,112,76,128,188,76,168,188,156,192,192,252,200,108,252,236,8,144,232,252,188,244,0,84,180,0,40,124,0,0,68,0,140,144,128,180,180,180,244,240,244,232,140,116,4,4,4,4,4,4,44,28,16,76,24,12,108,56,0,140,88,28,192,136,56,220,164,84,252,212,132,252,232,124,252,248,156,252,244,252,8,36,68,32,64,80,64,68,128,92,120,156,164,232,252,72,160,224,32,88,176,0,248,0,0,188,0,96,136,64,40,100,76,188,208,64,252,104,108,252,172,176,224,196,204,204,152,160,252,0,0,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],  
    fxs: ["expand"],
    tags: ['main', 'classic', 'war']
})

// mirror expand ctf no reset
mapSettings.set("hellhole/Dino City 2.lev", 
{
    modes: { ctf: [(new CTFSettings())
        .addFlagGreenSpawn(46,161)
        .addGreenSpawn(49,125)
        .addGreenSpawn(44,254)        
        .addFlagBlueSpawn(961,161)
        .addBlueSpawn(958,125)
        .addBlueSpawn(963,254)
        .setSettings(defWarFlagsSettings)
    ]},
    materials: [0,9,10,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,4,4,4,4,4,4,4,4,4,4,4,32,32,32,32,32,32,32,32,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,9,9,0,0,1,1,1,4,4,4,1,1,1,4,4,4,2,2,2,2,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,4,4,4,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,24,24,24,8,8,8,8,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,8,8,8,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    objects: [
        {type:"tank_spawner", x:0, y:0}, 
    ],
    mods: [             
        warmodExturl,    
        {from: warmodurl, options: ["merge_missing"] },                                  
        ],
    palette: [72,44,36,84,52,44,112,104,52,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,84,56,24,96,64,28,108,68,32,120,76,32,128,84,36,140,88,40,148,96,44,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,244,164,164,248,92,92,244,76,76,244,60,60,244,76,76,244,92,92,244,164,164,32,32,0,32,32,16,0,36,16,0,28,0,0,16,0,12,12,0,0,12,4,0,12,4,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,188,84,12,216,96,12,236,104,16,240,120,40,240,132,56,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,32,16,44,60,32,60,80,96,112,76,128,188,76,168,188,156,192,192,252,200,108,252,236,8,144,232,252,188,244,0,84,180,0,40,124,0,0,68,0,140,144,128,180,180,180,244,240,244,232,140,116,4,4,4,4,4,4,44,28,16,76,24,12,108,56,0,140,88,28,192,136,56,220,164,84,252,212,132,252,232,124,252,248,156,252,244,252,8,36,68,32,64,80,64,68,128,92,120,156,164,232,252,72,160,224,32,88,176,0,248,0,0,188,0,96,136,64,40,100,76,188,208,64,252,104,108,252,172,176,224,196,204,204,152,160,252,0,0,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],  
    fxs: ["mirror", "expand"],
    tags: ['classic', 'war']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/dsds/estatua_ctf_remix.png", 
{
    modes: { dtf: [(DTFSettings.fromJson({"spawns":[[[970,191+167]],[[918,20+167],[905,177+167],[914,103+167],[886,259+167]],[[86,19+167],[101,177+167],[121,257+167],[91,99+167]]]}))
        .setSettings(defWarFlagsSettings)
    ]},
    objects: [
        {type:"tank_spawner", x:0, y:0}, 
    ],   
    mods: [             
        warmodExturl,    
        {from: warmodurl, options: ["merge_missing"] },                                              
        ],        
    palette: true,
    backgroundLayer: "true",
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.BG,..._range(188,211))),
    tags: ['classic', 'war']
})

mapSettings.set("Larcelo/WOJNA1.lev", 
{
    modes: { 
        ctf: [
        (CTFSettings.fromJson({"spawns":[[[69,39]],[[114,53],[73,6],[33,36]],[[973,35],[893,53],[935,5]],[[935,37]]]}))
             .setSettings(defWarFlagsSettings),
        (CTFSettings.fromJson({"spawns":[[[69,39],[35,234]],[[114,53],[73,6],[127,207],[35,246]],[[879,210],[893,53],[935,5],[968,253]],[[935,37],[968,239]]]}))
              .setSettings(defWarFlagsSettings)
    ]},
    objects: [
        {type:"tank_spawner", x:0, y:0}, 
    ],   
    mods: [             
        warmodExturl,    
        {from: warmodurl, options: ["merge_missing"] },                            
        ],    
    fxs: ["expand"],
    tags: ['classic', 'war']
})

// predator
mapSettings.set("hellhole/Super Mario Bros 3.lev", 
{
    modes: { pred: [(new PredSettings).addLemonSpawn(205,173).setSettings(defDTFPredSettings)] },
    materials: [0,9,10,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,4,4,4,4,4,4,4,4,4,4,4,32,32,32,32,32,32,32,32,32,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,9,9,0,0,1,1,1,4,4,4,1,1,1,4,4,4,2,2,2,2,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,4,4,4,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,24,24,24,8,8,8,8,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,8,8,8,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    mods: [ 
        "kangur/dtf",
    ],
    palette: [72,44,36,84,52,44,112,104,52,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,84,56,24,96,64,28,108,68,32,120,76,32,128,84,36,140,88,40,148,96,44,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,244,164,164,248,92,92,244,76,76,244,60,60,244,76,76,244,92,92,244,164,164,32,32,0,32,32,16,0,36,16,0,28,0,0,16,0,12,12,0,0,12,4,0,12,4,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,188,84,12,216,96,12,236,104,16,240,120,40,240,132,56,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,32,16,44,60,32,60,80,96,112,76,128,188,76,168,188,156,192,192,252,200,108,252,236,8,144,232,252,188,244,0,84,180,0,40,124,0,0,68,0,140,144,128,180,180,180,244,240,244,232,140,116,4,4,4,4,4,4,44,28,16,76,24,12,108,56,0,140,88,28,192,136,56,220,164,84,252,212,132,252,232,124,252,248,156,252,244,252,8,36,68,32,64,80,64,68,128,92,120,156,164,232,252,72,160,224,32,88,176,0,248,0,0,188,0,96,136,64,40,100,76,188,208,64,252,104,108,252,172,176,224,196,204,204,152,160,252,0,0,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],    
    tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-builder-maps/maps/Sydney_s_stupid_singer_(blue_and_green).png", 
{
    modes: { ctf2: [(new CTF2Settings())
        .addFlagGreenSpawn(89, 163)
        .addGreenSpawn(47, 127)
        .addGreenSpawn(147, 138)
        .addGreenSpawn(240, 181)

        .addFlagBlueSpawn(1419, 163)
        .addBlueSpawn(1461, 127)
        .addBlueSpawn(1361, 138)
        .addBlueSpawn(1268, 181)
       
        .setSettings({
            forceRandomizeWeapons: true,
        })
    ] },  
    mods: [             
            "https://sylvodsds.gitlab.io/webliero-extended-mods/dsds/sub.zip",                       
        ],
    onPlayerBeforeSpawn: (player) => { // to implement
       // window.WLROOM.setPlayerWeapons(player, ["PROPELLER"]);
    },    
    onPlayerSpawn: (player) => {
        const num_weapons =  16
        let ret = [2]           
        while(ret.length!=5) {
            let wid = Math.round(Math.random()*num_weapons)
            if (!ret.includes(wid)) ret.push(wid)
        }
     
        window.WLROOM.setPlayerWeapons(player, ret.concat([666,666,666,666,666]));
     },
     palette: [0,0,0,60,8,0,60,32,0,116,100,80,0,96,0,12,124,12,204,36,36,120,120,120,36,36,36,36,36,204,36,168,36,36,204,204,72,16,0,80,20,0,88,24,0,96,32,0,104,36,0,112,40,0,124,48,0,28,28,28,36,36,36,44,44,44,52,52,52,60,60,60,68,68,68,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,8,8,88,32,32,144,56,56,200,96,96,196,136,136,196,60,60,60,96,96,96,132,132,132,168,168,168,0,48,0,0,84,0,12,124,12,64,140,64,116,164,116,60,60,60,96,96,96,132,132,132,168,168,168,120,120,200,160,160,196,204,204,196,12,32,0,40,64,0,68,96,0,100,128,0,72,24,4,108,72,40,148,120,76,188,168,112,108,72,40,148,120,76,188,168,112,152,52,0,112,32,0,24,24,24,60,60,60,96,96,96,132,132,132,168,168,168,204,204,204,148,148,148,96,96,96,104,12,0,132,52,0,160,92,0,188,132,0,120,36,0,168,0,0,140,0,0,116,0,0,152,0,0,124,0,0,168,0,0,140,0,0,116,0,0,168,0,0,140,0,0,116,0,0,32,32,144,56,56,200,96,96,196,32,32,144,56,56,200,96,96,196,100,88,0,88,76,0,76,64,0,68,52,0,84,44,0,112,84,24,140,128,56,168,172,88,200,200,140,196,196,204,204,0,0,200,0,0,200,4,0,200,32,0,200,60,0,200,88,0,200,116,0,200,144,0,200,172,0,196,184,12,196,196,32,196,196,64,196,196,100,192,192,132,192,192,164,192,192,200,0,84,0,12,124,12,64,140,64,0,84,0,12,124,12,64,140,64,200,12,12,196,76,76,196,140,140,56,56,200,96,96,196,136,136,196,96,96,196,12,124,12,64,140,64,116,164,116,64,140,64,100,88,0,88,68,0,76,48,0,64,28,0,52,8,0,40,0,0,56,56,88,96,96,144,140,140,200,152,152,196,172,172,196,0,64,0,0,84,0,4,104,4,12,124,12,204,152,152,196,116,116,200,44,44,196,28,28,196,12,12,196,28,28,196,44,44,196,116,116,0,0,68,0,0,68,0,0,68,0,0,68,12,0,0,16,0,0,20,0,0,24,0,0,204,204,204,172,172,172,140,140,140,108,108,108,76,76,76,108,108,108,140,140,140,172,172,172,60,28,0,76,36,0,92,48,8,108,60,16,124,72,24,0,0,0,0,0,0,32,28,0,72,68,0,112,104,0,152,144,0,196,184,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,204,0,0,204,0,0,204,24,0,204,60,0,204,96,0,204,132,0,204,168,0,204,204,0,120,192,0,36,184,0,0,176,0,204,0,0,184,0,0,168,0,0,148,0,20,132,0,40,112,0,64,96,0,88,76,0,108,60,4,132,40,12,156,24,20,180],
     tags: ['sub']   
})

mapSettings.set("https://webliero.gitlab.io/webliero-maps/kangaroo/Remixes/manson4e.png", 
    {
        modes: { haz: [
            (new HazSettings())
            .setZone(102, 74, 398, 255)
            .addSpawn(150, 337)
            .setSettings(defDTFHazSettings),
        ],
},

objects: (() => {
        const up = WLPI/2*3
        const down = WLPI/2
        const right = 0
        const left = WLPI
        
        let obj = []

        obj.push({wlx:"platform", form:"platform_classic", id: 81, x:250, y:91, angle: right, speed:0.2, steps:
            [    
               ["x","more_than",330,down], 
               ["y","more_than",207,left],
               ["x","lower_than",173,up], 
               ["y","lower_than",92,right],            
            ]
        })

        obj.push({ type:'fan',x:50,y:275,speed:6, angle: down}) // fan,
        obj.push({ type:'torch',x:70,y:347,speed:5, angle: up}) // torch,
        obj.push({ type:'death_vortex',x:25,y:55}) // death vortex,
        obj.push({ type:'laser_orange',x:422,y:78,speed:6, angle: right}) // orange right
        obj.push({ type:'laser_orange_alt',x:402,y:56,speed:6, angle: up}) // orange up
        obj.push({ type:'disappearing_platform',x:464,y:300}) // disappearing platform,
        obj.push({ type:'solid_square',x:416,y:300}) // solid square,
        obj.push({ type:'swamp',x:75,y:22}) // swamp,
        obj.push({ type:'door_left',x:416,y:341}) // door left 1,
        obj.push({ type:'door_left',x:124,y:341}) // door left 2,
        obj.push({wlx:"teleport",x:20,y:300,dist:2,toX:434,toY:21})  // teleport 1,
        obj.push({wlx:"teleport",x:482,y:20,dist:2,toX:460,toY:345})  // teleport 2,
        obj.push({wlx:"teleport",x:482,y:250,dist:2,toX:75,toY:22})  // teleport 3,
        //obj.push({wlx:"teleport",x:25,y:25,dist:2,toX:182,toY:50})
        obj.push({ type:'manson_teleport',x:25,y:25}) // special teleport 4,
        obj.push({wlx:"teleport",x:320,y:259,dist:2,toX:150,toY:337})  // teleport 5,
        obj.push({ type:'sudden_death',x:252,y:175}) // sudden death,
        obj.push({ type:'peacemaker',x:48,y:311}) // peacemaker 1,
        obj.push({ type:'peacemaker',x:456,y:311}) // peacemaker 2,
        obj.push({ type:'peacemaker',x:455,y:38}) // peacemaker 3,
        obj.push({ type:'peacemaker',x:45,y:38}) // peacemaker 4,
        obj.push({wlx:"directionnal",wobject:'dtf_solar_flare',x:259,y:275,freq:8,angle:WLPI*1.5,dist:1000,speed:4})
  
        return obj
     })(),

 mods: [ 
            "kangur/dtf",
            {
                from: "kangur/dtf",
                wObjects:{3:'dtf_solar_flare'}
            },
            "kangur/_lasers",
            "kangur/_platforms",
            "kangur/_fans",
            "kangur/_teleport",
            "kangur/_others",
            "kangur/_doors",
            "kangur/_special",
        ],    
        tags: ['classic', 'crazy', 'dtfmod']  
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/dsds/ztn3tourney_remix_dumbed.png", {
    modes: {pred: [(new PredSettings).addLemonSpawn(571,175).setSettings(defDTFPredSettings)],},
    objects: [
        {type:"torch_flame", x:180, y:157},
        {type:"torch_flame", x:262, y:157},
        {type:"torch_flame", x:306, y:157},
        {type:"torch_flame", x:346, y:157}, //-1
        {type:"torch_flame", x:530, y:145},
        {type:"torch_flame", x:611, y:145},
        {type:"torch_flame", x:324, y:373},
        {type:"torch_flame", x:342, y:373},
    ]
    ,
    mods: [ 
        "kangur/dtf",
        "dsds/_flame",
    ],
    palette: true,
    backgroundLayer: "true",
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(192,199))).map(replaceMatIndexBy(MATERIAL.BG,..._range(200,218))).map(replaceMatIndexBy(MATERIAL.BG,..._range(219,222))),
    tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set("hellhole/maskheim.lev", {
    modes: {
        racing: [
            (new LieroKartSettings())
                .addNumberLaps(5)
                .addSpawn(237,304)                
                .addStartZone(202, 285, 271, 320)
                .addStep(54, 260, 104, 338)                  
                .addStep(11, 132, 84, 197)                  
                .addStep(122, 15, 171, 91)                  
                .addStep(343, 2, 387, 72)                  
                .addStep(413, 178, 502, 241)                  
                .addStep(354, 280, 424, 349)                  
                .addStep(173, 275, 201, 326)//arrival
                .addWeapBonus(58,115)
                .addWeapBonus(72,123)
                .addWeapBonus(86,131)
                .addWeapBonus(255,62)
                .addWeapBonus(429,106)
                .addWeapBonus(443,100)
                .addWeapBonus(456,93)           
// no wrong way?

// laps annouce                                                                                                                 
                .setSettings(
                    {                        
                        timeLimit: 10,
                        maxDuplicateWeapons: 3,
                        loadingTimes: 666,
                        weapons: {
                            only: ["NO WEAPON"]
                        }
                    }
                )
        ],
    },
    objects: [
        {type:"just_a_line_for_fun", x:40, y:125},
        {type:"start_line", x:202, y:285},
        {type:"no_go_line", x:262, y:285},        
    ],      
    mods: [   
       "kangur/dtf",
       { from: "神風/speedrun", weapons: [0, 2]}, 
       new LineWobject('just_a_line_for_fun', 157, 42, 21, 1),      
       new LineWobject('start_line', 50, 0, 36, 10),      
       new LineWobject('no_go_line', 92, 0, 36, 10),
       "dsds/_bonus"   
    ],
    //fxs: ["replacecolor#97#160", "replacecolor#100#160", "replacecolor#18#29"],
    colorAnim: [129,131,133,136,152,159,168,171],
   // palette: true,
    //materials: defaultMaterials.map(noD).map(replaceMatIndexBy(MATERIAL.BG,157))   .map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,200))),
    tags: ['main', 'classic', 'speedrun']
});

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/dsds/jetpack/racing-track.png", {
    modes: {
        racing: [
            (new RacingSettings())
                .addNumberLaps(3)
                .addSpawn(508,216)                
                .addStartZone(318, 83, 709, 358)
                .addStep(1203, 60, 1439, 483)     
                .addStep(1909, 114, 2156, 526)     
                .addStep(2407, 404, 2785, 628)     
                .addStep(2252, 1017, 2438, 1391)     
                .addStep(1700, 601, 1902, 1037)     
                .addStep(920, 968, 1126, 1378)     
                .addStep(255, 964, 532, 1358)     
                .addStep(60, 518, 357, 746)     
                .addStep(710, 83, 801, 358) //arrival

                .setSettings(
                    {                        
                        timeLimit: 10,
                        maxDuplicateWeapons: 3,
                        loadingTimes: 0,
                        weapons: {
                            only: ["JETPACK", "ACCELERATE", "NO WEAPON"]
                        }
                    }
                )
        ],
    }, 
    onPlayerSpawn: (player) => {
        //forces weapon 1 to be "accelerate" and weapon 2 to be "jetpack"        
        window.WLROOM.setPlayerWeapons(player, [18,17,19,19,19,666,666,1,1,1]);
    },
    mods: [   
        {
            name: "tank_mod",
            json:"https://webliero.gitlab.io/webliero-mods/tank_mod/mod.json5",
            sprites:"https://webliero.gitlab.io/webliero-mods/tank_mod/sprites.wlsprt"
        },
       { from: "神風/speedrun", weapons: [0, 2]},       
    ],
    colorAnim: false,
    palette: true,
    materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(188,202))),
    tags: ['main', 'classic', 'speedrun']
});

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/dsds/jetpack/roundflyingmap-bigger-remix.png", {
    modes: {
        racing: [
            (new RacingSettings())
                .addNumberLaps(3)
                .addSpawn(1116,360)                
                .addStartZone(972, 30, 1278, 391)
                .addStep(1616, 270, 1972, 660)
                .addStep(1862, 962, 2249, 1313)
                .addStep(1479, 1664, 1986, 2061)
                .addStep(933, 1863, 1356, 2246)
                .addStep(248, 1627, 696, 2084)
                .addStep(3, 996, 405, 1442)
                .addStep(229, 315, 657, 718)
                .addStep(1275, 32, 1475, 451) //arrival

                .setSettings(
                    {                        
                        timeLimit: 10,
                        maxDuplicateWeapons: 3,
                        loadingTimes: 0,
                        weapons: {
                            only: ["JETPACK", "ACCELERATE", "NO WEAPON"]
                        }
                    }
                )
        ],
    },
    objects: [
        {type:"ventilo", x:1670, y:1161},
        {type:"ventilo", x:1754, y:1161},
        {type:"ventilo", x:1833, y:1161},
    ],
    onPlayerSpawn: (player) => {
        //forces weapon 1 to be "accelerate" and weapon 2 to be "jetpack"        
        window.WLROOM.setPlayerWeapons(player, [18,17,19,19,19,666,666,1,1,1]);
    },
    mods: [   
        {
            name: "tank_mod",
            json:"https://webliero.gitlab.io/webliero-mods/tank_mod/mod.json5",
            sprites:"https://webliero.gitlab.io/webliero-mods/tank_mod/sprites.wlsprt"
        },
       { from: "神風/speedrun", weapons: [0, 2]},
       "dsds/_ventilo"
    ],
    colorAnim: false,
    palette: true,
    materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(201,225))),
    tags: ['main', 'classic', 'speedrun']
});

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/dsds/jetpack/roundflyingmap-bigger-remix.png#alt", {
    modes: {
        racing: [
            (new RacingSettings())
                .addNumberLaps(3)
                .addSpawn(1116,360)                
                .addStartZone(972, 30, 1278, 391)
                .addStep(1616, 270, 1972, 660)
                .addStep(1862, 962, 2249, 1313)
                .addStep(1479, 1664, 1986, 2061)
                .addStep(933, 1863, 1356, 2246)
                .addStep(248, 1627, 696, 2084)
                .addStep(3, 996, 405, 1442)
                .addStep(229, 315, 657, 718)
                .addStep(1275, 32, 1475, 451) //arrival

                .setSettings(
                    {                        
                        timeLimit: 10,                     
                    }
                )
        ],
    },
    objects: [
        {type:"ventilo", x:1670, y:1161},
        {type:"ventilo", x:1754, y:1161},
        {type:"ventilo", x:1833, y:1161},
    ],
    onPlayerSpawn: (player) => {
            //forces weapon 1 to be "accelerate" and weapon 2 to be "fan"
            window.WLROOM.setPlayerWeapons(player, [0,1,2,3,4,666,666,1,1,1]);
    },
    mods: [   
       { from: "神風/speedrun"},
       "dsds/_ventilo"
    ],
    colorAnim: false,
    palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,61,173,61,252,84,84,168,168,168,85,85,85,84,84,252,84,216,84,84,252,252,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,72,64,61,64,66,72,73,77,84,81,76,70,88,76,72,91,85,79,96,84,80,93,86,75,82,89,99,93,89,71,100,87,83,56,56,136,81,81,193,105,105,249,145,145,245,185,185,245,110,110,110,145,145,145,181,181,181,217,217,217,32,96,32,45,133,45,62,174,62,113,189,113,165,213,165,111,111,111,146,146,146,182,182,182,218,218,218,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,157,121,89,197,169,125,237,217,161,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,147,147,147,180,180,180,216,216,216,253,253,253,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,217,1,1,189,1,1,165,1,1,200,0,0,172,0,0,218,2,2,190,2,2,166,2,2,216,0,0,188,0,0,110,27,25,82,82,194,106,106,250,146,146,246,80,80,192,107,107,251,147,147,247,149,137,1,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,253,1,1,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,245,233,61,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,46,134,46,63,175,63,114,190,114,47,135,47,64,176,64,115,191,115,248,60,60,244,124,124,244,188,188,104,104,248,148,148,248,184,184,244,144,144,244,65,177,65,116,192,116,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,89,41,1,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,245,165,165,248,92,92,245,77,77,244,60,60,244,76,76,244,92,92,244,164,164,84,40,0,88,40,0,92,44,0,96,48,0,60,28,0,64,28,0,68,32,0,72,36,0,252,252,252,221,221,221,189,189,189,158,158,158,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,1,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,1,0,44,37,32,102,96,86,103,97,97,111,99,94,118,111,103,110,115,105,164,106,49,120,124,114,122,124,109,121,126,123,135,128,76,150,153,145,0,0,1,0,6,2,5,7,7,4,16,3,5,23,7,6,35,10,1,65,10,5,43,12,4,54,12,9,31,14,22,51,27,3,96,34,2,120,33,0,208,37,5,159,48,46,48,46,22,122,53,51,57,55,55,66,58,74,80,76,6,244,86,72,168,97,131,209,208,136,250,212,201,203,201,1,3,2,4,0,0,0,4,0,0,0,4,4,1,0,4,1,1,1,4,1,1,1,4,254,2,2,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
    materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(201,225))),
    tags: ['main', 'classic', 'speedrun']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/神風/speedruns/testrun.png", {
    modes: {
        racing: [
            (new RacingSettings())
                .addNumberLaps(1)
                .addSpawn(40,54)
                .addFinalZone(0, 1571, 85, 1599)
                .addStartLine(165, 0, 165, 99)
                .setSettings(
                    {                        
                        timeLimit: 5
                    }
                )
        ],
    },
    onPlayerSpawn: (player) => {
        //forces weapon 1 to be "accelerate" and weapon 2 to be "fan"
        window.WLROOM.setPlayerWeapons(player, [0,1,2,3,4,666,666,1,1,1]);
    },
    objects: [
        {type:"start_line", x:165, y:0},
        {type:"end_line", x:85, y:1571},
    ],
    mods: [ 
        "神風/speedrun",
        new LineWobject('start_line', 50,  0,  100,  17), //color, to X, to Y, line width
        new LineWobject('end_line', 104, 0, 29, 17),
    ],
    colorAnim: false,
    tags: ['classic', 'speedrun']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/dsds/speedruns/sprint2_shadowed.png", {
    modes: {
        racing: [
            (new RacingSettings())
                .addNumberLaps(1)
                .addSpawn(88,2989)
                .addFinalLine(103, 4, 103, 23)
                .addStartLine(60, 2946, 111, 2946)
                .setSettings(
                    {                        
                        timeLimit: 10
                    }
                )
        ],
    },
    onPlayerSpawn: (player) => {
        //forces weapon 1 to be "accelerate" and weapon 2 to be "fan"
        window.WLROOM.setPlayerWeapons(player, [0,1,2,3,4,666,666,1,1,1]);
    },
    objects: [
        {type:"start_line", x:60, y:2942},
        {type:"end_line", x:103, y:4},
    ],
    mods: [ 
        "神風/speedrun",
        new LineWobject('start_line', 50, 52, 0, 9),
        new LineWobject('end_line', 104, 0, 20, 1),
    ],
    colorAnim: false,
    tags: ['main', 'classic', 'speedrun']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/神風/speedruns/sprint1.png", {
    modes: {
        racing: [
            (new RacingSettings())
                .addNumberLaps(1)
                .addSpawn(32,77)
                .addFinalZone(0, 0, 67, 44)
                .addStartLine(67, 54, 67, 99)
                .setSettings(
                    {                        
                        timeLimit: 5
                    }
                )
        ],
    },
    onPlayerSpawn: (player) => {
        //forces weapon 1 to be "accelerate" and weapon 2 to be "fan"
        window.WLROOM.setPlayerWeapons(player, [0,1,2,3,4,666,666,1,1,1]);
    },
    objects: [
        {type:"start_line", x:67, y:54},
        {type:"end_line", x:67, y:0},
    ],    
    mods: [ 
        "神風/speedrun",
        new LineWobject('start_line', 50, 0, 46, 17),
        new LineWobject('end_line', 104, 0, 45, 17),
    ],
    tags: ['classic', 'speedrun']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/flag/truth-flag.png", {
    "modes": {
        "ctf": [
            CTFSettings.fromJson({
                "spawns": [
                    [
                        [364,898],
                        [327,978],
                        [235,1001]
                    ],
                    [
                        [267,330]
                    ],
                    [
                        [1012,329]
                    ],
                    [
                        [915,898],
                        [951,977],
                        [1043,1001]
                    ]
                ]
            })
        ]
    },
    "mods": [
        "kangur/dtf",
        "dsds/_teleport_16x16"
    ],
    "objects": [
        {"wlx":"teleport","x":376,"y":288,"dist":2,"toX":236,"toY":873},
        {"wlx":"teleport","x":376,"y":320,"dist":2,"toX":363,"toY":1041},
        {"wlx":"teleport","x":376,"y":352,"dist":2,"toX":267,"toY":1129},
        {"wlx":"teleport","x":320,"y":237,"dist":2,"toX":555,"toY":794},
        {"wlx":"teleport","x":320,"y":401,"dist":2,"toX":546,"toY":1129},
        {"wlx":"teleport","x":320,"y":147,"dist":2,"toX":724,"toY":794},
        {"wlx":"teleport","x":320,"y":492,"dist":2,"toX":731,"toY":1129},
        {"wlx":"teleport","x":904,"y":288,"dist":2,"toX":1044,"toY":873},
        {"wlx":"teleport","x":904,"y":320,"dist":2,"toX":917,"toY":1041},
        {"wlx":"teleport","x":904,"y":352,"dist":2,"toX":1013,"toY":1129},
        {"wlx":"teleport","x":960,"y":237,"dist":2,"toX":725,"toY":794},
        {"wlx":"teleport","x":960,"y":401,"dist":2,"toX":734,"toY":1129},
        {"wlx":"teleport","x":960,"y":147,"dist":2,"toX":556,"toY":794},
        {"wlx":"teleport","x":960,"y":492,"dist":2,"toX":549,"toY":1129}
    ],
    "tags": ['classic', 'big', 'dtfmod']
});

var lootboxRepop = (args) => { 
    //console.log("on onExplode", JSON.stringify(args))
   garbageTimeouts.push(setTimeout(() => createObject({type:"lootboxw", x:args.x,y: args.y, onExplode:lootboxRepop}), 10000)); }

   
mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/flag/consequences-flag.png", {
    "modes": {
        "ctf": [
            CTFSettings.fromJson({"spawns": [[[454, 798], [310, 910], [262, 1066]], [[320, 240]], [[960, 240]], [[822, 798], [966, 910], [1014, 1066]]]}),
            CTFSettings.fromJson({"spawns": [[[822, 798], [966, 910], [1014, 1066]], [[320, 240]], [[960, 240]], [[454, 798], [310, 910], [262, 1066]]]}),
        ]
    },
    "mods": [
        "kangur/dtf",
        "dsds/_teleport_consequence_hole",
        {
            from: "kangur/_teleport",
            wObjects:{'3':'vortex_in','0':'vortex_out'}
        },
        {
            from: "https://www.vgm-quiz.com/dev/webliero/wledit/mods/behaviors0.38.zip",
          //  from: "https://www.vgm-quiz.com/dev/webliero/wledit/mods/latest/behaviors",
            wObjects:{'29':'lootboxw'}
        },
    ],
    "objects": [
        {"wlx": "teleport", "x": 264, "y": 353, "dist": 2, "toX": 588, "toY": 904},
        {type:"vortex_hole", x:264, y:353},        
        {"wlx": "teleport", "x": 320, "y": 353, "dist": 2, "toX": 308, "toY": 824},
        {type:"vortex_hole", x:320, y:353},
        {"wlx": "teleport", "x": 376, "y": 353, "dist": 2, "toX": 364, "toY": 1072},
        {type:"vortex_hole", x:376, y:353},
        {"wlx": "teleport", "x": 1016, "y": 353, "dist": 2, "toX": 916, "toY": 1072},
        {type:"vortex_hole", x:1016, y:353},
        {"wlx": "teleport", "x": 960, "y": 353, "dist": 2, "toX": 972, "toY": 824},
        {type:"vortex_hole", x:960, y:353},
        {"wlx": "teleport", "x": 904, "y": 353, "dist": 2, "toX": 692, "toY": 904},
        {type:"vortex_hole", x:904, y:353},
       {type:"lootboxw", x:292, y:340, onExplode: lootboxRepop},
       {type:"lootboxw", x:349, y:340, onExplode: lootboxRepop},
       {type:"lootboxw", x:928, y:340, onExplode: lootboxRepop},
       {type:"lootboxw", x:987, y:340, onExplode: lootboxRepop}
    ],
    "tags": ['classic', 'big', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/dsds/material_hack/cs_flag_one_fp.png", {
    modes: { ctf: [    
            (new CTFSettings())
            .setSettings({scoreLimit:9})
            .addFlagGreenSpawn(211, 270)
            .addGreenSpawn(34, 93)
            .addGreenSpawn(96, 380)
            .addGreenSpawn(209, 180)
            .addGreenSpawn(132, 194)
            .addGreenSpawn(132, 82)

            .addFlagBlueSpawn(1188, 270)
            .addBlueSpawn(1178, 183)
            .addBlueSpawn(1349, 188)
            .addBlueSpawn(1268, 204)
            .addBlueSpawn(1285, 405) 
            .addBlueSpawn(1272, 82) 
            .setOnFlagScore((worm, score) => {
                setTimeout(() => {
                    // window.WLROOM.removeObject(69)              
                    createObject({id:69, type: 'dirt_cage_green', x: 211, y: 283})
                    // window.WLROOM.removeObject(70)                
                    createObject({id:70, type: 'dirt_cage_blue', x: 1188, y: 283})
                }, 3000)
        
                if (typeof worm == 'undefined') return
                let isgreen = worm.team==1
                console.log("greenscore", worm, score)
                if (isgreen) {
                    if (score<10) {
                        window.WLROOM.removeObject(71)              
                        createObject({id:71, type: `${score}_green`, x: 253, y: 497})
                    } else {
                        //todo handle 10 +
                    }
                } else {
                    if (score<10) {
                        window.WLROOM.removeObject(72)                
                        createObject({id:72, type: `${score}_blue`, x: 1143, y: 497})
                    } else {
                        //todo handle 10 +
                    }
                }                       
            })
            .setOnGameStart((cgame) => {
               // window.WLROOM.removeObject(69)              
                createObject({id:69, type: 'dirt_cage_green', x: 211, y: 283})
               // window.WLROOM.removeObject(70)                
                createObject({id:70, type: 'dirt_cage_blue', x: 1188, y: 283})
                window.WLROOM.removeObject(71)              
                createObject({id:71, type: '0_green', x: 253, y: 497})
                window.WLROOM.removeObject(72)                
                createObject({id:72, type: '0_blue', x: 1143, y: 497})
            })
        ],},
    objects: (() => {
            const up = WLPI/2*3
            const down = WLPI/2
            const right = 0
            const left = WLPI
            
            let obj = []
             
            obj.push({ type:'laser_green_down',x:263,y:211,speed:6, angle: down}) // green left,        
            obj.push({ type:'laser_green_down',x:254,y:299,speed:6, angle: down}) // green left,        
          
    
            obj.push({ type:'laser_lightblue_down',x:699,y:157,speed:6, angle: down}) // orange down,       
    
            obj.push({ type:'laser_blue_down',x:1136,y:211,speed:6, angle: down}) // blue left,        
            obj.push({ type:'laser_blue_down',x:1145,y:299,speed:6, angle: down}) // blue left,        
    
            return obj
         })(),
    mods: [ 
        //"https://webliero.gitlab.io/webliero-mods/kami/cs_rewormed.zip",
            "kangur/dtf",
            "dsds/_cs_flag_one_lasers",
            new SolidSpriteWobject('dsds/00_sprites/dirt_cage/dirt_cage_blue'),
            new SolidSpriteWobject('dsds/00_sprites/dirt_cage/dirt_cage_green'),
            new SpriteWobject('dsds/00_sprites/numbers_40/0_green'),
            new SpriteWobject('dsds/00_sprites/numbers_40/1_green'),
            new SpriteWobject('dsds/00_sprites/numbers_40/2_green'),
            new SpriteWobject('dsds/00_sprites/numbers_40/3_green'),
            new SpriteWobject('dsds/00_sprites/numbers_40/4_green'),
            new SpriteWobject('dsds/00_sprites/numbers_40/5_green'),
            new SpriteWobject('dsds/00_sprites/numbers_40/6_green'),
            new SpriteWobject('dsds/00_sprites/numbers_40/7_green'),
            new SpriteWobject('dsds/00_sprites/numbers_40/8_green'),
            new SpriteWobject('dsds/00_sprites/numbers_40/9_green'),
            new SpriteWobject('dsds/00_sprites/numbers_40/0_blue'),
            new SpriteWobject('dsds/00_sprites/numbers_40/1_blue'),
            new SpriteWobject('dsds/00_sprites/numbers_40/2_blue'),
            new SpriteWobject('dsds/00_sprites/numbers_40/3_blue'),
            new SpriteWobject('dsds/00_sprites/numbers_40/4_blue'),
            new SpriteWobject('dsds/00_sprites/numbers_40/5_blue'),
            new SpriteWobject('dsds/00_sprites/numbers_40/6_blue'),
            new SpriteWobject('dsds/00_sprites/numbers_40/7_blue'),
            new SpriteWobject('dsds/00_sprites/numbers_40/8_blue'),
            new SpriteWobject('dsds/00_sprites/numbers_40/9_blue')
        ], 
    palette: true,
    colorAnim: [129,131,133,136,152,159, 188, 191],
    materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(192,205))),
    backgroundLayer: "true",
    tags: ['classic', 'big', 'dtfmod']
})

mapSettings.set("AlphaBlueWolf12/awbistrying_palette_shift.png", {
    modes: {pred: [(new PredSettings).addLemonSpawn(293,147).setSettings(defDTFPredSettings)],},
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,201))).map(replaceMatIndexBy(MATERIAL.ROCK,..._range(143,147))),
    colorAnim: false,
    backgroundLayer: "true",
    palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,61,173,61,252,84,84,168,168,168,85,85,85,84,84,252,84,216,84,84,252,252,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,76,76,76,84,84,84,92,92,92,100,100,100,109,109,109,116,116,116,125,125,125,132,132,132,140,140,140,180,164,154,234,208,193,56,56,136,81,81,193,105,105,249,145,145,245,185,185,245,110,110,110,145,145,145,181,181,181,217,217,217,32,96,32,45,133,45,62,174,62,113,189,113,165,213,165,111,111,111,146,146,146,182,182,182,218,218,218,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,157,121,89,197,169,125,237,217,161,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,147,147,147,180,180,180,216,216,216,253,253,253,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,217,1,1,189,1,1,165,1,1,200,0,0,172,0,0,218,2,2,190,2,2,166,2,2,216,0,0,188,0,0,164,0,0,82,82,194,106,106,250,146,146,246,30,71,154,99,123,234,170,189,253,149,137,1,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,253,1,1,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,245,233,61,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,46,134,46,63,175,63,114,190,114,47,135,47,64,176,64,115,191,115,248,60,60,244,124,124,244,188,188,104,104,248,148,148,248,184,184,244,144,144,244,65,177,65,116,192,116,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,89,41,1,30,74,149,44,77,148,58,88,161,60,91,193,60,95,227,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,245,165,165,248,92,92,245,77,77,244,60,60,244,76,76,244,92,92,244,164,164,255,171,207,255,172,207,255,171,208,255,173,207,254,205,223,254,205,224,254,206,223,255,205,223,252,252,252,221,221,221,189,189,189,158,158,158,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,1,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,64,99,178,74,106,248,72,113,233,101,121,180,98,123,200,122,141,215,122,145,238,118,147,245,114,153,249,123,155,247,135,162,249,135,166,245,144,174,255,154,181,255,53,43,44,245,1,108,246,8,115,244,31,115,96,84,80,249,42,125,80,104,164,250,57,129,253,61,138,239,78,147,255,74,143,212,87,173,230,83,162,145,132,127,123,138,193,138,147,194,169,154,187,142,160,235,149,159,223,179,157,173,155,166,217,172,177,218,212,178,160,195,185,210,185,190,231,218,194,189,206,209,248,243,236,228,4,1,0,4,1,1,1,4,1,1,1,4,254,2,2,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
    objects: [
        {type:"hide_box", x:4, y:284},
        {type:"chatbox", x:4, y:284},
    ],
    mods: [
        "dsds/_dtf_base_black_text",
        "kangur/dtf",
        { from:"https://sylvodsds.gitlab.io/webliero-extended-mods/Ophi/ABW_CHATBOX.zip",wObjects: {0:"chatbox"}},
        new LineWobject('hide_box', 143, 0, 65, 135),

    ],
    tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set("AlphaBlueWolf12/anthem-of-success-abw-ext.png", {
    modes: {
        dm: [
            (new SimpleSettings())
                .setSettings(defDTFDeathmatchSettings)
        ],
    },
    palette:true,
    backgroundLayer:"true",
    materials: defaultMaterials
        .map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
        .map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
    mods: [
        "kangur/dtf"
    ],
    colorAnim: false,
    tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/dsds/metal_1.png", {
    modes: {ctf: [
        (new CTFSettings())
        .addFlagGreenSpawn(189,478)
        .addGreenSpawn(262,351)
        .addGreenSpawn(111,274)
        .addGreenSpawn(90,470)
        .addGreenSpawn(75,423)
        .addFlagBlueSpawn(1093, 478)
        .addBlueSpawn(1149,480)
        .addBlueSpawn(1089,350)
        .addBlueSpawn(1186,281)
        .addBlueSpawn(1211,388)
        .setSettings(defWarFlagsSettings),
    ],},
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(189,222))).map(replaceMatIndexBy(MATERIAL.BG_DIRT,..._range(228,233))),
    backgroundLayer: "https://sylvodsds.gitlab.io/webliero-maps/dsds/metal_1_backgroundlayer.png",
    palette: true,
    objects: [   
        {type:"metal_fueltank", x:846, y:367},
        {type:"tank_spawner", x:0, y:0}, 
       ],
    mods: [
      //  "kangur/dtf",
      warmodExturl,    
      {from: warmodurl, options: ["merge_missing"] },
          "dsds/_metal_fueltank",      
    //        {
    //        name: "dsds/_metal_fueltank25",
    //        json: "https://7ec1-2a01-e0a-471-98e0-590b-4761-9e2e-262.eu.ngrok.io/mods/dsds/_metal_fueltank/mod.json5",
    //        sprites: "https://7ec1-2a01-e0a-471-98e0-590b-4761-9e2e-262.eu.ngrok.io/mods/dsds/_metal_fueltank/sprites.wlsprt",
    //    }       
    ],
    tags: ['main', 'classic', 'war']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-maps/dsds/U96_remixed_remixed.png", {
    modes: { haz: [
        (new HazSettings())
            .setZone(267,246,354,286)
            .addSpawn(63, 110)     
            .addSpawn(115, 110)
            .addSpawn(160, 110)
            .addSpawn(215, 110)
            .addSpawn(260, 110)
            .addSpawn(315, 110)
            .addSpawn(360, 110)
            .addSpawn(400, 110)       
            .setSettings(defWarHazSettings),         
    ]},
    palette: true,
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.BG,..._range(189,200))),
    backgroundLayer: "https://sylvodsds.gitlab.io/webliero-maps/dsds/U96_remixed_remixed_backgroundlayer.png",
    objects: (() => {
        let obj = [{type:"water", x:246, y:323}]
        
        for (let x = 256; x<504;x+=35) {
            obj.push({type:"water", x:x, y:323})
        }
        for (let x = 251; x<504;x+=35) {
            obj.push( {type:"water_bottom", x:x, y:338},)
        }                
        obj.push( {type:"u96_rocket", x:273, y:266},)
        obj.push( {type:"u96_rocket", x:283, y:266},)
        obj.push( {type:"u96_rocket", x:293, y:266},)
        obj.push( {type:"u96_heli", x:80, y:30, speed: 1, angle: 0},)     
     //   obj.push( {type:"u96_heli", x:200, y:160, speed: 1, angle: 0},)
        return obj
     })(),
    colorAnim: [129,132,133,136,152,159,168,171],
    mods: [
        //  "kangur/dtf",
        warmodExturl,    
        {from: warmodurl, options: ["merge_missing"] },
        "kangur/_water",        
        new LineWobject('water_bottom', 32, 35, 0, 12, 15, 0.5, 1),
        "dsds/_u96_rocket",
        "dsds/_u96_heli",    
    //  {
    //        name: "dsds/_u96_heli",
    //        json: "https://7ec1-2a01-e0a-471-98e0-590b-4761-9e2e-262.eu.ngrok.io/mods/dsds/_u96_heli/mod.json5",
    //        sprites: "https://7ec1-2a01-e0a-471-98e0-590b-4761-9e2e-262.eu.ngrok.io/mods/dsds/_u96_heli/sprites.wlsprt",
    //    }        
            
    ],
    tags: ['main', 'classic', 'war']
})

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/flag/monkey_business.png",
{
    modes: {
        ctf: [
            (new CTFSettings())
                .addFlagGreenSpawn(222,564)
                .addGreenSpawn(168,308)
                .addGreenSpawn(269,352)
                .addGreenSpawn(224,325)
                .addGreenSpawn(306,371)
                .addGreenSpawn(352,430)
                .addGreenSpawn(377,476)
                .addGreenSpawn(396,505)
                .addFlagBlueSpawn(1054,564)
                .addBlueSpawn(1080,294)
                .addBlueSpawn(971,342)
                .addBlueSpawn(1018,314)
                .addBlueSpawn(926,365)
                .addBlueSpawn(890,424)
                .addBlueSpawn(869,468)
                .addBlueSpawn(844,501)
        ]
    },
    palette:true,
    backgroundLayer:"true",
    materials: defaultMaterials
        .map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
        .map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
    mods: [
        "kangur/dtf"
    ],
    tags: ['big', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/zone/please_your_feet.png", {
    modes:{
        haz:[(new HazSettings())
            .setZone(620,260,700,347)
            .addSpawn(387,325)
            .addSpawn(889,327)
            .addSpawn(360,325)
            .addSpawn(921,332)
            .addSpawn(414,331)
            .addSpawn(866,335)
            .setSettings({...defDTFHazSettings, bonusDrops:"healthAndWeapons"})
        ],
        pred:[
            (new PredSettings())
                .addLemonSpawn(659,364)
                .addWormSpawn(888,324)
                .addWormSpawn(388,323)
                .addWormSpawn(608,233)
                .addWormSpawn(716,233)
                .addWormSpawn(574,380)
                .addWormSpawn(738,375)
                .addWormSpawn(660,141)
                .addWormSpawn(633,247)
                .addWormSpawn(689,247)
                .addWormSpawn(630,205)
                .addWormSpawn(696,205)
                .addWormSpawn(606,368)
                .addWormSpawn(712,368)
                .addWormSpawn(360,327)
                .addWormSpawn(918,331)
                .addWormSpawn(412,328)
                .addWormSpawn(863,335)
                .setSettings(defDTFPredSettings)
        ]
    },
    palette:true,
    backgroundLayer:"true",
    materials: defaultMaterials
        .map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
        .map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
    mods: [
        "kangur/dtf"
    ],
    tags: ['bad', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/flag/liero_exe_paint.png", {
	modes:{
		dtf: [ 
			(new DTFSettings())
			.addFlagSpawn(949, 346)
			.addFlagSpawn(982, 452)
			.addFlagSpawn(925, 94)
			.addFlagSpawn(675, 176)
			.addDefenseSpawn(916, 361)
			.addDefenseSpawn(859, 383)
			.addDefenseSpawn(839, 445)
			.addDefenseSpawn(920, 448)
			.addDefenseSpawn(570, 282)
			.addDefenseSpawn(667, 257)
			.addDefenseSpawn(561, 169)
			.addDefenseSpawn(626, 128)
			.addAttackSpawn(139, 266)
			.addAttackSpawn(314, 155)
			.addAttackSpawn(101, 351)
			.addAttackSpawn(99, 430)
			.addAttackSpawn(114, 140)
			.addAttackSpawn(137, 214)
			.addAttackSpawn(144, 367)
			.addAttackSpawn(72, 297)
		]
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf"
	],
	tags: ['bad', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/arena/nowhere_is_safe.png", {
	modes:{
		pred:[
			(new PredSettings())
				.addLemonSpawn(400,370)
				.addWormSpawn(188,400)
				.addWormSpawn(272,372)
				.addWormSpawn(440,330)
				.addWormSpawn(530,370)
				.addWormSpawn(600,440)
				.addWormSpawn(700,470)
				.addWormSpawn(812,408)
				.addWormSpawn(824,308)
                .setSettings(defDTFPredSettings)
		]
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
    objects: (() => {
        const up = WLPI/2*3
        const down = WLPI/2
        const right = 0
        const left = WLPI
        
        let obj = []
        obj.push({ wlx:"turret", wobject:'dtf_laser_pistol',x:160,y:96,freq:2,dist:1000,speed:1});
        obj.push({ wlx:"turret", wobject:'dtf_laser_pistol',x:556,y:72,freq:2,dist:1000,speed:1});
        //obj.push({ wlx:"turret", wobject:'dtf_solar_flare',x:216,y:160,freq:7,dist:1000,speed:4}); // calm down
        //obj.push({ wlx:"turret", wobject:'dtf_solar_flare',x:596,y:144,freq:9,dist:1000,speed:4}); // satan
        obj.push({ wlx:"directionnal", wobject:'dtf_doomsday',x:424,y:164,freq:1,angle:right,dist:500,speed:1});
        obj.push({ wlx:"directionnal", wobject:'dtf_doomsday',x:424,y:164,freq:1,angle:down,dist:500,speed:1});
        obj.push({ wlx:"directionnal", wobject:'dtf_hellfire',x:424,y:164,freq:1,angle:right,dist:500,speed:1});
        obj.push({ wlx:"directionnal", wobject:'dtf_hellfire',x:424,y:164,freq:1,angle:down,dist:500,speed:1});
        obj.push({ type:'laser_orange',x:460,y:138,speed:6, angle: right}) // orange right
        obj.push({ type:'laser_orange',x:374,y:214,speed:6, angle: left}) // orange left
        obj.push({ type:'laser_orange',x:234,y:46,speed:6, angle: left}) // orange left

        return obj
     })(),
	mods: [
		"kangur/dtf",
		"kangur/_lasers",
		{
			from: "kangur/dtf",
			wObjects:{3:'dtf_solar_flare',14:'dtf_doomsday',38:'dtf_laser_pistol',66:'dtf_hellfire'}
		}
	],
	tags: ['classic', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/flag/wrong_neighborhood.png", {
	modes:{
		ctf: [
			(new CTFSettings())
				.addFlagGreenSpawn(224,404)
				.addGreenSpawn(390,242)
				.addGreenSpawn(400,242)
				.addGreenSpawn(410,242)
				.addGreenSpawn(420,242)
				.addGreenSpawn(430,242)
				.addFlagBlueSpawn(720,472)
				.addBlueSpawn(590,242)
				.addBlueSpawn(600,242)
				.addBlueSpawn(610,242)
				.addBlueSpawn(620,242)
				.addBlueSpawn(630,242)
				.setSettings({
					timeLimit: 10,
					scoreLimit: 20,
					respawnDelay: 1,
					maxDuplicateWeapons: 0,
					bonusDrops:"health",
					bonusSpawnFrequency: 15,
					damageMultiplier: 1.75,
					loadingTimes: 0.3,
					weapons: {
						only: [
							"MACHINE GUN",
							"CHAINGUN",
							"CARBINE",
							"WINCHESTER",
							"LASER PISTOL",
							"MINIGUN",
							"GRENADE",
							"SHOCKWAVE BOMB",
							"MINI NUKE",
							"BAZOOKA",
							"FLAMETHROWER",
							"SPIKEBALLS",
							"SUPER SHOTGUN",
							"DARTGUN",
							"DOOMSDAY",
							"ORB LAUNCHER",
							"HYDROCHLORIC ACID",
							"THROW KNIFE",
							"ACID FAN",
							"VAMPIRE BITE"
						]
					}
				})
		]
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf"
	],
	tags: ['classic', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/racing/gotta_go_fast.png", {
	modes: {
			racing: [
					(new RacingSettings())
							.addNumberLaps(3)
							.addSpawn(1744,338)
							.addStartZone(1494,136,1824,397)
							.addStep(1919,134,1965,371)
							.addStep(2093,595,2339,709)
							.addStep(1920,984,1980,1262)
							.addStep(1624,460,1681,715)
							.addStep(891,1033,950,1297)
							.addStep(398,385,587,621)
							.addStep(1825,134,1871,360) //arrival
							.setSettings(
									{                        
											timeLimit: 10,
											maxDuplicateWeapons: 3,
											loadingTimes: 0,
											weapons: {
													only: ["JETPACK", "ACCELERATE", "NO WEAPON"]
											}
									}
							)
			],
	}, 
	onPlayerSpawn: (player) => {
			//forces weapon 1 to be "accelerate" and weapon 2 to be "jetpack"        
			window.WLROOM.setPlayerWeapons(player, [18,17,19,19,19,666,666,1,1,1]);
	},
	mods: [   
			{
					name: "tank_mod",
					json:"https://webliero.gitlab.io/webliero-mods/tank_mod/mod.json5",
					sprites:"https://webliero.gitlab.io/webliero-mods/tank_mod/sprites.wlsprt"
			},
		 { from: "神風/speedrun", weapons: [0, 2]},       
	],
	colorAnim: false,
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	tags: ['speedrun']
});


var kaiju_settings = {
	timeLimit: 10,
	scoreLimit: 20,
	respawnDelay: 1,
	maxDuplicateWeapons: 0,
	bonusDrops:"health",
	bonusSpawnFrequency: 15,
	damageMultiplier: 1.5,
	loadingTimes: 0.35,
	weapons: {
		only: [
			"HELLFIRE ROCKETS",
			"GRN LAUNCHER",
			"NAPALM",
			"INCENDIATOR",
			"TUPOLEV",
			"CLUSTER POD",
			"DESOLATOR",
			"FLAK CANNON",
			"PULSE GUN",
			"LIGHTNING GUN",
			"MACHINE GUN",
			"CHAINGUN",
			"CARBINE",
			"WINCHESTER",
			"LASER PISTOL",
			"MINIGUN",
			"GRENADE",
			"SHOCKWAVE BOMB",
			"MINI NUKE",
			"BAZOOKA",
			"FLAMETHROWER",
			"SPIKEBALLS",
			"SUPER SHOTGUN",
			"DARTGUN",
			"DOOMSDAY",
			"ORB LAUNCHER",
			"HYDROCHLORIC ACID",
			"THROW KNIFE",
			"ACID FAN",
			"VAMPIRE BITE"
		]
	}
};

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/party/all_out_attack.png", {
	modes: {
		haz: [
			(new HazSettings())
				.setZone(768+32, 0, 1280+32, 512)
				.addSpawn(1500,296)
				.addSpawn(1636,213)
				.addSpawn(1537,337)
				.addSpawn(1923,115)
				.addSpawn(458,164)
				.addSpawn(401,206)
				.addSpawn(447,321)
				.addSpawn(272,380)
				.setSettings(kaiju_settings)
		],
	// pred: [
	// 		(new PredSettings())
	// 			.addLemonSpawn(1056, 223)
	// 			.addWormSpawn(1500,296)
	// 			.addWormSpawn(1636,213)
	// 			.addWormSpawn(1537,337)
	// 			.addWormSpawn(1923,115)
	// 			.addWormSpawn(458,164)
	// 			.addWormSpawn(401,206)
	// 			.addWormSpawn(447,321)
	// 			.addWormSpawn(272,380)
	// 			.addWormSpawn(954,268)
	// 			.addWormSpawn(1104,281)
	// 			.setSettings(kaiju_settings)
	// 	],
	// 	ctf2: [
	// 		(new CTF2Settings())
	// 			.addFlagGreenSpawn(1544,282)
	// 			.addGreenSpawn(1500,296)
	// 			.addGreenSpawn(1636,213)
	// 			.addGreenSpawn(1537,337)
	// 			.addGreenSpawn(1104,281)
	// 			.addGreenSpawn(1923,115)
	// 			.addFlagBlueSpawn(468,201)
	// 			.addBlueSpawn(458,164)
	// 			.addBlueSpawn(401,206)
	// 			.addBlueSpawn(447,321)
	// 			.addBlueSpawn(272,380)
	// 			.addBlueSpawn(954,268)
	// 			.setSettings(kaiju_settings)
	// 	]
	},
	objects: (() => {
		const up = WLPI/2*3;
		const down = WLPI/2;
		const right = 0;
		const left = WLPI;

		let obj = [];
		for (let x = 0; x<=2050; x+=16) {
			obj.push({type:"sm_horizontal_line", x:x, y:512});
			obj.push({type:"sm_horizontal_line", x:x, y:0});
			if (x<=550) {
				obj.push({type:"sm_vertical_line", x:0, y:x});
				obj.push({type:"sm_vertical_line", x:2048, y:x});
			}
		}

		return obj;
	})(),
	mods: [
		"kangur/dtf",
		new LineWobject('sm_horizontal_line', 0, 16, 0, 1, 8, 666, 10),
		new LineWobject('sm_vertical_line', 0, 0, 16, 1, 8, 666, 10),
	],
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	tags: ['borders', 'party', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/flag/ozymandian_man.png",
{
	modes: {
		ctf: [
			(new CTFSettings())
				.addFlagGreenSpawn(70,466)
				.addFlagGreenSpawn(128,128)
				.addFlagGreenSpawn(340,576)
				.addGreenSpawn(196,274)
				.addGreenSpawn(212,272)
				.addGreenSpawn(244,284)
				.addGreenSpawn(172,280)
				.addFlagBlueSpawn(1186,474)
				.addFlagBlueSpawn(1168,116)
				.addFlagBlueSpawn(915,553)
				.addBlueSpawn(1038,296)
				.addBlueSpawn(1056,278)
				.addBlueSpawn(1078,278)
				.addBlueSpawn(1112,287)
		]
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf"
	],
	tags: ['big', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/flag/ozymandian_man_alt.png",
{
	modes: {
		ctf: [
			(new CTFSettings())
				.addFlagGreenSpawn(70,466)
				.addFlagGreenSpawn(128,128)
				.addFlagGreenSpawn(340,576)
				.addGreenSpawn(196,274)
				.addGreenSpawn(212,272)
				.addGreenSpawn(244,284)
				.addGreenSpawn(172,280)
				.addFlagBlueSpawn(1186,474)
				.addFlagBlueSpawn(1168,116)
				.addFlagBlueSpawn(915,553)
				.addBlueSpawn(1038,296)
				.addBlueSpawn(1056,278)
				.addBlueSpawn(1078,278)
				.addBlueSpawn(1112,287)
		]
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
        warmodExturl,    
        {from: warmodurl, options: ["merge_missing"] },
	],
	tags: ['big', 'war']
});


var war_games_settings = {
	timeLimit: 10,
	scoreLimit: 20,
	respawnDelay: 1,
	maxDuplicateWeapons: 0,
	bonusDrops:"healthAndWeapons",
	bonusSpawnFrequency: 3,
	weapons: {
		only: [
			"AK47",
			"APACHE REVOLVER",
			"AWP",
			"BARRETT M82",
			"CHIAPPA TRIPLE CROWN",
			"COLT 45",
			"DERINGER",
			"DESERT EAGLE",
			"DUAL P EIGHT LUGERS",
			"FN P90",
			"FOSTECH ORIGIN 12",
			"FURRER M1919 DOUBLE BARREL",
			"GATLING GUN",
			"GAU8A AVENGER",
			"GLOCK 19",
			"IMI UZI",
			"KRUMMLAUF",
			"LE PETIT PROTECTOR RING",
			"M16 CARABINE",
			"M1 GARAND",
			"M2HB",
			"M3 SUPER 90",
			"MAXIM MG08",
			"MP5",
            "MOSIN NAGANT",
			"P EIGHT LUGER",
			"PPSZ 41",
			"REMINGTON 870",
			"SG 552 COMMANDO",
			"SIG SAUER P226",
			"STEYR AUG",
            "STURMGEWEHR 44",
			"THOMPSON",
			"USP TACTICAL",
			"WINCHESTER",
			"XM1014",
			"XM214 MINIGUN"
		]
	}
};

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/flag/war_games.png", {
	modes: {
		ctf: [
			(new CTFSettings())
				.addFlagGreenSpawn(308,550)
				.addGreenSpawn(540,338)
				.addGreenSpawn(548,338)
				.addGreenSpawn(532,338)
				.addFlagBlueSpawn(970,636)
				.addBlueSpawn(846,338)
				.addBlueSpawn(854,338)
				.addBlueSpawn(838,338)
				.setSettings(war_games_settings)
		]
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
        warmodExturl,    
        {from: warmodurl, options: ["merge_missing"] },
	],
	tags: ['big', 'war']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/zone/sugar_zone.png", {
	modes:{
		haz:[(new HazSettings())
			.setZone(256,704,256*256,0)
			.addSpawn(64,96)
			.addSpawn(64+128,96)
			.addSpawn(64+128*2,96)
			.addSpawn(64+128*3,96)
			.setSettings({...defDTFHazSettings, forceRandomizeWeapons: true, bonusDrops:"health"})
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	objects: (() => {
		let obj = [];
		obj.push({ wlx:"turret", wobject:'dtf_winchester',x:96,y:608,freq:3,dist:500*500,speed:2});
		obj.push({ wlx:"turret", wobject:'dtf_winchester',x:416,y:608,freq:4,dist:500*500,speed:2});
		obj.push({ wlx:"turret", wobject:'dtf_winchester',x:96,y:800,freq:6,dist:500*500,speed:2});
		obj.push({ wlx:"turret", wobject:'dtf_winchester',x:416,y:800,freq:5,dist:500*500,speed:2});
		obj.push({ type:'sugar_roomu',x:256,y:704});
		return obj;
	})(),
	mods: [
		"kangur/dtf",
		"Larcelo/_sugar_roomu",
		{
			from: "kangur/dtf",
			wObjects:{65:'dtf_winchester'}
		}
	],
	tags: ['main', 'classic', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/zone/spice_zone.png", {
	modes:{
		haz:[(new HazSettings())
			.setZone(0,544,511,899)
			.addSpawn(64,96)
			.addSpawn(64+128,96)
			.addSpawn(64+128*2,96)
			.addSpawn(64+128*3,96)
			.setSettings({...defDTFHazSettings, forceRandomizeWeapons: true, bonusDrops:"health"})
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	objects: (() => {
		let obj = [];
		obj.push({ wlx:"turret", wobject:'dtf_winchester',x:96,y:608,freq:3,dist:500*500,speed:2});
		obj.push({ wlx:"turret", wobject:'dtf_winchester',x:416,y:608,freq:4,dist:500*500,speed:2});
		obj.push({ wlx:"turret", wobject:'dtf_winchester',x:96,y:800,freq:6,dist:500*500,speed:2});
		obj.push({ wlx:"turret", wobject:'dtf_winchester',x:416,y:800,freq:5,dist:500*500,speed:2});
		obj.push({ type:'sugar_roomu',x:256,y:704});
		return obj;
	})(),
	mods: [
		"kangur/dtf",
		"Larcelo/_sugar_roomu",
		{
			from: "kangur/dtf",
			wObjects:{65:'dtf_winchester'}
		}
	],
	tags: ['crazy', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/arena/hue_temple.png",{
	modes: {
		defuse: [
			(new DefuseSettings())
				.setBombSpawn(1000, 432)
				.addPlantingZone(640, 440)
				.addPlantingZone(640, 232)
				.addBlueSpawn(1184, 312)
				.addBlueSpawn(1184, 432)

				.addGreenSpawn(88, 312)
				.addGreenSpawn(88, 432)
				.setSettings({bonusDrops:"healthAndWeapons"})
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/defuse_the_bomb",
	],
	tags: ['cs', 'big'],
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/dm/hue_cave.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(defDTKDeathmatchSettings)
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"https://webliero.gitlab.io/webliero-mods/kami/dtk%201v1.zip"
	],
	colorAnim: false,
	tags: ['dtk', 'dm']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/dm/hue_cave_alt.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(defDefaultDeathmatchSettings)
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"https://webliero.gitlab.io/webliero-mods/Jerac/ReRevisited.zip"
	],
	colorAnim: false,
	tags: ['main', 'classic', 'rere']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/party/only_emperor.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(defSuperRFMSettings)
				.addSpawn(48,360)
				.addSpawn(132,180)
				.addSpawn(172,172)
				.addSpawn(228,180)
				.addSpawn(328,474)
				.addSpawn(412,146)
				.addSpawn(446,160)
				.addSpawn(520,166)
				.addSpawn(580,94)
				.addSpawn(720,228)
				.addSpawn(750,208)
				.addSpawn(788,206)
				.addSpawn(842,212)
				.addSpawn(870,220)
				.addSpawn(512,480)
				.addSpawn(430,486)
		],
	},
	objects: (() => {
		let obj = [];
		for (let x = 0; x<=960; x+=16) {
			obj.push({type:"sm_horizontal_line", x:x, y:540});
			obj.push({type:"sm_horizontal_line", x:x, y:0});
			if (x<=540) {
				obj.push({type:"sm_vertical_line", x:0, y:x});
				obj.push({type:"sm_vertical_line", x:960, y:x});
			}
		}
		return obj;
	})(),
	mods: [
		"https://webliero.gitlab.io/webliero-mods/kangaroo/RooFunMod%20(all%20weapons).zip",
		new LineWobject('sm_horizontal_line', 0, 16, 0, 1, 8, 666, 10),
		new LineWobject('sm_vertical_line', 0, 0, 16, 1, 8, 666, 10),
	],
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	tags: ['borders', 'party', 'rfm']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/party/thats_some_dream.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(defSuperSnowmodeSettings)
				.addSpawn(174,122)
				.addSpawn(250,122)
				.addSpawn(390,92)
				.addSpawn(264,232)
				.addSpawn(290,428)
				.addSpawn(392,408)
				.addSpawn(426,426)
				.addSpawn(556,154)
				.addSpawn(652,236)
				.addSpawn(832,219)
				.addSpawn(770,154)
				.addSpawn(744,366)
				.addSpawn(610,462)
		],
	},
	objects: (() => {
		let obj = [];
		for (let x = 0; x<=960; x+=16) {
			obj.push({type:"sm_horizontal_line", x:x, y:540});
			obj.push({type:"sm_horizontal_line", x:x, y:0});
			if (x<=540) {
				obj.push({type:"sm_vertical_line", x:0, y:x});
				obj.push({type:"sm_vertical_line", x:960, y:x});
			}
		}
		return obj;
	})(),
	mods: [
		"https://webliero.gitlab.io/webliero-mods/kami/Promode%20ReRevisited%20(kami%20physics%20changes).zip",
		{from: "https://webliero.gitlab.io/webliero-mods/Jerac/ReRevisited.zip", options: ["merge_all","replace_existing_weapons"]},
		{from: "https://webliero.gitlab.io/webliero-mods/snowmode/snowmode.zip", options: ["merge_missing"]},
		{from: "https://webliero.gitlab.io/webliero-mods/pro_mode/pro_mode.zip", options: ["merge_missing",{remove_weapon:"ACID GUN"},{remove_weapon:"FAN"}]},
		new LineWobject('sm_horizontal_line', 0, 16, 0, 1, 8, 666, 10),
		new LineWobject('sm_vertical_line', 0, 0, 16, 1, 8, 666, 10),
	],
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	tags: ['borders', 'party']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/remix/spaceshipint_reremixed.png", {
	modes: {
		dtf: [
			(new DTFSettings())
				.addFlagSpawn(530, 530)
				.addFlagSpawn(422, 454)
				.addDefenseSpawn(596, 466)
				.addDefenseSpawn(596, 498)
				.addAttackSpawn(1080, 586)
				.addAttackSpawn(1240, 436),
			(new DTFSettings())
				.addFlagSpawn(1048, 160)
				.addFlagSpawn(1230, 362)
				.addDefenseSpawn(1080, 332)
				.addDefenseSpawn(1230, 336)
				.addAttackSpawn(448, 460)
				.addAttackSpawn(480, 270),
		]
	},
	palette:true,
	backgroundLayer:"https://webliero.gitlab.io/webliero-maps/wgetch/ext/remix/spaceshipint_reremixed-bg.png",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.DIRT,..._range(192,223)))
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(224,255)))
		.map(replaceMatIndexBy(MATERIAL.BG, 3,4,5,6,7,8,9,10,11,39,40,41,42,43,44,45,46,47,120,121,122,123,124,125,160,161,162,163,164,165,166,167)),
	mods: [
		"kangur/dtf"
	],
	colorAnim: false,
	tags: ['ugly', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/zone/catacombs.png", {
	modes: {
		haz: [
			(new HazSettings())
				.setZone(212,200,90*90,0)
				.addSpawn(48,432)
				.addSpawn(48,1152)
				.addSpawn(1232,432)
				.addSpawn(1232,1152)
				.addSpawn(335,528)
				.addSpawn(335,1248)
				.addSpawn(945,528)
				.addSpawn(945,1248)
				.addSpawn(640,432)
				.addSpawn(640,1152)
				.setSettings({...defDTFHazSettings, forceRandomizeWeapons: true, bonusDrops:"healthAndWeapons"})
		]
	},
	objects: [
		{wlx:"teleport",dist:2,x:335,y:620,toX:335,toY:1248},
		{wlx:"teleport",dist:2,x:335,y:1340,toX:335,toY:528},
		{wlx:"teleport",dist:2,x:945,y:620,toX:945,toY:1248},
		{wlx:"teleport",dist:2,x:945,y:1340,toX:945,toY:528},
		{wlx:"teleport",dist:2,x:640,y:528,toX:640,toY:1152},
		{wlx:"teleport",dist:2,x:640,y:1248,toX:640,toY:432},
	],
	onGameStart: () => {
		console.log("init zone change");
		window.TMP_STATE = {
			l:1,
			z: [
				[212,200,90*90,0], //1
				[640,200,90*90,0], //2
				[1066,200,90*90,0], //3
				[212,920,90*90,0], //4
				[640,920,90*90,0], //5
				[1066,920,90*90,0], //6
			],
			b: [
				{id:60, type:"cata_b01", x:120, y:107},
				{id:61, type:"cata_b02", x:546, y:107},
				{id:62, type:"cata_b03", x:973, y:107},
				{id:63, type:"cata_b04", x:120, y:827},
				{id:64, type:"cata_b05", x:546, y:827},
				{id:65, type:"cata_b06", x:973, y:827},
			],
			r: [
				{id:70, type:"cata_r01", x:167, y:246},
				{id:71, type:"cata_r02", x:594, y:246},
				{id:72, type:"cata_r03", x:1020, y:246},
				{id:73, type:"cata_r04", x:167, y:966},
				{id:74, type:"cata_r05", x:594, y:966},
				{id:75, type:"cata_r06", x:1020, y:966},
			],
			n: [
				"I",
				"II",
				"III",
				"IV",
				"V",
				"VI",
			],
			lz:0,
			i: false,
		};
	},
	onGameTick: (g) => {
		let l = window.TMP_STATE.l;
		if (!l) return;
		const initZone = () => {
			const s = window.TMP_STATE;
			const maxZ = s.z.length;
			let nz = s.lz;
			do {
				nz = Math.floor(Math.random()*maxZ);
			} while (nz==s.lz);
			window.WLROOM.killObject(60+s.lz);
			window.WLROOM.killObject(70+s.lz);
			s.lz = nz;
			window.WLROOM.setZone(...s.z[nz]);
			createObject(s.b[nz]);
			createObject(s.r[nz]);
			const zname = s.n[nz];
			window.WLROOM.sendExtra({type:"overlay",text:`Zone changed to ${zname}`, color: '#00FFED'});
			announce(`Zone changed to ${zname}`);
			window.TMP_STATE.l = 1;
		}
		if (l===5 && !window.TMP_STATE.i) {
			window.TMP_STATE.i = true;
			initZone();
		}
		if (l>=2700) {
			initZone();
			return;
		}
		++window.TMP_STATE.l;
	},
	colorAnim: false,
	palette: true,
	backgroundLayer: "true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf",
		"kangur/_teleport",
		new SpriteWobject(`wgetch/sprites/catacombs/b01`,`cata_b01`, SpriteWobject.UNDERLAY, true),
		new SpriteWobject(`wgetch/sprites/catacombs/b02`,`cata_b02`, SpriteWobject.UNDERLAY, true),
		new SpriteWobject(`wgetch/sprites/catacombs/b03`,`cata_b03`, SpriteWobject.UNDERLAY, true),
		new SpriteWobject(`wgetch/sprites/catacombs/b04`,`cata_b04`, SpriteWobject.UNDERLAY, true),
		new SpriteWobject(`wgetch/sprites/catacombs/b05`,`cata_b05`, SpriteWobject.UNDERLAY, true),
		new SpriteWobject(`wgetch/sprites/catacombs/b06`,`cata_b06`, SpriteWobject.UNDERLAY, true),
		new SpriteWobject(`wgetch/sprites/catacombs/r01`,`cata_r01`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/catacombs/r02`,`cata_r02`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/catacombs/r03`,`cata_r03`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/catacombs/r04`,`cata_r04`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/catacombs/r05`,`cata_r05`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/catacombs/r06`,`cata_r06`, SpriteWobject.OVERLAY, true),
	],
	tags: ['big', 'bad', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/zone/sector_fourteen.png", {
	modes: {
		haz: [
			(new HazSettings())
				.setZone(148,226,236,314)
				.addSpawn(192,110)
				.addSpawn(384,110)
				.addSpawn(576,110)
				.addSpawn(768,110)
				.addSpawn(192,430)
				.addSpawn(384,430)
				.addSpawn(576,430)
				.addSpawn(768,430)
				.setSettings({...defDTFHazSettings, forceRandomizeWeapons: true, bonusDrops:"healthAndWeapons"})
		]
	},
	objects: [],
	onGameStart: () => {
		console.log("init zone change");
		window.TMP_STATE = {
			l:1,
			z: [
				[148,226,236,314], //1
				[340,226,428,314], //2
				[532,226,620,314], //3
				[724,226,812,314], //4
			],
			b: [
				{id:60, type:"sect_b01", x:161, y:253},
				{id:61, type:"sect_b02", x:354, y:251},
				{id:62, type:"sect_b03", x:536, y:248},
				{id:63, type:"sect_b04", x:733, y:247},
			],
			n: [
				"one",
				"two",
				"three",
				"four",
			],
			lz:0,
			i: false,
		};
	},
	onGameTick: (g) => {
		let l = window.TMP_STATE.l;
		if (!l) return;
		const initZone = () => {
			const s = window.TMP_STATE;
			const maxZ = s.z.length;
			let nz = s.lz;
			do {
				nz = Math.floor(Math.random()*maxZ);
			} while (nz==s.lz);
			window.WLROOM.killObject(60+s.lz);
			s.lz = nz;
			window.WLROOM.setZone(...s.z[nz]);
			createObject(s.b[nz]);
			const zname = s.n[nz];
			window.WLROOM.sendExtra({type:"overlay",text:`Zone changed to ${zname}`, color: '#00FFED'});
			announce(`Zone changed to ${zname}`);
			window.TMP_STATE.l = 1;
		}
		if (l===5 && !window.TMP_STATE.i) {
			window.TMP_STATE.i = true;
			initZone();
		}
		if (l>=1800) {
			initZone();
			return;
		}
		++window.TMP_STATE.l;
	},
	colorAnim: false,
	palette: true,
	backgroundLayer: "true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf",
		new SpriteWobject(`wgetch/sprites/sector_fourteen/b01`,`sect_b01`, SpriteWobject.UNDERLAY, true),
		new SpriteWobject(`wgetch/sprites/sector_fourteen/b02`,`sect_b02`, SpriteWobject.UNDERLAY, true),
		new SpriteWobject(`wgetch/sprites/sector_fourteen/b03`,`sect_b03`, SpriteWobject.UNDERLAY, true),
		new SpriteWobject(`wgetch/sprites/sector_fourteen/b04`,`sect_b04`, SpriteWobject.UNDERLAY, true),
	],
	tags: ['main', 'classic', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/zone/scenario.png", {
	modes: {
		haz: [
			(new HazSettings())
				.setZone(293,120,485,248)
				.addSpawn(264,160)
				.addSpawn(514,160)
				.addSpawn(770,160)
				.addSpawn(1016,160)
				.addSpawn(388,590)
				.addSpawn(640,590)
				.addSpawn(892,590)
				.addSpawn(604,160)
				.addSpawn(604,192)
				.addSpawn(676,160)
				.addSpawn(676,192)
				.addSpawn(604,304)
				.addSpawn(604,336)
				.addSpawn(676,304)
				.addSpawn(676,336)
				.addSpawn(604,448)
				.addSpawn(604,480)
				.addSpawn(676,448)
				.addSpawn(676,480)
				.setSettings({...crampedDTFHazSettings, forceRandomizeWeapons: true, bonusDrops:"health"})
		]
	},
	objects: [],
	onGameStart: () => {
		console.log("init zone change");
		window.TMP_STATE = {
			l:1,
			z: [
				[293,120,485,248], //1
				[293,264,485,392], //2
				[293,408,485,536], //3
				[797,120,989,248], //4
				[797,264,989,392], //5
				[797,408,989,536], //6
			],
			b: [
				{id:70, type:"flag_beacon", x:386, y:181},
				{id:71, type:"flag_beacon", x:386, y:325},
				{id:72, type:"flag_beacon", x:386, y:469},
				{id:73, type:"flag_beacon", x:890, y:181},
				{id:74, type:"flag_beacon", x:890, y:325},
				{id:75, type:"flag_beacon", x:890, y:469},
			],
			r: [
				{id:60, type:"scena_r01", x:289, y:116},
				{id:61, type:"scena_r02", x:289, y:260},
				{id:62, type:"scena_r03", x:289, y:404},
				{id:63, type:"scena_r04", x:793, y:116},
				{id:64, type:"scena_r05", x:793, y:260},
				{id:65, type:"scena_r06", x:793, y:404},
			],
			n: [
				"A3R",
				"A2G",
				"A1B",
				"Z3C",
				"Z2M",
				"Z1Y",
			],
			lz:0,
			i: false,
		};
	},
	onGameTick: (g) => {
		let l = window.TMP_STATE.l;
		if (!l) return;
		const initZone = () => {
			const s = window.TMP_STATE;
			const maxZ = s.z.length;
			let nz = s.lz;
			do {
				nz = Math.floor(Math.random()*maxZ);
			} while (nz==s.lz);
			window.WLROOM.killObject(70+s.lz);
			window.WLROOM.killObject(60+s.lz);
			s.lz = nz;
			window.WLROOM.setZone(...s.z[nz]);
			createObject(s.b[nz]);
			createObject(s.r[nz]);
			const zname = s.n[nz];
			window.WLROOM.sendExtra({type:"overlay",text:`Zone changed to ${zname}`, color: '#00FFED'});
			announce(`Zone changed to ${zname}`);
			window.TMP_STATE.l = 1;
		}
		if (l===5 && !window.TMP_STATE.i) {
			window.TMP_STATE.i = true;
			initZone();
		}
		if (l>=2700) {
			initZone();
			return;
		}
		++window.TMP_STATE.l;
	},
	colorAnim: false,
	palette: true,
	backgroundLayer: "true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,233)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255)))
		.map(replaceMatIndexBy(MATERIAL.BG_DIRT,..._range(234,243)))
        .map(replaceMatIndexBy(MATERIAL.BG_DIRT,..._range(82,84))),
	mods: [
		"kangur/dtf",
		new SpriteWobject(`wgetch/sprites/scenario/r01`,`scena_r01`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/scenario/r02`,`scena_r02`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/scenario/r03`,`scena_r03`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/scenario/r04`,`scena_r04`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/scenario/r05`,`scena_r05`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/scenario/r06`,`scena_r06`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/generic/flag_yellow`,`flag_beacon`, SpriteWobject.BEACON, true),
	],
	tags: ['classic', 'big', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/zone/double_igloo.png", {
	modes: {
		haz: [
			(new HazSettings())
				.setZone(256,336,48*48,0)
				.addSpawn(436,224)
				.addSpawn(502,224)
				.addSpawn(468,204)
				.addSpawn(560,202)
				.addSpawn(634,202)
				.addSpawn(596,178)
				.addSpawn(678,224)
				.addSpawn(772,224)
				.addSpawn(724,192)
				.setSettings({...crampedDTFHazSettings, forceRandomizeWeapons: true, bonusDrops:"health"})
		]
	},
	objects: [],
	onGameStart: () => {
		console.log("init zone change");
		window.TMP_STATE = {
			l:1,
			z: [
				[256,336,48*48,0], //1
				[938,336,48*48,0], //2
			],
			b: [
				{id:60, type:"flag_beacon", x:327, y:311},
				{id:61, type:"flag_beacon", x:863, y:311},
			],
			r: [
				{id:70, type:"flag_marker", x:327, y:311},
				{id:71, type:"flag_marker", x:863, y:311},
			],
			n: [
				"left igloo",
				"right igloo",
			],
			lz:0,
			period:1800,
			i: false,
		};
	},
	onGameTick: (g) => {
		let l = window.TMP_STATE.l;
		if (!l) return;
		const initZone = () => {
			const s = window.TMP_STATE;
			const maxZ = s.z.length;
			let nz = s.lz;
			do {
				nz = Math.floor(Math.random()*maxZ);
			} while (nz==s.lz);
			window.WLROOM.killObject(60+s.lz);
			window.WLROOM.killObject(70+s.lz);
			s.lz = nz;
			window.WLROOM.setZone(...s.z[nz]);
			createObject(s.b[nz]);
			createObject(s.r[nz]);
			const zname = s.n[nz];
			window.WLROOM.sendExtra({type:"overlay",text:`Zone changed to ${zname}`, color: '#00FFED'});
			announce(`Zone changed to ${zname}`);
			window.TMP_STATE.l = 1;
			window.TMP_STATE.period = 1800+Math.floor(Math.random()*1800);
		}
		if (l===5 && !window.TMP_STATE.i) {
			window.TMP_STATE.i = true;
			initZone();
		}
		if (l>=window.TMP_STATE.period) {
			initZone();
			return;
		}
		++window.TMP_STATE.l;
	},
	colorAnim: false,
	palette: true,
	backgroundLayer: "true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,233)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255)))
		.map(replaceMatIndexBy(MATERIAL.UNDEF,..._range(234,243))),
	mods: [
        "kangur/dtf_base_icefloor",
		"kangur/dtf",
		new SpriteWobject(`wgetch/sprites/generic/flag_yellow`,`flag_marker`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/generic/flag_yellow`,`flag_beacon`, SpriteWobject.BEACON, true),
	],
	tags: ['main', 'classic', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/arena/get_tilted.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.addSpawn(304,176)
				.addSpawn(278,360)
				.addSpawn(382,430)
				.addSpawn(448,230)
				.addSpawn(574,214)
				.addSpawn(976,544)
				.addSpawn(1002,360)
				.addSpawn(898,290)
				.addSpawn(832,490)
				.addSpawn(706,506)
				.setSettings(defRifleDeathmatchSettings)
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"https://webliero.gitlab.io/webliero-mods/Jerac/ReRevisited.zip"
	],
	colorAnim: false,
	tags: ['only']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/flag/passage2.png", {
	modes: {
		dtf: [
			(new DTFSettings())
				.addFlagSpawn(236, 192)
				.addFlagSpawn(356, 192)
				.addDefenseSpawn(316, 184)
				.addDefenseSpawn(408, 184)
				.addDefenseSpawn(416, 224)
				.addDefenseSpawn(464, 208)
				.addDefenseSpawn(568, 208)
				.addAttackSpawn(608, 208)
				.addAttackSpawn(712, 212)
				.addAttackSpawn(712, 236)
				.addAttackSpawn(820, 184)
				.addAttackSpawn(820, 264)
				.setSettings(crampedDTFHazSettings),
		]
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf"
	],
	colorAnim: false,
	tags: ['main', 'classic', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/zone/lumen_zone.png", {
	modes:{
		haz:[(new HazSettings())
			.setZone(270,201,363,262)
			.addSpawn(218,152)
			.addSpawn(218,152+32)
			.addSpawn(218,152+32*2)
			.addSpawn(218,152+32*3)
			.addSpawn(218,152+32*4)
			.addSpawn(416,152)
			.addSpawn(416,152+32)
			.addSpawn(416,152+32*2)
			.addSpawn(416,152+32*3)
			.addSpawn(416,152+32*4)
			.setSettings({...defDTFHazSettings, forceRandomizeWeapons: true, bonusDrops:"health"})
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,233)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(234,255))),
	objects: [
		{type:"lumen_overlay", x:222, y:312},
		{type:"hurt", x:222, y:330},
		{type:"hurt", x:232, y:330},
		{type:"hurt", x:242, y:330},
		{type:"hurt", x:252, y:330},
		{type:"hurt", x:262, y:330},
		{type:"hurt", x:272, y:330},
		{type:"hurt", x:282, y:330},
		{type:"hurt", x:292, y:330},
		{type:"hurt", x:302, y:330},
		{type:"hurt", x:312, y:330},
		{type:"hurt", x:322, y:330},
		{type:"hurt", x:332, y:330},
		{type:"hurt", x:342, y:330},
		{type:"hurt", x:352, y:330},
		{type:"hurt", x:362, y:330},
		{type:"hurt", x:372, y:330},
		{type:"hurt", x:382, y:330},
		{type:"hurt", x:392, y:330},
		{type:"hurt", x:402, y:330},
		{type:"hurt", x:412, y:330},
	],
	mods: [
		"kangur/dtf",
		"wgetch/_hurt",
		new AnimatedSpritesWobject(`https://sylvodsds.gitlab.io/webliero-extended-mods/wgetch/01_anim/lumen_zone.zip`, 'lumen_overlay', AnimatedSpritesWobject.OVERLAY, 15),
	],
	tags: ['main', 'classic', 'dtfmod']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/remix/wtf2_remix.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.addSpawn(73, 86)
				.addSpawn(262, 142)
				.addSpawn(303, 142)
				.addSpawn(285, 236)
				.addSpawn(590, 105)
				.addSpawn(525, 135)
				.addSpawn(455, 130)
				.addSpawn(75, 335)
				.addSpawn(232, 352)
				.addSpawn(364, 354)
				.addSpawn(190, 235)
				.setSettings(subDTFDeathmatchSettings)
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf"
	],
	colorAnim: false,
	tags: ['main', 'classic', 'dtfmod', 'gusanos']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/remix/poo_remix.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.addSpawn(367, 153)
				.addSpawn(267, 233)
				.addSpawn(452, 233)
				.addSpawn(497, 213)
				.addSpawn(220, 328)
				.addSpawn(292, 323)
				.addSpawn(442, 323)
				.addSpawn(512, 328)
				.addSpawn(312, 388)
				.addSpawn(417, 388)
				.setSettings(subDTFDeathmatchSettings)
		],
	},
	objects: [
		{type:"kicker", x:358, y:394},
		{type:"kicker", x:364, y:394},
		{type:"kicker", x:370, y:394},
		{type:"kicker", x:376, y:394},
	],
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf",
		"wgetch/_kicker",
	],
	colorAnim: false,
	tags: ['main', 'classic', 'dtfmod', 'gusanos']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/conversion/pokolenia.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.addSpawn(260, 140)
				.addSpawn(212, 454)
				.addSpawn(546, 374)
				.addSpawn(566, 144)
				.setSettings(subDTFDeathmatchSettings)
		],
	},
	objects: [
		{type:"pokolenia_rolling", x:389, y:181},
		{type:"pokolenia_screen", x:373, y:181},
		{type:"pokolenia_read", x:373, y:273},
	],
	onGameStart: () => {
		window.TMP_STATE = {
			t:1,
			objects: [
				{type:"pokolenia_bha", x:512, y:254, t:0},
				{type:"pokolenia_bha", x:508, y:269, t:20},
				{type:"pokolenia_bha", x:505, y:283, t:20*2},
				{type:"pokolenia_bha", x:502, y:296, t:20*3},
				{type:"pokolenia_bha", x:499, y:309, t:20*4},
				{type:"pokolenia_bha", x:496, y:321, t:20*5},
				{type:"pokolenia_bha", x:493, y:334, t:20*6},
				{type:"pokolenia_bha", x:490, y:346, t:20*7},
				{type:"pokolenia_bha", x:487, y:359, t:20*8},
				{type:"pokolenia_bha", x:484, y:371, t:20*9},
				{type:"pokolenia_bha", x:481, y:383, t:20*10},
				{type:"pokolenia_bha", x:478, y:396, t:20*11},
				{type:"pokolenia_bha", x:475, y:408, t:20*12},
				{type:"pokolenia_bha", x:475, y:408, t:20*12},
				{type:"pokolenia_bhb", x:625, y:220, t:0},
				{type:"pokolenia_bhb", x:628, y:235, t:20*1},
				{type:"pokolenia_bhb", x:631, y:251, t:20*2},
				{type:"pokolenia_bhb", x:634, y:267, t:20*3},
				{type:"pokolenia_bhb", x:637, y:283, t:20*4},
				{type:"pokolenia_bhb", x:640, y:300, t:20*5},
				{type:"pokolenia_bhc", x:223, y:207, t:0},
				{type:"pokolenia_bhc", x:220, y:226, t:40},
				{type:"pokolenia_bhc", x:217, y:242, t:40*2},
				{type:"pokolenia_bhc", x:214, y:260, t:40*3},
				{type:"pokolenia_bhc", x:211, y:277, t:40*4},
				{type:"pokolenia_bhc", x:208, y:296, t:40*5},
			],
		};
	},
	onGameTick: (g) => {
		let t = window.TMP_STATE.t;
		if (t===undefined||t>20*13) return;
		t -= 5;
		const s = window.TMP_STATE;
		for(let o of s.objects) {
			if(t==o.t) {
        createObject({type:o.type,x:o.x,y:o.y});
			}
		}
		++window.TMP_STATE.t;
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,233)))
		.map(replaceMatIndexBy(MATERIAL.UNDEF,..._range(234,238)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(239,255))),
	mods: [
		"kangur/dtf",
		new AnimatedSpritesWobject(`https://sylvodsds.gitlab.io/webliero-extended-mods/wgetch/01_anim/pokolenia_rolling.zip`, 'pokolenia_rolling', AnimatedSpritesWobject.UNDERLAY, 15),
		new AnimatedSpritesWobject(`https://sylvodsds.gitlab.io/webliero-extended-mods/wgetch/01_anim/pokolenia_screen.zip`, 'pokolenia_screen', AnimatedSpritesWobject.UNDERLAY, 240),
		new AnimatedSpritesWobject(`https://sylvodsds.gitlab.io/webliero-extended-mods/wgetch/01_anim/pokolenia_read.zip`, 'pokolenia_read', AnimatedSpritesWobject.OVERLAY, 180),
		new AnimatedSpritesWobject(`https://sylvodsds.gitlab.io/webliero-extended-mods/wgetch/01_anim/pokolenia_bha.zip`, 'pokolenia_bha', AnimatedSpritesWobject.OVERLAY, 5),
		new AnimatedSpritesWobject(`https://sylvodsds.gitlab.io/webliero-extended-mods/wgetch/01_anim/pokolenia_bhb.zip`, 'pokolenia_bhb', AnimatedSpritesWobject.OVERLAY, 5),
		new AnimatedSpritesWobject(`https://sylvodsds.gitlab.io/webliero-extended-mods/wgetch/01_anim/pokolenia_bhc.zip`, 'pokolenia_bhc', AnimatedSpritesWobject.OVERLAY, 5),
	],
	colorAnim: false,
	tags: ['main', 'classic', 'dtfmod', 'gusanos']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/remix/portal2_remix.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(subDTFDeathmatchSettings)
		],
	},
	objects: [
		{type:"kicker", x:7, y:333},
		{type:"kicker", x:7, y:333},
		{type:"kicker", x:7, y:328},
		{type:"kicker", x:7, y:328},
		{type:"kicker", x:501, y:243},
		{type:"kicker", x:501, y:243},
		{type:"kicker", x:501, y:238},
		{type:"kicker", x:501, y:238},
		{type:"kicker", x:592, y:293},
		{type:"kicker", x:592, y:293},
		{type:"kicker", x:592, y:288},
		{type:"kicker", x:592, y:288},
	],
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf",
		"wgetch/_kicker",
	],
	colorAnim: false,
	tags: ['main', 'classic', 'dtfmod', 'gusanos']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/remix/portal2_remix_alt.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(napalmDTFDeathmatchSettings)
		],
	},
	objects: [
		{type:"kicker", x:7, y:333},
		{type:"kicker", x:7, y:333},
		{type:"kicker", x:7, y:328},
		{type:"kicker", x:7, y:328},
		{type:"kicker", x:501, y:243},
		{type:"kicker", x:501, y:243},
		{type:"kicker", x:501, y:238},
		{type:"kicker", x:501, y:238},
		{type:"kicker", x:592, y:293},
		{type:"kicker", x:592, y:293},
		{type:"kicker", x:592, y:288},
		{type:"kicker", x:592, y:288},
	],
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf",
		"wgetch/_kicker",
	],
	colorAnim: false,
	tags: ['only', 'gusanos']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/flag/omaha_beach.png", {
	modes: {
		dtf: [
			(new DTFSettings())
				.addFlagSpawn(96, 466)
				.addFlagSpawn(96, 499)
				.addFlagSpawn(96, 532)
				.addFlagSpawn(168,516)
				.addDefenseSpawn(192, 422)
				.addDefenseSpawn(256, 502)
				.addDefenseSpawn(280, 575)
				.addAttackSpawn(2276, 642)
				.addAttackSpawn(2262, 642)
				.addAttackSpawn(2248, 642)
				.addAttackSpawn(2290, 642)
				.addAttackSpawn(2302, 642)
				.addAttackSpawn(2382, 642)
				.addAttackSpawn(2396, 642)
				.addAttackSpawn(2408, 642)
				.addAttackSpawn(2422, 642)
				.addAttackSpawn(2436, 642)
				.setSettings(defWarFlagsSettings),
		]
	},
	palette:true,
	backgroundLayer:"https://webliero.gitlab.io/webliero-maps/wgetch/ext/flag/omaha_beach-bg.png",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,233)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255)))
		.map(replaceMatIndexBy(MATERIAL.UNDEF,..._range(234,243))),
	mods: [
		warmodExturl,
		{from: warmodurl, options: ["merge_missing"] },
	],
	colorAnim: false,
	tags: ['big', 'war']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/dm/roomu_mob.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(boomerangWarDeathmatchSettings)
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		warmodExturl,
		{from: warmodurl, options: ["merge_missing"] },
	],
	colorAnim: false,
	tags: ['only', 'war']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/zone/uber_micro.png", {
	modes: {
		haz: [
			(new HazSettings())
				.setZone(290,253,32*32,0)
				.addSpawn(400,230)
				.addSpawn(500,230)
				.addSpawn(430,314)
				.addSpawn(470,314)
				.addSpawn(300,370)
				.addSpawn(600,370)
				.setSettings({...crampedDTFHazSettings, forceRandomizeWeapons: true, bonusDrops:"health"})
		]
	},
	objects: [],
	onGameStart: () => {
		console.log("init zone change");
		window.TMP_STATE = {
			l:1,
			z: [
				[290,253,32*32,0], //1
				[610,253,32*32,0], //2
				[450,133,48*48,0], //3
				[450,374,55*55,0], //4
			],
			b: [
				{id:60, type:"flag_beacon", x:288, y:213},
				{id:61, type:"flag_beacon", x:608, y:213},
				{id:62, type:"flag_beacon", x:448, y:78},
				{id:63, type:"flag_beacon", x:448, y:321},
			],
			r: [
				{id:70, type:"flag_marker", x:288, y:213},
				{id:71, type:"flag_marker", x:608, y:213},
				{id:72, type:"flag_marker", x:448, y:78},
				{id:73, type:"flag_marker", x:448, y:321},
			],
			n: [
				"left zone",
				"right zone",
				"upper zone",
				"lower zone",
			],
			lz:0,
			period:1800,
			i: false,
		};
	},
	onGameTick: (g) => {
		let l = window.TMP_STATE.l;
		if (!l) return;
		const initZone = () => {
			const s = window.TMP_STATE;
			const maxZ = s.z.length;
			let nz = s.lz;
			do {
				nz = Math.floor(Math.random()*maxZ);
			} while (nz==s.lz);
			window.WLROOM.killObject(60+s.lz);
			window.WLROOM.killObject(70+s.lz);
			s.lz = nz;
			window.WLROOM.setZone(...s.z[nz]);
			createObject(s.b[nz]);
			createObject(s.r[nz]);
			const zname = s.n[nz];
			window.WLROOM.sendExtra({type:"overlay",text:`Zone changed to ${zname}`, color: '#00FFED'});
			announce(`Zone changed to ${zname}`);
			window.TMP_STATE.l = 1;
			window.TMP_STATE.period = 1800+Math.floor(Math.random()*1800);
		}
		if (l===5 && !window.TMP_STATE.i) {
			window.TMP_STATE.i = true;
			initZone();
		}
		if (l>=window.TMP_STATE.period) {
			initZone();
			return;
		}
		++window.TMP_STATE.l;
	},
	colorAnim: false,
	palette: true,
	backgroundLayer: "true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,233)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255)))
		.map(replaceMatIndexBy(MATERIAL.UNDEF,..._range(234,243))),
	mods: [
		"kangur/dtf",
		new SpriteWobject(`wgetch/sprites/generic/flag_yellow`,`flag_marker`, SpriteWobject.OVERLAY, true),
		new SpriteWobject(`wgetch/sprites/generic/flag_yellow`,`flag_beacon`, SpriteWobject.BEACON, true),
	],
	tags: ['main', 'classic', 'dtfmod']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/arena/skunkworks.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(subDTFDeathmatchSettings)
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf",
	],
	colorAnim: false,
	tags: ['classic', 'dtfmod']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/arena/wrentch_arena.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(subDTFDeathmatchSettings)
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf",
	],
	colorAnim: false,
	tags: ['main', 'classic', 'dtfmod']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/arena/mobius.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(subDTFDeathmatchSettings)
		],
	},
	objects: (()=>{
		var o = [];
		var x1 = 213+16;
		var x2 = 213+640-16;
		var toX1 = 213+640-26;
		var toX2 = 213+26;
		for(var dy=96; dy<=256; dy+=16) {
			var y = 120+dy;
			o.push({"wlx":"teleport","x":x1,"y":y,"dist":2,"toX":toX1,"toY":y});
			o.push({"wlx":"teleport","x":x2,"y":y,"dist":2,"toX":toX2,"toY":y});
		}
		return o;
	})(),
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		"kangur/dtf",
		"dsds/_teleport_16x16"
	],
	colorAnim: false,
	tags: ['main', 'classic', 'dtfmod']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/arena/est_infernum.png", {
	modes: {
        dm: [
            (new SimpleSettings())
                .setSettings(defWarDeathmatchSettings)
        ],
        lms: [
            (new SimpleSettings())
                .setSettings(defWarLMSSettings)
        ],
    },
	objects: [
		{type:"tank_spawner", x:0, y:0},
	],
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		warmodExturl,
		{from: warmodurl, options: ["merge_missing"] },
	],
	colorAnim: false,
	tags: ['main', 'classic', 'war']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/arena/laitakaupunki.png", {
	modes: {
        dm: [
            (new SimpleSettings())
                .setSettings(defWarDeathmatchSettings)
        ],
    },
	objects: [
		{type:"tank_spawner", x:0, y:0},
	],
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
	mods: [
		warmodExturl,
		{from: warmodurl, options: ["merge_missing"] },
	],
	colorAnim: false,
	tags: ['classic', 'war']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/arena/wtn/wurm_tournament_nexus.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(subDTFDeathmatchSettings)
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,233)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255)))
		.map(replaceMatIndexBy(MATERIAL.UNDEF,..._range(234,243))),
	mods: [
		"kangur/dtf",
	],
	colorAnim: false,
	tags: ['main', 'classic', 'dtfmod']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/htf/field_of_honor-agonistic.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(subDTFHoldTheFlagSettings)
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,233)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255)))
		.map(replaceMatIndexBy(MATERIAL.UNDEF,..._range(234,243))),
	mods: [
		"kangur/dtf",
	],
	colorAnim: false,
	tags: ['main', 'classic', 'dtfmod']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/htf/field_of_honor-brutal.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(subDTFHoldTheFlagSettings)
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,233)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255)))
		.map(replaceMatIndexBy(MATERIAL.UNDEF,..._range(234,243))),
	mods: [
		"kangur/dtf",
	],
	colorAnim: false,
	tags: ['main', 'classic', 'dtfmod']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/htf/field_of_honor-concentric.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(subDTFHoldTheFlagSettings)
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,233)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255)))
		.map(replaceMatIndexBy(MATERIAL.UNDEF,..._range(234,243))),
	mods: [
		"kangur/dtf",
	],
	colorAnim: false,
	tags: ['main', 'classic', 'dtfmod']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/wgetch/ext/htf/field_of_honor-dualistic.png", {
	modes: {
		dm: [
			(new SimpleSettings())
				.setSettings(subDTFHoldTheFlagSettings)
		],
	},
	palette:true,
	backgroundLayer:"true",
	materials: defaultMaterials
		.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,233)))
		.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255)))
		.map(replaceMatIndexBy(MATERIAL.UNDEF,..._range(234,243))),
	mods: [
		"kangur/dtf",
	],
	colorAnim: false,
	tags: ['main', 'classic', 'dtfmod']
});


mapSettings.set("https://webliero.gitlab.io/webliero-maps/kangaroo/gusanos/koala.png", {
    modes:{
        pred:[
            (new PredSettings())
                .addLemonSpawn(527,100)
                .addWormSpawn(126,76)
                .addWormSpawn(128,210)
                .addWormSpawn(77,46)
                .addWormSpawn(425,270)
                .addWormSpawn(594,270)
                .addWormSpawn(95,340)
                .addWormSpawn(548,420)  
                .setSettings(newKhanSettings)
        ]
    },
   palette:true,
    backgroundLayer:"true",
    // materials: defaultMaterials.map(noUndef),
    // .map(replaceMatIndexBy(MATERIAL.ROCK, 213, ..._range(188,193))),
    colorAnim: defaultColorAnim,    
    mods: [
       //"kangur/dtf",
       //"https://www.vgm-quiz.com/dev/webliero/wledit/mods/khan1.49.zip",
       "https://www.vgm-quiz.com/dev/webliero/wledit/mods/latest/khan",
    ],
    tags: ['main', 'classic', 'khan']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/dsds/material_hack/moon_gaza_2_remixed.png", {
    modes: {
        racing: [
            (new LieroKartSettings())
                .addNumberLaps(3)
                .addSpawn(298,319)                
                .addStartZone(266, 267, 347, 349)
                .addStep(221, 299, 246, 348)     
                .addStep(150, 225, 189, 300)     
                .addStep(47, 301, 73, 349)     
                .addStep(154, 139, 183, 215)     
                .addStep(237, 174, 265, 250)     
                .addStep(272, 108, 304, 182)     
                .addStep(91, 76, 115, 137)     
                .addStep(0, 55, 72, 85)     
                .addStep(117, 0, 153, 63)     
                .addStep(342, 7, 369, 78)     
                .addStep(444, 99, 503, 123)     
                .addStep(392, 170, 457, 190)     
                .addStep(430, 230, 455, 280)     
                .addStep(375, 307, 394, 348)     
                  
                .addStep(250, 299, 265, 348) //arrival
                .addWeapBonus(197,261)   
                .addWeapBonus(164,257)  
                .addWeapBonus(228,275)   
                .addWeapBonus(96,316) 
                .addWeapBonus(70,324)  
                .addWeapBonus(33,324)  
                .addWeapBonus(250,212)  
                .addWeapBonus(280,226)  
                .addWeapBonus(308,234)  
                .addWeapBonus(23,61)  
                .addWeapBonus(36,38)  
                .addWeapBonus(48,15)  
                .addWeapBonus(241,68)  
                .addWeapBonus(281,55)  
                .addWeapBonus(328,35)  
                .addWeapBonus(463,314)  
                .addWeapBonus(463,329)  
                .addOnStartRace(() => {
                    window.WLROOM.killObject(69)
                    window.WLROOM.killObject(70)
                })
                .setSettings(
                    {                        
                        timeLimit: 10,
                        maxDuplicateWeapons: 5,
                        loadingTimes: 0.4,
                        weapons: {
                            only: ["NO WEAPON"]
                        }
                    }
                )
        ],
    }, 
    // onPlayerSpawn: (player) => {
    //     //forces weapon 1 to be "accelerate" and weapon 2 to be "jetpack"   
    //     let md =  modCache.get(currentModTK).wlk             
    //     let nw = md.weapons.getBy('name', "NO WEAPON").id
    //     window.WLROOM.setPlayerWeapons(player, [md.weapons.getBy('name', "VZ ONE PAWNEE").id,nw,nw,nw,nw,1,1,1,1,1]);
    //     // -- push pawnee to player       
       
    //     // setTimeout(()=>{
    //     //     let p = window.WLROOM.getPlayer(player.id)
    //     //     console.log("--player spawn", p.x, p.y, player.id)
    //     //     createObject({type: 'pawnee', x: p.x, y: p.y, owner: player.id})   
    //     // },100)
        
        
    // },
    objects: [     
        {id:69,type:"mg2r_pillar_left", x:258, y:325},
        {id:70,type:"mg2r_pillar_right", x:356, y:319},
        {type: "mg2r_eye",x: 280, y:  287},    
    ],  
    mods: [   
        "kangur/dtf",
       //   warmodurl,    
       //   {from: warmodExturl, options: ["merge_all","replace_existing_weapons",{remove_weapon:"VZ ONE PAWNEE"}] },          
        new DestroyableSolidSpriteWobject(`dsds/00_sprites/mg2r_pillars/mg2r_pillar_left`),
        new DestroyableSolidSpriteWobject(`dsds/00_sprites/mg2r_pillars/mg2r_pillar_right`),
        "dsds/_mg2r_eye",
       //{from:  "Larcelo/pawnee", options: ["merge_all","replace_existing_weapons", {constants:{noRope:true}}]},
       { from: "神風/speedrun", weapons: [0, 2]}, 
       "dsds/_bonus"  
    ],  
    backgroundLayer: "true",
    colorAnim: [129,132,133,136,152,159,168,171],  
    palette: true,
    materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(188,193))).map(replaceMatIndexBy(MATERIAL.BG,..._range(133,136))),
    tags: ['speedrun']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/kangaroo/Remixes/WebLieroRacing_alt.png", {
    modes: {
        racing: [
            (new RacingSettings())
                .addNumberLaps(1)
                .addSpawn(170,2985)
                .addFinalZone(357, 2436, 503, 2455)
                .addStartLine(103, 2970, 228, 2970)
                .setSettings(
                    {                        
                        timeLimit: 10,
                        maxDuplicateWeapons: 5,
                        weapons: {
                            only: ["NO WEAPON"]
                        }
                    }
                )
        ],
    },
    objects: [
        {type:"start_line", x:103, y:2970},
        {type:"end_line", x:357, y:2455},
    ],
    mods: [ 
        "kangur/dtf",
       { from: "神風/speedrun", weapons: [0, 2]},
        new LineWobject('start_line', 50,  125,  0,  10), //color, to X, to Y, line width
        new LineWobject('end_line', 104, 146, 0, 10),
    ],
    colorAnim: false,
    tags: ['classic', 'speedrun']
});

mapSettings.set("dsds/material_hack/metal_4.png", 
{
    modes:  { 
        pred: [
            (new PredSettings).addLemonSpawn(325,103)
                .setSettings(defWarExSettings)
        ],
        dm: [
            (new SimpleSettings())
                .setSettings(defWarDeathmatchSettings)
        ],
        lms: [
            (new SimpleSettings())
                .setSettings(defWarLMSSettings)
        ],
    },  
    objects: [     
        {type: "fan_left",x: 20, y:  43},    
        {type: "fan_right",x: 536, y:  43},    
        {type: "metal_4_shrooms",x: 180, y:  288},  
        {type: "metal_4_shrooms",x: 180+120, y:  288},  
        {type: "metal_4_shrooms",x: 180+240, y:  288},  
        {type: "metal_4_shrooms",x: 180+360, y:  288},  
        {type: "metal_4_fire_extinguisher",x: 600, y:  198},  
        {type: "metal_4_platform",x: 327, y:  256},  
        {type: "metal_4_bottom_mask",x: 341, y:  305},
        {type:"tank_spawner", x:0, y:0}, 
    ],
    mods: [             
       // "kangur/dtf",
       warmodExturl,    
       {from: warmodurl, options: ["merge_missing"] },
        "dsds/_metal_4_fan",
        "dsds/_metal_4_fan_right",
        "dsds/_metal_4_fire_extinguisher",
        "dsds/_metal_4_platform",
        // {
        //     name: "_metal_4_fan_right",
        //     json:`${ngrokUrl}dsds/_metal_4_fan_right/mod.json5`,
        //     sprites:`${ngrokUrl}dsds/_metal_4_fan_right/sprites.wlsprt`
        // },
        // {
        //     name: "_metal_4_fan",
        //     json:`${ngrokUrl}dsds/_metal_4_fan/mod.json5`,
        //     sprites:`${ngrokUrl}dsds/_metal_4_fan/sprites.wlsprt`
        // },
        // {
        //     name: "_metal_4_fire_extinguisher",
        //     json:`${ngrokUrl}/dsds/_metal_4_fire_extinguisher/mod.json5`,
        //     sprites:`${ngrokUrl}/dsds/_metal_4_fire_extinguisher/sprites.wlsprt`
        // },
        // {
        //     name: "_metal_4_platform#12",
        //     json:`${ngrokUrl}/dsds/_metal_4_platform/mod2.json5`,
        //     sprites:`${ngrokUrl}/dsds/_metal_4_platform/sprites2.wlsprt`
        // },
        new SpriteWobject(`dsds/00_sprites/metal_4/shrooms`,'metal_4_shrooms', SpriteWobject.OVERLAY), 
        new SpriteWobject(`dsds/00_sprites/metal_4/metal_4_bottom_mask`,'metal_4_bottom_mask', SpriteWobject.OVERLAY),                                  
        ],  
   // fxs: ["addbg#40#0#0#0#0"],
    materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(118,119))).map(replaceMatIndexBy(MATERIAL.BG,..._range(219,233))).map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,218))),
    palette: true,
    backgroundLayer: "https://webliero.gitlab.io/webliero-maps/dsds/material_hack/metal_4_bglayer.png",
    tags: ['main', 'classic', 'war']
})

mapSettings.set("神風/salpausselka.png", {
    modes:{
        "skiijumping": [(new SkiiJumpSettings())            
        .setSpawn(1, 85)                
        .setStartX(310)
        .setResetX(2796)
        .setSettings(
            {                        
                timeLimit: 10,
                
            }
        )]

    },
    materials: defaultMaterials.map(noUndef),
    objects: [              
    ],
    mods: [ 
        "https://webliero.gitlab.io/webliero-mods/kami/ski%20jumping.zip",
        {from:  "https://webliero.gitlab.io/webliero-mods/kami/ski%20jumping.zip", options: ["merge_all","replace_existing_weapons", {constants:{noRope:true}}]},  
        new SpriteWobject(`dsds/00_sprites/skijump/flag`)                
    ],
    tags: ['skiijumping']
}
);

mapSettings.set("https://webliero.gitlab.io/webliero-maps/%E7%A5%9E%E9%A2%A8/rocketclimber1.png", {
    modes: {
        racing: [
            (new RacingSettings())
                .addNumberLaps(1)
                .addSpawn(250,2560) 
                .addStartZone(180,2530,340, 2590)                               
                .addFinalZone(0, 0, 500, 20) //arrival
                .addOnStartRace(() => {
                    window.WLROOM.killObject(68)
                    window.WLROOM.killObject(69)
                    window.WLROOM.killObject(70)
                     setTimeout(()=>{window.WLROOM.killObject(71)},400)
                    // setTimeout(()=>{window.WLROOM.killObject(72)},800)
                    // setTimeout(()=>{window.WLROOM.killObject(73)},1200)
                    //setTimeout(()=>{window.WLROOM.killObject(73)},1600)
                })
                .setSettings(
                    {                        
                        timeLimit: 10,
                      //  maxDuplicateWeapons: 3,
                        loadingTimes: 0,
                       // weapons: {
                        //     only: ["JETPACK", "ACCELERATE", "NO WEAPON"]
                        // }
                    }
                )
        ],
    }, 
    onPlayerSpawn: (player) => {
        //forces weapon 1 to be "accelerate" and weapon 2 to be "jetpack"        
        //window.WLROOM.setPlayerWeapons(player, [18,17,19,19,19,666,666,1,1,1]);
    },
    objects: [
            { id: 68, type:"rocketclimber1_block_small", x: 15+88, y: 2539+23},
            { id: 69, type:"rocketclimber1_block_small", x: 310+88, y: 2539+23},
            { id: 70, type:"rocketclimber1_block", x: 15+88+147, y: 2391+23+52},
             { id: 71, type:"rocketclimber1_block", x: 15+88+147, y: 2243+23+52},
            // { id: 72, type:"rocketclimber1_block", x: 15+88, y: 2095+23},
            // { id: 73, type:"rocketclimber1_block", x: 15+88, y: 1946+23},
        ],
    palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,244,164,164,248,92,92,244,76,76,244,60,60,244,76,76,244,92,92,244,164,164,90,81,72,90,81,72,90,81,72,90,81,72,90,81,72,90,81,72,90,81,72,90,81,72,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,252,0,0,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
    mods: [   
        "https://webliero.gitlab.io/webliero-mods/kami/rocket%20climber.zip",
        {from:  "https://webliero.gitlab.io/webliero-mods/kami/rocket%20climber.zip", options: ["merge_all","replace_existing_weapons", {constants:{noRope:true}}]},
       //{ from: "神風/speedrun", weapons: [0, 2]},   
       new DestroyableSolidSpriteWobject(`dsds/00_sprites/rocketclimber1/rocketclimber1_block`),
       new DestroyableSolidSpriteWobject(`dsds/00_sprites/rocketclimber1/rocketclimber1_block_small`),
    ],
    tags: ['rockclimber', 'speedrun']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/hellhole/6xwheel.lev", {
    modes: {
         haz: [[83,93,87*87,0]],
        //haz: [[251,93,87*87,0]], //2
        // haz: [[419,93,87*87,0]], //3
        //haz: [[83,265,87*87,0]], //4
        //haz: [[251,265,87*87,0]], //5
        //haz: [[419,265,87*87,0]], //6
    },
    objects: [     
                //{id:60, type:"1", x:66, y:82},              
    ],
    onGameStart: ()=> {
        console.log("init zone change")
         window.TMP_STATE = {         
            l:1,
            z: [
                [83,93,87*87,0], //1
                [251,93,87*87,0], //2
                [419,93,87*87,0], //3
                [83,265,87*87,0], //4
                [251,265,87*87,0], //5
                [419,265,87*87,0], //6
            ],
            o: [
                {id:60, type:"1", x:66, y:82},
                {id:61, type:"2", x:237, y:82},
                {id:62, type:"3", x:406, y:82},
                {id:63, type:"4", x:66, y:251},
                {id:64, type:"5", x:238, y:251},
                {id:65, type:"6", x:406, y:250},
            ],
            b: [
                {id:70, type:"bw_beacon_1", x:83, y:93},
                {id:71, type:"bw_beacon_2", x:251, y:93},
                {id:72, type:"bw_beacon_3", x:419, y:93},
                {id:73, type:"bw_beacon_4", x:83, y:265},
                {id:74, type:"bw_beacon_5", x:251, y:265},
                {id:75, type:"bw_beacon_6", x:421, y:265},
            ],
            lz:0,
            i: false,
        }      
    },
    onGameTick: (g)=> {        
        let l = window.TMP_STATE.l        
        if (!l) return;
        const initZone = () => {
            const s = window.TMP_STATE
            const maxZ = s.z.length
            let nz = s.lz
            do {
                nz = Math.floor(Math.random()*maxZ)
            } while (nz==s.lz)
            window.WLROOM.killObject(60+s.lz)
            window.WLROOM.killObject(70+s.lz)
            s.lz=nz            
            window.WLROOM.setZone(...s.z[nz])
            createObject(s.o[nz])
            createObject(s.b[nz])
            window.WLROOM.sendExtra({type:"overlay",text:`Zone changed to ${nz+1}`, color: '#00FFED'})           
            window.TMP_STATE.l =1
        }
        if (l===5 && !window.TMP_STATE.i) {
            window.TMP_STATE.i=true;
            initZone()
        }
        if (l>=1800) {
            initZone()
            return
        } 
        ++window.TMP_STATE.l;
    },
    colorAnim: defaultColorAnim,     
    palette: palettes.get("default").map(replaceColor(160, 26, 26, 26)).map(replaceColor(161, 28, 28, 28)).map(replaceColor(162, 31, 31, 31)).map(replaceColor(163, 34, 34, 34)).map(replaceColor(164, 9, 9, 9)).map(replaceColor(165, 9, 9, 9)).map(replaceColor(166, 9, 9, 9)).map(replaceColor(167, 9, 9, 9)),   
    mods: [   
        "kangur/dtf",
        new SpriteWobject(`dsds/00_sprites/6x/1`),    
        new SpriteWobject(`dsds/00_sprites/6x/2`),    
        new SpriteWobject(`dsds/00_sprites/6x/3`),    
        new SpriteWobject(`dsds/00_sprites/6x/4`),    
        new SpriteWobject(`dsds/00_sprites/6x/5`),    
        new SpriteWobject(`dsds/00_sprites/6x/6`),
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_beacon_1`,`bw_beacon_1`, SpriteWobject.BEACON, SpriteWobject.ANCHOR_CENTER),     
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_beacon_2`,`bw_beacon_2`, SpriteWobject.BEACON, SpriteWobject.ANCHOR_CENTER),     
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_beacon_3`,`bw_beacon_3`, SpriteWobject.BEACON, SpriteWobject.ANCHOR_CENTER),     
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_beacon_4`,`bw_beacon_4`, SpriteWobject.BEACON, SpriteWobject.ANCHOR_CENTER),     
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_beacon_5`,`bw_beacon_5`, SpriteWobject.BEACON, SpriteWobject.ANCHOR_CENTER),       
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_beacon_6`,`bw_beacon_6`, SpriteWobject.BEACON, SpriteWobject.ANCHOR_CENTER),       
   ],
    tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set("https://sylvodsds.gitlab.io/webliero-builder-maps/maps/Wesley_s_wandering_waitress.png", {
    modes: {
        ctf: [
            // spawn flag left
            (new CTFSettings())
                .addFlagGreenSpawn(278,315)
                .addGreenSpawn(29,193)
                .addGreenSpawn(145,208)
                .addGreenSpawn(285,214)                
                .addGreenSpawn(42,300)
                .addGreenSpawn(230,309)
                .addGreenSpawn(268,451)
                .addFlagBlueSpawn(1220,315)
                .addBlueSpawn(1216,214)
                .addBlueSpawn(1355,209)
                .addBlueSpawn(1475,194)
                .addBlueSpawn(1460,304)
                .addBlueSpawn(1272,311)
                .addBlueSpawn(1234,449)
                .setSettings({weapons:{ban:["BAYRAKTAR","DIRT GRINDER","SILENT KILLER","HELL RITUAL","KILLER DRONE","BERSERK","DEATH AURA","BEHOLDER","MAGIC ARMOR","INVULNERABILITY","SUICIDE VEST","ABSORBING VORTEX","HOMING MISSILE","ADRENALINE RUSH","LOOTBOX","PANDORA BOX","ION CANNON","TRANSLOCATOR","STARGATE","SPEED UP"]}})
                
        ],},  
    objects: (() => {
        const up = WLPI/2*3
        const down = WLPI/2
        const right = 0
        const left = WLPI
        const width = 1500;
        let obj = []
        obj.push({ type:'green_drop_top',x:359,y:8})      
        obj.push({ type:'green_drop_bottom',x:359,y:595}),        

        obj.push({ type:'blue_drop_top',x:1140,y:8}) // blue,                
        obj.push({ type:'blue_drop_bottom',x:1140,y:595}),

        obj.push({ type:'laser_green',x:730,y:393,speed:6, angle: WLPI*0.7})
        obj.push({ type:'laser_green',x:179,y:246,speed:6, angle: right}) 
        obj.push({ type:'laser_green_down',x:432,y:337,speed:6, angle: down})     

        obj.push({ type:'laser_blue',x:width-730,y:393,speed:6, angle: WLPI*0.3}) // blue left,        
        obj.push({ type:'laser_blue',x:width-179,y:246,speed:6, angle: left}) // blue left,        
        obj.push({ type:'laser_blue_down',x:width-432,y:337,speed:6, angle: down}) // blue right,  

          
        return obj
        })(),  
    onGameStart: () => {
        const width = 1500;
        const handleBonus =  (team)=> (args) => {      
            let s = window.TMP_STATE         
            if (!args.player) return;
            let weap = window.WLROOM.getPlayerWeapons(args.player)
            if (weap.length==0) return;
            let reload = [-1,-1,-1,-1,-1]
            for (let x = 1; x<5; x++) {
                let w = weap[x]
                weap[x] = w.id
                reload[x] = w.ammo                                  
            }
            weap[0]=s.wl[Math.floor(Math.random()*s.wl.length)]
            reload[0]=666
            window.WLROOM.setPlayerWeapons(args.player, weap.concat(reload));
            if (team==1) {
                s.gh = args.player
            } else {
                s.bh = args.player
            }
        }
        window.TMP_STATE = {
            c: 0,
            h: handleBonus,
            g: { type:'bonus_green',x:175, y:317 },
            b: { type:'bonus_blue',x:width-175, y:317 },
            gh: null,
            bh: null,
            wl : []
        } 
        window.TMP_STATE.g.onExplode=window.TMP_STATE.h(1)
        window.TMP_STATE.b.onExplode=window.TMP_STATE.h(2)
        createObject( window.TMP_STATE.g)  
        createObject( window.TMP_STATE.b) // blue right, 

        const possible =[
             "DIRT GRINDER","ABSORBING VORTEX","SILENT KILLER","HELL RITUAL","STARGATE","TRANSLOCATOR",
             "KILLER DRONE","BERSERK","HOMING MISSILE","LOOTBOX","PANDORA BOX","ADRENALINE RUSH", "BAYRAKTAR",
             "DEATH AURA","BEHOLDER","MAGIC ARMOR","INVULNERABILITY","SUICIDE VEST","ION CANNON","SPEED UP"];
        let weap = window.WLROOM.getWeapons()
        for (let w of weap) {
            if (possible.includes(w.name)) {
                window.TMP_STATE.wl.push(w.id)
                // this.#weaponsAmmo.push(w.X)
                console.log(`adding weapon ${w.name} ${w.id}`)
            }                      
        }        
    },   
    onGameTick: (g)=> {        
        let s = window.TMP_STATE       
        if (!s) return;        
        ++s.c;        
        if (s.c<200) return;
        s.c =0;
        if (s.gh !==null) {
            let p = window.WLROOM.getPlayer(s.gh)            
            if (!p || p.team!=1) {
                createObject(s.g)
                s.gh = null
            }
        }
        if (s.bh!==null) {
            let p = window.WLROOM.getPlayer(s.bh)
            if (!p || p.team!=2) {
                createObject(s.b)
                s.bh=null
            }
        }        
     },
    onPlayerKilled: (killed, killer) => {
        let s = window.TMP_STATE      
        
        if (s.gh !== null && killed.id==s.gh) {
            s.gh= -1            
        }
        if (s.bh !== null && killed.id==s.bh) {
            s.bh= -1            
        }
        
    },
    onFlagScore: (worm, score) => {
        window.TMP_STATE.gh = -1;
        window.TMP_STATE.bh = -1;
    },
    mods: [   
         "kangur/dtf",
         "kangur/_lasers",
         "dsds/_www_drop",
         "dsds/_www_drop_blue",
         // { from: "kangur/_super_weapons", weapons: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]}, 
         "kangur/_super_weapons",
         "dsds/_bonus_ht_green",
         "dsds/_bonus_ht_blue"
        // {
        //     name: "bonusg",
        //     json: `${ngrokUrl}dsds/_bonus_ht_green/mod.json5#3`,
        //     sprites:`${ngrokUrl}dsds/_bonus_ht_green/sprites.wlsprt#3`
        // },  
        // {
        //     name: "dropb",
        //     json: `${ngrokUrl}dsds/_www_drop_blue/mod.json5#3`,
        //     sprites:`${ngrokUrl}dsds/_www_drop_blue/sprites.wlsprt#3`
        // },  
    ],
    colorAnim: defaultColorAnim,
    backgroundLayer: "true",
    palette: palettes.get("default").map(replaceColor(160, 3, 17, 21)).map(replaceColor(167, 9, 9, 9)),
   
    tags: ['classic', 'big', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/dsds/material_hack/Zeppelin_remix.png", {
    modes: { haz: [
        (new HazSettings())
            .setZone(116,72,408,133)
            .addSpawn(30, 300)     
            .addSpawn(90, 300)     
            .addSpawn(150, 300)     
            .addSpawn(210, 300)     
            .addSpawn(270, 300)     
            .addSpawn(330, 300)     
            .addSpawn(390, 300)     
            .addSpawn(450, 300)     
            .setSettings(defWarHazSettings),         
    ]},
    palette: true,
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.BG,..._range(188,203))),
    backgroundLayer: "https://webliero.gitlab.io/webliero-maps/dsds/material_hack/Zeppelin_remix_BGLayer.png",
    objects: [
        {type:"zep", x:73, y:200, speed: 0.15, angle: 0},
        {type:"zep", x:383, y:272, speed: 0.15, angle: WLPI},
    ],
    colorAnim: [129,132,133,136,152,159,168,171],
    mods: [
        //  "kangur/dtf",
        warmodExturl,    
        {from: warmodurl, options: ["merge_missing"] },
               
       "dsds/_zeppelin_small",
    //  {
    //        name: "dsds/_zeppelin_small",
    //        json: `${ngrokMUrl}dsds/_zeppelin_small/mod.json5`,
    //        sprites: `${ngrokMUrl}dsds/_zeppelin_small/sprites.wlsprt`,
    //    }        
            
    ],
    tags: ['main', 'classic', 'war']
})

mapSettings.set("https://webliero.gitlab.io/webliero-maps/dsds/material_hack/e1.png", {
    modes: {
        haz: [[146,171,35*35,0]],
       //haz: [[251,93,87*87,0]], //2
       // haz: [[419,93,87*87,0]], //3
       //haz: [[83,265,87*87,0]], //4
       //haz: [[251,265,87*87,0]], //5
       //haz: [[419,265,87*87,0]], //6
   },
   objects: [     
               //{id:60, type:"1", x:66, y:82},              
   ],
   onGameStart: ()=> {
       console.log("init zone change")
        window.TMP_STATE = {         
           l:1,
           z: [
           [146, 171, 35*35, 0],
           [454, 70, 35*35, 0],
           [264,117, 35*35, 0],
           [327,260, 35*35, 0],
           [472,262, 35*35, 0],
           [591,231, 35*35, 0],
           [120,252, 35*35, 0],
           [195,64, 35*35, 0],
            [384, 116, 35*35, 0],
           ],         
           lz:0,
           i: false,
       }      
   },
   onGameTick: (g)=> {        
       let l = window.TMP_STATE.l        
       if (!l) return;
       const initZone = () => {
           const s = window.TMP_STATE
           const maxZ = s.z.length
           let nz = s.lz
           do {
               nz = Math.floor(Math.random()*maxZ)
           } while (nz==s.lz)
           window.WLROOM.killObject(60)
           window.WLROOM.killObject(61)
           window.WLROOM.killObject(62)
           s.lz=nz            
           window.WLROOM.setZone(...s.z[nz])
          createObject({id:60, type:"e1_zone_bg", x:s.z[nz][0]-30, y:s.z[nz][1]-31},)
          createObject({id:61, type:"e1_zone_hex", x:s.z[nz][0], y:s.z[nz][1]},)
          createObject({id:62, type:"e1_zone_x", x:s.z[nz][0]-19, y:s.z[nz][1]-31},)
          // window.WLROOM.sendExtra({type:"overlay",text:`Zone changed to ${nz+1}`, color: '#00FFED'})
           announce(`Zone changed`)
           window.TMP_STATE.l =1
       }
       if (l===5 && !window.TMP_STATE.i) {
           window.TMP_STATE.i=true;
           initZone()
       }
       if (l>=1800) {
           initZone()
           return
       } 
       ++window.TMP_STATE.l;
   },
    palette:true,
    backgroundLayer:"true",
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.BG, ..._range(188,206), 225)).map(replaceMatIndexBy(MATERIAL.ROCK, ..._range(207,222))),
    colorAnim: defaultColorAnim,    
    mods: [
        //"https://www.vgm-quiz.com/dev/webliero/wledit/mods/khan1.43abc.zip",
       //"kangur/dtf",
        "https://www.vgm-quiz.com/dev/webliero/wledit/mods/latest/khan",
        new DestroyableSolidSpriteWobject(`dsds/00_sprites/e1/e1_zone_hex`),
        new SpriteWobject(`dsds/00_sprites/e1/e1_zone_x`,'e1_zone_x', SpriteWobject.OVERLAY), 
        new SpriteWobject(`dsds/00_sprites/e1/e1_zone_bg`,'e1_zone_bg', SpriteWobject.UNDERLAY), 
    ],
    tags: ['main', 'classic', 'khan']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/Ed/vgmq_world_alt_palette.png", {
    modes:{
        pred:[
            (new PredSettings())
                .addLemonSpawn(422,335)
                .addWormSpawn(126,76)
                .addWormSpawn(692,207)
                .addWormSpawn(360,201)
                .addWormSpawn(441,457)
                .addWormSpawn(722,66)
                .addWormSpawn(713,424)
                .addWormSpawn(200,474)
                .setSettings(newKhanSettings)        
        ]
    },
   palette:true,
    backgroundLayer:"true",
    materials: defaultMaterials.map(noUndef),
     //   .map(replaceMatIndexBy(MATERIAL.ROCK, 213, ..._range(188,193))),
    colorAnim: defaultColorAnim,
    mods: [
       // "kangur/dtf",
       //"https://www.vgm-quiz.com/dev/webliero/wledit/mods/khan1.49.zip",
        "https://www.vgm-quiz.com/dev/webliero/wledit/mods/latest/khan",
    ],
    tags: ['main', 'classic', 'khan']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/Ed/CTF_underworld.png", {
    modes:{
        ctf:[
            
        (new CTFSettings())
        .addFlagGreenSpawn(17, 117)
        .addGreenSpawn(59, 50)
        .addGreenSpawn(262, 118)
        .addGreenSpawn(124, 113)
        .addGreenSpawn(87, 403)

        .addFlagBlueSpawn(1200-17, 117)
        .addBlueSpawn(1200-59, 50)
        .addBlueSpawn(1200-262, 118)
        .addBlueSpawn(1200-124, 113)
        .addBlueSpawn(1200-87, 403)    

        .setSettings(newKhanSettings)
    ],},      
        
    palette:[0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,115,54,37,188,103,88,209,135,81,151,90,61,90,44,18,136,84,47,159,96,51,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,216,210,145,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,188,136,252,191,136,203,106,64,175,112,39,93,47,30,167,118,67,150,122,81,224,252,252,28,28,28,32,32,32,32,32,32,36,36,36,0,0,0,0,0,0,0,0,0,0,0,0,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,252,252,252,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,252,252,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
    backgroundLayer:"true",
    materials: defaultMaterials
        .map(replaceMatIndexBy(MATERIAL.ROCK, 213)),
    colorAnim: defaultColorAnim, 
    mods: [
        //"kangur/dtf",
        //"https://www.vgm-quiz.com/dev/webliero/wledit/mods/khan1.49.zip",
        "https://www.vgm-quiz.com/dev/webliero/wledit/mods/latest/khan",
    ],
    tags: ['classic', 'khan']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/dsds/material_hack/ufo1.png", {
    modes: { haz: [
        (new HazSettings())
            .setZone(252,101,35*36,0)
            .addSpawn(30, 300)     
            .addSpawn(90, 300)     
            .addSpawn(150, 300)     
            .addSpawn(210, 300)     
            .addSpawn(270, 300)     
            .addSpawn(330, 300)     
            .addSpawn(390, 300)     
            .addSpawn(450, 300)     
            .setSettings(defWarHazSettings),         
    ]},
    palette: true,
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.BG,..._range(196,222))).map(replaceMatIndexBy(MATERIAL.BG,..._range(224,228))),
    backgroundLayer: "true",
    objects: [
        // {type:"ufo1_circle_reflect", x:248, y:69},
         {type:"ufo1_overlay_cable_left", x:195, y:103},
         {type:"ufo1_overlay_cable_center", x:312, y:98},
         {type:"ufo1_overlay_cable_right", x:372, y:107},
         {type:"ufo1_overlay_pipe_center_0", x:212, y:147},
         {type:"ufo1_overlay_pipe_center_1", x:248, y:150},
         {type:"ufo1_overlay_pipe_center_2", x:291, y:147},
         {type:"ufo1_overlay_pipe_left", x:136, y:140},
         {type:"ufo1_overlay_pipe_right", x:363, y:140},
         {type:"ufo1_overlay_ship", x:252, y:160},
         {type:"ufo1_ray_overlay", x:201, y:337},
         {id: 69, type:"ufo1_small", x:73, y:200, speed: 0.25, angle: WLPI},
         {id: 70, type:"ufo1_small", x:373, y:260, speed: 0.15, angle: WLPI},
         {id: 71, type:"ufo1_small", x:433, y:290, speed: 0.25, angle: WLPI},
         {id: 72, type:"ufo1_small", x:273, y:240, speed: 0.35, angle: WLPI},
         {type:"cow", x:16, y:334, speed: 0.35, angle: 0},
         {type:"cow", x:116, y:334, speed: 0.35, angle: 0},
         {type:"cow", x:156, y:334, speed: 0.35, angle: 0},
         {type:"cow", x:216, y:334, speed: 0.35, angle: 0},
         {type:"cow", x:296, y:334, speed: 0.35, angle: 0},
         {type:"cow", x:316, y:334, speed: 0.35, angle: 0},
         {type:"cow", x:356, y:334, speed: 0.35, angle: 0},
         {type:"cow", x:416, y:334, speed: 0.35, angle: 0},
    ],
    colorAnim: [129,132,133,136,152,159,224,228],
    mods: [
        warmodExturl,    
        {from: warmodurl, options: ["merge_missing"] },
         new SpriteWobject(`dsds/00_sprites/ufo1/ufo1_overlay_cable_center`,'ufo1_overlay_cable_center', SpriteWobject.OVERLAY),
         new SpriteWobject(`dsds/00_sprites/ufo1/ufo1_overlay_cable_left`,'ufo1_overlay_cable_left', SpriteWobject.OVERLAY),
         new SpriteWobject(`dsds/00_sprites/ufo1/ufo1_overlay_cable_right`,'ufo1_overlay_cable_right', SpriteWobject.OVERLAY),
         new SpriteWobject(`dsds/00_sprites/ufo1/ufo1_overlay_pipe_center_0`,'ufo1_overlay_pipe_center_0', SpriteWobject.OVERLAY),
         new SpriteWobject(`dsds/00_sprites/ufo1/ufo1_overlay_pipe_center_1`,'ufo1_overlay_pipe_center_1', SpriteWobject.OVERLAY),
         new SpriteWobject(`dsds/00_sprites/ufo1/ufo1_overlay_pipe_center_2`,'ufo1_overlay_pipe_center_2', SpriteWobject.OVERLAY),
         new SpriteWobject(`dsds/00_sprites/ufo1/ufo1_overlay_pipe_left`,'ufo1_overlay_pipe_left', SpriteWobject.OVERLAY),
         new SpriteWobject(`dsds/00_sprites/ufo1/ufo1_overlay_pipe_right`,'ufo1_overlay_pipe_right', SpriteWobject.OVERLAY),
         new SpriteWobject(`dsds/00_sprites/ufo1/ufo1_overlay_ship`,'ufo1_overlay_ship', SpriteWobject.OVERLAY),
         new SpriteWobject(`dsds/00_sprites/ufo1/ufo1_ray_overlay`,'ufo1_ray_overlay', SpriteWobject.OVERLAY),
         "dsds/_ufo1",  
         "dsds/_ufo1_cow", 
        //  {
        //        name: "dsds/_ufo1_cow",
        //        json: `${ngrokUrl}dsds/_ufo1_cow/mod.json5`,
        //        sprites: `${ngrokUrl}dsds/_ufo1_cow/sprites.wlsprt`,
        //    }        
                
    ],
    tags: ['main', 'classic', 'war']
})

mapSettings.set("https://webliero.gitlab.io/webliero-maps/dsds/material_hack/bw2.png", 
{
    modes: {
        pred: [
            (new PredSettings).addLemonSpawn(415,166)
                .setSettings(defWarExSettings)
        ],
        dm: [
            (new SimpleSettings())
                .setSettings(defWarDeathmatchSettings)
        ],
        lms: [
            (new SimpleSettings())
                .setSettings(defWarLMSSettings)
        ],
    },
    objects: [     
        {type: "bw2_boule",x: 415, y:  278},
        {type:"tank_spawner", x:0, y:0}, 
    ],
    mods: [             
       // "kangur/dtf",
       warmodExturl,    
       {from: warmodurl, options: ["merge_missing"] },
       // {from: warmodExturl, options: ["merge_all","replace_existing_weapons", {remove_weapon:"PROTECTION ZONE"}] },       
        "dsds/_bw2_boule",                                   
        ],  
   // fxs: ["addbg#40#0#0#0#0"],
    materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(213,222))),
    palette: true,
    backgroundLayer: "https://webliero.gitlab.io/webliero-maps/dsds/material_hack/bw2_bglayer.png",
    tags: ['main', 'classic', 'war']
})

mapSettings.set(`https://webliero.gitlab.io/webliero-maps/dsds/material_hack/superbomberman.png`, {
    modes: { bomb: [
        (new BombSettings())
            .setGrid(64, 96, 13, 11)
            .addSpawn(0, 0)     
            .addSpawn(12, 0)     
            .addSpawn(0, 10)     
            .addSpawn(12, 10)
            .addBrickWObjectName('grey_brick')      
    ]},
    palette: true,
    materials: defaultMaterials.map(noUndef),
    backgroundLayer: "true",
    colorAnim: [129,132,133,136,152,159,224,228],
    mods: [         
        "dsds/bomberman"                     
    ],
    tags: ['bomb']
})

mapSettings.set("https://webliero.gitlab.io/webliero-maps/dsds/material_hack/bw4.png", {
    modes: {
        pred: [(new PredSettings).addLemonSpawn(713,62).setSettings(defDTFPredSettings)],       
   },  
    palette:true,
    backgroundLayer:"https://webliero.gitlab.io/webliero-maps/dsds/material_hack/bw4_bglayer.png",
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.BG, ..._range(188,198))).map(replaceMatIndexBy(MATERIAL.ROCK, ..._range(199,217))),
    colorAnim: defaultColorAnim,    
    mods: [
       "kangur/dtf",
    ],
    tags: ['classic', 'big', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/dsds/material_hack/bw4.png#blue", {
    modes: {
        haz: [[703,392,35*35,0]],
       //haz: [[369,432, 35*35, 0]], //2
   },
   objects: [     
               //{id:60, type:"1", x:66, y:82},              
   ],
   onGameStart: ()=> {
       console.log("init zone change")
        window.TMP_STATE = {         
           l:1,
           z: [
          [703,392, 35*35, 0],
            [843, 127, 35*35, 0],
           [573, 127, 35*35, 0],
           [1094,120, 35*35, 0],
           [397,180, 35*35, 0],
           [81,123, 35*35, 0],
           [108,408, 35*35, 0],
           [1188,497, 35*35, 0],
           [369,432, 35*35, 0],
           [1009,329, 35*35, 0],
             [1197, 270, 35*35, 0],
           ],         
           lz:0,
           i: false,
       }      
   },
   onGameTick: (g)=> {        
       let l = window.TMP_STATE.l        
       if (!l) return;
       const initZone = () => {
           const s = window.TMP_STATE
           const maxZ = s.z.length
           let nz = s.lz
           do {
               nz = Math.floor(Math.random()*maxZ)
           } while (nz==s.lz)
           window.WLROOM.killObject(60)
           window.WLROOM.killObject(61)
           window.WLROOM.killObject(62)
           s.lz=nz            
           window.WLROOM.setZone(...s.z[nz])
          createObject({id:60, type:"bw4_zone_bg", x:s.z[nz][0]-30, y:s.z[nz][1]-32},)
          createObject({id:61, type:"bw4_zone_hex", x:s.z[nz][0], y:s.z[nz][1]},)
          createObject({id:62, type:"bw4_zone_x", x:s.z[nz][0]+14, y:s.z[nz][1]+30},)
          // window.WLROOM.sendExtra({type:"overlay",text:`Zone changed to ${nz+1}`, color: '#00FFED'})
           announce(`Zone changed`)
           window.TMP_STATE.l =1
       }
       if (l===5 && !window.TMP_STATE.i) {
           window.TMP_STATE.i=true;
           initZone()
       }
       if (l>=1800) {
           initZone()
           return
       } 
       ++window.TMP_STATE.l;
   },
    palette:[0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,136,212,252,152,220,252,164,232,252,176,236,252,188,252,252,212,252,252,224,252,252,70,72,69,81,83,80,93,95,92,101,103,100,108,111,108,115,117,115,123,125,122,131,133,130,140,142,139,148,150,148,156,158,155,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,109,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,61,172,60,112,188,112,164,212,164,108,109,108,145,144,144,181,180,180,217,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,157,120,88,197,168,124,237,216,160,200,100,0,160,80,0,72,72,72,108,108,109,144,145,144,180,181,180,216,217,216,252,252,252,196,196,196,144,144,145,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,217,0,0,189,0,0,165,0,0,216,1,0,188,1,0,164,1,0,81,80,192,105,104,248,145,144,244,80,81,192,104,105,248,144,145,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,45,132,44,60,173,60,113,188,112,44,133,44,60,172,61,112,189,112,248,60,60,244,124,124,244,188,188,104,104,249,144,144,245,185,184,244,145,145,244,61,173,60,112,188,113,165,212,164,113,189,112,149,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,45,52,152,52,61,173,61,137,212,252,136,213,252,153,220,252,165,232,252,177,236,252,189,252,252,213,252,252,225,252,252,7,7,7,7,13,23,13,14,29,14,14,14,10,20,44,19,19,19,30,22,48,25,25,25,253,252,252,220,220,220,188,188,188,157,156,156,125,124,124,156,157,156,189,188,188,221,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,1,1,1,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,245,232,60,18,27,60,44,26,36,31,31,31,39,35,77,36,38,27,30,44,82,42,44,59,61,62,113,67,62,43,71,73,100,92,88,93,10,10,10,26,27,25,29,25,38,36,38,35,43,38,52,43,45,42,52,54,51,56,52,66,62,64,62,67,63,77,165,167,164,172,174,172,181,183,180,189,191,188,198,200,197,210,212,209,224,226,223,233,236,232,255,254,255,1,4,1,1,1,4,4,4,1,1,4,4,5,1,1,1,5,1,1,1,5,5,5,1,5,5,5,1,5,5,3,2,2,2,3,2,2,2,3,3,3,2,2,3,3,4,2,2,252,253,252,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,252,253,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
    backgroundLayer:"https://webliero.gitlab.io/webliero-maps/dsds/material_hack/bw4_bglayer.png",
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.BG, ..._range(188,198))).map(replaceMatIndexBy(MATERIAL.ROCK, ..._range(199,217))),
    colorAnim: defaultColorAnim,    
    mods: [
       "kangur/dtf",
        //"https://www.vgm-quiz.com/dev/webliero/wledit/mods/latest/khan",
        new DestroyableSolidSpriteWobject(`dsds/00_sprites/bw4/bw4_zone_hex`),
        new SpriteWobject(`dsds/00_sprites/bw4/bw4_zone_x`,'bw4_zone_x', SpriteWobject.OVERLAY), 
        new SpriteWobject(`dsds/00_sprites/bw4/bw4_zone_bg`,'bw4_zone_bg', SpriteWobject.UNDERLAY), 
    ],
    tags: ['big', 'dtfmod']
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/dsds/material_hack/bw_circle_zones.png", {
    modes: {
         haz: [[263,90,88*88,0]],
        //haz: [[251,93,88*88,0]], //2
        // haz: [[419,93,88*88,0]], //3
        //haz: [[83,265,88*88,0]], //4
        //haz: [[251,265,88*88,0]], //5
    },
    objects: [     
                //{id:60, type:"1", x:66, y:82},              
    ],
    onGameStart: ()=> {
        console.log("init zone change")
         window.TMP_STATE = {         
            l:1,
            z: [
                [263,90,88*88,0], //1
                [90,262,87*87,0], //2
                [263,262,87*87,0], //3
                [435,262,87*87,0], //4
                [263,435,87*87,0], //5
            ],
            o: [
                {id:60, type:"bw_circle_zone_1", x:238, y:65},
                {id:61, type:"bw_circle_zone_2", x:65, y:237},
                {id:62, type:"bw_circle_zone_3", x:238, y:237},
                {id:63, type:"bw_circle_zone_4", x:410, y:237},
                {id:64, type:"bw_circle_zone_5", x:238, y:410},
            ],
            b: [
                {id:70, type:"bw_beacon_1", x:263, y:90},
                {id:71, type:"bw_beacon_2", x:90, y:262},
                {id:72, type:"bw_beacon_3", x:263, y:262},
                {id:73, type:"bw_beacon_4", x:435, y:262},
                {id:74, type:"bw_beacon_5", x:263, y:435},
            ],
            lz:0,
            i: false,
        }      
    },
    onGameTick: (g)=> {        
        let l = window.TMP_STATE.l        
        if (!l) return;
        const initZone = () => {
            const s = window.TMP_STATE
            const maxZ = s.z.length
            let nz = s.lz
            do {
                nz = Math.floor(Math.random()*maxZ)
            } while (nz==s.lz)
            window.WLROOM.killObject(60+s.lz)
            window.WLROOM.killObject(70+s.lz)
            s.lz=nz            
            window.WLROOM.setZone(...s.z[nz])
            createObject(s.o[nz])
            createObject(s.b[nz])
            window.WLROOM.sendExtra({type:"overlay",text:`Zone changed to ${nz+1}`, color: '#00FFED'})            
            window.TMP_STATE.l =1
        }
        if (l===5 && !window.TMP_STATE.i) {
            window.TMP_STATE.i=true;
            initZone()
        }
        if (l>=1800) {
            initZone()
            return
        } 
        ++window.TMP_STATE.l;
    },
    colorAnim: defaultColorAnim,    
    materials: defaultMaterials.map(noUndef), 
    backgroundLayer: "true",
    palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,61,173,61,252,84,84,168,168,168,85,85,85,84,84,252,84,216,84,84,252,252,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,76,76,76,84,84,84,92,92,92,100,100,100,109,109,109,116,116,116,125,125,125,132,132,132,140,140,140,148,148,148,157,157,157,56,56,136,81,81,193,105,105,249,145,145,245,185,185,245,110,110,110,145,145,145,181,181,181,217,217,217,32,96,32,45,133,45,62,174,62,113,189,113,165,213,165,111,111,111,146,146,146,182,182,182,218,218,218,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,157,121,89,197,169,125,237,217,161,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,147,147,147,180,180,180,216,216,216,253,253,253,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,217,1,1,189,1,1,165,1,1,200,0,0,172,0,0,218,2,2,190,2,2,166,2,2,216,0,0,188,0,0,164,0,0,82,82,194,106,106,250,146,146,246,80,80,192,107,107,251,147,147,247,149,137,1,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,253,1,1,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,245,233,61,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,46,134,46,63,175,63,114,190,114,47,135,47,64,176,64,115,191,115,248,60,60,244,124,124,244,188,188,104,104,248,148,148,248,184,184,244,144,144,244,65,177,65,116,192,116,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,89,41,1,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,245,165,165,248,92,92,245,77,77,244,60,60,244,76,76,244,92,92,244,164,164,8,0,0,23,0,0,30,0,0,41,0,0,53,0,0,19,12,11,74,0,0,107,0,0,252,252,252,221,221,221,189,189,189,158,158,158,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,1,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,7,0,3,25,0,1,59,0,7,43,0,7,87,0,0,212,18,31,11,0,36,97,0,29,227,21,48,64,34,49,34,20,57,0,53,51,0,0,53,248,15,62,121,50,77,80,94,74,0,66,84,38,71,101,0,35,119,250,119,102,199,116,124,121,103,145,195,181,147,92,45,235,0,89,220,104,12,241,255,82,232,221,192,205,205,229,229,92,217,226,226,231,236,185,250,254,0,19,19,18,55,29,32,32,36,59,81,35,41,132,26,26,118,35,46,52,69,121,211,43,44,0,0,4,4,1,0,4,1,1,1,4,1,1,1,4,254,2,2,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],   
    mods: [   
        "kangur/dtf",
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_circle_zone_1`,`bw_circle_zone_1`, SpriteWobject.UNDERLAY),     
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_circle_zone_2`,`bw_circle_zone_2`, SpriteWobject.UNDERLAY),  
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_circle_zone_3`,`bw_circle_zone_3`, SpriteWobject.UNDERLAY),  
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_circle_zone_4`,`bw_circle_zone_4`, SpriteWobject.UNDERLAY),  
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_circle_zone_5`,`bw_circle_zone_5`, SpriteWobject.UNDERLAY),     
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_beacon_1`,`bw_beacon_1`, SpriteWobject.BEACON, SpriteWobject.ANCHOR_CENTER),     
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_beacon_2`,`bw_beacon_2`, SpriteWobject.BEACON, SpriteWobject.ANCHOR_CENTER),     
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_beacon_3`,`bw_beacon_3`, SpriteWobject.BEACON, SpriteWobject.ANCHOR_CENTER),     
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_beacon_4`,`bw_beacon_4`, SpriteWobject.BEACON, SpriteWobject.ANCHOR_CENTER),     
        new SpriteWobject(`dsds/00_sprites/bw_circle_zones/bw_beacon_5`,`bw_beacon_5`, SpriteWobject.BEACON, SpriteWobject.ANCHOR_CENTER),     
           
   ],
    tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set("https://webliero.gitlab.io/webliero-maps/dsds/material_hack/bw_jungle_fall.png", 
{
    modes:  { pred: [(new PredSettings).addLemonSpawn(349,179).setSettings(defWarExSettings)
        .addWormSpawn(21, 90)
        .addWormSpawn(137, 207)
        .addWormSpawn(18, 255)
        .addWormSpawn(110, 373)
        .addWormSpawn(357, 359)
        .addWormSpawn(347, 282)
        .addWormSpawn(347, 193)
        .addWormSpawn(583, 275)
        .addWormSpawn(521, 408)
        .addWormSpawn(519, 88)
        .addWormSpawn(628, 113)
    ] },  
    objects: [     
        {type: "bw_jungle_overlay_leaves_left",x: 0, y:  0},    
        {type: "bw_jungle_overlay_leaves_center",x: 330, y:  0},    
        {type: "bw_jungle_overlay_leaves_right",x: 504, y:  0},  
        {type: "bw_jungle_overlay_wook_bott_2",x: 284, y:  325}, 
        {type: "bw_jungle_overlay_wook_bott_1",x: 309, y:  325},  
        {type: "bw_jungle_overlay_wook_bott_1",x: 381, y:  325},  
        {type: "bw_jungle_overlay_wook_bott_2",x: 414, y:  325},  
        {type: "bw_jungle_overlay_wook_top",x: 303, y:  234},  
        {type: "bw_jungle_overlay_wook_top",x: 392, y:  234},
        {type:"tank_spawner", x:0, y:0}, 
    ],
    onGameStart: () => {   
        let weap = window.WLROOM.getWeapons()
        window.TMP_STATE = {
            weapToForce: -1,
            weapLen: weap.length-1,
            weapBan: []
        } 
        for (let w of weap) {
            if (w.name==="KNIFE") {
                window.TMP_STATE.weapToForce = w.id
                console.log(`adding weapon ${w.name} ${w.id}`)
            } else if(defWarExSettings.weapons.ban.includes(w.name)) {
                window.TMP_STATE.weapBan.push(w.id)
            }                      
        }        
    },   
    onPlayerSpawn: (player) => {
        if (!player) return
        let ret = [window.TMP_STATE.weapToForce]
        let reload = [666,666,666,666,666]
        while(ret.length!=5) {
            let wid = Math.round(Math.random()* window.TMP_STATE.weapLen)
            if (!window.TMP_STATE.weapBan.includes(wid) && !ret.includes(wid)) ret.push(wid)
        }
        window.WLROOM.setPlayerWeapons(player, ret.concat(reload));
    },
    mods: [             
       //"kangur/dtf",
         warmodExturl,    
        {from: warmodurl, options: ["merge_missing"] },       
        new SpriteWobject(`dsds/00_sprites/bw_jungle/bw_jungle_overlay_leaves_left`,'bw_jungle_overlay_leaves_left', SpriteWobject.OVERLAY),                              
        new SpriteWobject(`dsds/00_sprites/bw_jungle/bw_jungle_overlay_leaves_center`,'bw_jungle_overlay_leaves_center', SpriteWobject.OVERLAY), 
        new SpriteWobject(`dsds/00_sprites/bw_jungle/bw_jungle_overlay_leaves_right`,'bw_jungle_overlay_leaves_right', SpriteWobject.OVERLAY), 
        new SpriteWobject(`dsds/00_sprites/bw_jungle/bw_jungle_overlay_wook_bott_1`,'bw_jungle_overlay_wook_bott_1', SpriteWobject.OVERLAY), 
        new SpriteWobject(`dsds/00_sprites/bw_jungle/bw_jungle_overlay_wook_bott_2`,'bw_jungle_overlay_wook_bott_2', SpriteWobject.OVERLAY), 
        new SpriteWobject(`dsds/00_sprites/bw_jungle/bw_jungle_overlay_wook_top`,'bw_jungle_overlay_wook_top', SpriteWobject.OVERLAY), 

        ],  
   // fxs: ["addbg#40#0#0#0#0"],
    materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(188,193))).map(replaceMatIndexBy(MATERIAL.BG,..._range(168,171))),
    palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,61,173,61,252,84,84,168,168,168,85,85,85,84,84,252,84,216,84,84,252,252,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,76,76,76,84,84,84,92,92,92,100,100,100,109,109,109,116,116,116,125,125,125,132,132,132,140,140,140,148,148,148,157,157,157,56,56,136,81,81,193,105,105,249,145,145,245,185,185,245,110,110,110,145,145,145,181,181,181,217,217,217,32,96,32,45,133,45,62,174,62,113,189,113,165,213,165,111,111,111,146,146,146,182,182,182,218,218,218,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,157,121,89,197,169,125,237,217,161,130,107,53,176,188,105,189,164,112,200,100,0,160,80,0,72,72,72,108,108,108,147,147,147,180,180,180,216,216,216,253,253,253,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,217,1,1,189,1,1,165,1,1,200,0,0,172,0,0,218,2,2,190,2,2,166,2,2,216,0,0,188,0,0,164,0,0,82,82,194,106,106,250,146,146,246,80,80,192,107,107,251,147,147,247,149,137,1,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,253,1,1,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,245,233,61,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,46,134,46,63,175,63,114,190,114,50,110,36,81,142,45,131,180,68,248,60,60,244,124,124,244,188,188,104,104,248,148,148,248,184,184,244,144,144,244,65,177,65,116,192,116,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,89,41,1,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,245,165,165,248,92,92,245,77,77,244,60,60,244,76,76,244,92,92,244,164,164,19,25,11,31,39,32,33,40,24,48,56,48,41,60,34,54,57,38,66,74,64,67,76,52,49,56,49,49,56,49,67,74,67,90,94,90,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,1,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,3,0,5,10,3,15,21,15,89,94,86,93,97,73,124,127,114,28,39,28,39,58,39,40,84,39,80,91,39,23,17,15,25,18,3,38,32,22,47,31,6,48,31,20,34,36,13,62,41,15,47,49,26,63,49,31,74,52,18,98,71,31,93,76,51,125,105,74,148,135,106,159,138,87,188,158,83,243,215,135,224,212,200,29,20,11,49,18,8,69,33,21,82,33,16,112,53,29,137,48,26,105,74,47,166,72,40,118,88,30,144,79,45,200,106,60,161,131,69,185,146,45,234,142,83,238,201,66,4,1,1,1,4,1,1,1,4,254,2,2,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
    backgroundLayer: "https://webliero.gitlab.io/webliero-maps/dsds/material_hack/bw_jungle_fall_backgroundlayer.png",
    colorAnim: defaultColorAnim,    
    tags: ['main', 'classic', 'war']
})

mapSettings.set("dsds/material_hack/bw5.png", 
{
    modes:  { pred: [(new PredSettings).addLemonSpawn(421,410).setSettings(defDTFPredSettings)
    ] },  
    objects: [     
        {type: "bw5_barrier",x: 662, y:  352},    
        {wlx:"teleport",x:8,y:182,dist:2,toX:708,toY:327},
        {wlx:"teleport",x:734,y:327,dist:2,toX:40,toY:182},
    ],
    mods: [             
       "kangur/dtf", 
       "kangur/_teleport",    
        new SpriteWobject(`dsds/00_sprites/bw5/bw5_barrier`,'bw5_barrier', SpriteWobject.OVERLAY),                              

        ],  
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.BG,..._range(188,201))).map(replaceMatIndexBy(MATERIAL.ROCK,..._range(202,221))),
    palette: true,
    backgroundLayer: "true",
    colorAnim: defaultColorAnim,    
    tags: ['main', 'classic', 'dtfmod']
})


var aim_trainer_settings = {
    timeLimit: 999,
    scoreLimit: 3,
    respawnDelay: 3,
    loadingTimes: 0.4,
    weaponChangeDelay: 0,
    maxDuplicateWeapons: 0,
    forceRandomizeWeapons: false,
    bonusDrops:"health",
    bonusSpawnFrequency: 3,
    gameMode: "lms",
	weapons:{ban:["STATIC AIM TRAINER","STATIC AND DYNAMIC AIM TRAINER", "DYNAMIC AIM TRAINER", "ADVENTURE AIM TRAINER EASY", "ADVENTURE AIM TRAINER NORMAL", "ADVENTURE AIM TRAINER HARD", "ADVENTURE AIM TRAINER IMPOSSIBLE"]}
};

var wgetch_maps_aim_trainer = [
	"aim_trainer/aim_trainer_bunker",
	"aim_trainer/roomu_zone",
	"aim_trainer/roomu_zone_medium",
	"aim_trainer/roomu_zone_small",
	"dm/roomu_mob_alt",
];

for(var name of wgetch_maps_aim_trainer) {
	mapSettings.set(`https://webliero.gitlab.io/webliero-maps/wgetch/ext/${name}.png`,{
		modes: {
			lms: [
				(new SimpleSettings())
					.setSettings(aim_trainer_settings)
			],
		},
		palette:true,
		colorAnim: [129,132,133,136,152,159,168,171],
		backgroundLayer:"true",
		materials: defaultMaterials
			.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(188,243)))
			.map(replaceMatIndexBy(MATERIAL.BG,..._range(244,255))),
		mods: [
			"Larcelo/aim_trainer"
		],
		tags: ['aim_trainer'],
		subMenu: 'aim_trainer'
	});
}

mapSettings.set("https://webliero.gitlab.io/webliero-maps/dsds/AIMTRAIN.LEV",{
	modes: {
		lms: [
			(new SimpleSettings())
				.setSettings(aim_trainer_settings)
		],
	},
	colorAnim: [129,132,133,136,152,159,168,171],
	mods: [
		"Larcelo/aim_trainer"
	],
	tags: ['aim_trainer'],
    subMenu: 'aim_trainer'
});

mapSettings.set(`${olx_base}Pirates.png`,{
    modes: {
        lms: [
            (new SimpleSettings())
                .setSettings(aim_trainer_settings)
        ],
    },
    palette:true,
    colorAnim: [129,132,133,136,152,159,168,171],
    backgroundLayer: `${olx_base}Pirates-background.png`,
    materials: defaultMaterials
        .map(replaceMatIndexBy(MATERIAL.DIRT,..._range(192,223)))
        .map(replaceMatIndexBy(MATERIAL.ROCK,..._range(224,255)))
        .map(replaceMatIndexBy(MATERIAL.BG, 3,4,5,6,7,8,9,10,11,39,40,41,42,43,44,45,46,47,120,121,122,123,124,125,160,161,162,163,164,165,166,167)),
    palette: [0,0,0,108,56,0,108,80,0,0,0,1,65,38,21,16,24,18,5,17,4,27,36,35,49,82,39,46,52,52,89,143,60,57,76,77,120,64,8,128,68,8,136,72,12,144,80,16,152,84,20,160,88,24,172,96,28,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,90,90,90,144,144,144,180,180,180,216,216,216,8,17,37,112,135,219,2,7,3,176,192,220,5,11,17,70,117,57,34,50,67,24,44,18,17,30,5,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,157,120,88,197,168,124,237,216,160,200,100,0,160,80,0,72,72,72,108,109,108,145,144,144,181,180,180,217,216,216,252,252,252,196,196,196,144,145,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,217,0,0,189,0,0,165,0,0,216,1,0,188,1,0,164,1,0,81,80,192,105,104,248,145,144,244,80,81,192,104,105,248,144,145,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,36,62,36,18,12,4,155,92,52,37,62,20,217,163,133,126,71,39,248,60,60,244,124,124,244,188,188,104,104,249,144,144,245,185,184,244,145,145,244,60,172,60,112,188,112,164,212,164,113,188,112,149,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,61,172,60,252,200,200,244,164,164,248,92,92,244,76,76,244,60,60,245,76,76,244,92,92,245,164,164,93,103,95,113,179,71,97,112,139,130,143,168,172,127,83,65,101,40,248,240,16,67,89,111,253,252,252,220,220,220,188,188,188,157,156,156,125,124,124,156,157,156,189,188,188,221,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,2,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,245,232,60,0,1,0,1,1,0,1,1,1,0,1,1,26,26,24,96,80,64,144,136,104,72,104,72,120,104,80,168,160,128,24,36,36,72,56,32,40,56,56,248,223,138,48,72,72,248,184,96,64,88,88,80,104,104,64,48,24,48,40,16,96,120,120,136,152,152,112,136,136,88,72,56,112,48,32,176,192,192,176,104,96,248,0,248,200,128,136,0,2,0,0,0,2,79,107,79,2,2,0,2,2,2,0,2,2,103,87,67,63,31,31,95,40,31,3,0,0,128,74,45,51,23,15,103,56,32,20,81,111,36,17,8,176,140,116,36,103,117,113,49,34,48,83,42,153,99,67,66,118,54,78,49,55,112,79,88,78,47,39,134,118,105,71,32,23,63,32,40,47,16,24,187,192,184,50,122,130,56,44,19,30,51,30,3,51,93,36,39,37,92,151,158,94,69,69,112,106,82,45,15,23,99,49,40],
    mods: [
        "dsds/_aimtrainer_pirate_skin",
    ],
    tags: ['aim_trainer'],
    subMenu: 'aim_trainer'
});

var olx_maps_aim_trainer = [
	"747",
	"Deth_City",
	"Fossil_Facility_2nd_section",
	"Lamda__Razvisited_",
	"Liero_Factory__Revisited_",
	"Lieroplatz_metro",
	"Sunrise_Mansion",
	"WormWood_Park",
	"zzzzzzzzzzzzzz",
];

for(var name of olx_maps_aim_trainer) {
	let olx_map_url = olx_base + name + ".png";
	let olx_bg_url = olx_base + name + "-background.png";

	mapSettings.set(olx_map_url,{
		modes: {
			lms: [
				(new SimpleSettings())
					.setSettings(aim_trainer_settings)
			],
		},
		palette:true,
		colorAnim: [129,132,133,136,152,159,168,171],
		backgroundLayer: olx_bg_url,
		materials: defaultMaterials
			.map(replaceMatIndexBy(MATERIAL.DIRT,..._range(192,223)))
			.map(replaceMatIndexBy(MATERIAL.ROCK,..._range(224,255)))
			.map(replaceMatIndexBy(MATERIAL.BG, 3,4,5,6,7,8,9,10,11,39,40,41,42,43,44,45,46,47,120,121,122,123,124,125,160,161,162,163,164,165,166,167)),
		mods: [
			"Larcelo/aim_trainer"
		],
		tags: ['aim_trainer'],
		subMenu: 'aim_trainer'
	});
}

mapSettings.set(`dsds/material_hack/defuse.png`,{
    modes: {
        defuse: [
            (new DefuseSettings())
                .setBombSpawn(673, 368)
                .addPlantingZone(1215, 460)
                .addBlueSpawn(145, 480)
                .addBlueSpawn(204, 480)
                .addBlueSpawn(267, 480)
                .addBlueSpawn(199, 778)
                .addBlueSpawn(371, 130)
                .addBlueSpawn(539, 210)
                .addBlueSpawn(690, 100)

                .addGreenSpawn(1128, 230)
                .addGreenSpawn(1290, 230)
                .addGreenSpawn(1209, 460)
                .addGreenSpawn(1209, 619)
                .addGreenSpawn(1128, 539)
                .addGreenSpawn(1290, 539)
                .setSettings({bonusDrops:"healthAndWeapons"})
        ],
    },
    palette:true,
    backgroundLayer: "true",
    materials: defaultMaterials
        .map(noUndef)
        .map(replaceMatIndexBy(MATERIAL.BG,..._range(189,200))),
    mods: [
        "kangur/dtf",
        "kangur/dtb_template",
    ],
    tags: ['classic', 'big', 'dtfmod'],
});

mapSettings.set("https://webliero.gitlab.io/webliero-maps/神風/cs/cs_ash.png#defuseB", {
    modes: { defuse: [
            (new DefuseSettings())
                .setBombSpawn(884, 408)
                .addPlantingZone(215, 408)

                .addBlueSpawn(897, 390)
                .addBlueSpawn(1017, 258)
                .addBlueSpawn(982, 380)
                .addBlueSpawn(1000, 500)
                .addBlueSpawn(1060, 440)

	            .addGreenSpawn(183, 390)
                .addGreenSpawn(80, 258)
                .addGreenSpawn(127, 380)
                .addGreenSpawn(100, 500)
                .addGreenSpawn(40, 440)
                .setSettings({bonusDrops:"healthAndWeapons"})
        ],
       defuse: [
            (new DefuseSettings())
                .setBombSpawn(215, 408)
	            .addPlantingZone(884, 408)

                .addBlueSpawn(897, 390)
                .addBlueSpawn(1017, 258)
                .addBlueSpawn(982, 380)
                .addBlueSpawn(1000, 500)
                .addBlueSpawn(1060, 440)

	            .addGreenSpawn(183, 390)
                .addGreenSpawn(80, 258)
                .addGreenSpawn(127, 380)
                .addGreenSpawn(100, 500)
                .addGreenSpawn(40, 440)
                .setSettings({bonusDrops:"healthAndWeapons"})
        ],
},
    mods: [
            "kangur/defuse_the_bomb",
        ],
    palette: [0,0,0,108,56,0,108,80,0,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,136,212,252,152,220,252,164,232,252,176,236,252,188,252,252,212,252,252,224,252,252,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,136,212,252,136,212,252,152,220,252,164,232,252,176,236,252,188,252,252,212,252,252,224,252,252,28,28,28,32,32,32,32,32,32,36,36,36,0,0,0,0,0,0,0,0,0,0,0,0,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,108,76,44,124,84,48,140,96,56,156,108,64,172,120,72,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,252,252,252,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,252,252,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
    tags: ['main', 'classic', 'cs']
})

mapSettings.set(`https://webliero.gitlab.io/webliero-maps/dsds/material_hack/lava_death_trap.png`, {
    modes: {  haz: [
        (new HazSettings())
        .setZone(328,235,78*78, 0)
        .addSpawn(328,77)
        .addSpawn(207,104)
        .addSpawn(443,104)
        .addSpawn(150,235)
        .addSpawn(504,235)
        .addSpawn(169,320)
        .addSpawn(488,320)
        .addSpawn(241,305)
        .addSpawn(418,305)        
        .addSpawn(328,235)
        .addSpawn(190,125)
        .addSpawn(468,125)
        .addSpawn(192,255)
        .addSpawn(463,255)
        .addSpawn(328,136)
        .setSettings(defDTFHazSettings)
    ], },
    objects: [
        {type:"ldt_lava", x:233, y:432},
        {type:"ldt_bg", x:263, y:325},
        {type:"ldt_bg_center", x:265, y:174},
        {type:"lava_trap", x:265, y:174},
    ],
    materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(188,204))),
    palette: true,
    mods: [ 
        "kangur/dtf",
        new AnimatedSpritesWobject(`https://sylvodsds.gitlab.io/webliero-extended-mods/dsds/01_anim/lava_death_trap_lava.zip`, 'ldt_lava', AnimatedSpritesWobject.OVERLAY, 15),         
        new AnimatedSpritesWobject(`https://sylvodsds.gitlab.io/webliero-extended-mods/dsds/01_anim/lava_death_trap_background.zip`, 'ldt_bg', AnimatedSpritesWobject.UNDERLAY, 15),
        (new ComplexAnimatedSpritesWobject(`https://sylvodsds.gitlab.io/webliero-extended-mods/dsds/01_anim/lava_death_trap_center.zip`, 'ldt_bg_center', AnimatedSpritesWobject.UNDERLAY, 9))
                .setBaseAnim(_range(0, 20))
                .addAnim([..._range(21, 29),..._range(9,20)])
                .addAnim([..._range(0, 8),..._range(30,41)])
                .addAnim([..._range(42, 54),..._range(13,20)])
                .addAnim([..._range(55, 73),..._range(10,20)])
                .addAnim([..._range(0, 9),..._range(74,80),..._range(17,20)])
                .addAnim([..._range(81, 103)]),
        "dsds/_lava_death_trap"
       ],
    tags: ['main', 'classic', 'dtfmod']
})

mapSettings.set(`dsds/material_hack/digging_basro.png`, 
    {
        modes: { duel: [
            (new DuelSettings())                       
            .addSpawn(773, 773)            
            .addSpawn(510, 870)            
            .addSpawn(815, 870)            
            .addSpawn(908, 870)
            .addSpawn(1015, 870)
            .addSpawn(713, 783)
            .addSpawn(861, 830)
            .setSettings({                              
                scoreLimit: 3,
                weaponChangeDelay: 90,            
            })
            .setTrainingSettings({   
                scoreLimit: 8,
                timeLimit: 5,            
                gameMode: "lms",
                weaponChangeDelay: 90,
            })       
        ],     
    
},
// onGameTick: (g) => {      
//    let players = getActivePlayers();        
//    for (let p of players) {
//        if (p.x==null || p.y==null) continue
//        if (p.x<5 || p.x>1490 || p.y<80 || p.y>1250) {
//            console.log(p.x, p.y)
//            window.WLROOM.setPlayerHealth(p.id, 0)
//        }
//    }
// },
objects: (() => {      
    let obj = []
    for (let x = 0; x<=1500; x+=50)  { 
        obj.push({type:"blacklineofdeath", x:x, y:1300})
        obj.push({type:"blacklineofdeath", x:x, y:80})
        obj.push({type:"vertical_line", x:1, y:x})
        obj.push({type:"vertical_line", x:1499, y:x})
     }
        return obj
     })(),
 palette: true,
 materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.DIRT,..._range(188,203))),
 mods: [             
            "https://sylvodsds.gitlab.io/webliero-extended-mods/%E7%A5%9E%E9%A2%A8/dirts.zip",           
           "https://sylvodsds.gitlab.io/webliero-extended-mods/dsds/_blacklineofdeath.zip",
           new LineWobject('vertical_line', 0, 0, 50, 1, 50, 666, 10)                  
        ],    
    tags: ['classic', 'dirts']
})

mapSettings.set(`dsds/material_hack/diggin_basro_and_friends.png`, 
    {
        modes: { duel: [
            (new DuelSettings())                       
            .addSpawn(763, 728)            
            .addSpawn(766, 859)            
            .addSpawn(625, 850)            
            .addSpawn(890, 821)
            .addSpawn(756, 902)
            .setSettings({                              
                scoreLimit: 3,
                weaponChangeDelay: 90,            
            })
            .setTrainingSettings({   
                scoreLimit: 8,
                timeLimit: 5,            
                gameMode: "lms",
                weaponChangeDelay: 90,
            })       
        ],     
    
},
// onGameTick: (g) => {      
//    let players = getActivePlayers();        
//    for (let p of players) {
//        if (p.x==null || p.y==null) continue
//        if (p.x<5 || p.x>1490 || p.y<80 || p.y>1250) {
//            console.log(p.x, p.y)
//            window.WLROOM.setPlayerHealth(p.id, 0)
//        }
//    }
// },
objects: (() => {      
    let obj = []
    for (let x = 0; x<=1500; x+=50)  { 
        obj.push({type:"blacklineofdeath", x:x, y:1300})
        obj.push({type:"blacklineofdeath", x:x, y:80})
        obj.push({type:"vertical_line", x:1, y:x})
        obj.push({type:"vertical_line", x:1499, y:x})
     }
        return obj
     })(),
 palette: true,
 materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.DIRT,..._range(188,203))),
 mods: [             
            "https://sylvodsds.gitlab.io/webliero-extended-mods/%E7%A5%9E%E9%A2%A8/dirts.zip",           
           "https://sylvodsds.gitlab.io/webliero-extended-mods/dsds/_blacklineofdeath.zip",
           new LineWobject('vertical_line', 0, 0, 50, 1, 50, 666, 10)                  
        ],    
    tags: ['main', 'classic', 'dirts']
})



mapSettings.set(`dsds/material_hack/ai7.png`, 
{
    modes:  { dm: [
        (new SimpleSettings())
            .setSettings(defDTFDeathmatchSettings)
    ], },  
    objects: (() => {
        let ret= [
             {type: "ai7_bg_sm", x: 0, y:  0}, 
             {type: "ai7_overlay_ceiling_support_one", x: 658, y:  165}, 
             {type: "ai7_overlay_ceiling_support_two", x: 713, y:  165}, 
             {type: "ai7_screen", x: 838, y:  400}, 
        ]
        for (let x=774+250; x<=860+250; x+=43) {
            ret.push( {type: "ai7_underlay_support", x: x, y:  382+150}    )
            ret.push( {type: "ai7_overlay_support", x: x+11, y:  373+150}    )
        }
        for (let x=280; x<=1150; x-=120) {
            for (let y=584; y>=178; y-=18) {
                ret.push( {type: "ai7_underlay_indus", x: x, y:  y})
                x+=18
            }
        }
        return ret
    })(),
    mods: [             
       "kangur/dtf",   
        new ParallaxSpriteWobject(`dsds/00_sprites/ai7/ai7_bg_sm2`,'ai7_bg_sm', 0, 0, 1500, 800),                              
        new SpriteWobject(`dsds/00_sprites/ai7/ai7_overlay_support`,`ai7_overlay_support`, SpriteWobject.OVERLAY, true),
        new SpriteWobject(`dsds/00_sprites/ai7/ai7_underlay_support`,`ai7_underlay_support`, SpriteWobject.UNDERLAY, true),
        new SpriteWobject(`dsds/00_sprites/ai7/ai7_overlay_ceiling_support_one`,`ai7_overlay_ceiling_support_one`, SpriteWobject.OVERLAY, true),
        new SpriteWobject(`dsds/00_sprites/ai7/ai7_overlay_ceiling_support_two`,`ai7_overlay_ceiling_support_two`, SpriteWobject.OVERLAY, true),
        new SpriteWobject(`dsds/00_sprites/ai7/ai7_underlay_indus`,`ai7_underlay_indus`, SpriteWobject.UNDERLAY, true),
        (new ComplexAnimatedSpritesWobject(`https://sylvodsds.gitlab.io/webliero-extended-mods/dsds/01_anim/ai7_screen.zip`, 'ai7_screen', AnimatedSpritesWobject.OVERLAY, 7))
                .setBaseAnim([4,4,4,4,4,4,4,4])
                .addAnim([..._range(0, 3),..._range(0, 3),..._range(0, 3)])
                .addAnim([5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6])
                .addAnim([..._range(7, 19),18,19,18,19,18,19,18,19,18,19,18,19,18,19])              ,
        ],  
    materials: defaultMaterials.map(replaceMatIndexBy(MATERIAL.ROCK,181)).map(replaceMatIndexBy(MATERIAL.ROCK,..._range(189,195))),
    palette: true,
    colorAnim: defaultColorAnim,    
    tags: ['main', 'classic', 'dtfmod']
})

/*
mapSettings.set(`https://webliero.gitlab.io/webliero-maps/dsds/turdmustdie.png`,{
    modes: {
        pred: [
            (new PredSettings()).addLemonSpawn(421,410)
        ],
    },
    //palette:true,
    objects: [
        // {type:"tank_tiger", x:221, y:210},
        // {type:"tank_tiger", x:240, y:310},
        {type:"e1_zone_bg", x:20, y:20},
    ],
    backgroundLayer: "true",
    colorAnim: defaultColorAnim,    
    mods: [
        // {
        //                 name: "Larcelo/war_mod_tanks#3",
        //                 json:`${ngrokMUrl}Larcelo/tmp/mod.json5#3`,
        //                 sprites:`${ngrokMUrl}Larcelo/tmp/sprites.wlsprt#3`
        //             },
                    "https://www.vgm-quiz.com/dev/webliero/wledit/mods/behaviors0.39.zip?5",
                   
        new SpriteWobject(`dsds/00_sprites/e1/e1_zone_bg`,'e1_zone_bg', SpriteWobject.OVERLAY, true)
    ],
    tags: ['main', 'tank'],
});

mapSettings.set(`https://webliero.gitlab.io/webliero-maps/神風/cs/cs_desertbase.png`,{
    modes: {
        pred: [
            (new PredSettings()).addLemonSpawn(421,410)
        ],
    },
    //palette:true,
    objects: [
        {type:"tank_tiger", x:421, y:410},
        {type:"tank_tiger", x:440, y:410},
        {type:"tank_tiger", x:500, y:210},
        {type:"tank_tiger", x:540, y:310},
        //{type:"e1_zone_bg", x:421, y:500},
    ],
    backgroundLayer: "true",
    colorAnim: defaultColorAnim,    
    mods: [
        {
                        name: "Larcelo/war_mod_tanks#3",
                        json:`${ngrokMUrl}Larcelo/tmp/mod.json5#3`,
                        sprites:`${ngrokMUrl}Larcelo/tmp/sprites.wlsprt#3`
                    },
                    {from:"https://www.vgm-quiz.com/dev/webliero/wledit/mods/behaviors0.39.zip?5", behaviors:["player info"]}
                   // {from:"https://www.vgm-quiz.com/dev/webliero/wledit/mods/behaviors0.39.zip?5", behaviors:["player info"]}
       // new SpriteWobject(`dsds/00_sprites/e1/e1_zone_bg`,'e1_zone_bg', SpriteWobject.UNDERLAY)
    ],
    tags: ['main', 'tank'],
});
*/
// mapSettings.set("https://webliero.gitlab.io/webliero-maps/Larcelo/Aqualiris_alt.lev", {
//     modes: {
//         racing: [
//             (new RacingSettings())
//                 .addNumberLaps(3)
//                 .addSpawn(347,271)                
//                 .addStartZone(312, 212, 383, 343)
//                 .addStep(251, 208, 277, 345)                 
//                 .addStep(2, 198, 85, 221)                 
//                 .addStep(85, 221, 277, 345)                 
//                 .addStep(130, 87, 159, 156) 
//                 .addStep(258, 87, 289, 142) 
//                 .addStep(223, 38, 275, 85) 
//                 .addStep(103, 39, 140, 85) 
//                 .addStep(0, 51, 56, 102) 
//                 .addStep(86, 0, 121, 36)
//                 .addStep(328, 0, 398, 35)
//                 .addStep(460, 56, 503, 109)
//                 .addStep(382, 199, 485, 252)
//                 .addStep(443, 292, 480, 346)
                
                
//                 .addStep(279, 208, 311, 345) //arrival

//                 .setSettings(
//                     {                        
//                         timeLimit: 10,
//                         maxDuplicateWeapons: 5,
//                         loadingTimes: 0.4,
//                         weapons: {
//                             only: ["NO WEAPON"]
//                         }
//                     }
//                 )
//         ],
//     }, 
//     onPlayerSpawn: (player) => {
//         //forces weapon 1 to be "accelerate" and weapon 2 to be "jetpack"   
//         let md =  modCache.get(currentModTK).wlk     
//         let nw = md.weapons.getBy('name', "NO WEAPON").id
//         window.WLROOM.setPlayerWeapons(player, [md.weapons.getBy('name', "VZ ONE PAWNEE").id,nw,nw,nw,nw,1,1,1,1,1]);
//         // -- push pawnee to player       
       
//         // setTimeout(()=>{
//         //     let p = window.WLROOM.getPlayer(player.id)
//         //     console.log("--player spawn", p.x, p.y, player.id)
//         //     createObject({type: 'pawnee', x: p.x, y: p.y, owner: player.id})   
//         // },100)
        
        
//     },
//     objects: [     
//         {type:"start_line", x:279, y:207},
//         {type:"no_go_line", x:384, y:268},        
//     ],  
//     mods: [   
//          "kangur/dtf",
//         // warmodurl,    
//         // {from: warmodExturl, options: ["merge_all","replace_existing_weapons"] },
//          {from:  "Larcelo/pawnee", options: ["merge_all","replace_existing_weapons"]},
//         // {
//         //     name: "pawnee#20",
//         //     json:"https://2a92-2a01-e0a-471-98e0-88f1-7ca4-e2e1-49c5.eu.ngrok.io/mods/Larcelo/pawnee/mod.json5",
//         //     sprites:"https://2a92-2a01-e0a-471-98e0-88f1-7ca4-e2e1-49c5.eu.ngrok.io/mods/Larcelo/pawnee/sprites.wlsprt"
//         // },
//        { from: "神風/speedrun", weapons: [2]},
//        new LineWobject('start_line', 50, 0, 134, 15),
//        new LineWobject('no_go_line', 92, 0, 63, 15),   
//     ],
//     colorAnim: false,
//     palette: true,
//     materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(188,202))),
//     tags: ['racing', 'speedrun']
// });

// mapSettings.set("https://webliero.gitlab.io/webliero-maps/Larcelo/Tatooine2.lev", {
//     modes: {
//         racing: [
//             (new RacingSettings())
//                 .addNumberLaps(3)
//                 .addSpawn(180,263)                
//                 .addStartZone(151, 245, 210, 274)
//                 .addStep(292, 237, 329, 292)     
//                 .addStep(249, 285, 283, 347)     
//                 .addStep(139, 280, 187, 350)     
//                 .addStep(7, 307, 71, 346)     
//                 .addStep(37, 228, 112, 262)     
//                 .addStep(73, 115, 121, 168)     
//                 .addStep(189, 124, 232, 182)     
//                 .addStep(137, 51, 192, 102)     
//                 .addStep(60, 5, 107, 53)     
//                 .addStep(209,1, 250, 46)     
//                 .addStep(276, 75, 339, 114)     
//                 .addStep(345, 131, 387, 191)     
//                 .addStep(421, 65,499, 106)     
//                 .addStep(363, 284, 417, 344)     
//                 .addStep(290, 188, 332, 234)     
//                 .addStep(151, 185, 196, 239)     
               
//                 .addStep(211, 245, 231, 274) //arrival

//                 .setSettings(
//                     {                        
//                         timeLimit: 10,
//                         maxDuplicateWeapons: 5,
//                         loadingTimes: 0.4,
//                         weapons: {
//                             only: ["NO WEAPON"]
//                         }
//                     }
//                 )
//         ],
//     }, 
//     onPlayerSpawn: (player) => {
//         //forces weapon 1 to be "accelerate" and weapon 2 to be "jetpack"   
//         let md =  modCache.get(currentModTK).wlk     
//         let nw = md.weapons.getBy('name', "NO WEAPON").id
//         window.WLROOM.setPlayerWeapons(player, [md.weapons.getBy('name', "VZ ONE PAWNEE").id,nw,nw,nw,nw,1,1,1,1,1]);
//         // -- push pawnee to player       
       
//         // setTimeout(()=>{
//         //     let p = window.WLROOM.getPlayer(player.id)
//         //     console.log("--player spawn", p.x, p.y, player.id)
//         //     createObject({type: 'pawnee', x: p.x, y: p.y, owner: player.id})   
//         // },100)
        
        
//     },
//     objects: [     
//         {type:"start_line", x:211, y:245},
//         {type:"no_go_line", x:151, y:245},        
//     ],  
//     mods: [   
//         "kangur/dtf",
//         //  warmodurl,    
//         //  {from: warmodExturl, options: ["merge_all","replace_existing_weapons"] },
//          {from:  "Larcelo/pawnee", options: ["merge_all","replace_existing_weapons"]},
//         // {
//         //     name: "pawnee#20",
//         //     json:"https://2a92-2a01-e0a-471-98e0-88f1-7ca4-e2e1-49c5.eu.ngrok.io/mods/Larcelo/pawnee/mod.json5",
//         //     sprites:"https://2a92-2a01-e0a-471-98e0-88f1-7ca4-e2e1-49c5.eu.ngrok.io/mods/Larcelo/pawnee/sprites.wlsprt"
//         // },
//        { from: "神風/speedrun", weapons: [2]},
//        new LineWobject('start_line', 50, 0, 29, 15),
//        new LineWobject('no_go_line', 92, 0, 29, 15),   
//     ],
//     colorAnim: false,
//     palette: true,
//     materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(188,202))),
//     tags: ['racing', 'speedrun']
// });



// mapSettings.set("https://webliero.gitlab.io/webliero-maps/Larcelo/Tatooine2.lev#alt", {
//     modes: {
//         racing: [
//             (new RacingSettings())
//                 .addNumberLaps(3)
//                 .addSpawn(180,263)                
//                 .addStartZone(151, 245, 210, 274)
//                 .addStep(292, 237, 329, 292)     
//                 .addStep(249, 285, 283, 347)     
//                 .addStep(139, 280, 187, 350)     
//                 .addStep(7, 307, 71, 346)     
//                 .addStep(37, 228, 112, 262)     
//                 .addStep(73, 115, 121, 168)     
//                 .addStep(189, 124, 232, 182)     
//                 .addStep(137, 51, 192, 102)     
//                 .addStep(60, 5, 107, 53)     
//                 .addStep(209,1, 250, 46)     
//                 .addStep(276, 75, 339, 114)     
//                 .addStep(345, 131, 387, 191)     
//                 .addStep(421, 65,499, 106)     
//                 .addStep(363, 284, 417, 344)     
//                 .addStep(290, 188, 332, 234)     
//                 .addStep(151, 185, 196, 239)     
               
//                 .addStep(211, 245, 231, 274) //arrival

//                 .setSettings(
//                     {                        
//                         timeLimit: 10,
//                         maxDuplicateWeapons: 5,
//                         loadingTimes: 0.4,
//                         weapons: {
//                             only: ["NO WEAPON"]
//                         }
//                     }
//                 )
//         ],
//     }, 
//     onPlayerSpawn: (player) => {
//         //forces weapon 1 to be "accelerate" and weapon 2 to be "jetpack"   
//         // let md =  modCache.get(currentModTK).wlk     
//         // let nw = md.weapons.getBy('name', "NO WEAPON").id
//         // window.WLROOM.setPlayerWeapons(player, [md.weapons.getBy('name', "VZ ONE PAWNEE").id,nw,nw,nw,nw,1,1,1,1,1]);
//         // -- push pawnee to player       
       
//         setTimeout(()=>{
//             let p = window.WLROOM.getPlayer(player.id)
//             console.log("--player spawn", p.x, p.y, player.id)
//             createObject({type: 'pawnee', x: p.x, y: p.y, owner: player.id})   
//         },100)
        
        
//     },
//     objects: [     
//         {type:"start_line", x:211, y:245},
//         {type:"no_go_line", x:151, y:245},        
//     ],  
//     mods: [   
//         "kangur/dtf",
//         //  warmodurl,    
//         //  {from: warmodExturl, options: ["merge_all","replace_existing_weapons",{remove_weapon:"VZ ONE PAWNEE"}] },
//          {from:  "Larcelo/pawnee", wObjects: {0:"pawnee"}},
//         // {
//         //     name: "pawnee#20",
//         //     json:"https://2a92-2a01-e0a-471-98e0-88f1-7ca4-e2e1-49c5.eu.ngrok.io/mods/Larcelo/pawnee/mod.json5",
//         //     sprites:"https://2a92-2a01-e0a-471-98e0-88f1-7ca4-e2e1-49c5.eu.ngrok.io/mods/Larcelo/pawnee/sprites.wlsprt"
//         // },
//        { from: "神風/speedrun", weapons: [2]},
//        new LineWobject('start_line', 50, 0, 29, 15),
//        new LineWobject('no_go_line', 92, 0, 29, 15),   
//     ],
//     colorAnim: false,
//     palette: [72,44,36,84,52,44,112,104,52,164,148,128,0,144,0,60,172,60,252,84,84,168,168,168,84,84,84,84,84,252,84,216,84,84,252,252,84,56,24,96,64,28,108,68,32,120,76,32,128,84,36,140,88,40,148,96,44,76,76,76,84,84,84,92,92,92,100,100,100,108,108,108,116,116,116,124,124,124,132,132,132,140,140,140,148,148,148,156,156,156,56,56,136,80,80,192,104,104,248,144,144,244,184,184,244,108,108,108,144,144,144,180,180,180,216,216,216,32,96,32,44,132,44,60,172,60,112,188,112,164,212,164,108,108,108,144,144,144,180,180,180,216,216,216,168,168,248,208,208,244,252,252,244,60,80,0,88,112,0,116,144,0,148,176,0,120,72,52,156,120,88,196,168,124,236,216,160,156,120,88,196,168,124,236,216,160,200,100,0,160,80,0,72,72,72,108,108,108,144,144,144,180,180,180,216,216,216,252,252,252,196,196,196,144,144,144,152,60,0,180,100,0,208,140,0,236,180,0,168,84,0,216,0,0,188,0,0,164,0,0,200,0,0,172,0,0,216,0,0,188,0,0,164,0,0,216,0,0,188,0,0,164,0,0,80,80,192,104,104,248,144,144,244,80,80,192,104,104,248,144,144,244,148,136,0,136,124,0,124,112,0,116,100,0,132,92,40,160,132,72,188,176,104,216,220,136,248,248,188,244,244,252,252,0,0,248,24,4,248,52,8,248,80,16,248,108,20,248,136,24,248,164,32,248,192,36,248,220,40,244,232,60,244,244,80,244,244,112,244,244,148,240,240,180,240,240,212,240,240,248,44,132,44,60,172,60,112,188,112,44,132,44,60,172,60,112,188,112,248,60,60,244,124,124,244,188,188,104,104,248,144,144,244,184,184,244,144,144,244,60,172,60,112,188,112,164,212,164,112,188,112,148,136,0,136,116,0,124,96,0,112,76,0,100,56,0,88,40,0,104,104,136,144,144,192,188,188,248,200,200,244,220,220,244,40,112,40,44,132,44,52,152,52,60,172,60,252,200,200,244,164,164,248,92,92,244,76,76,244,60,60,244,76,76,244,92,92,244,164,164,32,32,0,32,32,16,0,36,16,0,28,0,0,16,0,12,12,0,0,12,4,0,12,4,252,252,252,220,220,220,188,188,188,156,156,156,124,124,124,156,156,156,188,188,188,220,220,220,188,84,12,216,96,12,236,104,16,240,120,40,240,132,56,0,0,0,40,36,8,80,76,20,120,116,28,160,152,40,200,192,48,244,232,60,0,0,0,32,16,44,60,32,60,80,96,112,76,128,188,76,168,188,156,192,192,252,200,108,252,236,8,144,232,252,188,244,0,84,180,0,40,124,0,0,68,0,140,144,128,180,180,180,244,240,244,232,140,116,4,4,4,4,4,4,44,28,16,76,24,12,108,56,0,140,88,28,192,136,56,220,164,84,252,212,132,252,232,124,252,248,156,252,244,252,8,36,68,32,64,80,64,68,128,92,120,156,164,232,252,72,160,224,32,88,176,0,248,0,0,188,0,96,136,64,40,100,76,188,208,64,252,104,108,252,172,176,224,196,204,204,152,160,252,0,0,252,36,0,252,72,0,252,108,0,252,144,0,252,180,0,252,216,0,252,252,0,168,240,0,84,232,0,0,224,0,252,0,0,232,4,20,216,12,44,196,20,68,180,24,88,160,32,112,144,40,136,124,44,156,108,52,180,88,60,204,72,68,228],
//     materials: defaultMaterials.map(noUndef).map(replaceMatIndexBy(MATERIAL.BG,..._range(188,202))),
//     tags: ['racing', 'speedrun']
// });

// mapSettings.set("https://sylvodsds.gitlab.io/webliero-builder-maps/maps/bg/bg.png", {
//     modes: {
//         pred: [[252, 175]],

//     },
//     objects: [
//         {type:"start_line", x:67, y:54},
//         {type:"end_line", x:67, y:0},
//         {type:"more_line", x:167, y:20},
//         {type:"more_line", x:267, y:100},
//         {type:"one_more_line", x:0, y:100},
//         {type:"we_love_lines", x:50, y:200},
//     ],
//     mods: [ 
//         "神風/speedrun",
//         new LineWobject('start_line', 50, 0, 46, 17),
//         new LineWobject('end_line', 113, 20, 45, 17),
//         new LineWobject('more_line', 86, 120, 150, 17),
//         new LineWobject('one_more_line', 123, 320, 150, 1),
//         new LineWobject('we_love_lines', 127, 320, 0, 4),
//     ],
//     tags: ['main', 'racing', 'speedrun']
// });

// mapSettings.set("qmaps/qmap10.png", {
//     modes: {  haz: [[392,233,484,345]],},
//     backgroundLayer: "true",
//     tags: ['main']
// })

function preloadMaps() {
    for (const [m, ms] of mapSettings.entries()) {
        getMapData(getMapUrl(m))
        if (ms.backgroundLayer && (ms.backgroundLayer.substring(0,8)=="https://"||ms.backgroundLayer.substring(0,7)=="http://")) {
            getMapData(getMapUrl(ms.backgroundLayer))
        }
        createThumbnail(m)
    }
    
}

function buildTags(m) {
    let ret = []
    m.forEach(s => {
        if (typeof s.tags!= 'undefined') {
            for (const i of s.tags) {
                if (!ret.includes(i)) {
                    ret.push(i)
                }
            }
        }
        if (typeof s.modes!= 'undefined') {
            for (const [i,t] of Object.entries(s.modes)) {
                if (!ret.includes(i)) {
                    ret.push(i)
                }
            }
        }
    })
    return ret
}

function autoTags(m) {
	for(const [n, s] of m) {
		var t = s.tags||{};
		let addTag = (tag) => {
            let fixed = (tag=="ctf2")?"ctf":tag;
			if(!t.includes(fixed)) {
				t.push(fixed);
			}
		};
		for(const mode in s.modes) {
			let msl = s.modes[mode];
			if(mode=="dm") {
				for(const ms of msl) {
					let gs = ms.toJSON().settings;
					if(gs.gameMode) {
						addTag(gs.gameMode);
					} else {
						addTag(mode);
					}
				}
			} else {
				addTag(mode);
			}
		}
        //console.log(JSON.stringify([n,t]))
		s.tags = t;
	}
}

var mypool = Array.from(mapSettings.keys());
var mypoolIdx = [...mypool.sort()];

autoTags(mapSettings);
tags = buildTags(mapSettings)
console.log(JSON.stringify(tags))

if (typeof window != 'undefined') {  // browser context only
    shuffleArray(mypool)
    // preload maps
    preloadMaps()


    //console.log(JSON.stringify(mapToObj(mapSettings)))

    console.log(`reloaded settings with ${mypool.length} maps`)
    if (typeof announce == "function") announce(`reloaded settings with ${mypool.length} maps`)
    if (typeof preloadMods == "function") preloadMods()

} else { // node js context
    exports.mapSettings = mapSettings
}
