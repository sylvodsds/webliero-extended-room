var spriteCache = new Map()
async function fetchPngData(url) {   
    console.log("fetchPng "+url)
    try {
        let buf = await (await fetch(url)).arrayBuffer();
        return window.__ReadPNG(buf)
    } catch (e) {
        console.log('error fetchPng',e)
    }    
}

class SpriteWobject {
    static OVERLAY = 1;
    static UNDERLAY = 0;
    static BEACON = 2;
    static ANCHOR_AUTO = 0;
    static ANCHOR_TOP_LEFT = 1;
    static ANCHOR_CENTER = 2;
    #name = 'SPRITE'
    #uri = ''
    #data = null
    #omod = null
    #baseUrl = 'https://sylvodsds.gitlab.io/webliero-extended-mods/'
    #drawOnMap = false
    #anchorPosition = 0
    #layer = -1;
    #json = `{
        "weapons": [],
        "wObjects":
        [
          {
            // wid0
            "detectDistance": 0,
            "blowAway": 0,
            "gravity": 0,
            "exploSound": -1,
            "addSpeed": 0,
            "distribution": 0,
            "multSpeed": 1,
            "createOnExp": -1,
            "dirtEffect": -1,
            "wormExplode": false,
            "explGround": false,
            "wormCollide": false,
            "collideWithObjects": false,
            "affectByExplosions": 0,
            "bounce": 0,
            "bounceFriction": 0,
            "timeToExplo": 0,
            "timeToExploV": 0,
            "hitDamage": 0,
            "bloodOnHit": 0,
            "startFrame": 2,
            "numFrames": 0,
            "loopAnim": false,
            "shotType": 0,
            "repeat": 1,
            "colorBullets": 0,
            "splinterAmount": 1,
            "splinterColour": 50,
            "splinterType": -1,
            "splinterScatter": 0,
            "objTrailType": -1,
            "objTrailDelay": 0,
            "partTrailType": 0,
            "partTrailObj": -1,
            "partTrailDelay": 0,
            "speed": 0,
            "immutable": true,
            "fixed": true,
            "platform": false,
            "removeonsobject": 0,
            "teamImmunity": 0,
            "detonable": 0,
            "id": 0,
            %%layer%%
            "name": "%%name%%"
          }
        ],
        "nObjects": [],
        "sObjects": [],
        "constants": {},
        "textures": [],
        "colorAnim": [],
        "textSpritesStartIdx": "",
        "crossHairSprite": "",
        "name": "SPRITE_MOD",
        "author": "",
        "version": ""
      }`
    
    #baseSprite = `V0xTUFJUAAABAAAAbDgAbFAApJSAAJAAPKw8/FRUqKioVFRUVFT8VNhUVPz8eEAIgEQIiEgMkFAQmFQUoFgYrGAcTExMVFRUXFxcZGRkbGxsdHR0fHx8hISEjIyMlJSUnJycODiIUFDAaGj4kJD0uLj0bGxskJCQtLS02NjYIGAgLIQsPKw8cLxwpNSkbGxskJCQtLS02NjYqKj40ND0/Pz0PFAAWHAAdJAAlLAAeEg0nHhYxKh87NignHhYxKh87NigyGQAoFAASEhIbGxskJCQtLS02NjY/Pz8xMTEkJCQmDwAtGQA0IwA7LQAqFQA2AAAvAAApAAAyAAArAAA2AAAvAAApAAA2AAAvAAApAAAUFDAaGj4kJD0UFDAaGj4kJD0lIgAiHwAfHAAdGQAhFwooIRIvLBo2NyI+Pi89PT8/AAA+BgE+DQI+FAQ+GwU+IgY+KQg+MAk+Nwo9Og89PRQ9PRw9PSU8PC08PDU8PD4LIQsPKw8cLxwLIQsPKw8cLxw+Dw89Hx89Ly8aGj4kJD0uLj0kJD0PKw8cLxwpNSkcLxwlIgAiHQAfGAAcEwAZDgAWCgAaGiIkJDAvLz4yMj03Nz0KHAoLIQsNJg0PKw8/MjI9KSk+Fxc9ExM9Dw89ExM9Fxc9KSkVCgAWCgAXCwAYDAAPBwAQBwARCAASCQA/Pz83NzcvLy8nJycfHx8nJycvLy83NzcbEwsfFQwjGA4nGxArHhIAAAAKCQIUEwUeHQcoJgoyMAw9Og8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/AAA/CQA/EgA/GwA/JAA/LQA/NgA/PwAqPAAVOgAAOAA/AAA6AQU2AwsxBREtBhYoCBwkCiIfCycbDS0WDzMSETkAgAJAAkA/P/5/wAAAAA6AAAAAAAAADo6OgAAAAAAACA6ISAAAAAAACEgIB8AAAAAACEmJiZDQgAAACAhISAfAAAfIB8hIAAAAB4gISEgHwAAAB8gACAfAAAAAAgAYwD9/57/RUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVF`

    constructor(uri, name='', layer = -1, anchorPosition=0) {
        this.#anchorPosition = anchorPosition
        if (uri.substring(0,8)!='https://') {
            this.#uri=this.#baseUrl+uri+'.png'
        } else {
            this.#uri = uri
        }               
        this.#name = name!=""?name:this.#uri.split('/').pop().toLowerCase().replace('.png','')
        if (layer>=0) {
            this.#layer = layer
        }
       
    }

    async fetchPng(name, force = false) {
        if (!spriteCache.has(name) || force) {        
            spriteCache.set(name, {name: name, png: await fetchPngData(this.#uri)})
            console.log('----image data loaded for ', this.#uri)
        }
        return spriteCache.get(name).png
    }

    async loadData() {
      //  let tpl = await preloadMod('dsds/_sprite', false)
      //  this.#omod = new WLK_Mod(tpl.zip)
        this.#data = await this.fetchPng(this.#uri)        
    }
    
    getLayerProp = () => {
        if (this.#layer===-1) return ''
        if (this.#layer===0) return '"underlay": true,'
        if (this.#layer===1) return '"overlay": true,'
        if (this.#layer===2) return '"beacon": true,'
    }

    toWLK_Mod = () => {     
        console.log(this.#name, "to wlk mod")
        let mod = new WLK_Mod()
    //    console.log(this.#json.replace("%%name%%", this.#name).replace("%%layer%%", this.getLayerProp()));
        mod.import_json5(this.#json.replace("%%name%%", this.#name).replace("%%layer%%", this.getLayerProp()))
        mod.import_wlsprt(_base64ToArrayBuffer(this.#baseSprite))        
        mod.add('spr', this.getSprite())
        mod.config = {name:"", version:"", author:""}

        return mod
    }

    nameString = () =>  `sprite_${this.#name}`   
    getName = () => this.#name
    getSprite = (id=1) => {
        let cleaned = this.#data
        console.log("image size",cleaned.width,cleaned.height, cleaned.image.size)
        const anchor = this.#computeAnchor(cleaned)

       /* let sp = new WLK_Sprite(cleaned.width, cleaned.height, anchor.x, anchor.y)
        sp.data = cleaned.data
        sp.palette = palette
        sp.updateHash()*/
       
        return {
            id: id,
            width: cleaned.width,
            height: cleaned.height,
            data: cleaned.image,
            x: anchor.x,
            y: anchor.y
        }
    }
    

    #computeAnchor = (mdata) => {    
        if(this.#anchorPosition==1) { return {x: 0, y: 0} }
        if(this.#anchorPosition==2) { return {x: -Math.round(mdata.width/2), y: -Math.round(mdata.height/2)} }
        const w = mdata.width
        const h = mdata.height
        const anchorable = [MATERIAL.ROCK, MATERIAL.UNDEF, MATERIAL.WORM, MATERIAL.DIRT, MATERIAL.DIRT_2]
        for (let j = h-1; j > 0; j--) {
            const startx = Math.round(w/2)-(w%2)            
            let ii = startx-1
            let iii = startx
            while (iii < w || ii >=0) {
                let p = defaultMaterials[mdata.image[(j * w) + ii]]
                if (typeof p != 'undefined') {
                    if (p && anchorable.includes(p)) {
                        return {x: -ii, y: -j}
                    }
                }
                ii--
                let pp = defaultMaterials[mdata.image[(j * w) + iii]]
                if (typeof pp != 'undefined') {
                    if (pp && anchorable.includes(pp)) {
                        return {x: -iii, y: -j}
                    }
                }
                iii++
            }
        }
        return {x: 0, y: 0}
    }
}
class SolidSpriteWobject extends SpriteWobject {
    constructor(uri, name='') {
        super(uri, name)
    }
    #json = `{
        "weapons": [],
        "wObjects":
        [
            {
                // wid0 used in BLUE BRICK
                "detectDistance": 0,
                "blowAway": 0,
                "gravity": 0,
                "exploSound": -1,
                "addSpeed": 0,
                "distribution": 0,
                "multSpeed": 1,
                "createOnExp": -1,
                "dirtEffect": 0,
                "wormExplode": false,
                "explGround": true,
                "wormCollide": false,
                "collideWithObjects": false,
                "affectByExplosions": false,
                "bounce": 0,
                "bounceFriction": 1,
                "timeToExplo": 1,
                "timeToExploV": 0,
                "hitDamage": 0,
                "bloodOnHit": 0,
                "startFrame": 3,
                "numFrames": 0,
                "loopAnim": false,
                "shotType": 0,
                "repeat": 0,
                "colorBullets": 100,
                "splinterAmount": 1,
                "splinterColour": 0,
                "splinterType": 0,
                "splinterScatter": 0,
                "objTrailType": -1,
                "objTrailDelay": 0,
                "partTrailType": 0,
                "partTrailObj": -1,
                "partTrailDelay": 0,
                "speed": 180,
                "id": 0,
                "name": "%%name%%",
                "teamImmunity": 0
              }          
        ],
        "nObjects": [
            {
                // nid0 splinterType for wid0
                "detectDistance": 10,
                "gravity": 0,
                "speed": 0,
                "speedV": 0,
                "distribution": 0,
                "blowAway": 2,
                "bounce": 0,
                "hitDamage": 0,
                "wormExplode": false,
                "explGround": true,
                "wormDestroy": false,
                "bloodOnHit": 0,
                "startFrame": 3,
                "numFrames": 0,
                "drawOnMap": true,
                "colorBullets": 67,
                "createOnExp": -1,
                "affectByExplosions": false,
                "dirtEffect": -1,
                "splinterAmount": 0,
                "splinterColour": 0,
                "splinterType": -1,
                "bloodTrail": false,
                "bloodTrailDelay": 0,
                "leaveObj": -1,
                "leaveObjDelay": 0,
                "timeToExplo": 1,
                "timeToExploV": 0,
                "id": 0,
                "name": "nid 0",
                "teamImmunity": 0
              }          
        ],
        "sObjects": [],
        "constants": {},
        "textures":   [
            {
              "nDrawBack": false,
              "mFrame": 1,
              "sFrame": 2,
              "rFrame": 0,
              "id": 0,
              "name": "tid 0"
            }
          ],
        "colorAnim": [],
        "textSpritesStartIdx": "",
        "crossHairSprite": "",
        "name": "SPRITE_MOD",
        "author": "",
        "version": ""
      }`

    #baseSprite = `V0xTUFJUAAABAAAAbDgAbFAApJSAAJAAPKw8/FRUqKioVFRUVFT8VNhUVPz8eEAIgEQIiEgMkFAQmFQUoFgYrGAcTExMVFRUXFxcZGRkbGxsdHR0fHx8hISEjIyMlJSUnJycODiIUFDAaGj4kJD0uLj0bGxskJCQtLS02NjYIGAgLIQsPKw8cLxwpNSkbGxskJCQtLS02NjYqKj40ND0/Pz0PFAAWHAAdJAAlLAAeEg0nHhYxKh87NignHhYxKh87NigyGQAoFAASEhIbGxskJCQtLS02NjY/Pz8xMTEkJCQmDwAtGQA0IwA7LQAqFQA2AAAvAAApAAAyAAArAAA2AAAvAAApAAA2AAAvAAApAAAUFDAaGj4kJD0UFDAaGj4kJD0lIgAiHwAfHAAdGQAhFwooIRIvLBo2NyI+Pi89PT8/AAA+BgE+DQI+FAQ+GwU+IgY+KQg+MAk+Nwo9Og89PRQ9PRw9PSU8PC08PDU8PD4LIQsPKw8cLxwLIQsPKw8cLxw+Dw89Hx89Ly8aGj4kJD0uLj0kJD0PKw8cLxwpNSkcLxwlIgAiHQAfGAAcEwAZDgAWCgAaGiIkJDAvLz4yMj03Nz0KHAoLIQsNJg0PKw8/MjI9KSk+Fxc9ExM9Dw89ExM9Fxc9KSkVCgAWCgAXCwAYDAAPBwAQBwARCAASCQA/Pz83NzcvLy8nJycfHx8nJycvLy83NzcbEwsfFQwjGA4nGxArHhIAAAAKCQIUEwUeHQcoJgoyMAw9Og8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/AAA/CQA/EgA/GwA/JAA/LQA/NgA/PwAqPAAVOgAAOAA/AAA6AQU2AwsxBREtBhYoCBwkCiIfCycbDS0WDzMSETkAwAJAAgA/P/6/wAAAAAgIAAAAAAAACAhISAAAAAAACEgIB8AAAAAACEmJiZDQgAAACAhISAfAAAfIB8hIAAAAB4gISEgHwAAAB8gACAfAAAAAAEAAQAAAAAABhAAEAD5//n/YWFeYV5hX2FeXmFhYGBeXmFfYWBhX19fYWFeYV9fYGFfXl9gX19fYF9hXl5fYV5gX2BhYF9eX2BgYWBgYF5eYV9fX2BgX15fYGFeYV5eXl5gYF5fYF9fYF5gXmBfYV5gXmFhXl5fYF5eX2FgYGFeYGBgXl9gYWBgYF9gXl9eYF5hX2FfYWFhYV9hYF5eXmBhXmBeXl5fYWBhXl9gXl5hX2FeXl5fXl9fYV9hXl5fYV5hYV9fXmFeYWBhYV5gX15eYGBeYWBeXl9hYV5hYWFeX19gX2FeYF5gYV9hYGFgYGFeYV9fYF9fYV9hX2BgYGBfYWBgXmFgYGBhYGFfYV9eYA==`

      toWLK_Mod = () => {     
        console.log(this.getName(), " solid sprite to wlk mod")
        let mod = new WLK_Mod()
        mod.import_json5(this.#json.replace("%%name%%", this.getName()))
        mod.import_wlsprt(_base64ToArrayBuffer(this.#baseSprite))        
        mod.add('spr', this.getSprite(3))
        mod.config = {name:"", version:"", author:""}

        return mod
    }
}

class DestroyableSolidSpriteWobject extends SpriteWobject {
    constructor(uri, sound = 0, name='') {
        super(uri, name)
        this.#sound = sound
    }
    #sound =0;
    #json = `{
        "weapons": [],
        "wObjects":
        [
          {
            // wid0
            "detectDistance": 10,
            "blowAway": 0,
            "gravity": 0,
            "exploSound": %%sound%%,
            "addSpeed": 0,
            "distribution": 0,
            "multSpeed": 1,
            "createOnExp": -1,
            "dirtEffect": -1,
            "wormExplode": false,
            "explGround": false,
            "wormCollide": false,
            "collideWithObjects": false,
            "affectByExplosions": 0,
            "bounce": 0,
            "bounceFriction": 0,
            "timeToExplo": 0,
            "timeToExploV": 0,
            "hitDamage": 0,
            "bloodOnHit": 0,
            "startFrame": 6,
            "numFrames": 0,
            "loopAnim": false,
            "shotType": 0,
            "repeat": 0,
            "colorBullets": 100,
            "splinterAmount": 1,
            "splinterColour": 0,
            "splinterType": 0,
            "splinterScatter": 0,
            "objTrailType": 0,
            "objTrailDelay": 1,
            "partTrailType": 0,
            "partTrailObj": -1,
            "partTrailDelay": 0,
            "speed": 0,
            "immutable": 4,
            "fixed": true,
            "platform": false,
            "removeonsobject": 0,
            "teamImmunity": 0,
            "detonable": 0,
            "behavior===undefined ? -1 : a": -1,
            "id": 0,
            "name": "%%name%%"
          }
        ],
        "nObjects":
        [
          {
            // nid0 splinterType for wid0
            "detectDistance": 1,
            "gravity": 0,
            "speed": 0,
            "speedV": 0,
            "distribution": 0,
            "blowAway": 0,
            "bounce": 0,
            "hitDamage": 0,
            "wormExplode": false,
            "explGround": false,
            "wormDestroy": false,
            "bloodOnHit": 0,
            "startFrame": 2,
            "numFrames": 0,
            "drawOnMap": false,
            "colorBullets": 0,
            "createOnExp": -1,
            "affectByExplosions": false,
            "dirtEffect": 0,
            "splinterAmount": 0,
            "splinterColour": 0,
            "splinterType": -1,
            "bloodTrail": false,
            "bloodTrailDelay": 0,
            "leaveObj": -1,
            "leaveObjDelay": 0,
            "timeToExplo": 1,
            "timeToExploV": 0,
            "immutable": false,
            "teamImmunity": 0,
            "id": 0,
            "name": "nid 0"
          }
        ],
        "sObjects":
        [
          {
            // sid0 objTrailType for wid0
            "startSound": -1,
            "numSounds": 0,
            "animDelay": 0,
            "startFrame": 2,
            "numFrames": 0,
            "detectRange": 2,
            "damage": 0,
            "blowAway": 0,
            "shadow": false,
            "shake": 0,
            "flash": 0,
            "dirtEffect": 1,
            "id": 0,
            "name": "sid 0"
          }
        ],
        "constants": {},
        "textures":
        [
          {
            "nDrawBack": true,
            "mFrame": 7,
            "sFrame": 4,
            "rFrame": 0,
            "id": 0,
            "name": "tid 0"
          },
          {
            "nDrawBack": false,
            "mFrame": 7,
            "sFrame": 5,
            "rFrame": 0,
            "id": 1,
            "name": "tid 1"
          }
        ],
        "behaviors": [],
        "colorAnim": [],
        "textSpritesStartIdx": "",
        "crossHairSprite": "",
        "name": "destroyable",
        "author": "",
        "version": ""
      }`

    #baseSprite = `V0xTUFJUAAABAAAAbDgAbFAApJSAAJAAPa09/FRUqKioVVVVVFT8VNhUVPz8eEAIgEQIiEgMkFAQmFQUoFgYrGAcOTY2PTZCRz9TVVNTZ2BsdW56eHBygIJ/ioCEjY6Lo6aiODiIUVHBaWn5kZH1ubn1bm5ukZGRtbW12dnZIGAgLYUtPq4+cb1xpdWlb29vkpKStra22traqKj40ND0/Pz0PFAAWHAAdJAAlLAAeEg0nXlZxal97dmhnHhYxKh87NigyGQAoFAASEhIbGxsk5OTtLS02NjY/f39xMTEkJCQmDwAtGQA0IwA7LQAqFQA2QEBvQEBpQEByAAArAAA2gICvgICpgICjwgLgBEQkw8OUlLCamr6kpL2UFDAa2v7k5P3lYkBiHwAfHAAdGQAhFwooIRIvLBo2NyI+Pi89PT8/QEB+BgE+DQI+FAQ+GwU+IgY+KQg+MAk+Nwo9ek99PRQ9PRw9PSU8PC08PDU8PD4LoYuP68/cr5yL4cvQLBAc79z+Dw89Hx89Ly8aGj4lJT4uLj0kJD0QbFBdMB0pNSkcLxwlIgAiHQAfGAAcEwAZDgAWSkBaGiIkJDAvLz4yMj03Nz0KHAoLIQsNJg0PKw8/MjI9aWl+Fxc9U1N9Dw89ExM9Fxc9KSkMg8VOg4UNRATPg0XMBIVQQ0UKQ0aMBMg/Pz83d3dvb29np6efHx8nJycvLy83NzcbEwsfFQwjGA4nGxArHhIAAABKCQIUEwUeHQcoJgoyMAw9Og8OhcmQhkrSR8wUiE2VCQ9YCI+AAEAVAgMYQ0QYw4Kaw4QcwwOdQ8JihIQUyBalxYXlxYdoBUWqhMVqhQbtBITbidawhgcqiEiux0ftSIijCpgSkNNRUdE0iAlS0dKySMnTUhYV1FeYF9eiIGOmZuYrrCt58rJ7dig/d/e//n4/f/8BAEBAQQBAQEE/gIC/CQA/EgA/GwA/JAA/LQA/NgA/PwAqPAAVOgAAOAA/AAA6AQU2AwsxBREtBhYoCBwkCiIfCycbDS0WDzMSETkBgAJAAgA/P/6/wAAAAAgIAAAAAAAACAhISAAAAAAACEgIB8AAAAAACEmJiZDQgAAACAhISAfAAAfIB8hIAAAAB4gISEgHwAAAB8gACAfAAAAAAQABAD2/+n/FhYWFhYbGxYWGxsWFhYWFgEAAQD/////AAQABAD2/+n/BgYGBgYGBgYGBgYGBgYGBhAAEAD5//n/oaCjoqKgoqKhoKCjoKOhoqKho6Cio6KhoaOio6OjoaOgoKOhoaOjoaCjoKGhoqChoqChoaCioKGjoKCioKCjo6GgoKKhoaGioqOioaGgoaOjoKOgo6Kgo6OjoaChoKGhoqOioqOgoaCgoqOgo6KhoqOgoKGgoaKho6OioKCgoKGgo6Ggo6OgoKCgoaKioKOgo6CgoaGgoaOjo6KhoKGioKCho6Kho6GjoqOhoqOhoKOioqKioaGioKGgoqOjoqOhoaChoqOjo6Cjo6KjoqCgoKGioKCjoaOio6Cio6KgoqOhoKCgo6OioqOgo6KgoaCgoaGioaGio6KjoKCgoaGgohAAEAD5//n/YWFeYV5hX2FeXmFhYGBeXmFfYWBhX19fYWFeYV9fYGFfXl9gX19fYF9hXl5fYV5gX2BhYF9eX2BgYWBgYF5eYV9fX2BgX15fYGFeYV5eXl5gYF5fYF9fYF5gXmBfYV5gXmFhXl5fYF5eX2FgYGFeYGBgXl9gYWBgYF9gXl9eYF5hX2FfYWFhYV9hYF5eXmBhXmBeXl5fYWBhXl9gXl5hX2FeXl5fXl9fYV9hXl5fYV5hYV9fXmFeYWBhYV5gX15eYGBeYWBeXl9hYV5hYWFeX19gX2FeYF5gYV9hYGFgYGFeYV9fYF9fYV9hX2BgYGBfYWBgXmFgYGBhYGFfYV9eYA==`

    #createMask = (id = 1, color = 6, removeAnchor = false) => {  
        let msk = this.getSprite2(id)      
        const w = msk.width
        const h = msk.height        
        let ret = msk.data.slice(0); // copy
        for (let j = 0; j < h; j++) {
            for (let i = 0; i < w; i++) {
                if (0!==msk.data[(j * w) + i]) { // if not 0 black we replace with 6
                    ret[(j * w) + i] = color
                }
            }
        }
        if (removeAnchor) {
            let x = -1*msk.x;
            let y = -1*msk.y;
            console.log("anchor----------",msk.x, msk.y, x, y)
            ret[(y*w)+x] = 0;
        }
        msk.data =  Uint8Array.from(ret);
        return msk
    }

    #centerAnchor = (sprite) => {
        sprite.x =  -Math.round(sprite.width/2);
        sprite.y =  -Math.round(sprite.height/2);
        return sprite;
    }
    getSprite2 = (id = 1) => {
        return this.#centerAnchor(this.getSprite(id))
    }

    toWLK_Mod = () => {     
        console.log(this.getName(), " destroyable solid sprite to wlk mod")
        let mod = new WLK_Mod()
        mod.import_json5(this.#json.replace("%%name%%", this.getName()).replace("%%sound%%", this.#sound))
        mod.import_wlsprt(_base64ToArrayBuffer(this.#baseSprite))        
        mod.add('spr', this.getSprite2(6))
        mod.add('spr', this.#createMask(7))        
        mod.config = {name:"", version:"", author:""}

        return mod
    }
}
if (typeof window === 'undefined') { // node js context    
    exports.SpriteWobject = SpriteWobject
    exports.SolidSpriteWobject = SolidSpriteWobject
    exports.DestroyableSolidSpriteWobject = DestroyableSolidSpriteWobject
}
