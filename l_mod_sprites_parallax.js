
class ParallaxSpriteWobject {
    #name = 'PX'
    #uri = ''
    #data = null
    #baseUrl = 'https://sylvodsds.gitlab.io/webliero-extended-mods/'
    #box = [ 0, 0, 504, 350];
    behavior = `let cp =  api.getCurrentPlayer();

    if (!cp) {
        api.setObjectFrame(this, 0);
        return;
    }
    const w = %%width%%;
    const h = %%height%%;
    const minx = %%minx%%;
    const miny = %%miny%%;
    const maxx = %%maxx%%;
    const maxy = %%maxy%%;
    const cw = maxx-minx;
    const ch = maxy-miny;

    
  
    let x = cp.x/cw*(cw-w);
    let y = cp.y/ch*(ch-h);

    //console.log('player.x', cp.x, 'map width', cw, 'px width', w, cp.x/cw, (cw-w), x, y);
    this.x = x;
    this.y = y;
    api.setObjectFrame(this, 1);
    `
    #json = `{
        "weapons": [],
        "wObjects":
        [
          {
            // wid0
            "detectDistance": 0,
            "blowAway": 0,
            "gravity": 0,
            "exploSound": -1,
            "addSpeed": 0,
            "distribution": 0,
            "multSpeed": 1,
            "createOnExp": -1,
            "dirtEffect": -1,
            "wormExplode": false,
            "explGround": false,
            "wormCollide": false,
            "collideWithObjects": false,
            "affectByExplosions": 0,
            "bounce": 0,
            "bounceFriction": 0,
            "timeToExplo": 0,
            "timeToExploV": 0,
            "hitDamage": 0,
            "bloodOnHit": 0,
            "startFrame": 1,
            "numFrames": 1,
            "loopAnim": false,
            "shotType": 0,
            "repeat": 1,
            "colorBullets": 0,
            "splinterAmount": 1,
            "splinterColour": 50,
            "splinterType": -1,
            "splinterScatter": 0,
            "objTrailType": -1,
            "objTrailDelay": 0,
            "partTrailType": 0,
            "partTrailObj": -1,
            "partTrailDelay": 0,
            "speed": 0,
            "immutable": true,
            "fixed": true,
            "platform": false,
            "removeonsobject": 0,
            "teamImmunity": 0,
            "behavior": 0,
            "detonable": 0,
            "id": 0,
            "underlay": true,
            "name": "%%name%%"
          }
        ],
        "nObjects": [],
        "sObjects": [],
        "constants": {},
        "behaviors": [
          {
            "id": 0,
            "name": "%%name%%_parallax_behavior",
            "update": "%%behavior%%"
          }
        ],
        "textures": [],
        "colorAnim": [],
        "textSpritesStartIdx": "",
        "crossHairSprite": "",
        "name": "SPRITE_MOD",
        "author": "",
        "version": ""
      }`
    

     //data:application/binary;base64, 
    #baseSprite = `V0xTUFJUAAABAAAAbDgAbFAApJSAAJAAPKw8/FRUqKioVFRUVFT8VNhUVPz8eEAIgEQIiEgMkFAQmFQUoFgYrGAcTExMVFRUXFxcZGRkbGxsdHR0fHx8hISEjIyMlJSUnJycODiIUFDAaGj4kJD0uLj0bGxskJCQtLS02NjYIGAgLIQsPKw8cLxwpNSkbGxskJCQtLS02NjYqKj40ND0/Pz0PFAAWHAAdJAAlLAAeEg0nHhYxKh87NignHhYxKh87NigyGQAoFAASEhIbGxskJCQtLS02NjY/Pz8xMTEkJCQmDwAtGQA0IwA7LQAqFQA2AAAvAAApAAAyAAArAAA2AAAvAAApAAA2AAAvAAApAAAUFDAaGj4kJD0UFDAaGj4kJD0lIgAiHwAfHAAdGQAhFwooIRIvLBo2NyI+Pi89PT8/AAA+BgE+DQI+FAQ+GwU+IgY+KQg+MAk+Nwo9Og89PRQ9PRw9PSU8PC08PDU8PD4LIQsPKw8cLxwLIQsPKw8cLxw+Dw89Hx89Ly8aGj4kJD0uLj0kJD0PKw8cLxwpNSkcLxwlIgAiHQAfGAAcEwAZDgAWCgAaGiIkJDAvLz4yMj03Nz0KHAoLIQsNJg0PKw8/MjI9KSk+Fxc9ExM9Dw89ExM9Fxc9KSkVCgAWCgAXCwAYDAAPBwAQBwARCAASCQA/Pz83NzcvLy8nJycfHx8nJycvLy83NzcbEwsfFQwjGA4nGxArHhIAAAAKCQIUEwUeHQcoJgoyMAw9Og8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/AAA/CQA/EgA/GwA/JAA/LQA/NgA/PwAqPAAVOgAAOAA/AAA6AQU2AwsxBREtBhYoCBwkCiIfCycbDS0WDzMSETkAgAJAAkA/P/5/wAAAAA6AAAAAAAAADo6OgAAAAAAACA6ISAAAAAAACEgIB8AAAAAACEmJiZDQgAAACAhISAfAAAfIB8hIAAAAB4gISEgHwAAAB8gACAfAAAAAAEAAQAAAAAAAA==`

    constructor(uri, name='', minx=0, miny=0, maxx=504, maxy=350) {  
        if (uri.substring(0,8)!='https://') {
            this.#uri=this.#baseUrl+uri+'.png'
        } else {
            this.#uri = uri
        }               
        this.#name = name!=""?name:this.#uri.split('/').pop().toLowerCase().replace('.png','')  
        this.#box = [minx,miny,maxx,maxy]
    }

    async fetchPng(name, force = false) {
        if (!spriteCache.has(name) || force) {        
            spriteCache.set(name, {name: name, png: await fetchPngData(this.#uri)})
            console.log('----image data loaded for ', this.#uri, spriteCache.get(name).png.width, spriteCache.get(name).png.height)
        }
        return spriteCache.get(name).png
    }

    async loadData() {
        this.#data = await this.fetchPng(this.#uri)            
    }
    

    toWLK_Mod = () => {   
        this.prepare()  
        console.log(this.#name, "to wlk mod")
        let mod = new WLK_Mod()
        let j = this.#json.replace("%%behavior%%", this.behavior.replace(/\n/g,"\\n"))
                .replaceAll("%%name%%", this.#name)
                .replace("%%width%%", this.#data.width)
                .replace("%%height%%", this.#data.height)
                .replace("%%minx%%", this.#box[0])
                .replace("%%miny%%", this.#box[1])
                .replace("%%maxx%%", this.#box[2])
                .replace("%%maxy%%", this.#box[3])
                ;
        console.log(j)
        mod.import_json5(j)
        mod.import_wlsprt(_base64ToArrayBuffer(this.#baseSprite))  
        mod.add('spr', this.getSprite())
        mod.config = {name:"", version:"", author:""}

        return mod
    }

    prepare = () => {}

    nameString = () =>  `anim_${this.#name}`   
    getName = () => this.#name
    getSprite = (id=1) => {
        let cleaned = this.#data
        console.log("image size",cleaned.width,cleaned.height, cleaned.image.size)
    //    const anchor = this.#computeAnchor(cleaned)

       /* let sp = new WLK_Sprite(cleaned.width, cleaned.height, anchor.x, anchor.y)
        sp.data = cleaned.data
        sp.palette = palette
        sp.updateHash()*/
       
        return {
            id: id,
            width: cleaned.width,
            height: cleaned.height,
            data: cleaned.image,
            x: 0,
            y: 0
        }
    }
    

    // #computeAnchor = (mdata) => {
    //     const w = mdata.width
    //     const h = mdata.height
    //     const anchorable = [MATERIAL.ROCK, MATERIAL.UNDEF, MATERIAL.WORM, MATERIAL.DIRT, MATERIAL.DIRT_2]
    //     for (let j = h-1; j > 0; j--) {
    //         const startx = Math.round(w/2)-(w%2)            
    //         let ii = startx-1
    //         let iii = startx
    //         while (iii < w || ii >=0) {
    //             let p = defaultMaterials[mdata.image[(j * w) + ii]]
    //             if (typeof p != 'undefined') {
    //                 if (p && anchorable.includes(p)) {
    //                     return {x: -ii, y: -j}
    //                 }
    //             }
    //             ii--
    //             let pp = defaultMaterials[mdata.image[(j * w) + iii]]
    //             if (typeof pp != 'undefined') {
    //                 if (pp && anchorable.includes(pp)) {
    //                     return {x: -iii, y: -j}
    //                 }
    //             }
    //             iii++
    //         }
    //     }
    //     return {x: 0, y: 0}
    // }
}


// complex behavior
/*
[r, frame, lastAnim] = api.unpack(this,'iii',[0, 0, 0]);

let mulberry32= (a) => {
      var t = a += 0x6D2B79F5;
      t = Math.imul(t ^ t >>> 15, t | 1);
      t ^= t + Math.imul(t ^ t >>> 7, t | 61);
      return Math.round(((t ^ t >>> 14) >>> 0) / 4294967296 * 100);
};


let anims = %%anims%%;

((o) => {
    if (tick%%%tickskip%%===0) { 
        frame=(() => {
           if (!lastAnim && r<%%repeat%%) {         
              if (frame==anims[lastAnim].length-1) {
                  ++r;
                  if (r==%%repeat%%) {
                      r = 0;
                      lastAnim = (mulberry32(tick)%(anims.length-1))+1;
                  }
                  return 0;
              } 
              return frame+1;
           }
        let isEnd=(frame==anims[lastAnim].length-1);
        if (isEnd) {
            lastAnim = 0;
            return 0;
        }
            return frame+1;
        })();
    }
    console.log(frame, lastAnim, r);
    api.setObjectFrame(o, anims[lastAnim][frame]);
    
})(this);

api.pack(this,'iii',[r, frame, lastAnim]); 

*/



if (typeof window === 'undefined') { // node js context    
    exports.ParallaxSpriteWobject = ParallaxSpriteWobject
}