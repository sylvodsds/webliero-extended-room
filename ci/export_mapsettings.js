#!/usr/bin/env node
const ms = require('../mapsettings')
const fs = require("fs");

const mapToObj = m => {
    return Array.from(m).reduce((obj, [key, value]) => {
      obj[key] = value;
      return obj;
    }, {});
  };

console.log(JSON.stringify(mapToObj(ms.mapSettings)))
fs.writeFile('public/mapsettings.json', JSON.stringify(mapToObj(ms.mapSettings)), () =>{ console.log("done")})