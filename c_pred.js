function loadPredsettings(s) {
    if (typeof s.startGame == "function") {
        currentMapSettings = s; 
        s.startGame()
    } else {
        console.log("pred", ...s)                
        window.WLROOM.setSpawn(0,...s)
    }   
}

function randomizeAllSpawns() {
    for (let k in s) {
        window.WLROOM.setSpawn(...randomItem(s[k]))
    }
}

class PredSettings {
    constructor() {}
    static ORDER = {
        TRUE: true,
        LOOP_LAST_FOUR: "loop_last_four"
    } 
    #types = ["lemon", "worm"]
    #spawns = [[],[]]
    #order = false
    #orders = [-1,-1]
    #settings = []
    
    setSettings = (settings) => {this.#settings=settings; return this;}    
    hasSettings = () => this.#settings && Object.entries(this.#settings).length>0

    toJSON = () => ({
        spawns : this.#spawns,
        order: this.#order,
        orders: this.#order?this.#orders:null
    })

    #addSpawn = (type, x, y) => {
        this.#spawns[type].push([x,y])
    }

    addLemonSpawn=(x,y)=> {this.#addSpawn(0,x,y); return this;}
    addWormSpawn=(x,y)=> {this.#addSpawn(1,x,y); return this;}
    setOrder = (order) => {this.#order=order; return this;}

    startGame = () => {
        console.log("starting pred")
        this.#orders = [-1,-1]
        this.#randomizeLemonSpawn()
        this.#nextSpawns()     
        loadGameSettings("pred", this.#settings)   
    }
    
    #nextSpawns = () => {
        if (!this.#order) {
            this.#randomizeWormSpawn()
        } else {
            let k = 1;
            let last_idx = this.#spawns[k].length-1
            var next = 0
            if (this.#order==PredSettings.ORDER.LOOP_LAST_FOUR && (last_idx==this.#orders[k])) {
                this.#orders[k] = last_idx-4
            } else {
                this.#orders[k] = (this.#orders[k]>=last_idx)? 0 : this.#orders[k] + 1
            }            
            window.WLROOM.setSpawn(k, ...this.#spawns[k][this.#orders[k]])
        }
    }
    endGame = () => {
        console.log("pred clear")
    }
    onPlayerKilled = (killed, killer) => {
       this.#nextSpawns()
    } 
    #randomizeLemonSpawn = () => {
        window.WLROOM.setSpawn(0, ...randomItem(this.#spawns[0]))
    }
    #randomizeWormSpawn = () => {
        if (this.#spawns[1].length) {
            window.WLROOM.setSpawn(1, ...randomItem(this.#spawns[1]))
        }        
    }

}

if (typeof window === 'undefined') { // node js contexts    
    exports.PredSettings = PredSettings
}