class LineWobject {
    #name = 'line'
    #detect = 20
    #damage = 0
    #blood = 0
    #color = 0
    #x = 0
    #y = 0
    #zx = 0
    #zy = 0
    #weight = 6

    constructor(name, color, zx, zy, weight = 6, detect = 20, damage = 0, blood = 0) {
            this.#name = name
            this.#color = color            
            this.#zx = zx
            this.#zy = zy
            this.#weight = weight
            this.#detect = detect
            this.#damage = damage
            this.#blood = blood
    }

    #json = `{
        "weapons": [],
        "wObjects":
        [
          {
            // wid0
            "detectDistance": %%detect%%,
            "blowAway": 0,
            "gravity": 0,
            "exploSound": -1,
            "addSpeed": 0,
            "distribution": 0,
            "multSpeed": 1,
            "createOnExp": -1,
            "dirtEffect": -1,
            "wormExplode": false,
            "explGround": false,
            "wormCollide": false,
            "collideWithObjects": false,
            "affectByExplosions": 0,
            "bounce": 0,
            "bounceFriction": 0,
            "timeToExplo": 0,
            "timeToExploV": 0,
            "hitDamage": %%damage%%,
            "bloodOnHit": %%blood%%,
            "startFrame": 2,
            "numFrames": 0,
            "loopAnim": false,
            "shotType": 0,
            "repeat": 1,
            "colorBullets": 0,
            "splinterAmount": 1,
            "splinterColour": 50,
            "splinterType": -1,
            "splinterScatter": 0,
            "objTrailType": -1,
            "objTrailDelay": 0,
            "partTrailType": 0,
            "partTrailObj": -1,
            "partTrailDelay": 0,
            "speed": 0,
            "immutable": true,
            "fixed": true,
            "platform": false,
            "removeonsobject": 0,
            "teamImmunity": 0,
            "detonable": 0,
            "id": 0,
            "name": "%%name%%"
          }
        ],
        "nObjects": [],
        "sObjects": [],
        "constants": {},
        "textures": [],
        "colorAnim": [],
        "textSpritesStartIdx": "",
        "crossHairSprite": "",
        "name": "LINE_MOD",
        "author": "",
        "version": ""
      }`
    
      #baseSprite = `V0xTUFJUAAABAAAAbDgAbFAApJSAAJAAPKw8/FRUqKioVFRUVFT8VNhUVPz8eEAIgEQIiEgMkFAQmFQUoFgYrGAcTExMVFRUXFxcZGRkbGxsdHR0fHx8hISEjIyMlJSUnJycODiIUFDAaGj4kJD0uLj0bGxskJCQtLS02NjYIGAgLIQsPKw8cLxwpNSkbGxskJCQtLS02NjYqKj40ND0/Pz0PFAAWHAAdJAAlLAAeEg0nHhYxKh87NignHhYxKh87NigyGQAoFAASEhIbGxskJCQtLS02NjY/Pz8xMTEkJCQmDwAtGQA0IwA7LQAqFQA2AAAvAAApAAAyAAArAAA2AAAvAAApAAA2AAAvAAApAAAUFDAaGj4kJD0UFDAaGj4kJD0lIgAiHwAfHAAdGQAhFwooIRIvLBo2NyI+Pi89PT8/AAA+BgE+DQI+FAQ+GwU+IgY+KQg+MAk+Nwo9Og89PRQ9PRw9PSU8PC08PDU8PD4LIQsPKw8cLxwLIQsPKw8cLxw+Dw89Hx89Ly8aGj4kJD0uLj0kJD0PKw8cLxwpNSkcLxwlIgAiHQAfGAAcEwAZDgAWCgAaGiIkJDAvLz4yMj03Nz0KHAoLIQsNJg0PKw8/MjI9KSk+Fxc9ExM9Dw89ExM9Fxc9KSkVCgAWCgAXCwAYDAAPBwAQBwARCAASCQA/Pz83NzcvLy8nJycfHx8nJycvLy83NzcbEwsfFQwjGA4nGxArHhIAAAAKCQIUEwUeHQcoJgoyMAw9Og8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/AAA/CQA/EgA/GwA/JAA/LQA/NgA/PwAqPAAVOgAAOAA/AAA6AQU2AwsxBREtBhYoCBwkCiIfCycbDS0WDzMSETkAgAJAAkA/P/5/wAAAAA6AAAAAAAAADo6OgAAAAAAACA6ISAAAAAAACEgIB8AAAAAACEmJiZDQgAAACAhISAfAAAfIB8hIAAAAB4gISEgHwAAAB8gACAfAAAAAAgAYwD9/57/RUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVF`
    
    nameString = () =>  `line_${this.#name}_${this.#color}_${this.#x}_${this.#y}_${this.#zx}_${this.#zy}_${this.#weight}`   

    toWLK_Mod = () => {
        let mod = new WLK_Mod()
        mod.import_json5(this.#json.replace("%%name%%", this.#name).replace("%%damage%%", this.#damage).replace("%%blood%%", this.#blood).replace("%%detect%%", this.#detect))
        mod.import_wlsprt(_base64ToArrayBuffer(this.#baseSprite))        
        mod.add('spr', this.#getSprite())
        mod.config = {name:"", version:"", author:""}
      //  console.log(mod.write_zip({type:"base64"}))
        return mod
    }
    #getSprite = () => {
       const vert = this.#isVertical()
       let lc = this.#getlinecoordinates(this.#x, this.#y, this.#zx, this.#zy, this.#weight, vert)  
       const width = this.#zx + (vert?this.#weight:0)
       const height = this.#zy + (vert?0:this.#weight)
       
        let d = new Uint8Array(function*(color) {
            for (let y = 0; y < height; y++) {
                for (let x = 0; x < width; x++) {
                    if (typeof lc[x] != 'undefined' && lc[x].includes(y)) {
                        yield color
                    } else {
                        yield 0
                    }
                }
            }
        }(this.#color))

       
       return {
           id: 1,
           width: width,
           height: height,
           data: d,
           x: 0,
           y: 0
       }
    }
    #isVertical = () => {
        const slope = (this.#zy - this.#y) / (this.#zx - this.#x);
        return slope==Infinity?true:slope<1?false:true;      
    }
    //https://jsfiddle.net/5n3286zv/2/
    #getlinecoordinates = (x0, y0, x1, y1, w, isvertical) => {
        let ret = {} //ordered by x
        var dx = Math.abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
        var dy = Math.abs(y1 - y0), sy = y0 < y1 ? 1 : -1; 
        var err = (dx>dy ? dx : -dy)/2;           
        while (true) {            
          for (let x = 0; x<w; x++) {
            let rx = x0+(isvertical?x:0)
            let ry = y0+(isvertical?0:x)
            if (typeof ret[rx] == 'undefined') {
                ret[rx] = []
            }
            ret[rx].push(ry)
          } 
          if (x0 === x1 && y0 === y1) break;
          var e2 = err;
          if (e2 > -dx) { err -= dy; x0 += sx; }
          if (e2 < dy) { err += dx; y0 += sy; }
        }
        return ret;
    }
}


function _base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}


if (typeof window === 'undefined') { // node js context
    exports.LineWobject = LineWobject
}