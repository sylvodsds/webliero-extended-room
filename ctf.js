
function loadCTFsettings(s) {
        currentMapSettings = s; 
        console.log(typeof s)
        s.startGame()   
}

class CTFSettings {
    #gameMode = 'ctf'
    constructor(gameMode) {this.#gameMode=gameMode??'ctf';}
    static ORDER = {
        TRUE: true,
        RANDOM_SAME_INDEX: "random_same_index"
    } 
    #types = ["flag green", "defense","attack","flag blue"]
    #flagForTeam = {1:0,2:3}
    #spawns = [[],[],[],[]]
    #order = false
    #orders = [-1,-1,-1,-1]
    #settings = []
    #onGameStart = null
    #onFlagSpawnChange = null
    #onFlagScore = null
    #currentSpawns = []

    #addSpawn = (type, x, y) => {
        this.#spawns[type].push([x,y])
    }
    addSpawns = (spawns) => { this.#spawns=spawns; return this;}

    addFlagGreenSpawn=(x,y)=> {this.#addSpawn(0,x,y); return this;}
    addGreenSpawn=(x,y)=> {this.#addSpawn(1,x,y); return this;}
    addBlueSpawn=(x,y)=> {this.#addSpawn(2,x,y); return this;}
    addFlagBlueSpawn=(x,y)=> {this.#addSpawn(3,x,y); return this;}
    setOrder = (order) => {this.#order=order; return this;}
    setSettings = (settings) => {this.#settings=settings; return this;}    
    hasSettings = () => this.#settings && Object.entries(this.#settings).length>0
    setOnGameStart = (callback) => {this.#onGameStart = callback; return this;}
    setOnFlagSpawnChange = (callback) => {this.#onFlagSpawnChange = callback; return this;}
    setOnFlagScore = (callback) => {this.#onFlagScore = callback; return this;}

    toJSON = () => ({
        gameMode : this.#gameMode,
        spawns : this.#spawns,
        order: this.#order,
      //  orders: this.#order?this.#orders:null, internal?
        settings: this.#settings
    })

    startGame = () => {
        console.log("starting ctf")
        this.#orders = [-1,-1,-1,-1]
        this.onFlagScore()
        loadGameSettings(this.#gameMode, this.#settings)
        
    }
        
    onGameStart = () => {
        if (this.#onGameStart != null) {
            this.#onGameStart(this)
        }
    }
    
    endGame = () => {
        console.log("ctf clear")
    }
    

    onFlagScore = (worm, score) => {
        if (this.#onFlagScore != null) {
            this.#onFlagScore(worm, score)
        }
        if (!this.#order) {
            this.#randomizeAllSpawns()
        } else {
            this.#nextSpawns()
        }
    }   

    onPlayerKilled = (killed, killer) => {
        if (!this.#order) {            
            if ([1,2].includes(killed.team)) {
                window.WLROOM.setSpawn(killed.team, ...randomItem(this.#spawns[killed.team]))
            }            
        }
    } 

    getCurrentFlagSpawn = (color) => {
        let x=0
        let y=0
        let s = this.#currentSpawns[color=='green'?0:3]
        if (typeof s != 'undefined' && s.length==2) {
            x=s[0]
            y=s[1]
        }
        return {x:x,y:y}
    }

    #nextSpawns = () => {
        switch (this.#order) {
            case true:
                this.#nextSpawnsForward()
                break
            case CTFSettings.ORDER.RANDOM_SAME_INDEX:
                this.#nextSpawnsRandomSameIdx()
                break
        }
    }
    #setSpawn = (k, x, y) => {
        window.WLROOM.setSpawn(k, x, y)
        if (this.#onFlagSpawnChange != null && (k==0 || k == 3)) {
            this.#onFlagSpawnChange(k==0?'green':'blue', x, y)
        }
        this.#currentSpawns[k] = [x,y]
    }

    #nextSpawnsForward = () => {
        for (let k in this.#orders) {          
            this.#orders[k] = (this.#orders[k]>=this.#spawns[k].length-1)? 0 : this.#orders[k] + 1             
            this.#setSpawn(k,...this.#spawns[k][this.#orders[k]])
        } 
    }
    #nextSpawnsRandomSameIdx = () => {
        [1,2].forEach((k) => {
            let idx = randomIdx(this.#spawns[k])            
            this.#setSpawn(k, ...this.#spawns[k][idx])
            this.#setSpawn(this.#flagForTeam[k], ...this.#spawns[k][idx])
        })
    }
    #randomizeAllSpawns= () => {
        for (let k in this.#spawns) {
            this.#setSpawn(k, ...randomItem(this.#spawns[k]))
        }
    }
    static fromJson = (json, type ="ctf") => {
        let s = (type=="ctf2") ?new CTF2Settings():new CTFSettings()
        let obj = typeof json == 'string' ? JSON.parse(json): json

        if (obj.settings) {
            s.setSettings(obj.settings)
        }
        if (obj.order) {    
            s.setOrder(obj.order)
        }
        if (obj.spawns) {
            s.addSpawns(obj.spawns)
        }
        return s
    }
}

class CTF2Settings extends CTFSettings {
    constructor() {
        super("ctf2")
    }
    static fromJson = (json) => {
        return super.fromJson(json, "ctf2")
      }
}


if (typeof window === 'undefined') { // node js context
    exports.CTF2Settings = CTF2Settings
    exports.CTFSettings = CTFSettings
}