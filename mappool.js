var mapCache = new Map();
var baseURL = "https://webliero.gitlab.io/webliero-maps";

var currentMap = 0;
var currentMapName = "";
var currentMapParams = null;
var currentMapSettings = null;
var currentTagsDefault = ['main'];
var currentTags = currentTagsDefault;
var currentTagsUpdated = Date.now();

var loadMe = null;
var mapStartHandler = null;
var mapTickHandler = null;
var mapSpawnHandler = null;
var mapKillHandler = null;
var mapFlagScoreHandler = null;
var objectsSteps = []
var objectStepTimeoutId;
var lalaMode = null;
var garbageTimeouts = [];

const WLPI = 3.141592653589793;

function cleanGarbage() {
    for (let tid of garbageTimeouts) {
        clearTimeout(tid)
    }
    garbageTimeouts = []
}

function getMapUrl(name) {
    if (name.substring(0,8)=='https://'||name.substring(0,7)=='http://') {
        return name;
    }
    return baseURL + '/' +  name;
}

function loadPool(name) {
	(async () => {
	    mypool = await (await fetch(baseURL + '/' +  name)).json();
	})();
}

async function getMapData(mapUrl) {
    let obj = mapCache.get(mapUrl)
    if (obj) {
      return obj;
    }
    try {
        obj = await (await fetch(mapUrl)).arrayBuffer();        
    }catch(e) {
        console.log("getmapdata", mapUrl, e, JSON.stringify(e))
        return null;
    }

    
    mapCache.set(mapUrl, obj)
    return obj;
}

function maybeResetCurrentTags() {
    // reverts to the default tags after 30 minutes
    // so we don't get stuck in some playlist forever
    let now = Date.now();
    let since = now - currentTagsUpdated;
    if(since > 1000*60*30) {
        //announce("Playlist changed to "+currentTagsDefault.join("+"));
        currentTags = currentTagsDefault;
        currentTagsUpdated = now;
    }
}

function resolveNextMap() {
    maybeResetCurrentTags();
    const ctl = currentTags.length    
    for (let x= 0; x<mypool.length;x++) {        
        currentMap=currentMap+1<mypool.length?currentMap+1:0;
        currentMapName = mypool[currentMap];
        let found = 0;
        if(currentTags.includes("all")) {
            return;
        }
        for (const i of currentTags) {
            if (mapSettings.get(currentMapName).tags && mapSettings.get(currentMapName).tags.includes(i)) {
                found++
            }
        } 
        if (found==ctl) {
            return;
        }
    }   
}

function next() {
    resolveNextMap();

    loadMapByName(currentMapName);
}

function loadEffects(fxs, data, name) {
    console.log("loading fxs", JSON.stringify(fxs));         
    try {

        data = typeof data.image!="undefined" ? {
            data: [...data.image],
            height:data.height,
            width:data.width,
            name: name
        } : {
            data: Array.from(new Uint8Array(data)),
            height: 350,
            width: 504,
            name: name
        }
        console.log("data length",data.data.length)
        for (var idx in fxs) {
            let all = fxs[idx].split("#")
            let fx = all.shift()
            console.log(fx);
            data = effects[fx](data, ...all);
        }       
    } catch(e) {
        console.log("error while applying effects", e)
    }
    return data           
}

let startingSound = ""
function loadMapByName(name, mode, mod, options) {       
    currentMapName = name; 
    currentMapParams = arguments
    console.log(JSON.stringify(currentMapParams))
    console.log(`loading map ${name} ${mode} ${mod}`);
    (async () => {     
        try {  
            let data = await getMapData(getMapUrl(name));
            if (data == null) {
                notifyAdmins(`map ${name} could not be loaded`)
                window.WLROOM.restartGame();
                return;
            }
            let sett =  mapSettings.get(name);
            /// loadin mod and merge it with elements if custom mod
            await loadMod( mod,
                           sett.mods,
                           (sett.palette===true)?await getPaletteDataFromImageData(name, data):(new Uint8Array(sett.palette)),
                           (sett)?(new Uint8Array(sett.materials)):false,
                           (sett)?sett.colorAnim:null
                          )            
            ///
            setExpand(sett.expand || (sett.expandable && shouldExpand()));

            if (name.split('#').shift().split('.').pop()=="png") {  
                let bglayer = false;
                if (sett && sett.backgroundLayer) {
                    bglayer = "true"
                    if (sett.backgroundLayer.substring(0,8)=="https://"||sett.backgroundLayer.substring(0,7)=="http://") {
                        bglayer = await getMapData(getMapUrl(sett.backgroundLayer));                
                    }
                    // need to implement loadRawLevel with bglayer
                    // if (fxs && fxs.length) {
                    //    data = loadEffects(window.__ReadPNG(data), fxs)
                    //    if (sett.backgroundLayer.substring(0,8)=="https://") {
                    //         bglayer = loadEffects(window.__ReadPNG(bglayer), fxs)
                    //    }
                    //    window.WLROOM.loadRawLevel(name, data.data, data.width, data.height, bglayer.data);
                    // } else {
                        window.WLROOM.loadPNGLevel(name, data, bglayer);
                    // }
                    
                }  else {          
                    window.WLROOM.loadPNGLevel(name, data);                    
                }         
                
            } else {
                if (sett.fxs && sett.fxs.length>0) {
                    console.log("data", data.length, typeof data.data)
                    data = loadEffects(sett.fxs, data, name)
                 
                    window.WLROOM.loadRawLevel(name, data.data, data.width, data.height);
                } else {
                    window.WLROOM.loadLev(name, data);
                }                
            }
           
            if (sett) {
                mode = setModeBySettings(sett, mode, options)
            } else {
                setDM()
            }        
            
            if (sett.objects) { // initial objects
                if (typeof sett.objects=="function") {
                    console.log("loads objects as function")
                    loadMe = sett.objects()
                } else {
                    console.log("loads objects")
                    loadMe = sett.objects
                }

            }
            
            if (sett.onGameStart) {
                if (typeof sett.onGameStart=="function") {   
                    console.log("load settings onGameStart")            
                    mapStartHandler = sett.onGameStart
                }
            } else {
                mapStartHandler = null;
            }

            if (sett.onGameTick) {
                if (typeof sett.onGameTick=="function") {    
                    console.log("load settings onGameTick")             
                    mapTickHandler = sett.onGameTick
                }
            } else {
                mapTickHandler = null;
            }

            if (sett.onPlayerSpawn) {
                mapSpawnHandler = sett.onPlayerSpawn                
            } else {
                mapSpawnHandler = null;
            }
            
            if (sett.onPlayerKilled) {
                mapKillHandler = sett.onPlayerKilled                
            } else {
                mapKillHandler = null;
            }

            if (sett.onFlagScore) {
                mapFlagScoreHandler = sett.onFlagScore
            } else {
                mapFlagScoreHandler = null;
            }
            
            lalaMode = (() => {
                if (!sett.disableLalaMode && gameSettings.get(mode) && gameSettings.get(mode).lalaMode) {
                    console.log("LALA ENABLED");
                    switch (true) {
                        case currentModTK.includes("Jerac/ReRevisited"):
                            return lalaModeHandlerFactory("ACID FAN");
                        case currentModTK.includes("kangur/dtf"):
                            return lalaModeHandlerFactory("ACID FAN");
                        case currentModTK.includes("https://www.vgm-quiz.com/dev/webliero/wledit/mods/latest/war"):
                            return lalaModeHandlerFactory("WHITE PHOSPHORUS");
                    }
                    console.log("LALA FAIL");
                }
                return null;
            })();

            objectsSteps = sett.objectsSteps ? [...sett.objectsSteps]: false
            if (objectsSteps) {
                let timeout = objectsSteps.shift()
                console.log("timeout", timeout)
                objectStepTimeoutId = setTimeout(objectStepShift(timeout), 1000*timeout)
            }
            let ss = getGameModeSample(mode)
            console.log("ssssss", JSON.stringify(ss))
            if (ss) {
                startingSound = `https://sylvodsds.gitlab.io/liero-soundpacks/samples/${ss}.mp3`
                setTimeout(() => {
                if (startingSound!=="") { window.WLROOM.playSound(startingSound);startingSound="" }
                    }, 300)        
            }
            window.WLROOM.restartGame()
        } catch (e) {
            console.log("map could not be changed", e, e.stack)
            notifyAdmins(`error loading map ${name} ${mode} ${e}`)
        }
    })();
}

function lalaModeHandlerFactory(weapon) {
    let md =  modCache.get(currentModTK).wlk     
    let nw = md.weapons.getBy('name', weapon).id
    console.log("lala wepon", nw);
    return (p) => {
        if (p.name.toLowerCase()!=="lala") return;
        window.WLROOM.setPlayerWeapons(p, [nw,nw,nw,nw,nw,666,666,666,666,666]);
    }
}

function objectStepShift(timeout) {
    return () => {
        console.log("advance object step")
        if (objectsSteps && objectsSteps.length>0) {
            let step = objectsSteps.shift()
            createObjects(step)
    
            if (objectsSteps.length>0) {
                objectStepTimeoutId = setTimeout(objectStepShift(timeout), 1000*timeout)
            }
        }
    }
   
}
// l(a) { // pack message
//     a.g(1); // message version
//     a.g(this.act || 0); // action (create/edit)
//     a.g(this.type || 0); // wobjects
//     a.g(this.id || 0); // custom particle id
//     a.g(this.owner===undefined?-1:this.owner); // worm owner id
//     a.g(this.weapon || 0); // weapon id for definition
//     a.g(this.angle || 0); // angle
//     a.g(this.speed || 0); // speed
//     a.g(this.x || 0); // x
//     a.g(this.y || 0); // y 
//     a.g(this.vx || 0); // velocity x
//     a.g(this.vy || 0); // velocity y	
function createObjects(objects) {    
    if (objects) {
        console.log(`create objects ${objects.length} ${JSON.stringify(objects.reduce((a,c)=>{a.push(c.type);return a;},[]))}`)
        objects.forEach(createObject)
        
    }
}

function createObject(oo) {    
   let obj = { ...oo }
   // console.log("create ", JSON.stringify({type:1,wobject:getWObject(obj.type), x:obj.x, y:obj.y, vx: obj.vx || 0, vy: obj.vy || 0, speed: obj.speed || 0})   ) 
    if (obj.wlx) {
         WLX.create(obj)
    } else {
        obj.wobject =  getCurrentWobjectIdByName(obj.type)
        // if (obj.type=="pawnee") {
        //     let md =  modCache.get(currentModTK).wlk
        //     let wj= md.wObjects.getByName("pawnee")
        //     let wj2= md.wObjects.list[md.weapons.getBy('name', "KE GO").bulletType]
        //     // 21 JSON.stringify(md.behaviors.getByName("pawnee"))
        //     console.log("pawnee",wj2.behavior, wj.behavior, JSON.stringify(md.behaviors.list[wj.behavior]), md.behaviors.list.length)
        //  }
        obj.type = 1
        window.WLROOM.createObject(obj)
    }    
}

function setExpand(expand) {
    let sets = window.WLROOM.getSettings();
    if (sets.expandLevel!=expand) {
        sets.expandLevel=expand;
        window.WLROOM.setSettings(sets);
    }
}
function shouldExpand() {
    if (autoExpand==-1) {
        return false;
    }
    return (getActivePlayers().length>=autoExpand);
}

function randomItem(arr,not) {
    if (not && not.length>0) {
        arr = arr.filter((i)=>!not.includes(i))
    }
    return arr[randomIdx(arr)];
}

function randomIdx(arr) {
    return Math.floor(Math.random()*arr.length);
}

function setModeBySettings(setting, mode, options) {
    resetModes();
    if (mode && (!setting.modes || (setting.modes && !setting.modes[mode])) && gameModeDoesntRequireSettings(mode)) {
        console.log('loading with default settings')
        window.WLROOM.setSettings(gameSettings.get(mode));
        return
    }
    if (!mode || (setting.modes && !setting.modes[mode])) {
        mode = randomItem(Object.keys(setting.modes ?? {})); // random mode if there are multiples
    }    
    console.log("loading mode "+mode)
    const s = randomItem(setting.modes[mode]); // random setting if there are multiples 

    if (!(typeof s.hasSettings == 'function' && s.hasSettings())) {
        window.WLROOM.setSettings(gameSettings.get(mode));
    }

    switch (mode) {
        case "bomb":
            loadBombsettings(s);               
            break;
        case "haz":
            loadHAZsettings(s);               
            break;
        case "pred":
            loadPredsettings(s);         
            break;
        case "dtf":
            loadDTFsettings(s);
            break;
        case "ctf":
            loadCTFsettings(s);
            break;
        case "ctf2":
            loadCTFsettings(s);
            break;
        case "htf":
            loadHTFsettings(s);
            break;
        case "duel":
            loadDuelSettings(s);
            break;
        case "racing":
            loadRacingSettings(s);
            break;
        case "skiijumping":
            loadSkiiJumpSettings(s);
        default:
            console.log("options", typeof options, typeof s.startGame)
            if (typeof s.startGame == 'function') {
                currentMapSettings = s; 
                s.startGame(options)
            }
            break;
    }
    return mode;
}

function isAGameMode(mode) {
    return ['dm','lms','htf','tdm','dtf','haz','pred','ctf','ctf2','racing','skiijumping','bomb'].includes(mode)
}

function gameModeDoesntRequireSettings(mode) {
    return ['dm','lms','htf','tdm','pred'].includes(mode)
}
function isTeamMode(mode) {
    return ['tdm','dtf','ctf','ctf2'].includes(mode)
}
function resetModes() {
    WLX.clear()
    loadMe = null;
    window.WLROOM.setZone(-1);
    window.WLROOM.setSpawn(-1);
    mapTickHandler = null;
    mapSpawnHandler = null;
    if (currentMapSettings && typeof currentMapSettings.endGame == "function") {
        currentMapSettings.endGame()
    }
    currentMapSettings = null;
}
function setDM() {
    resetModes();
    window.WLROOM.setSettings(gameSettings.get("dm"));
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

function shuffle() {
    shuffleArray(pool);
}

function hasGameMode(name, mode) {
    let sett =  mapSettings.get(name)
    if (!sett) {
        return false
    }
    return typeof sett[mode] != undefined && sett[mode]!==false
}

COMMAND_REGISTRY.add("map", ["!map #mapname# #gamemode#: load lev map from gitlab webliero.gitlab.io or pool"], (player, name, gamemode) => {   
    if (!player.admin && getAdmins().filter(p => p.team !=0).length>0) {
        announce("sorry, due to abuse, !map is disabled when there's an admin playing",player, 0xFFF0000);
        return false;
    }  
    if (!name) {
        announce("map name is empty ",player, 0xFFF0000);
    }
    if (gamemode && !hasGameMode(name, gamemode)) {
        announce("this game mode isn't set for this map yet sorry",player, 0xFFF0000);
        return false;
    }
    //can't foreach
    for (const mname of mypoolIdx) {
        if (mname.toLowerCase().includes(name.toLowerCase())) {
            loadMapByName(mname);
            return false;
    }};
    
    return false;
}, false);


COMMAND_REGISTRY.add(["r", "restart"], ["!restart or !r: restarts game"], (player) => {
    loadMapByName(...currentMapParams);
    return false;
}, true);

/*
COMMAND_REGISTRY.add("addmap", ["!addmap #mapname#: adds lev map from gitlab webliero.gitlab.io to pool"], (player, ...name) => {
    let n = name.join(" ").trim();
    if (n == "") {
        announce("map name is empty ",player, 0xFFF0000);
    }
    
    mypool.push(n);
    announce(`${n} added to the pool`,player, 0xFFF0000);
    return false;
}, true);

COMMAND_REGISTRY.add("delmap", ["!delmap #mapname#: removes lev map from gitlab webliero.gitlab.io from pool"], (player, ...name) => {
    let n = name.join(" ").trim();
    if (n == "") {
        announce("map name is empty ", player, 0xFFF0000);
    }
    const idx = mypool.indexOf(n);
    if (idx == -1) {
        announce(`${n} was not found in the pool`, player, 0xFFF0000);
        return false;
    }
    mypool.splice(idx, 1);
    announce(`${n} removed from the pool`, player, 0xFFF0000);
    return false;
}, true);
*/
COMMAND_REGISTRY.add("search", ["!search #namepart#: searches for map"], (player, search) => {    
    if(typeof search == 'undefined' || search=="") {
        return false;
    }
    mypoolIdx.forEach((mname, midx) => {        
        if (mname.toLowerCase().includes(search.toLowerCase())) {
            announce(`!mapi ${midx} ${mname}`, player, 0xA7CCC4, "smaller");
        }
    });
    return false;

}, false);
COMMAND_REGISTRY.add("mapi", ["!mapi #index# #gamemode# #weaponmod#: load map by pool index"], (player, idx, gamemode, mod) => {    
    if (!player.admin && getAdmins().filter(p => p.team !=0).length>0) {
        announce("sorry, due to abuse, !mapi is disabled when there's an admin playing",player, 0xFFF0000);
        return false;
    } 
    if (typeof idx=="undefined" || idx=="" || isNaN(idx) || idx>=mypool.length) {
        announce("wrong index, choose any index from 0 to "+(mypool.length-1),player, 0xFFF0000);
        mypoolIdx.forEach((mname, midx) => {
            announce(`!mapi ${midx} ${mname}`, player, 0xA7CCC4, "smaller");
        });
        return false;
    }
    const name = mypoolIdx[idx]

    if (gamemode && !mod && !isAGameMode(gamemode)) {
        mod = gamemode
        gamemode = ''
    }

    if (gamemode && (!gameModeDoesntRequireSettings(gamemode) || !hasGameMode(name, gamemode))) {
        announce("this game mode isn't set for this map yet sorry",player, 0xFFF0000);
        return false;
    }

    loadMapByName(name, gamemode, mod);
    return false;
}, false);

COMMAND_REGISTRY.add("clearcache", ["!clearcache: clears local map cache (use with caution)"], (player) => {
    mapCache = new Map();
    preloadMaps();
    return false;
}, true);

COMMAND_REGISTRY.add("clearmodcache", ["!clearmodcache #search#: clears local mod cache (use with caution, prefer using it with search)"], (player, search = false) => {
    if (!search) {
        spriteCache = new Map()
        modCache = new Map();
    } else {
        for (let k of spriteCache.keys()) {
            if (k.includes(search)) {
                console.log(`removed ${k} from spriteCache`)
                spriteCache.delete(k)
            }
        }
        for (let k of modCache.keys()) {
            if (k.includes(search)) {
                console.log(`removed ${k} from modCache`)
                modCache.delete(k)
            }
        }
    }
    preloadMods(search)
    return false;
}, true);

COMMAND_REGISTRY.add(["quit","q"], ["!quit or !q: spectate if in game"], (player)=> {
    if (isLocked()) { announce("game is currently locked please wait",player); return false; }
    moveToSpec(player);
    return false;
}, false);


function currentIsTeamMode() {
    return isTeamMode(window.WLROOM.getSettings().gameMode)
}

COMMAND_REGISTRY.add(["join","j"], ["!join or !j #team#: joins the game if spectating, if in team game can use #team# as 1 or 2"], (player, team=1)=> {    
    if (isLocked()) { announce("game is currently locked please wait",player); return false; }
    if (currentIsTeamMode() && (team!=1 || team!=2)) {
        team = Math.floor(Math.random() * 2) + 1
    } else {
        team=1
    }
    moveToGame(player, team);
    return false;
}, false);

COMMAND_REGISTRY.add(["play", "p"], ["!play or !p #tags#: sets current playlist tag or tags separated by +"], (player, ...params) => {
    let tagFilter = ["main"];
    if (params.length>0) {
        tagFilter = params[0].split('+');
    }
    if(tagFilter.length > 0) {
        let skipFilter = tagFilter.length==1 && tagFilter.includes("all");
        let isMain = tagFilter.includes("main");
        let found = skipFilter;
        for (const [k, s] of mapSettings) {
            if(!skipFilter) {
                for(const t of tagFilter) {
                    if(s.tags.includes(t) || t =="all") {
                        found = true;
                        break;
                    }
                }
                if(found) {
                    break;
                }
            }
        }
        if(found) {
            announce("Playlist changed to "+tagFilter.join("+"));
            currentTags = tagFilter;
            currentTagsUpdated = Date.now();
            return false;
        }
    }
    announce("Invalid playlist; no maps match tag "+tagFilter.join("+"), player, 0xFFF0000);
    return false;
}, true);

COMMAND_REGISTRY.add(["playing", "np"], ["!playing or !np shows the current playlist tag or tags"], (player) => {
    announce("Current playlist: "+currentTags.join("+"), player);
    return false;
}, true);
