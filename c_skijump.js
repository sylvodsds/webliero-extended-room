
function loadSkiiJumpSettings(s) {
        currentMapSettings = s; 
        console.log(typeof s)
        s.startGame()   
}

class SkiiJumpSettings {
    #spawn = []
    #settings = []  
    #startX = 0;
    #resetX = 0;
    #challengers = new Map()
    #currentScores = new Map()
    #bestScore = [0]
    #falling = []
    setSpawn = (x, y) => { this.#spawn = [x,y]; return this;}
    setStartX = (x) => { this.#startX = x; return this;}
    setResetX = (x) => { this.#resetX = x; return this;}
    setSettings = (settings) => {this.#settings=settings; return this;}
    hasSettings = () => this.#settings && Object.entries(this.#settings).length>0


    toJSON() { return {        
        spawn : this.#spawn,
        settings: this.#settings,       
        }
    }

    startGame() {     
        this.#challengers = new Map()
        this.#currentScores = new Map()
        this.#falling = []
        setLock(false)
       
        console.log("starting skii jumping")      
        window.WLROOM.setSpawn(1, ...this.#spawn)            
        loadGameSettings('skiijumping', this.#settings)       
        for (let p of getActivePlayers()) {
            this.#addChallenger(p, false)
        }
    }
  
    endGame = () => {             
        console.log("skii clear")
    }
    #addChallenger = (p, sig = true) => {
        if (this.#challengers.has(p.id)) return
        console.log(`skii challenger ADD ${p.name}`)
        if (sig) announce(`new challenger ${p.name}`)
        this.#challengers.set(p.id, p)
    }
    #removeChallenger = (p) => {
        if (!this.#challengers.has(p.id)) return
        this.#challengers.delete(p.id);
    }
    onGameTick = (g) => {      
        let players = getActivePlayers();        
       
        for (let p of players) {
            // if (p.worm && p.worm.sa)
            // console.log("pvy", p.worm.sa.b, p.worm.sa.a);
          if (p.x<this.#startX) return;
          if (p.x>=this.#resetX) { this.#resetPlayer(p); return; }
          if (this.#currentScores.has(p.id)) return;
     //     console.log("pvy", p.worm.sa.wa, p.worm.sa.Ca/*JSON.stringify(p)*/);
          if (p.worm.sa.a>0 && !this.#falling.includes(p.id)) { this.#falling.push(p.id); return; } 
          if (!this.#falling.includes(p.id)) return;
          if (p.worm.sa.a<=0) {
            this.#currentScores.set(p.id, p.x);            
            announce(`"${p.name}" hit ground at ${p.x}`)    
            createObject({type:"flag", x:Math.round(p.x), y:Math.round(p.y)-8})               
            if (p.x>this.#bestScore[0]) {
                if (this.#bestScore.length==2) {
                     announce(`"${p.name}" has new best score beating ${this.#bestScore[1]} who had ${this.#bestScore[0]}`)       
                }
                this.#bestScore = [p.x, p.name]                
            } 
          }

        }     
       
    }
    #resetPlayer = (p) => {
        window.WLROOM.setPlayerHealth(p.id, 0)
        if (this.#falling.includes(p.id)) { this.#falling = this.#falling.filter((vp)=>vp!=p.id)}
        this.#currentScores.delete(p.id)
    }
    onGameStart() {     
    }

    onPlayerTeamChange = (p, bp) => {
       
            if (p.team==0) {
                console.log(`${p.name} moved to spec`); 
                this.#removeChallenger(p)               
            } else {
                console.log(`${p.name} moved to game`);
                this.#addChallenger(p, true)
            }
        
    }


    #getlinecoordinates = (x0, y0, x1, y1) => {
        let ret = []
        var dx = Math.abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
        var dy = Math.abs(y1 - y0), sy = y0 < y1 ? 1 : -1; 
        var err = (dx>dy ? dx : -dy)/2;        
        while (true) {
          ret.push([x0, y0])
          if (x0 === x1 && y0 === y1) break;
          var e2 = err;
          if (e2 > -dx) { err -= dy; x0 += sx; }
          if (e2 < dy) { err += dx; y0 += sy; }
        }
        return ret;
    }
    #intersect = (px, py, zx, zy, zxx, zyy) => {
        // px = Math.round(px)
        // py = Math.round(py)
      return  px>=zx && py>=zy &&px<=zxx && py<=zyy
    }
    #lineintersect = (px, py, coords) => {                
        px = Math.floor(px)
        py = Math.floor(py)
        for (let c of coords) {
            if (c.length!=2) continue;
            if ((c[0] >= px-this.INTERSECT_RANGE && c[0] <= px+this.INTERSECT_RANGE)
                 && (c[1] <= py+this.INTERSECT_RANGE && c[1] >= py-this.INTERSECT_RANGE )) {
                    return true
                 } 
        }
        return false
    }

}



if (typeof window === 'undefined') { // node js context
    exports.SkiiJumpSettings = SkiiJumpSettings
}   