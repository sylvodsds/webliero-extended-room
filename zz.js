window.WLROOM.onPlayerLeave = function(player) {  
    if (currentMapSettings && typeof currentMapSettings.onPlayerLeave == "function") {
        currentMapSettings.onPlayerLeave(player)
    }
	auth.delete(player.id);
}

window.WLROOM.onGameEnd2 = function() {
    if (currentMapSettings && typeof currentMapSettings.onGameEnd2 == "function") {
        currentMapSettings.onGameEnd2()
    } else {
        next();
    }
}
window.WLROOM.onGameEnd = function() {
    console.log("--------------end")
    cleanGarbage()
    
    if (currentMapSettings && typeof currentMapSettings.onGameEnd == "function") {
        currentMapSettings.onGameEnd()
    }
	if (objectStepTimeoutId) {
        objectsSteps=[];
        clearTimeout(objectStepTimeoutId)
    }
}

window.WLROOM.onPlayerTeamChange = function(p, bp) {
    if (currentMapSettings && typeof currentMapSettings.onPlayerTeamChange == "function") {
        currentMapSettings.onPlayerTeamChange(p, bp)
    }
	// if (p.team==0) {
        // console.log(`${p.name} moved to spec`);
	// } else {
		// console.log(`${p.name} moved to game`);	
	// }
	// 
}

window.WLROOM.onGameStart = () => {
    console.log("--------------start")
    cleanGarbage()

    if (currentMapSettings && typeof currentMapSettings.onGameStart == "function") {
        currentMapSettings.onGameStart()
    } 

    if (mapStartHandler && typeof mapStartHandler == "function") {
        mapStartHandler()
    }

    if (loadMe) {
        createObjects(loadMe);        
        loadMe = null;
    }
}

function getCurrentGameMode() {
    return window.WLROOM.getSettings()['gameMode']
}

function getGameModeSample(gmode) {
    if (gmode && ["haz","ctf","dtf","pred", "ctf2"].includes(gmode)) {
        switch (gmode) {
            case "haz":
                return "domination"
                break
            case "ctf":
            case "ctf2":
                return "capture_the_flag"
                break
            case "dtf":
                return "defend_the_flag"
                break
            case "pred":
                return "predator"
            break
        }
    }
    return false
}
window.WLROOM.onGameTick = (g) => {    
    if (currentMapSettings && typeof currentMapSettings.onGameTick == "function") {
        currentMapSettings.onGameTick(g)
    }
    if (mapTickHandler) {
        mapTickHandler(g)
    }
    WLX.update()
}

function announce(msg, player, color, style) {
	window.WLROOM.sendAnnouncement(msg, typeof player =='undefined' || player == null?null:player.id, color!=null?color:0xb2f1d3, style !=null?style:"", 1);
}

function notifyAdmins(msg, logNotif = false) {
	getAdmins().forEach((a) => { window.WLROOM.sendAnnouncement(msg, a.id); });
}

function getAdmins() {
	return window.WLROOM.getPlayerList().filter((p) => p.admin);
}

function moveAllPlayersToSpectator() {
    for (let p of window.WLROOM.getPlayerList()) {
        window.WLROOM.setPlayerTeam(p.id, 0);
    }
}
